﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using VitalsService.DataAccess.EF;
using VitalsService.DataAccess.EF.Repositories;
using VitalsService.Domain.DbEntities;
using VitalsService.Domain.Dtos;
using VitalsService.Domain.Enums;
using VitalsService.DomainLogic.Services.Interfaces;
using VitalsService.Helpers;

namespace VitalsService.DomainLogic.Services.Implementations
{
    /// <summary>
    /// ThresholdsService.
    /// </summary>
    public class EnhancedThresholdsService : IEnhancedThresholdsService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<EnhancedThreshold> _EnhancedthresholdRepository;
        private readonly IRepository<EnhancedThresholdCondition> _EnhancedthresholdConditionRepository;
        private readonly IRepository<EnhancedThresholdPatient> _EnhancedthresholdPatientRepository;
        private readonly IRepository<EnhancedThresholdRange> _EnhancedthresholdRangeRepository;
        private readonly IRepository<EnhancedThresholdSetAnalysis> _EnhancedthresholdSetAnalysisRepository;
        private readonly IRepository<EnhancedThresholdAlerts> _EnhancedthresholdAlertsRepository;
        private readonly IRepository<PatientActiveETs> _PatientActiveETsRepository;
        private readonly IRepository<PatientThreshold> _thresholdRepository;
        private readonly IRepository<AlertSeverity> _alertSeverityRepository;
        private readonly IRepository<VitalAlert> _vitalAlertsRepository;
        private readonly IMapper _mapper;



        /// <summary>
        /// Initializes a new instance of the <see cref="EnhancedThresholdsService"/> class.
        /// </summary>
        /// <param name="unitOfWork">The unit of work.</param>
        public EnhancedThresholdsService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _EnhancedthresholdRepository = unitOfWork.CreateRepository<EnhancedThreshold>();
            _EnhancedthresholdConditionRepository = unitOfWork.CreateRepository<EnhancedThresholdCondition>();
            _EnhancedthresholdPatientRepository = unitOfWork.CreateRepository<EnhancedThresholdPatient>();
            _EnhancedthresholdRangeRepository = unitOfWork.CreateRepository<EnhancedThresholdRange>();
            _EnhancedthresholdSetAnalysisRepository = unitOfWork.CreateRepository<EnhancedThresholdSetAnalysis>();
            _EnhancedthresholdAlertsRepository = unitOfWork.CreateRepository<EnhancedThresholdAlerts>();
            _PatientActiveETsRepository = unitOfWork.CreateRepository<PatientActiveETs>();
            _thresholdRepository = unitOfWork.CreateRepository<PatientThreshold>();
            _alertSeverityRepository = unitOfWork.CreateRepository<AlertSeverity>();
            _vitalAlertsRepository = unitOfWork.CreateRepository<VitalAlert>();
            _mapper = mapper;
        }

        #region Enhanced Threshold
        /// <summary>
        /// Creates new Enhanced Threshold.
        /// </summary>
        /// <param name="enthreshold">The Enhanced Threshold.</param>
        /// <returns>
        /// Created entity.
        /// </returns>
        public async Task<OperationResultDto<Guid, CreateUpdateEnhancedThresholdStatus>> CreateEnhancedThreshold(EnhancedThreshold enthreshold)
        {

            _EnhancedthresholdRepository.Insert(enthreshold);
            await _unitOfWork.SaveAsync();

            return await Task.FromResult(
                new OperationResultDto<Guid, CreateUpdateEnhancedThresholdStatus>()
                {
                    Status = CreateUpdateEnhancedThresholdStatus.Success,
                    Content = enthreshold.Id
                }
            );
        }


        /// <summary>
        /// Updates the threshold.
        /// </summary>
        /// <param name="enthreshold">The Enhanced Threshold.</param>
        /// <returns></returns>
        public async Task<CreateUpdateEnhancedThresholdStatus> UpdateEnhancedThreshold(EnhancedThreshold enthreshold)
        {

            _EnhancedthresholdRepository.Update(enthreshold);
            await _unitOfWork.SaveAsync();

            return CreateUpdateEnhancedThresholdStatus.Success;
        }

        /// <summary>
        /// Deletes the Enhanced threshold.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="enthresholdId">The threshold identifier.</param>
        /// <returns></returns>
        public async Task<GetDeleteEnhancedThresholdStatus> DeleteEnhancedThreshold(int customerId, Guid enthresholdId)
        {
            var existingEnhancedThreshold =
                await GetEnhancedThreshold(customerId, enthresholdId);
            int count = await GetActiveEnhancedThresholds(enthresholdId);
            if (existingEnhancedThreshold != null && count <= 0)
            {
                existingEnhancedThreshold.IsDeleted = true;
                _EnhancedthresholdRepository.Update(existingEnhancedThreshold);
                //_EnhancedthresholdRepository.Delete(existingEnhancedThreshold);
                await _unitOfWork.SaveAsync();
                return await Task.FromResult(GetDeleteEnhancedThresholdStatus.Success);
            }

            if (count > 0)
                return await Task.FromResult(GetDeleteEnhancedThresholdStatus.UnableToDelete);

            return await Task.FromResult(GetDeleteEnhancedThresholdStatus.NotFound);
        }

        /// <summary>
        /// Gets the Enhanced Threshold.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="enthresholdId">The enthreshold identifier.</param>
        /// <returns></returns>
        public async Task<EnhancedThreshold> GetEnhancedThreshold(
            int customerId,
            Guid enthresholdId
        )
        {
            var result = (await _EnhancedthresholdRepository
                .FindAsync(
                    t => t.CustomerId == customerId && //t.IsActive == true &&
                        t.Id == enthresholdId,
                    o => o.OrderBy(e => e.Id),
                    new List<Expression<Func<EnhancedThreshold, object>>>
                    {
                        e => e.ETConditions,
                        e => e.ETPatients,
                        e => e.EThresholdRanges,
                        e => e.EThresholdSetAnalysis
                    }
                )
            ).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// Gets the Enhanced thresholds.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <param name="etType">The EnhancedThresholdType.</param>
        /// <returns></returns>
        public async Task<PagedResult<EnhancedThreshold>> GetEnhancedThresholds(int customerId, BaseSearchDto request, EnhancedThresholdType etType)
        {
            Expression<Func<EnhancedThreshold, bool>> expression = t => t.CustomerId == customerId && t.IsDeleted == false; //&& t.IsActive == true;
            Expression<Func<EnhancedThreshold, bool>> expressionDy = null;

            if (etType != EnhancedThresholdType.All)
            {
                if (etType == EnhancedThresholdType.Organization)
                {
                    expressionDy = t => t.ETConditions == null
                                     && t.ETPatients == null
                                     && t.Type == EnhancedThresholdType.Organization;

                }
                else if (etType == EnhancedThresholdType.Condition)
                {
                    expressionDy = t => t.ETPatients == null//t.ETConditions != null 
                                     && (t.Type == EnhancedThresholdType.Condition
                                            || t.Type == EnhancedThresholdType.Organization);
                }
                else if (etType == EnhancedThresholdType.Patient)
                {
                    expressionDy = t => t.ETConditions == null
                                     && t.ETPatients != null
                                     && t.Type == EnhancedThresholdType.Patient;
                }
                expression = PredicateBuilder.And(expression, expressionDy);
            }

            if (request != null)
            {
                if (!string.IsNullOrEmpty(request.Q))
                {
                    var terms = request.Q.Split(' ').Where(r => !string.IsNullOrWhiteSpace(r));

                    foreach (var term in terms)
                    {
                        expression = expression.And(t => t.Name.Contains(term));
                    }
                }
            }

            return await _EnhancedthresholdRepository
                .FindPagedAsync(
                    expression,
                o => o.OrderBy(e => e.Id),
               new List<Expression<Func<EnhancedThreshold, object>>>
                    {
                        e => e.ETConditions,
                        e => e.ETPatients,
                        e => e.EThresholdRanges,
                        e => e.EThresholdSetAnalysis
                    },
                startIndex: request != null ? request.Skip : (int?)null,
                limit: request != null ? request.Take : (int?)null
                );
        }

        /// <summary>
        /// Gets the Active Enhanced thresholds.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <param name="etType">The EnhancedThresholdType.</param>
        /// <param name="patientId">The PatientId.</param>
        /// <returns></returns>
        public async Task<PagedResult<EnhancedThreshold>> GetActiveEnhancedThresholds(int customerId, BaseSearchDto request, EnhancedThresholdType etType, Guid patientId)
        {
            Expression<Func<EnhancedThreshold, bool>> expression = t => t.CustomerId == customerId && t.IsDeleted == false;// && t.IsActive == true;
            Expression<Func<EnhancedThreshold, bool>> expressionDy = null;


            var patientActiveETs = (await _PatientActiveETsRepository.FindAsync(t => t.PatientId == patientId && t.Type == etType)).ToList();//t.isActive == true && 

            if (etType != EnhancedThresholdType.All)
            {
                if (etType == EnhancedThresholdType.Organization)
                {
                    expressionDy = t => t.ETConditions == null
                                     && t.ETPatients == null
                                     && t.Type == EnhancedThresholdType.Organization;

                }
                else if (etType == EnhancedThresholdType.Condition)
                {
                    expressionDy = t => t.ETPatients == null//t.ETConditions != null 
                                     && (t.Type == EnhancedThresholdType.Condition
                                            || t.Type == EnhancedThresholdType.Organization);
                }
                else if (etType == EnhancedThresholdType.Patient)
                {
                    expressionDy = t => t.ETConditions == null
                                     && t.ETPatients != null
                                     && t.Type == EnhancedThresholdType.Patient;
                }
                expression = PredicateBuilder.And(expression, expressionDy);
            }

            if (request != null)
            {
                if (!string.IsNullOrEmpty(request.Q))
                {
                    var terms = request.Q.Split(' ').Where(r => !string.IsNullOrWhiteSpace(r));

                    foreach (var term in terms)
                    {
                        expression = expression.And(t => t.Name.Contains(term));
                    }
                }
            }
            //if (patientActiveETs != null && patientActiveETs.Count > 0)
            //{
            //    var patientActiveETIds = patientActiveETs.Select(e => e.EnhancedThresholdId).ToList();
            //    patientActiveETIds = patientActiveETIds.Distinct().OrderBy(e => e).ToList();

            //    foreach (var etIds in patientActiveETIds)
            //    {
            //        // Here have to pass active EnhancedThresholdIds
            //        //expression = expression.And(t => t.Id == etIds);
            //        //string ActiveEts = 
            //    }
            //}

            var activeEnThresolds = await _EnhancedthresholdRepository
                .FindPagedAsync(
                    expression,
                o => o.OrderBy(e => e.Id),
               new List<Expression<Func<EnhancedThreshold, object>>>
                    {
                        e => e.ETConditions,
                        e => e.ETPatients,
                        e => e.EThresholdRanges,
                        e => e.EThresholdSetAnalysis
                    },
                startIndex: request != null ? request.Skip : (int?)null,
                limit: request != null ? request.Take : (int?)null
                );

            if (activeEnThresolds != null && patientActiveETs != null)
            {
                activeEnThresolds = ProcessActiveETs(activeEnThresolds, patientActiveETs, etType);
            }

            return activeEnThresolds;
        }

        private PagedResult<EnhancedThreshold> ProcessActiveETs(PagedResult<EnhancedThreshold> activeEnThresolds, List<PatientActiveETs> patientActiveETs,
                                    EnhancedThresholdType etType)
        {
            var activeEnhancedThreshods = new PagedResult<EnhancedThreshold>();
            if (activeEnhancedThreshods.Results == null)
                activeEnhancedThreshods.Results = new List<EnhancedThreshold>();

            foreach (var et in activeEnThresolds.Results)
            {
                var activeEt = patientActiveETs
                   .SingleOrDefault(p => p.EnhancedThresholdId == et.Id && p.Type == etType);

                if ((activeEt == null && et.IsActive) || (activeEt != null && activeEt.isActive))
                {
                    activeEnhancedThreshods.Results.Add(et);
                }
            }

            return activeEnhancedThreshods;
        }

        /// <summary>
        /// Activate/Deactivate Customer/Condition Level Enhanced threshold for any patient.
        /// </summary>
        /// <param name="customerId">The customerId.</param>
        /// <param name="PatientActivateETRequestDto">The Patient Activate ET RequestDto.</param>
        /// <returns></returns>
        public async Task<ActivateDeactivateETStatus> UpdateETActiveState(int customerId, PatientActiveETs patientActiveEts)
        {
#warning : implemented for old isActive field in EnhancedThreshold. Needs to remove
            /*var enThresholds = await GetEnhancedThresholds(customerId, null, patientActiveEts.Type);

            var existingEThreshold = enThresholds.Results.Where(e => e.Id == patientActiveEts.EnhancedThresholdId).FirstOrDefault();
            if (existingEThreshold != null)
            {
                existingEThreshold.IsActive = patientActiveEts.isActive;

                _EnhancedthresholdRepository.Update(existingEThreshold);
                await _unitOfWork.SaveAsync();
            }*/

            ////delete existing entry
            var existingPatientETs = await GetAllETsPatient(patientActiveEts.PatientId);
            var result = (await _PatientActiveETsRepository
                .FindAsync(
                    t => t.PatientId == patientActiveEts.PatientId &&
                        t.EnhancedThresholdId == patientActiveEts.EnhancedThresholdId &&
                        t.Type == patientActiveEts.Type,
                    o => o.OrderBy(e => e.Id),
                    new List<Expression<Func<PatientActiveETs, object>>>
                    {
                    }
                )
            ).SingleOrDefault();

            if (result != null)
            {
                _PatientActiveETsRepository.Delete(result);
            }

            _PatientActiveETsRepository.Insert(patientActiveEts);
            await _unitOfWork.SaveAsync();

            return ActivateDeactivateETStatus.Success;
        }

        /// <summary>
        /// Gets the Enhanced threshold Alerts.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <returns></returns>
        public async Task<PagedResult<PatientActiveETs>> GetAllETsPatient(Guid patientId)
        {

            Expression<Func<PatientActiveETs, bool>> expression = t => t.PatientId == patientId;
            return await _PatientActiveETsRepository
                .FindPagedAsync(
                   expression,
                o => o.OrderBy(e => e.Id),
               new List<Expression<Func<PatientActiveETs, object>>>
               {
               }
                );
        }

        #endregion

        #region Enhanced Threshold Condition
        /// <summary>
        /// Creates new Enhanced Threshold Condition.
        /// </summary>
        /// <param name="enthresholdCondition">The Enhanced Threshold Condition.</param>
        /// <returns>
        /// Created entity.
        /// </returns>
        public async Task<OperationResultDto<Guid, CreateUpdateEnhancedThresholdStatus>> CreateEnhancedThresholdCondition(EnhancedThresholdCondition enthresholdCondition)
        {

            _EnhancedthresholdConditionRepository.Insert(enthresholdCondition);
            await _unitOfWork.SaveAsync();

            return await Task.FromResult(
                new OperationResultDto<Guid, CreateUpdateEnhancedThresholdStatus>()
                {
                    Status = CreateUpdateEnhancedThresholdStatus.Success,
                    Content = enthresholdCondition.Id
                }
            );
        }

        /// <summary>
        /// Gets the Enhanced threshold Condition.
        /// </summary>
        /// <param name="etConditionId">The Enthreshold Condition.</param>
        /// <returns></returns>
        public async Task<EnhancedThresholdCondition> GetEnhancedThresholdCondition(
            Guid etConditionId
        )
        {
            return (await _EnhancedthresholdConditionRepository.FindAsync(
                            t => t.Id == etConditionId
                        )
                    )
                    .FirstOrDefault();
        }

        /// <summary>
        /// Gets the Enhanced threshold Condition.
        /// </summary>
        /// <param name="conditionId">The Condition.</param>
        /// <returns></returns>
        public async Task<IList<EnhancedThreshold>> GetEThresholdListConditionId(
            int customerId,
            Guid conditionId, Guid patientId
        )
        {
            IList<EnhancedThreshold> enThresholdList = new List<EnhancedThreshold>();
            var enThreshold = (await GetActiveEnhancedThresholds(customerId, null, EnhancedThresholdType.Condition, patientId)).Results;
            var ETConditionsAvail = enThreshold.Where(etC => etC.ETConditions != null).ToList();
            if (ETConditionsAvail.Count > 0)
            {
                var enThresholdConditions = ETConditionsAvail.Where(p => (p.ETConditions != null) ? (p.ETConditions.ConditionId == conditionId) : 1 == 1 && p.CustomerId == customerId);
                enThresholdList = enThresholdConditions.Take(100).ToList();
            }
            return enThresholdList;
        }

        /// <summary>
        /// Deletes the Enhanced threshold Condition.
        /// </summary> 
        /// <param name="etConditionId">The etCondition identifier.</param>
        /// <returns></returns>
        public async Task<GetDeleteEnhancedThresholdStatus> DeleteEnhancedThresholdCondition(Guid etConditionId)
        {
            var existingEnhancedThresholdCondition =
                await GetEnhancedThresholdCondition(etConditionId);

            if (existingEnhancedThresholdCondition != null)
            {
                _EnhancedthresholdConditionRepository.Delete(existingEnhancedThresholdCondition);

                await _unitOfWork.SaveAsync();

                return await Task.FromResult(GetDeleteEnhancedThresholdStatus.Success);
            }

            return await Task.FromResult(GetDeleteEnhancedThresholdStatus.NotFound);
        }

        #endregion

        #region Enhanced Threshold Patient
        /// <summary>
        /// Creates new Enhanced Threshold Patient.
        /// </summary>
        /// <param name="enthresholdPatient">The Enhanced Threshold Patient.</param>
        /// <returns>
        /// Created entity.
        /// </returns>
        public async Task<OperationResultDto<Guid, CreateUpdateEnhancedThresholdStatus>> CreateEnhancedThresholdPatient(EnhancedThresholdPatient enthresholdPatient)
        {

            _EnhancedthresholdPatientRepository.Insert(enthresholdPatient);
            await _unitOfWork.SaveAsync();

            return await Task.FromResult(
                new OperationResultDto<Guid, CreateUpdateEnhancedThresholdStatus>()
                {
                    Status = CreateUpdateEnhancedThresholdStatus.Success,
                    Content = enthresholdPatient.Id
                }
            );
        }
        /// <summary>
        /// Gets the Enhanced threshold Patient.
        /// </summary>
        /// <param name="etPatientId">The EnThreshold Patient.</param>
        /// <returns></returns>
        public async Task<EnhancedThresholdPatient> GetEnhancedThresholdPatient(
            Guid etPatientId
        )
        {
            return (await _EnhancedthresholdPatientRepository.FindAsync(
                            t => t.Id == etPatientId
                        )
                    )
                    .FirstOrDefault();
        }

        /// <summary>
        /// Gets the Enhanced threshold Patient.
        /// </summary>
        /// <param name="patientId">The Patient.</param>
        /// <returns></returns>
        public async Task<IList<EnhancedThreshold>> GetEThresholdListPatientId(
            int customerId,
            Guid patientId
        )
        {
            IList<EnhancedThreshold> enThresholdList = new List<EnhancedThreshold>();
            var enThreshold = (await GetActiveEnhancedThresholds(customerId, null, EnhancedThresholdType.Patient, patientId)).Results;
            var IsETConditionsAvail = enThreshold.Where(etC => etC.ETPatients != null).ToList();
            if (IsETConditionsAvail.Count > 0)
            {
                var enThresholdPatients = enThreshold.Where(p => p.ETPatients.PatientId == patientId && p.CustomerId == customerId);
                enThresholdList = enThresholdPatients.Take(100).ToList();
            }
            return enThresholdList;

        }

        /// <summary>
        /// Deletes the Enhanced threshold Patient.
        /// </summary> 
        /// <param name="etPatientId">The etPatient identifier.</param>
        /// <returns></returns>
        public async Task<GetDeleteEnhancedThresholdStatus> DeleteEnhancedThresholdPatient(Guid etPatientId)
        {
            var existingEnhancedThresholdPatient =
                await GetEnhancedThresholdPatient(etPatientId);

            if (existingEnhancedThresholdPatient != null)
            {
                _EnhancedthresholdPatientRepository.Delete(existingEnhancedThresholdPatient);

                await _unitOfWork.SaveAsync();

                return await Task.FromResult(GetDeleteEnhancedThresholdStatus.Success);
            }

            return await Task.FromResult(GetDeleteEnhancedThresholdStatus.NotFound);
        }

        #endregion

        #region Enhanced Threshold Alerts
        /// <summary>
        /// Creates Enhanced threshold alerts
        /// </summary>
        /// <param name="enthresholdAlert"></param>
        /// <returns></returns>
        public async Task<OperationResultDto<Guid, CreateUpdateEnhancedThresholdStatus>> CreateEnhancedThresholdAlert(EnhancedThresholdAlerts enthresholdAlert)
        {

            var highestAlertSeverity = await _alertSeverityRepository
                              .FirstOrDefaultAsync(
                                  s => s.CustomerId == enthresholdAlert.CustomerId && s.Name.ToLower() == "red",
                                  null
                              );


            var existingViolation = (await _EnhancedthresholdAlertsRepository.FindAsync(
                            t => t.ViolatedMeasurementId == enthresholdAlert.ViolatedMeasurementId && t.IsDeleted == false)).ToList();

            enthresholdAlert.AlertSeverity = highestAlertSeverity;
            //if (existingViolation.Count == 0)
            //{
                _EnhancedthresholdAlertsRepository.Insert(enthresholdAlert);
                await _unitOfWork.SaveAsync();

            //}

            return await Task.FromResult(
                new OperationResultDto<Guid, CreateUpdateEnhancedThresholdStatus>()
                {
                    Status = CreateUpdateEnhancedThresholdStatus.Success,
                    Content = enthresholdAlert.Id
                }
            );
        }

        /// <summary>
        /// Gets the Enhanced threshold Alert.
        /// </summary>
        /// <param name="etAlertId">The EnThreshold Alert ID.</param>
        /// <returns></returns>
        public async Task<EnhancedThresholdAlerts> GetEnhancedThresholdAlert(
            Guid etAlertId
        )
        {
            return (await _EnhancedthresholdAlertsRepository.FindAsync(
                            t => t.Id == etAlertId && t.IsDeleted == false
                        )
                    )
                    .FirstOrDefault();
        }

        /// <summary>
        /// Gets the Enhanced threshold Alerts.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <returns></returns>
        public async Task<PagedResult<EnhancedThresholdAlerts>> GetEnhancedThresholdAlerts(int customerId)
        {
            Expression<Func<EnhancedThresholdAlerts, bool>> expression = t => t.CustomerId == customerId && t.IsDeleted == false;


            return await _EnhancedthresholdAlertsRepository
                .FindPagedAsync(
                    expression,
                o => o.OrderBy(e => e.Id),
               new List<Expression<Func<EnhancedThresholdAlerts, object>>>
                    {
                        e => e.EnhancedThreshold,
                        e => e.EnhancedThreshold.ETConditions,
                        e => e.AlertSeverity
                    }
                );
        }

        /// <summary>
        /// Acknowledges the ET alerts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="acknowledgedBy">The acknowledged by.</param>
        /// <param name="etAlertsIds">The ET alerts ids.</param>
        /// <returns></returns>
        public async Task<CreateUpdateAlertStatus> AcknowledgeEnThAlerts(int customerId, Guid acknowledgedBy, IList<Guid> etAlertsIds)
        {
            var existingETAlerts = await _EnhancedthresholdAlertsRepository.FindAsync(
                a => a.CustomerId == customerId && etAlertsIds.Contains(a.Id)
            );

            if (existingETAlerts.Count != etAlertsIds.Count || existingETAlerts.Any(a => a.Acknowledged))
            {
                return CreateUpdateAlertStatus.OneOfProvidedAlertsDoesNotExistOrAlreadyAcknowledged;
            }

            foreach (var existingETAlert in existingETAlerts)
            {
                existingETAlert.Acknowledged = true;
                existingETAlert.AcknowledgedBy = acknowledgedBy;
                existingETAlert.AcknowledgedUtc = DateTime.UtcNow;
                _EnhancedthresholdAlertsRepository.Update(existingETAlert);
            }

            await _unitOfWork.SaveAsync();

            return CreateUpdateAlertStatus.Success;
        }


        /// <summary>
        /// UnAcknowledges the ET alerts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="unacknowledgedBy">The unacknowledged by.</param>
        /// <param name="etAlertsIds">The ET alerts ids.</param>
        /// <returns></returns>
        public async Task<CreateUpdateAlertStatus> UnAcknowledgeEnThAlerts(int customerId, Guid unacknowledgedBy, IList<Guid> etAlertsIds)
        {
            var existingETAlerts = await _EnhancedthresholdAlertsRepository.FindAsync(
                a => a.CustomerId == customerId && etAlertsIds.Contains(a.Id)
            );

            foreach (var existingETAlert in existingETAlerts)
            {
                existingETAlert.Acknowledged = false;
                existingETAlert.AcknowledgedBy = null;
                existingETAlert.AcknowledgedUtc = null;
                _EnhancedthresholdAlertsRepository.Update(existingETAlert);
            }

            await _unitOfWork.SaveAsync();

            return CreateUpdateAlertStatus.Success;
        }

        /// <summary>
        /// Ignore the ET alerts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="etAlertsIds">The ET alerts ids.</param>
        /// <returns></returns>
        public async Task<CreateUpdateAlertStatus> IgnoreEnhancedThresholdAlert(int customerId, IList<Guid> etAlertsIds)
        {
            var existingETAlerts = await _EnhancedthresholdAlertsRepository.FindAsync(
                a => a.CustomerId == customerId && etAlertsIds.Contains(a.Id)
            );

            foreach (var existingETAlert in existingETAlerts)
            {
                existingETAlert.IsDeleted = true;
                _EnhancedthresholdAlertsRepository.Update(existingETAlert);
            }

            await _unitOfWork.SaveAsync();

            return CreateUpdateAlertStatus.Success;
        }

        /// <summary>
        /// Deletes the Enhanced threshold Alert.
        /// </summary> 
        /// <param name="etAlertId">The et Alert identifier.</param>
        /// <returns></returns>
        public async Task<GetDeleteEnhancedThresholdStatus> DeleteEnhancedThresholdAlert(Guid etAlertId)
        {
            var existingEnhancedThresholdAlert =
                await GetEnhancedThresholdAlert(etAlertId);

            if (existingEnhancedThresholdAlert != null)
            {
                _EnhancedthresholdAlertsRepository.Delete(existingEnhancedThresholdAlert);

                await _unitOfWork.SaveAsync();

                return await Task.FromResult(GetDeleteEnhancedThresholdStatus.Success);
            }

            return await Task.FromResult(GetDeleteEnhancedThresholdStatus.NotFound);
        }

        #endregion

        #region others
        /// <summary>
        /// Gets the Enhanced threshold Range.
        /// </summary>
        /// <param name="RangeType">The Range Type.</param>
        /// <returns></returns>
        public async Task<EnhancedThresholdRange> GetEnhancedThresholdRange(
            string RangeType
        )
        {
            return (await _EnhancedthresholdRangeRepository.FindAsync(
                            t => t.RangeText == RangeType
                        )
                    )
                    .FirstOrDefault();

        }

        /// <summary>
        /// Gets the Enhanced threshold Set Analysis.
        /// </summary>
        /// <param name="SetAnalysisType">The SetAnalysis Type.</param>
        /// <returns></returns>
        public async Task<EnhancedThresholdSetAnalysis> GetEnhancedThresholdSetAnalysis(
            int SetAnalysisTypeId
        )
        {
            return (await _EnhancedthresholdSetAnalysisRepository.FindAsync(
                            t => t.Weight == SetAnalysisTypeId
                        )
                    )
                    .FirstOrDefault();
        }

        /// <summary>
        /// Gets the Enhanced threshold Set Analysis.
        /// </summary>
        /// <param name="SetAnalysisType">The SetAnalysis Type.</param>
        /// <returns></returns>
        public async Task<int> GetActiveEnhancedThresholds(
           Guid enhancedThresholdId
        )
        {
            return (await _PatientActiveETsRepository.FindAsync(
                            t => t.EnhancedThresholdId == enhancedThresholdId && t.isActive == true
                        )
                    ).Count();
        }

        #endregion


    }
}
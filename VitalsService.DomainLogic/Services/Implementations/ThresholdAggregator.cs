﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using VitalsService.Domain.DbEntities;
using VitalsService.Domain.Enums;
using VitalsService.DomainLogic.Services.Interfaces;

namespace VitalsService.DomainLogic.Services.Implementations
{
    /// <summary>
    /// ThresholdAggregator.
    /// </summary>
    public class ThresholdAggregator : IThresholdAggregator
    {
        private readonly IMapper _mapper;

        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="mapper"></param>
        public ThresholdAggregator(IMapper mapper)
        {
            _mapper = mapper;
        }


        /// <summary>
        /// Aggregates the thresholds.
        /// </summary>
        /// <param name="defaultThresholds">The default thresholds.</param>
        /// <param name="patientsThresholds">The patients thresholds.</param>
        /// <returns></returns>
        public IList<Threshold> AggregateThresholds(IList<DefaultThreshold> defaultThresholds, IList<PatientThreshold> patientsThresholds)
        {
            var allThresholds = new List<Threshold>();

            // Add default and patient thresholds to the same list. Not sure why we're mapping the objects to themselves.
            allThresholds.AddRange(_mapper.Map<IList<Threshold>>(defaultThresholds));
            allThresholds.AddRange(_mapper.Map<IList<Threshold>>(patientsThresholds));

            // Group all thresholds by name (aka VitalType), alert severity, and customerId.
            var groupedByNameAndSeverity = allThresholds
                .GroupBy(t => new { t.Name, t.AlertSeverityId, t.CustomerId })
                .ToList();

            var aggregatedThresholds = new List<Threshold>();

            foreach (var group in groupedByNameAndSeverity)
            {
                if (group.Key.AlertSeverityId == null && groupedByNameAndSeverity.Any(g => g.Key.AlertSeverityId != null))
                {
                    // This one has no severity, but there are others with a severity. Skip this one.
                    continue;
                }

                if (group.Any(g => g is PatientThreshold))
                {
                    // One of the group items is a patient threshold. Grab it, and add it to the list of aggregated thresholds.
                    //TODO: is Single really safe here?
                    aggregatedThresholds.Add(group.Single(g => g is PatientThreshold));
                }
                else if (group.Any(g => g is DefaultThreshold))
                {
                    // No patient thresholds in the group, but there are default thresholds. Get only default condition thresholds.
                    var conditionThresholdsInGroup = group
                        .OfType<DefaultThreshold>()
                        .Where(t => t.DefaultType.ToLower() == ThresholdDefaultType.Condition.ToString().ToLower());

                    if (conditionThresholdsInGroup.Any())
                    {
#warning This causes Maestro's Trends page to fail because it can't parse this threshold type (not one of the KnowTypes). Do we need to enhance this object, or make define a new KnownType in Maestro?
                        // See: https://dan-account.toodlestorm.com/1f08d006-1092-4064-9d3a-a660015e0679/Patients/PatientDetails/6fa3084d-f78a-4e2b-8184-a69b0031acee/Trends/

                        aggregatedThresholds.Add(new Threshold()
                        {
                            AlertSeverity = conditionThresholdsInGroup.First().AlertSeverity,
                            AlertSeverityId = conditionThresholdsInGroup.First().AlertSeverityId,
                            CustomerId = conditionThresholdsInGroup.First().CustomerId,
                            Name = conditionThresholdsInGroup.First().Name,
                            Type = conditionThresholdsInGroup.First().Type,
                            Unit = conditionThresholdsInGroup.First().Unit,
                            MaxValue = conditionThresholdsInGroup.Min(t => t.MaxValue),
                            MinValue = conditionThresholdsInGroup.Max(t => t.MinValue)
                        });
                    }
                    else
                    {
                        // It's not a condition threshold; it's some other type of default threshold. According to the enum, this means
                        //   it's a "customer" default threshold.
                        //TODO: is Single really safe here?
                        aggregatedThresholds.Add(group.First(g => g is DefaultThreshold));
                    }
                }
            }

            return aggregatedThresholds;
        }

        /// <summary>
        /// Aggregates the threshold with patientThresholdTypes
        /// </summary>
        /// <param name="defaultThresholds"></param>
        /// <param name="patientsThresholds"></param>
        /// <param name="patientThresholdTypes"></param>
        /// <returns></returns>
        public IList<Threshold> CalculateAggregateThresholds(IList<DefaultThreshold> defaultThresholds, IList<PatientThreshold> patientsThresholds, IList<PatientThresholdType> patientThresholdTypes)
        {
            var allThresholds = new List<Threshold>();

            // Add default and patient thresholds to the same list. Not sure why we're mapping the objects to themselves.
            allThresholds.AddRange(_mapper.Map<IList<Threshold>>(defaultThresholds));
            allThresholds.AddRange(_mapper.Map<IList<Threshold>>(patientsThresholds));


            // Group all thresholds by name (aka VitalType), alert severity, and customerId.
            var groupedByNameAndSeverity = allThresholds
                .GroupBy(t => new { t.Name, t.AlertSeverityId, t.CustomerId })
                .ToList();

            var aggregatedThresholds = new List<Threshold>();

            foreach (var group in groupedByNameAndSeverity)
            {
                var vitalThresholdTypeList = patientThresholdTypes.Where(t => t.VitalType.ToString() == group.Key.Name && !t.IsDeleted).ToList();
                var currentVitalThresholdType = (vitalThresholdTypeList.Any() ? vitalThresholdTypeList.First() : null);

                if (group.Key.AlertSeverityId == null && groupedByNameAndSeverity.Any(g => g.Key.AlertSeverityId != null))
                {
                    // This one has no severity, but there are others with a severity. Skip this one.
                    continue;
                }
                if (group.Any(g => g is PatientThreshold))
                {
                    // One of the group items is a patient threshold. Grab it, and add it to the list of aggregated thresholds.
                    //TODO: is Single really safe here?
                    aggregatedThresholds.Add(group.Single(g => g is PatientThreshold));
                }
                else if (group.Any(g => g is DefaultThreshold))
                {
                    var conditionThresholdsInGroup = group
                            .OfType<DefaultThreshold>()
                            .Where(t => t.DefaultType.ToLower() == ThresholdDefaultType.Condition.ToString().ToLower());

                    if (conditionThresholdsInGroup.Any())
                    {
                        if (currentVitalThresholdType != null && currentVitalThresholdType.ThresholdType != null  
                                     && currentVitalThresholdType.ThresholdType.Equals("Condition"))
                        {
                            // No patient thresholds in the group, but there are default thresholds. Get only default condition thresholds.
                            var filterConditionThresholdsInGroup = conditionThresholdsInGroup.Where(t => t.Name == group.Key.Name && t.ConditionId == currentVitalThresholdType.ConditionId);
                            if (filterConditionThresholdsInGroup.Any())
                            {
                                aggregatedThresholds.Add(filterConditionThresholdsInGroup.First()); //MS-3438 Fix
                            }
                        }
                    }

                    var customerThresholdsInGroup = group
                        .OfType<DefaultThreshold>()
                        .Where(t => t.DefaultType.ToLower() == ThresholdDefaultType.Customer.ToString().ToLower());

                    if (customerThresholdsInGroup.Any())
                    {
                        //If any threshold type is assigned for patient measurement then fetch corresponding threshold
                        if (currentVitalThresholdType != null && currentVitalThresholdType.ThresholdType != null
                                                   && currentVitalThresholdType.ThresholdType.Equals("Customer"))
                        {
                            // No patient thresholds in the group, but there are default thresholds. Get only default condition thresholds.
                            var filterCustomerThresholdsInGroup = customerThresholdsInGroup.Where(t => t.Name == group.Key.Name);
                            if (filterCustomerThresholdsInGroup.Any())
                            {
                                aggregatedThresholds.Add(filterCustomerThresholdsInGroup.First());
                            }
                        }
                        else if (currentVitalThresholdType == null) //if no threshold type assigned to patient then fetch customer thresholds
                        {
                            aggregatedThresholds.Add(group.First(g => g is DefaultThreshold));
                        }
                    }
                    // It's not a condition threshold; it's some other type of default threshold. According to the enum, this means
                    //   it's a "customer" default threshold.
                    //TODO: is Single really safe here?
                    //aggregatedThresholds.Add(group.Single(g => g is DefaultThreshold));
                }

            }

            return aggregatedThresholds;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using EntityFramework.Extensions;
using Microsoft.Practices.ServiceLocation;
using VitalsService.DataAccess.EF;
using VitalsService.DataAccess.EF.Repositories;
using VitalsService.Domain.DbEntities;
using VitalsService.Domain.DomainObjects;
using VitalsService.Domain.Dtos;
using VitalsService.Domain.Enums;
using VitalsService.DomainLogic.Services.Interfaces;
using VitalsService.Helpers;

namespace VitalsService.DomainLogic.Services.Implementations
{
    /// <summary>
    /// AlertsService.
    /// </summary>
    public class AlertsService : IAlertsService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Alert> _alertRepository;
        private readonly IRepository<HealthSession> _healthSessionRepository;
        private readonly IRepository<AlertSeverity> _alertSeverityRepository;

        private readonly Dictionary<VitalType, Dictionary<decimal, decimal>> vitalMinMaxValueMap = new Dictionary<VitalType, Dictionary<decimal, decimal>>()
        {
           { VitalType.SystolicBloodPressure, new Dictionary<decimal, decimal>() { { 0, 300 } } },
            { VitalType.DiastolicBloodPressure, new Dictionary<decimal, decimal>(){ { 0, 300 } } },
            { VitalType.HeartRate, new Dictionary<decimal, decimal>() { { 20, 300 } } },
            { VitalType.Temperature, new Dictionary<decimal, decimal>() { { 90, 110 } } },
            { VitalType.Weight, new Dictionary<decimal, decimal>() { { 0, (decimal)600 } } },
            { VitalType.BodyMassIndex, new Dictionary<decimal, decimal>() { { 0, 1000 } } }, // ToDo: Add MaxValue
            { VitalType.OxygenSaturation, new Dictionary<decimal, decimal>() { { 20, 100 } } },
            { VitalType.BloodGlucose, new Dictionary<decimal, decimal>() { { 20, 600 } } },
            { VitalType.PeakExpiratoryFlow, new Dictionary<decimal, decimal>() { { 20, 900 } } },
            { VitalType.ForcedExpiratoryVolume, new Dictionary<decimal, decimal>() { { 0, (decimal)20 } } },
            { VitalType.WalkingSteps, new Dictionary<decimal, decimal>() { { 0, 100000 } } },
            { VitalType.RunningSteps, new Dictionary<decimal, decimal>() { { 0, 100000 } } },
            { VitalType.ForcedVitalCapacity, new Dictionary<decimal, decimal>() { { 0, 999 } } },
            { VitalType.FEV1_FVC, new Dictionary<decimal, decimal>() { { 0, 101 } } }
        };

        /// <summary>
        /// Initializes a new instance of the <see cref="AlertsService"/> class.
        /// </summary>
        /// <param name="unitOfWork">The unit of work.</param>
        public AlertsService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _alertRepository = unitOfWork.CreateRepository<Alert>();
            _healthSessionRepository = unitOfWork.CreateRepository<HealthSession>();
            _alertSeverityRepository = unitOfWork.CreateRepository<AlertSeverity>();
        }

        /// <summary>
        /// Creates the alert.
        /// </summary>
        /// <param name="alert">The alert.</param>
        /// <returns></returns>
        public async Task<OperationResultDto<Guid, CreateUpdateAlertStatus>> CreateAlert(Alert alert)
        {
            if (alert.AlertSeverityId != null)
            {
                var existingAlertSeverity =
                    (await _alertSeverityRepository.FindAsync(
                            s => s.CustomerId == alert.CustomerId && s.Id == alert.AlertSeverityId
                        )
                    )
                    .FirstOrDefault();

                if (existingAlertSeverity == null)
                {
                    return await Task.FromResult(
                        new OperationResultDto<Guid, CreateUpdateAlertStatus>()
                        {
                            Status = CreateUpdateAlertStatus.AlertSeverityWithSuchIdDoesNotExist
                        }
                    );
                }
            }
            else
            {
                var highestAlertSeverity = await _alertSeverityRepository
                    .FirstOrDefaultAsync(
                        s => s.CustomerId == alert.CustomerId,
                        s => s.OrderByDescending(e => e.Severity)
                    );

                if (highestAlertSeverity != null)
                {
                    alert.AlertSeverity = highestAlertSeverity;
                }
            }

            _alertRepository.Insert(alert);
            await _unitOfWork.SaveAsync();

            return await Task.FromResult(
                new OperationResultDto<Guid, CreateUpdateAlertStatus>()
                {
                    Status = CreateUpdateAlertStatus.Success,
                    Content = alert.Id
                }
            );
        }

        /// <summary>
        /// Ignore the given home sensing alert.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="ignoredBy"></param>
        /// <param name="alertId"></param>
        /// <returns></returns>
        public async Task<CreateUpdateAlertStatus> IgnoreHomeSensingAlert(int customerId, Guid ignoredBy, Guid alertId)
        {
            var existingAlert = (await _alertRepository.FindAsync(a => a.CustomerId == customerId && a.Id == alertId))
                .SingleOrDefault();

            if (existingAlert == null)
            {
                return CreateUpdateAlertStatus.AlertDoesNotExist;
            }

            var homeSensingAlert = existingAlert as HomeSensingAlert;
            if (homeSensingAlert == null)
            {
                // It's not an Home Sensing alert. Can't be ignored.
                return CreateUpdateAlertStatus.AlertDoesNotExist;
            }

            homeSensingAlert.IsIgnored = true;
            homeSensingAlert.IgnoredBy = ignoredBy;
            homeSensingAlert.IgnoredUtc = DateTime.UtcNow;

            _alertRepository.Update(existingAlert);

            await _unitOfWork.SaveAsync();

            return CreateUpdateAlertStatus.Success;
        }

        /// <summary>
        /// Acknowledges the alerts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="acknowledgedBy">The acknowledged by.</param>
        /// <param name="alertsIds">The alerts ids.</param>
        /// <returns></returns>
        public async Task<CreateUpdateAlertStatus> AcknowledgeAlerts(int customerId, Guid acknowledgedBy, IList<Guid> alertsIds)
        {
            var existingAlerts = await _alertRepository.FindAsync(
                a => a.CustomerId == customerId && alertsIds.Contains(a.Id)
            );

            if (existingAlerts.Count != alertsIds.Count || existingAlerts.Any(a => a.Acknowledged))
            {
                return CreateUpdateAlertStatus.OneOfProvidedAlertsDoesNotExistOrAlreadyAcknowledged;
            }

            foreach (var existingAlert in existingAlerts)
            {
                existingAlert.Acknowledged = true;
                existingAlert.AcknowledgedBy = acknowledgedBy;
                existingAlert.AcknowledgedUtc = DateTime.UtcNow;
                _alertRepository.Update(existingAlert);
            }

            await _unitOfWork.SaveAsync();

            return CreateUpdateAlertStatus.Success;
        }


        /// <summary>
        /// UnAcknowledges the alerts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="unacknowledgedBy">The unacknowledged by.</param>
        /// <param name="alertsIds">The alerts ids.</param>
        /// <returns></returns>
        public async Task<CreateUpdateAlertStatus> UnAcknowledgeAlerts(int customerId, Guid unacknowledgedBy, IList<Guid> alertsIds)
        {
            var existingAlerts = await _alertRepository.FindAsync(
                a => a.CustomerId == customerId && alertsIds.Contains(a.Id)
            );

            foreach (var existingAlert in existingAlerts)
            {
                existingAlert.Acknowledged = false;
                existingAlert.AcknowledgedBy = null;
                existingAlert.AcknowledgedUtc = null;
                _alertRepository.Update(existingAlert);
            }

            await _unitOfWork.SaveAsync();

            return CreateUpdateAlertStatus.Success;
        }

        /// <summary>
        /// Gets the alerts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public PagedResult<PatientAlerts> GetAlerts(int customerId, AlertsSearchDto request)
        {
            Expression<Func<Alert, bool>> expression = a => a.CustomerId == customerId;

            if (request != null)
            {
                expression = expression.And(a => a.Acknowledged == request.Acknowledged);

                if (!string.IsNullOrEmpty(request.Q))
                {
                    var terms = request.Q.Split(' ').Where(r => !string.IsNullOrWhiteSpace(r));

                    foreach (var term in terms)
                    {
                        expression = expression.And(a => a.Title.Contains(term));
                    }
                }

                if (request.PatientIds != null && request.PatientIds.Any())
                {
                    expression = expression.And(a => request.PatientIds.Contains(a.PatientId));
                }

                if (request.Types != null && request.Types.Any())
                {
                    expression = expression.And(a => a.Type.HasValue && request.Types.Contains(a.Type.Value));
                }

                if (request.AcknowledgedFrom.HasValue)
                {
                    expression = expression.And(a => a.AcknowledgedUtc >= request.AcknowledgedFrom.Value);
                }

                if (request.AcknowledgedTo.HasValue)
                {
                    expression = expression.And(a => a.AcknowledgedUtc <= request.AcknowledgedTo.Value);
                }

                if (request.SeverityIds != null && request.SeverityIds.Any())
                {
                    expression = expression.And(a => a.AlertSeverityId.HasValue && request.SeverityIds.Contains(a.AlertSeverityId.Value));
                }
            }
            else
            {
                // No search parameters specified. Return all unacknowledged alerts.
                expression = expression.And(a => !a.Acknowledged);
            }

            var dbContext = ServiceLocator.Current.GetInstance<DbContext>();

            // Filter out any home sensing alerts that have been ignored.
            var alertsGroupedByPatientQuery = dbContext
                .Set<Alert>()
                .Where(expression)
                .GroupBy(a => a.PatientId)
                .Select(g => new PatientAlerts() { PatientId = g.Key, Alerts = g.ToList() })
                .OrderBy(e => e.PatientId)
                .AsQueryable();

            var alertsGroupedByPatient = alertsGroupedByPatientQuery.Future();

            // This was added to avoid getting measurements, thresholds, vitals and severities from db one by one (by id) 
            //   which used to be happening during mapping.
            // This is intended to optimize getting alerts.
            var severities = alertsGroupedByPatientQuery.SelectMany(a => a.Alerts.Select(va => va.AlertSeverity)).Future();

            if (request != null && !request.IsBrief)
            {
                // Get measurements for VitalAlerts.
                var measurements = alertsGroupedByPatientQuery
                    .SelectMany(a => a.Alerts.OfType<VitalAlert>().Select(va => va.Vital.Measurement))
                    .Include(m => m.Vitals)
                    .Future();

                // Get thresholds for VitalAlerts.
                var thresholds = alertsGroupedByPatientQuery
                    .SelectMany(a => a.Alerts.OfType<VitalAlert>().Select(va => va.Threshold))
                    .Future();

                // Get vitals for VitalSlerts.
                var vitals = alertsGroupedByPatientQuery
                    .SelectMany(a => a.Alerts.OfType<VitalAlert>().Select(va => va.Vital))
                    .Future();

                // Get health session element values/notes/health sessions/health session element alerts.
                var healthSessionElements = alertsGroupedByPatientQuery
                    .SelectMany(pa => pa.Alerts.OfType<HealthSessionElementAlert>().Select(a => a.HealthSessionElement))
                    .Include(el => el.Values)
                    .Include(el => el.Notes)
                    .Include(el => el.HealthSession)
                    .Include(el => el.HealthSessionElementAlert)
                    .Future();

                // Get health session element values.
                var healthSessionElementsValues = alertsGroupedByPatientQuery
                    .SelectMany(pa => pa.Alerts.OfType<HealthSessionElementAlert>().SelectMany(a => a.HealthSessionElement.Values))
                    .Include(val => val.HealthSessionElement)
                    .Future();
            }

            var result = new PagedResult<PatientAlerts>()
            {
                Total = alertsGroupedByPatient.LongCount()
            };

            if (request != null)
            {
                result.Results = alertsGroupedByPatient
                    .Skip(request.Skip)
                    .Take(request.Take)
                    .ToList();
            }
            else
            {
                result.Results = alertsGroupedByPatient.ToList();
            }

#warning Band-aid: querying for alerts needs fixing. The queries generated by EF and the resulting performance are catastrophes.
            // EF doesn't provide a good way to query a property that only exists on a specific sub-type. In this case, IsIgnored only exists 
            //   on Home Sensing alerts. As such, we have to manually go through the results in memory and remove them.
            // The one saving grace is that the act of ignoring a Home Sensing Alert also acknowledges it, so ignored alerts should already be
            //   filtered out. However, it requires two service calls, so in the event that the ack fails, we still need this code here to ensure
            //   no ignored Home Sensing alerts slip through.
            foreach (var patient in result.Results)
            {
                // Keep the alert if it's NOT a home sensing alert, or if it is, and it has not yet been ignored.
                patient.Alerts = patient.Alerts
                    .Where(a =>
                        !(a is HomeSensingAlert)
                        || ((a is HomeSensingAlert) && ((HomeSensingAlert)a).IsIgnored == false))
                    .ToList();
            }

            return result;
        }

        private Vital convertUnit(Vital vital, string vitalName)
        {
            decimal cValue;
            switch (vitalName)
            {
                case "Weight":
                    if (!vital.Unit.Equals("Lbs") && vital.Unit.Equals("kg"))
                    {
                        cValue = (vital.Value * Convert.ToDecimal(0.453));
                        vital.Value = cValue;
                        vital.Unit = "Lbs";
                    }

                    break;
                case "BloodGlucose":
                    if (!vital.Unit.Equals("mgdl") && vital.Unit.Equals("mmol_L"))
                    {
                        cValue = (vital.Value * 18);
                        vital.Value = cValue;
                        vital.Unit = "mgdl";
                    }

                    break;
                case "Temperature":
                    if (!vital.Unit.Equals("F") && vital.Unit.Equals("C"))
                    {
                        cValue = (vital.Value + 32);
                        vital.Value = cValue;
                        vital.Unit = "F";
                    }

                    break;
                default:

                    break;
            }
            return vital;
        }
        private Measurement getConvertedUnits(Measurement measurement)
        {
            foreach (var vital in measurement.Vitals)
            {
                var cVital = convertUnit(vital, vital.Name);
                vital.Value = (cVital.Value.ToString().Split('.').Length > 1) ? Math.Round(cVital.Value, 2) : cVital.Value;
                vital.Unit = cVital.Unit;
            }
            return measurement;
        }

        /// <summary>
        /// Generates the violation alerts for no severity
        /// </summary>
        /// <param name="measurement"></param>
        /// <param name="aggregatedThresholds"></param>
        public void CreateViolationAlertsForNoSeverity(Measurement measurement, IList<Threshold> aggregatedThresholds)
        {
            if (aggregatedThresholds.Any())
            {
                var vitals = measurement.Vitals;

                foreach (var vital in vitals)
                {
                    var maxViolatedThresholds =
                        aggregatedThresholds.Where(t => t.Name.ToString() == vital.Name && vital.Value >= t.MaxValue).ToList();//MS-2859 fix

                    var minViolatedThresholds =
                        aggregatedThresholds.Where(t => t.Name.ToString() == vital.Name && vital.Value <= t.MinValue).ToList();//MS-2859 fix

                    if (maxViolatedThresholds.All(t => t.AlertSeverity != null))
                    {
                        maxViolatedThresholds = maxViolatedThresholds.OrderByDescending(t => t.AlertSeverity.Severity).ToList();
                    }

                    if (minViolatedThresholds.All(t => t.AlertSeverity != null))
                    {
                        minViolatedThresholds = minViolatedThresholds.OrderByDescending(t => t.AlertSeverity.Severity).ToList();
                    }

                    //we take FirstOrDefault because maxViolatedThresholds is descending ordered by AlertSeverityId (if there are severities), so the violated threshold with max severity will be on firt place.
                    var maxViolatedThreshold = maxViolatedThresholds.FirstOrDefault();
                    //the same as maxViolatedThreshold
                    var minViolatedThreshold = minViolatedThresholds.FirstOrDefault();

                    if (maxViolatedThreshold == null && minViolatedThreshold == null)
                    {
                        maxViolatedThresholds =
                        aggregatedThresholds.Where(t => t.Name.ToString() == vital.Name && vital.Value < t.MaxValue).ToList();//MS-2859 fix

                        minViolatedThresholds =
                            aggregatedThresholds.Where(t => t.Name.ToString() == vital.Name && vital.Value > t.MinValue).ToList();//MS-2859 fix

                        if (maxViolatedThresholds.All(t => t.AlertSeverity != null))
                        {
                            maxViolatedThresholds = maxViolatedThresholds.OrderByDescending(t => t.AlertSeverity.Severity).ToList();
                        }

                        if (minViolatedThresholds.All(t => t.AlertSeverity != null))
                        {
                            minViolatedThresholds = minViolatedThresholds.OrderByDescending(t => t.AlertSeverity.Severity).ToList();
                        }

                        //we take FirstOrDefault because maxViolatedThresholds is descending ordered by AlertSeverityId (if there are severities), so the violated threshold with max severity will be on firt place.
                        maxViolatedThreshold = maxViolatedThresholds.LastOrDefault();
                        //the same as maxViolatedThreshold
                        minViolatedThreshold = minViolatedThresholds.LastOrDefault();

                    }

                    if (minViolatedThreshold != null || maxViolatedThreshold != null)
                    {
                        var violatedThresholdValue =
                            minViolatedThreshold != null ?
                            minViolatedThreshold.MinValue :
                            maxViolatedThreshold.MaxValue;

                        var violatedThreshold = minViolatedThreshold ?? maxViolatedThreshold;

                        var alertTitle = string.Format(
                            "{0}: {1} {2} violated threshold of {3} {4} on {5}",
                            vital.Name,
                            vital.Value,
                            vital.Unit,
                            violatedThresholdValue,
                            violatedThreshold.Unit,
                            measurement.ObservedUtc
                        );

                        vital.VitalAlert = new VitalAlert
                        {
                            Id = SequentialGuidGenerator.Generate(),
                            Threshold = violatedThreshold,
                            CustomerId = measurement.CustomerId,
                            PatientId = measurement.PatientId,
                            Type = AlertType.VitalsViolation,
                            Title = alertTitle,
                            Body = null,
                            OccurredUtc = measurement.ObservedUtc,
                            ExpiresUtc = null,
                            Weight = 0,
                            AlertSeverity = violatedThreshold.AlertSeverity
                        };
                    }
                }
            }
        }

        public async Task CreateViolationAlerts(Measurement measurement, IList<Threshold> aggregatedThresholds)
        {
            var lowestAlertSeverity = await _alertSeverityRepository
                               .FirstOrDefaultAsync(
                                   s => s.CustomerId == measurement.CustomerId,
                                   s => s.OrderBy(e => e.Severity)
                               );

            var cmeasurement = getConvertedUnits(measurement);
            var vitals = measurement.Vitals;
            //If the customer has severities assigned
            if (lowestAlertSeverity != null)
            {
               this.CreateViolationAlertsForSeverities(measurement, aggregatedThresholds,lowestAlertSeverity);
            }
            //If the customer has no severities assigned
            else
            {
                this.CreateViolationAlertsForNoSeverity(measurement, aggregatedThresholds);
            }
        }
        /// <summary>
        /// Create alerts assign them to to measurement.Vitals in case of vital violation
        /// </summary>
        /// <param name="measurement">The measurement</param>
        /// <param name="aggregatedThresholds">The aggregated thresholds</param>
        /// <returns></returns>
        public void CreateViolationAlertsForSeverities(Measurement measurement, IList<Threshold> aggregatedThresholds, AlertSeverity lowestAlertSeverity)
        {
            //var lowestAlertSeverity = await _alertSeverityRepository
            //                   .FirstOrDefaultAsync(
            //                       s => s.CustomerId == measurement.CustomerId,
            //                       s => s.OrderBy(e => e.Severity)
            //                   );

            var cmeasurement = getConvertedUnits(measurement);
            var vitals = measurement.Vitals;

            foreach (var vital in vitals)
            {
                if (aggregatedThresholds.Any())
                {
                    var aggregatedThresholdsForVital = aggregatedThresholds.Where(t => t.Name.ToString() == vital.Name).ToList();

                    if (aggregatedThresholdsForVital.Any())
                    {
                        var maxViolatedThresholds =
                        aggregatedThresholdsForVital.Where(t => t.Name.ToString() == vital.Name && vital.Value >= t.MaxValue).ToList();//MS-2859 fix

                        var minViolatedThresholds =
                            aggregatedThresholdsForVital.Where(t => t.Name.ToString() == vital.Name && vital.Value <= t.MinValue).ToList();//MS-2859 fix

                        if (maxViolatedThresholds.All(t => t.AlertSeverity != null))
                        {
                            maxViolatedThresholds = maxViolatedThresholds.OrderByDescending(t => t.AlertSeverity.Severity).ToList();
                        }

                        if (minViolatedThresholds.All(t => t.AlertSeverity != null))
                        {
                            minViolatedThresholds = minViolatedThresholds.OrderByDescending(t => t.AlertSeverity.Severity).ToList();
                        }

                        //we take FirstOrDefault because maxViolatedThresholds is descending ordered by AlertSeverityId (if there are severities), so the violated threshold with max severity will be on firt place.
                        var maxViolatedThreshold = maxViolatedThresholds.FirstOrDefault();
                        //the same as maxViolatedThreshold
                        var minViolatedThreshold = minViolatedThresholds.FirstOrDefault();

                        if (maxViolatedThreshold == null && minViolatedThreshold == null)
                        {
                            maxViolatedThresholds =
                            aggregatedThresholdsForVital.Where(t => t.Name.ToString() == vital.Name && vital.Value < t.MaxValue).ToList();//MS-2859 fix

                            minViolatedThresholds =
                                aggregatedThresholdsForVital.Where(t => t.Name.ToString() == vital.Name && vital.Value > t.MinValue).ToList();//MS-2859 fix

                            if (maxViolatedThresholds.All(t => t.AlertSeverity != null))
                            {
                                maxViolatedThresholds = maxViolatedThresholds.OrderByDescending(t => t.AlertSeverity.Severity).ToList();
                            }

                            if (minViolatedThresholds.All(t => t.AlertSeverity != null))
                            {
                                minViolatedThresholds = minViolatedThresholds.OrderByDescending(t => t.AlertSeverity.Severity).ToList();
                            }

                            if (maxViolatedThresholds.Count > 0)
                            {
                                //we take FirstOrDefault because maxViolatedThresholds is descending ordered by AlertSeverityId (if there are severities), so the violated threshold with max severity will be on firt place.
                                maxViolatedThreshold = maxViolatedThresholds.LastOrDefault();
                            }
                            if (minViolatedThresholds.Count > 0)
                            {
                                //the same as maxViolatedThreshold
                                minViolatedThreshold = minViolatedThresholds.LastOrDefault();
                            }

                        }
                        //M-3465 Fix
                        if (maxViolatedThreshold != null || minViolatedThreshold != null)
                        {
                            var violatedThresholdValue =
                                minViolatedThreshold != null ?
                                minViolatedThreshold.MinValue :
                                maxViolatedThreshold.MaxValue;

                            var violatedThreshold = minViolatedThreshold ?? maxViolatedThreshold;

                            var alertTitle = string.Format(
                                "{0}: {1} {2} violated threshold of {3} {4} on {5}",
                                vital.Name,
                                vital.Value,
                                vital.Unit,
                                violatedThresholdValue,
                                violatedThreshold.Unit,
                                measurement.ObservedUtc
                            );

                            vital.VitalAlert = new VitalAlert
                            {
                                Id = SequentialGuidGenerator.Generate(),
                                Threshold = violatedThreshold,
                                ThresholdId = violatedThreshold.Id,//MS-3438 Fix
                                CustomerId = measurement.CustomerId,
                                PatientId = measurement.PatientId,
                                Type = AlertType.VitalsViolation,
                                Title = alertTitle,
                                Body = null,
                                OccurredUtc = measurement.ObservedUtc,
                                ExpiresUtc = null,
                                Weight = 0,
                                AlertSeverity = violatedThreshold.AlertSeverity
                            };
                        }

                    }
                    else
                    {
                        //if no aggregate thresholds does not exists for the vital
                        var vitalType = Enum.Parse(typeof(VitalType), vital.Name);

                        Dictionary<decimal, decimal> vitalMinMaxValues;
                        vitalMinMaxValueMap.TryGetValue((VitalType)vitalType, out vitalMinMaxValues);

                        if (vitalMinMaxValues.Any())
                        {
                            var minValue = vitalMinMaxValues.FirstOrDefault().Key;
                            var maxValue = vitalMinMaxValues.FirstOrDefault().Value;


                            var alertTitle = string.Format(
                                "{0}: {1} {2} violated threshold of {3} {4} on {5}",
                                vital.Name,
                                vital.Value,
                                vital.Unit,
                                minValue,
                                vital.Unit,
                                measurement.ObservedUtc
                            );

                            var threshold = new DefaultThreshold()
                            {
                                Id = SequentialGuidGenerator.Generate(),
                                CustomerId = measurement.CustomerId,
                                MaxValue = maxValue,
                                MinValue = minValue,
                                IsDeleted = false,
                                Type = ThresholdType.Basic.ToString(),
                                AlertSeverity = lowestAlertSeverity,
                                Name = vitalType.ToString(),
                                Unit = vital.Unit,
                                DefaultType = ThresholdDefaultType.Customer.ToString()
                            };

                            vital.VitalAlert = new VitalAlert
                            {
                                Id = SequentialGuidGenerator.Generate(),
                                CustomerId = measurement.CustomerId,
                                PatientId = measurement.PatientId,
                                Threshold = threshold,
                                Type = AlertType.VitalsViolation,
                                Title = alertTitle,
                                Body = null,
                                OccurredUtc = measurement.ObservedUtc,
                                ExpiresUtc = null,
                                Weight = 0,
                                AlertSeverity = lowestAlertSeverity
                            };
                        }
                    }
                }
                else
                {
                    //If no aggregate thresholds exists
                    var vitalType = Enum.Parse(typeof(VitalType), vital.Name);

                    Dictionary<decimal, decimal> vitalMinMaxValues;
                    vitalMinMaxValueMap.TryGetValue((VitalType)vitalType, out vitalMinMaxValues);

                    if (vitalMinMaxValues.Any())
                    {
                        var minValue = vitalMinMaxValues.FirstOrDefault().Key;
                        var maxValue = vitalMinMaxValues.FirstOrDefault().Value;

                        var alertTitle = string.Format(
                               "{0}: {1} {2} violated threshold of {3} {4} on {5}",
                               vital.Name,
                               vital.Value,
                               vital.Unit,
                               minValue,
                               vital.Unit,
                               measurement.ObservedUtc
                           );

                        var threshold = new DefaultThreshold()
                        {
                            Id = SequentialGuidGenerator.Generate(),
                            CustomerId = measurement.CustomerId,
                            MaxValue = maxValue,
                            MinValue = minValue,
                            IsDeleted = false,
                            Type = ThresholdType.Basic.ToString(),
                            AlertSeverity = lowestAlertSeverity,
                            Name = vitalType.ToString(),
                            Unit = vital.Unit,
                            DefaultType = ThresholdDefaultType.Customer.ToString()
                        };

                        vital.VitalAlert = new VitalAlert
                        {
                            Id = SequentialGuidGenerator.Generate(),
                            CustomerId = measurement.CustomerId,
                            PatientId = measurement.PatientId,
                            Threshold = threshold,
                            Type = AlertType.VitalsViolation,
                            Title = alertTitle,
                            Body = null,
                            OccurredUtc = measurement.ObservedUtc,
                            ExpiresUtc = null,
                            Weight = 0,
                            AlertSeverity = lowestAlertSeverity
                        };
                    }
                }
            }
        }

        /// <summary>
        /// Returns alerts by id.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="alertid">The alert identifier.</param>
        /// <returns>
        /// Updated entity.
        /// </returns>
        public async Task<Alert> GetById(int customerId, Guid patientId, Guid alertId)
        {
            var result = await _alertRepository
                .FindAsync(
                    m => m.CustomerId == customerId && m.PatientId == patientId && m.Id == alertId,
                    null
                );

            return result.FirstOrDefault();
        }
    }
}
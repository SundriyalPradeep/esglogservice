﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using VitalsService.DataAccess.EF;
using VitalsService.DataAccess.EF.Repositories;
using VitalsService.Domain.DbEntities;
using VitalsService.Domain.Dtos;
using VitalsService.Domain.Enums.Questionnaires;
using VitalsService.DomainLogic.Services.Interfaces;

namespace VitalsService.DomainLogic.Services.Implementations
{
    public class QuestionnaireResponseService : IQuestionnaireResponseService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<QuestionnaireResponse> _questionnaireResponseRepository;

        public QuestionnaireResponseService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;

            _questionnaireResponseRepository = _unitOfWork.CreateRepository<QuestionnaireResponse>();
        }


        public async Task<OperationResultDto<QuestionnaireResponse, QuestionnaireStatus>> CreateAsync(QuestionnaireResponse questionnaireResp)
        {
            _questionnaireResponseRepository.Insert(questionnaireResp);
            await _unitOfWork.SaveAsync();

            return new OperationResultDto<QuestionnaireResponse, QuestionnaireStatus>
            {
                Status = QuestionnaireStatus.Success,
                Content = questionnaireResp
            };
        }

        public async Task<UpdateQuestionnaireResponseStatus> RecordActionTakenAsync(int customerId, Guid patientId, Guid questionnaireResponseId, Guid answerChoiceId, string answerChoiceText, string freeText, Guid exacerbationAnswerChoiceId, string exacerbationAnswerChoiceText)
        {
            var existingQuestionnaireResponse = (await _questionnaireResponseRepository
                .FindAsync(qr => qr.CustomerId == customerId && qr.PatientId == patientId && qr.Id == questionnaireResponseId))
                .SingleOrDefault();

            if (existingQuestionnaireResponse == null)
            {
                return UpdateQuestionnaireResponseStatus.QuestionnaireResponseNotFound;
            }

            existingQuestionnaireResponse.ActionTakenAnswerChoiceId = answerChoiceId;
            existingQuestionnaireResponse.ActionTakenAnswerChoiceText = answerChoiceText?.Trim();
            existingQuestionnaireResponse.ActionTakenFreeText = freeText?.Trim();
            existingQuestionnaireResponse.ExacerbationAnswerChoiceId = exacerbationAnswerChoiceId;
            existingQuestionnaireResponse.ExacerbationAnswerChoiceText = exacerbationAnswerChoiceText?.Trim();
            existingQuestionnaireResponse.UpdatedUtc = DateTime.UtcNow;

            await _unitOfWork.SaveAsync();

            return UpdateQuestionnaireResponseStatus.Success;
        }

        public async Task<PagedResult<QuestionnaireResponse>> GetQuestionnaireResponsesAsync(int customerId, Guid patientId, BaseSearchDto request)
        {
            Expression<Func<QuestionnaireResponse, bool>> expression = qr => qr.CustomerId == customerId && qr.PatientId == patientId;

            // Note: we explicitly do not include the child question/answer objects. Maestro currently has no need for them.
            return (await _questionnaireResponseRepository
                .FindPagedAsync(
                    expression,
                    o => o.OrderByDescending(qr => qr.CreatedUtc),
                    startIndex: request?.Skip,
                    limit: request?.Take
                )
                );
        }

        public async Task<QuestionnaireResponse> GetQuestionnaireResponseAsync(int customerId, Guid patientId, Guid questionnaireResponseId)
        {
            return (await _questionnaireResponseRepository
                .FindAsync(
                    t => t.CustomerId == customerId && t.PatientId == patientId && t.Id == questionnaireResponseId,
                    null,
                    new List<Expression<Func<QuestionnaireResponse, object>>>
                    {
                        e => e.QuestionAnswers
                    }
                )
            ).SingleOrDefault();
        }
    }
}

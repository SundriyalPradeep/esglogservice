﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using NLog;
using VitalsService.DataAccess.Document;
using VitalsService.DataAccess.Document.Repository;
using VitalsService.DataAccess.EF;
using VitalsService.DataAccess.EF.Repositories;
using VitalsService.Domain.DbEntities;
using VitalsService.Domain.DocumentDb;
using VitalsService.Domain.Dtos;
using VitalsService.Domain.Enums;
using VitalsService.Domain.EsbEntities;
using VitalsService.DomainLogic.Services.Interfaces;
using VitalsService.Helpers;

namespace VitalsService.DomainLogic.Services.Implementations
{
    /// <summary>
    /// Provides methods to manage measurments and vitals.
    /// </summary>
    public class MeasurmentsService : IMeasurementsService
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Measurement> _measurementRepository;
        private readonly IDocumentDbRepository<RawMeasurement> _rawMeasurementRepository;
        private readonly IEsb _esb;
        private readonly IMapper _mapper;
        private readonly IRepository<Vital> _vitalRepository;
        /// <summary>
        /// Initializes a new instance of the <see cref="MeasurmentsService"/> class.
        /// </summary>
        /// <param name="unitOfWork">The unit of work.</param>
        /// <param name="documentRepositoryFactory">The document repository factory.</param>
        /// <param name="esb">The esb.</param>
        /// <param name="mapper"></param>
        public MeasurmentsService(
            IUnitOfWork unitOfWork,
            IDocumentRepositoryFactory documentRepositoryFactory,
            IEsb esb,
            IMapper mapper
        )
        {
            _unitOfWork = unitOfWork;
            _measurementRepository = unitOfWork.CreateRepository<Measurement>();
            _rawMeasurementRepository = documentRepositoryFactory.Create<RawMeasurement>(VitalsServiceCollection.RawMeasurment);
            _esb = esb;
            _vitalRepository = unitOfWork.CreateRepository<Vital>();
            _mapper = mapper;
        }

        /// <summary>
        /// Creates new measurment with a set of vitals.
        /// </summary>
        /// <param name="measurement"></param>
        /// <param name="rawData">Raw data received from peripheral</param>
        /// <param name="sendMeasurementToEsb">bool flag specifies measurement should be sent to esb or not</param>
        /// <returns>
        /// Created entity.
        /// </returns>
        public async Task<Measurement> Create(Measurement measurement, object rawData, bool sendMeasurementToEsb)
        {
            _measurementRepository.Insert(measurement);

            await _unitOfWork.SaveAsync();

            var rawMeasurement = new RawMeasurement
            {
                Id = measurement.Id,
                CustomerId = measurement.CustomerId,
                PatientId = measurement.PatientId,
                Observed = measurement.ObservedUtc,
                RawJson = rawData
            };

            await _rawMeasurementRepository.CreateItemAsync(rawMeasurement);

            if (sendMeasurementToEsb)
            {
                try
                {
                    var measurementMessage = _mapper.Map<Measurement, MeasurementMessage>(measurement);
                    await _esb.PublishMeasurement(measurementMessage);
                }
                catch (Exception e)
                {
                    logger.Error(e, "An error occured when try to publish measurement to Esb");
                }
            }

            return measurement;
        }

        /// <summary>
        /// Updates measurement data.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="measurement">The measurement.</param>
        /// <returns></returns>
        public async Task<OperationResultDto<Measurement, UpdateMeasurementStatus>> Update(int customerId, Measurement measurement)
        {
            var existingMeasurement = await GetById(customerId, measurement.PatientId, measurement.Id);

            if (existingMeasurement == null)
            {
                return new OperationResultDto<Measurement, UpdateMeasurementStatus>()
                {
                    Content = null,
                    Status = UpdateMeasurementStatus.MeasurementNotFound
                };
            }

            //existingMeasurement.IsInvalidated = measurement.IsInvalidated;

            _measurementRepository.Update(existingMeasurement);
            await _unitOfWork.SaveAsync();

            return new OperationResultDto<Measurement, UpdateMeasurementStatus>()
            {
                Content = existingMeasurement,
                Status = UpdateMeasurementStatus.Success
            };
        }

        /// <summary>
        /// Updates vital data.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="measurement">The measurement.</param>
        /// <returns></returns>
        public async Task<OperationResultDto<Vital, UpdateMeasurementStatus>> UpdateIsInvalidated(int customerId, Vital vital)
        {
            _vitalRepository.Update(vital);
            await _unitOfWork.SaveAsync();

            return new OperationResultDto<Vital, UpdateMeasurementStatus>()
            {
                Content = vital,
                Status = UpdateMeasurementStatus.Success
            };
        }



        /// <summary>
        /// Returs patients with appropriate search criteria.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="filterMeasurement">The filter.</param>
        /// <param name="clientId">The client identifier.</param>
        /// <returns></returns>
        public async Task<PagedResult<Measurement>> Search(
            int customerId,
            Guid patientId,
            MeasurementsSearchDto filterMeasurement,
            string clientId = null
        )
        {
            Expression<Func<Measurement, bool>> expression = m => m.PatientId == patientId && m.CustomerId == customerId;

            if (filterMeasurement != null)
            {
                if (!string.IsNullOrEmpty(filterMeasurement.Q))
                {
                    var terms = filterMeasurement.Q.Split(' ').Where(r => !string.IsNullOrWhiteSpace(r));

                    foreach (var term in terms)
                    {
                        expression = expression.And(m => m.Vitals.Any(v => v.Name.Contains(term)));
                    }
                }

                if (filterMeasurement.ObservedFrom.HasValue)
                {
                    expression = expression
                        .And(m => m.ObservedUtc >= filterMeasurement.ObservedFrom.Value);
                }

                if (filterMeasurement.ObservedTo.HasValue)
                {
                    expression = expression
                        .And(m => m.ObservedUtc <= filterMeasurement.ObservedTo.Value);
                }

                if (filterMeasurement.IsInvalidated.HasValue)
                {
                    //    expression = expression
                    //        .And(m => m.Vitals.IsInvalidated == filterMeasurement.IsInvalidated.Value);
                }

                if (!string.IsNullOrEmpty(filterMeasurement.ObservedTz))
                {
                    expression = expression.And(m => m.ObservedTz.ToLower() == filterMeasurement.ObservedTz.ToLower());
                }
            }

            if (!string.IsNullOrEmpty(clientId))
            {
                expression = expression.And(hs => hs.ClientId.ToLower() == clientId.ToLower());
            }
            

            var result = await _measurementRepository
                .FindPagedAsync(
                    expression,
                o => o.OrderBy(e => e.Id),
                new List<Expression<Func<Measurement, object>>>
                    {
                        m => m.MeasurementNotes,
                        m => m.MeasurementValues,
                        m => m.Vitals,
                        m => m.Vitals.Select(v => v.VitalAlert.Threshold),
                        m => m.Vitals.Select(v => v.VitalAlert.AlertSeverity),
                        m => m.Device
                    },
                asNoTracking: true,
                startIndex: filterMeasurement != null ? filterMeasurement.Skip : (int?)null,
                limit: filterMeasurement != null ? filterMeasurement.Take : (int?)null
                );
            var measurementList = result.Results;
            if (measurementList != null)
            {
                //var vitalList = measurementList.Select(m => m.Vitals.Where(v => v.IsInvalidated = false)).ToList();
                foreach (var item in measurementList)
                {
                    item.Vitals = item.Vitals.Where(p => p.IsInvalidated == false).ToList();
                }
            }


            measurementList = measurementList.Where(p => p.Vitals.Count > 0).ToList();
            result.Results = measurementList;
            return result;
        }

        /// <summary>
        /// Returns vital by id.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="measurementId">The measurement identifier.</param>
        /// <returns>
        /// Updated entity.
        /// </returns>
        public async Task<Measurement> GetById(int customerId, Guid patientId, Guid measurementId)
        {
            var includes = new List<Expression<Func<Measurement, object>>>
            {
                m => m.MeasurementNotes,
                m => m.Vitals,
                m => m.Vitals.Select(v => v.VitalAlert.Threshold),
                m => m.Vitals.Select(v => v.VitalAlert.AlertSeverity),
                m => m.Device
            };

            var result = await _measurementRepository
                .FindAsync(
                    m => m.CustomerId == customerId && m.PatientId == patientId && m.Id == measurementId,
                    null,
                    includes
                );

            return result.FirstOrDefault();
        }

        /// <summary>
        /// Returns vital by id.
        /// </summary>
        /// <param name="vitalId">The measurement identifier.</param>
        /// <returns>
        /// Updated entity.
        /// </returns>
        public async Task<Vital> GetVitalById(Guid vitalId)
        {
            return (await _vitalRepository.FindAsync(
                         t => t.Id == vitalId
                     )
                 )
                 .FirstOrDefault();

        }
    }
}
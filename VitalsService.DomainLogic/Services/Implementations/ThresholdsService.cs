﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using VitalsService.DataAccess.EF;
using VitalsService.DataAccess.EF.Repositories;
using VitalsService.Domain.DbEntities;
using VitalsService.Domain.Dtos;
using VitalsService.Domain.Enums;
using VitalsService.DomainLogic.Services.Interfaces;
using VitalsService.Helpers;
using NLog;

namespace VitalsService.DomainLogic.Services.Implementations
{
    /// <summary>
    /// ThresholdsService.
    /// </summary>
    public class ThresholdsService : IThresholdsService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<PatientThreshold> _thresholdRepository;
        private readonly IRepository<AlertSeverity> _alertSeverityRepository;
        private readonly IRepository<VitalAlert> _vitalAlertsRepository;
        private readonly IRepository<PatientThresholdType> _patientThresholdTypeRepository;
        private readonly IMapper _mapper;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="ThresholdsService"/> class.
        /// </summary>
        /// <param name="unitOfWork">The unit of work.</param>
        public ThresholdsService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _thresholdRepository = unitOfWork.CreateRepository<PatientThreshold>();
            _alertSeverityRepository = unitOfWork.CreateRepository<AlertSeverity>();
            _vitalAlertsRepository = unitOfWork.CreateRepository<VitalAlert>();
            _patientThresholdTypeRepository = unitOfWork.CreateRepository<PatientThresholdType>();
            _mapper = mapper;
        }

        /// <summary>
        /// Creates new measurment with a set of vitals.
        /// </summary>
        /// <param name="threshold">The threshold.</param>
        /// <returns>
        /// Created entity.
        /// </returns>
        public async Task<OperationResultDto<Guid, CreateUpdateThresholdStatus>> CreateThreshold(PatientThreshold threshold)
        {
            try
            {
                //To save the patientthresholdtype without values if min and max are null
                if (!(threshold.MinValue == null && threshold.MaxValue == null))
                {
                    var existingAlertSeverities = await _alertSeverityRepository
                    .FindAsync(a => a.CustomerId == threshold.CustomerId);

                    if (threshold.AlertSeverityId.HasValue)
                    {
                        var existingAlertSeverity =
                            existingAlertSeverities.SingleOrDefault(a => a.Id == threshold.AlertSeverityId.Value);

                        if (existingAlertSeverity == null)
                        {
                            return new OperationResultDto<Guid, CreateUpdateThresholdStatus>()
                            {
                                Status = CreateUpdateThresholdStatus.AlertSeverityDoesNotExist
                            };
                        }
                    }
                    else
                    {
                        if (existingAlertSeverities.Any())
                        {
                            return new OperationResultDto<Guid, CreateUpdateThresholdStatus>()
                            {
                                Status = CreateUpdateThresholdStatus.ExistingAlertSeverityShouldBeUsed
                            };
                        }
                    }

                    var existingPatientVitalThresholds = await _thresholdRepository
                        .FindAsync(t =>
                            t.CustomerId == threshold.CustomerId &&
                            t.PatientId == threshold.PatientId &&
                            t.Name.ToLower() == threshold.Name.ToLower() &&
                            t.AlertSeverityId == threshold.AlertSeverityId &&
                            t.IsDeleted == false
                        );

                    if (existingPatientVitalThresholds.Any())
                    {
                        return new OperationResultDto<Guid, CreateUpdateThresholdStatus>()
                        {
                            Status = CreateUpdateThresholdStatus.VitalThresholdAlreadyExists
                        };
                    }

                    _thresholdRepository.Insert(threshold);
                    await _unitOfWork.SaveAsync();
                }

                return await Task.FromResult(
                    new OperationResultDto<Guid, CreateUpdateThresholdStatus>()
                    {
                        Status = CreateUpdateThresholdStatus.Success,
                        Content = threshold.Id
                    }
                );

            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Failure Create Thresholds");

                // We want to know about this, but we don't want it to cause an application failure.
                return null;
            }
        }

        /// <summary>
        /// Updates the threshold.
        /// </summary>
        /// <param name="threshold">The threshold.</param>
        /// <returns></returns>
        public async Task<CreateUpdateThresholdStatus> UpdateThreshold(PatientThreshold threshold)
        {
            //To save the patientthresholdtype without values if min and max are null
            if (!(threshold.MinValue == null && threshold.MaxValue == null))
            {
                var existingPatientThresholds = await _thresholdRepository
                    .FindAsync(t =>
                        t.CustomerId == threshold.CustomerId &&
                        t.PatientId == threshold.PatientId && t.IsDeleted == false
                    );

                var requestedThreshold = existingPatientThresholds.SingleOrDefault(t => t.Id == threshold.Id);

                if (requestedThreshold == null)
                {
                    return await Task.FromResult(CreateUpdateThresholdStatus.VitalThresholdDoesNotExist);
                }

                var existingAlertSeverities = await _alertSeverityRepository
                    .FindAsync(a => a.CustomerId == threshold.CustomerId);

                if (threshold.AlertSeverityId.HasValue)
                {
                    var existingAlertSeverity =
                        existingAlertSeverities.SingleOrDefault(a => a.Id == threshold.AlertSeverityId.Value);

                    if (existingAlertSeverity == null)
                    {
                        return await Task.FromResult(CreateUpdateThresholdStatus.AlertSeverityDoesNotExist);
                    }
                }
                else
                {
                    if (existingAlertSeverities.Any())
                    {
                        return await Task.FromResult(CreateUpdateThresholdStatus.ExistingAlertSeverityShouldBeUsed);
                    }
                }

                if (
                    requestedThreshold.Name.ToLower() != threshold.Name.ToLower() ||
                    requestedThreshold.AlertSeverityId != threshold.AlertSeverityId
                )
                {
                    var conflictingPatientThresholds = existingPatientThresholds
                        .Where(t =>
                            t.Id != threshold.Id &&
                            t.Name.ToLower() == threshold.Name.ToLower() &&
                            t.AlertSeverityId == threshold.AlertSeverityId
                        )
                        .ToList();

                    if (conflictingPatientThresholds.Any())
                    {
                        return await Task.FromResult(CreateUpdateThresholdStatus.VitalThresholdAlreadyExists);
                    }
                }

                _mapper.Map(threshold, requestedThreshold, typeof(PatientThreshold), typeof(PatientThreshold));

                _thresholdRepository.Update(requestedThreshold);
                await _unitOfWork.SaveAsync();
            }

            return CreateUpdateThresholdStatus.Success;
        }

        /// <summary>
        /// Updates the patientthresholdtype
        /// </summary>
        /// <param name="patientThresholdType"></param>
        /// <returns></returns>
        public async Task<CreateUpdateThresholdStatus> UpdatePatientThresholdType(PatientThresholdType patientThresholdType)
        {
            var existingPatientThresholdTypes = await _patientThresholdTypeRepository
                .FindAsync(t =>
                    t.PatientId == patientThresholdType.PatientId && t.IsDeleted == false
                );


            if (existingPatientThresholdTypes.Any())
            {
                foreach (PatientThresholdType item in existingPatientThresholdTypes)
                {
                    if (item.VitalType == patientThresholdType.VitalType && (item.CustomerId == 0 || item.CustomerId == patientThresholdType.CustomerId))
                    {
                        item.IsDeleted = true;
                        item.CustomerId = patientThresholdType.CustomerId;
                        _patientThresholdTypeRepository.Update(item);
                    }
                }

            }

            var isPatientThresholdTypeExists = await _patientThresholdTypeRepository
                .FindAsync(t =>
                    t.PatientId == patientThresholdType.PatientId && ((VitalType)t.VitalType == patientThresholdType.VitalType)
                );

            if (!isPatientThresholdTypeExists.Any())
            {
                _patientThresholdTypeRepository.Insert(patientThresholdType);
            }
            else
            {
                var existingThresholdType = isPatientThresholdTypeExists.FirstOrDefault();

                existingThresholdType.IsDeleted = false;
                existingThresholdType.CustomerId = patientThresholdType.CustomerId;
                if (existingThresholdType.ThresholdType != null && patientThresholdType.ThresholdType != null)
                {
                    existingThresholdType.ThresholdType = patientThresholdType.ThresholdType;
                    existingThresholdType.ConditionId = patientThresholdType.ConditionId;
                }

                _patientThresholdTypeRepository.Update(existingThresholdType);
            }

            await _unitOfWork.SaveAsync();

            return await Task.FromResult(CreateUpdateThresholdStatus.Success);
        }

        /// <summary>
        /// Deletes the threshold.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="thresholdId">The threshold identifier.</param>
        /// <returns></returns>
        public async Task<GetDeleteThresholdStatus> DeleteThreshold(int customerId, Guid patientId, Guid thresholdId)
        {
            var existingPatientVitalThreshold =
                await GetThreshold(customerId, patientId, thresholdId);

            if (existingPatientVitalThreshold != null)
            {
                var vitalAlerts = existingPatientVitalThreshold.VitalAlerts.ToList();

                // Explicitly removing all Vital alerts and alert details dependent on this patient's threshold
                //foreach (var vitalAlert in vitalAlerts)
                //{
                //    _vitalAlertsRepository.Delete(vitalAlert);
                //}

                //_thresholdRepository.Delete(existingPatientVitalThreshold);

                //  #region To update isDelete status to threshold

                existingPatientVitalThreshold.IsDeleted = true;
                _thresholdRepository.Update(existingPatientVitalThreshold);

                // #endregion

                await _unitOfWork.SaveAsync();

                return await Task.FromResult(GetDeleteThresholdStatus.Success);
            }

            return await Task.FromResult(GetDeleteThresholdStatus.NotFound);
        }

        /// <summary>
        /// Gets the threshold.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="thresholdId">The threshold identifier.</param>
        /// <returns></returns>
        public async Task<PatientThreshold> GetThreshold(
            int customerId,
            Guid patientId,
            Guid thresholdId
        )
        {
            return (await _thresholdRepository
                .FindAsync(
                    t => t.CustomerId == customerId &&
                        t.PatientId == patientId &&
                        t.Id == thresholdId &&
                        t.IsDeleted == false,

                    o => o.OrderBy(e => e.Id),
                    new List<Expression<Func<PatientThreshold, object>>>
                    {
                        e => e.VitalAlerts,
                        e => e.AlertSeverity
                    }
                )
            ).SingleOrDefault();
        }

        /// <summary>
        /// Gets the thresholds.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public async Task<PagedResult<PatientThreshold>> GetThresholds(int customerId, Guid patientId, BaseSearchDto request)
        {
            Expression<Func<PatientThreshold, bool>> expression = t => t.CustomerId == customerId && t.PatientId == patientId && t.IsDeleted == false;

            if (request != null)
            {
                if (!string.IsNullOrEmpty(request.Q))
                {
                    var terms = request.Q.Split(' ').Where(r => !string.IsNullOrWhiteSpace(r));

                    foreach (var term in terms)
                    {
                        expression = expression.And(t => t.Name.Contains(term));
                    }
                }
            }

            return await _thresholdRepository
                .FindPagedAsync(
                    expression,
                o => o.OrderBy(e => e.Id),
                new List<Expression<Func<PatientThreshold, object>>>
                    {
                        e => e.VitalAlerts,
                        e => e.AlertSeverity
                    },
                startIndex: request != null ? request.Skip : (int?)null,
                limit: request != null ? request.Take : (int?)null
                );
        }

        /// <summary>
        /// Gets the PatientThresholds Types.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns></returns>
        public async Task<List<PatientThresholdType>> GetPatientThresholdsTypes(int customerId, Guid patientId)
        {
            return (
                    await _patientThresholdTypeRepository.FindAsync(t => (t.CustomerId == 0 || t.CustomerId == customerId) && t.PatientId == patientId && t.IsDeleted == false));
        }

        /// <summary>
        /// Get all the patient threshold types based on patientid and vitalname
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="vitalType"></param>
        /// <returns></returns>
        public async Task<PatientThresholdType> GetPatientThresholdsTypes(int customerId, Guid patientId, string vitalType)
        {
            return (
                    await _patientThresholdTypeRepository.FindAsync(t => (t.CustomerId == 0 || t.CustomerId == customerId) && t.PatientId == patientId && t.IsDeleted == false && t.VitalType.ToString() == vitalType)
                   ).FirstOrDefault();
        }
    }
}
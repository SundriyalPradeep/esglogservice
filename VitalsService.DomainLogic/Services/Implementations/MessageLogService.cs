﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VitalsService.ContentStorage.Azure.Services.Interfaces;
using VitalsService.DataAccess.EF;
using VitalsService.DataAccess.EF.Repositories;
using VitalsService.DomainLogic.Services.Interfaces;
using VitalsService.Domain.DbEntities;


namespace VitalsService.DomainLogic.Services.Implementations
{
    class MessageLogService : IMessageLogService
    {
        private readonly IContentStorage contentStorage;
        private readonly IUnitOfWork unitOfWork;
        private readonly IRepository<ESGMessageTransaction> esgTransactionRepository;
        private readonly IRepository<ESGMessageContent> esgMessageBody;
        private readonly IRepository<ESGMessageAcknowledgement> messageAckRepository;



        public MessageLogService(IUnitOfWork unitOfWork, IContentStorage contentStorage)
        {
            this.unitOfWork = unitOfWork;
            this.esgTransactionRepository = unitOfWork.CreateRepository<ESGMessageTransaction>();
            this.messageAckRepository = unitOfWork.CreateRepository<ESGMessageAcknowledgement>();
            this.esgMessageBody = unitOfWork.CreateRepository<ESGMessageContent>();
            this.contentStorage = contentStorage;
        }

        public async Task<Guid> LogMessage(ESGMessageTransaction messageLog)
        {
            this.esgTransactionRepository.Insert(messageLog);

            await this.unitOfWork.SaveAsync();

            return messageLog.Id;
        }

        public async Task<Guid> StoreMessageBody(ESGMessageContent messageBody)
        {
            this.esgMessageBody.Insert(messageBody);
            await this.unitOfWork.SaveAsync();

            return messageBody.TransactionId; ;

        }

        public async Task<Guid> LogMessageAck(ESGMessageAcknowledgement messageLog)
        {
            this.messageAckRepository.Insert(messageLog);
            await this.unitOfWork.SaveAsync();

            return messageLog.ESGMessageTransactionId;
        }

        public async Task<Guid> GetTransactionId(string MessageControlId)
        {
            var result = await esgTransactionRepository
               .FirstOrDefaultAsync(
                   m => m.MessageControlId == MessageControlId,
                   null
               );

            return result != null ?result.Id :Guid.Empty ;
        }

        public async Task<ESGMessageContent> GetMessageContent(string ICN, string DFN, string MessageType)
        {


            var result = await esgTransactionRepository
                .FirstOrDefaultAsync(
                    m => m.DFN == DFN && m.ICN == ICN,
                    null
                );

            // return result.Id;

            var msg = await this.esgMessageBody.FirstOrDefaultAsync(m => m.TransactionId == result.Id);
            return msg;
        }

        public async Task<string> GetDFN(string ICN, string SSN, string MessageType)
        {


            var result = await esgTransactionRepository
                .FirstOrDefaultAsync(
                    m => m.SSN == SSN || m.ICN == ICN,
                    null
                );

            return result.DFN;

            //var msg = await this.esgMessageBody.FirstOrDefaultAsync(m => m. == result.Id);
            //return msg;
        }

        public async Task<string> GetEnrollmentDate(string ICN)
        {


            var result = await esgTransactionRepository
                .FirstOrDefaultAsync(
                    m => m.ICN == ICN,
                    null
                );

            return result.EnrollmentDate;

            //var msg = await this.esgMessageBody.FirstOrDefaultAsync(m => m. == result.Id);
            //return msg;
        }

        public async Task<ESGMessageContent> GetRegistrationMessage(string QBPMessageControlId)
        {


            ESGMessageTransaction resultTransId = await esgTransactionRepository.FirstOrDefaultAsync(i => i.MessageControlId == QBPMessageControlId);
            var A04Transaction = await esgTransactionRepository.FirstOrDefaultAsync(i => i.Id == resultTransId.RegistrationId);
            ESGMessageContent A04MsgContent = await esgMessageBody.FirstOrDefaultAsync(i => i.TransactionId == A04Transaction.Id);
            return A04MsgContent;
        }
    }
}
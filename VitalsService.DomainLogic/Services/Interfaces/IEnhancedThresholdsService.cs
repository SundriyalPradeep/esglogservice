﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VitalsService.Domain.DbEntities;
using VitalsService.Domain.Dtos;
using VitalsService.Domain.Enums;

namespace VitalsService.DomainLogic.Services.Interfaces
{
    /// <summary>
    /// IThresholdsService
    /// </summary>
    public interface IEnhancedThresholdsService
    {

        /// <summary>
        /// Creates new Enhanced Threshold.
        /// </summary>
        /// <param name="enthreshold">The Enhanced Threshold.</param>
        /// <returns>
        /// Created entity.
        /// </returns>
        Task<OperationResultDto<Guid, CreateUpdateEnhancedThresholdStatus>> CreateEnhancedThreshold(EnhancedThreshold enthreshold);
        /// <summary>
        /// Creates new Enhanced Threshold Condition.
        /// </summary>
        /// <param name="enthresholdCondition">The Enhanced Threshold Condition.</param>
        /// <returns>
        /// Created entity.
        /// </returns>
        Task<OperationResultDto<Guid, CreateUpdateEnhancedThresholdStatus>> CreateEnhancedThresholdCondition(EnhancedThresholdCondition enthresholdCondition);
        /// <summary>
        /// Creates new Enhanced Threshold Patient.
        /// </summary>
        /// <param name="enthresholdPatient">The Enhanced Threshold Patient.</param>
        /// <returns>
        /// Created entity.
        /// </returns>
        Task<OperationResultDto<Guid, CreateUpdateEnhancedThresholdStatus>> CreateEnhancedThresholdPatient(EnhancedThresholdPatient enthresholdPatient);

        /// <summary>
        /// Updates the Enhanced threshold.
        /// </summary>
        /// <param name="_Ethreshold">The threshold.</param>
        /// <returns></returns>
        Task<CreateUpdateEnhancedThresholdStatus> UpdateEnhancedThreshold(EnhancedThreshold _Ethreshold);


        /// <summary>
        /// Deletes the Enhanced threshold.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="enthresholdId">The threshold identifier.</param>
        /// <returns></returns>
        Task<GetDeleteEnhancedThresholdStatus> DeleteEnhancedThreshold(int customerId, Guid enthresholdId);

        /// <summary>
        /// Gets the Enhanced Threshold.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="enthresholdId">The enthreshold identifier.</param>
        /// <returns></returns>
        Task<EnhancedThreshold> GetEnhancedThreshold(int customerId, Guid enthresholdId);

        // <summary>
        /// Gets the Enhanced thresholds.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <param name="etType">The EnhancedThresholdType.</param>
        /// <returns></returns>
        Task<PagedResult<EnhancedThreshold>> GetEnhancedThresholds(int customerId, BaseSearchDto request, EnhancedThresholdType etType);

        // <summary>
        /// Gets the Active Enhanced thresholds.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <param name="etType">The EnhancedThresholdType.</param>
        /// <param name="patientId">The PatientId.</param>
        /// <returns></returns>
        Task<PagedResult<EnhancedThreshold>> GetActiveEnhancedThresholds(int customerId, BaseSearchDto request, EnhancedThresholdType etType,Guid patientId);

        /// <summary>
        /// Gets the Enhanced threshold Range.
        /// </summary>
        /// <param name="RangeType">The Range Type.</param>
        /// <returns></returns>
        Task<EnhancedThresholdRange> GetEnhancedThresholdRange(string RangeType);

        /// <summary>
        /// Gets the Enhanced threshold Set Analysis.
        /// </summary>
        /// <param name="SetAnalysisType">The SetAnalysis Type.</param>
        /// <returns></returns>
        Task<EnhancedThresholdSetAnalysis> GetEnhancedThresholdSetAnalysis(int SetAnalysisTypeId);

        /// <summary>
        /// Gets the Enhanced threshold Condition.
        /// </summary>
        /// <param name="etConditionId">The Enthreshold Condition.</param>
        /// <returns></returns>
        Task<EnhancedThresholdCondition> GetEnhancedThresholdCondition(Guid etConditionId);

        /// <summary>
        /// Deletes the Enhanced threshold Condition.
        /// </summary> 
        /// <param name="etConditionId">The etCondition identifier.</param>
        /// <returns></returns>
        Task<GetDeleteEnhancedThresholdStatus> DeleteEnhancedThresholdCondition(Guid etConditionId);

        /// <summary>
        /// Gets the Enhanced threshold list by Condition
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="conditionId"></param>
        /// <param name="patientId"></param>
        /// <returns></returns>
        Task<IList<EnhancedThreshold>> GetEThresholdListConditionId(int customerId, Guid conditionId,Guid patientId);

        /// <summary>
        /// Gets the Enhanced threshold Patient.
        /// </summary>
        /// <param name="PatientId">The Patient.</param>
        /// <returns></returns>
        Task<EnhancedThresholdPatient> GetEnhancedThresholdPatient(Guid PatientId);



        /// <summary>
        /// Gets the Enhanced threshold list by Patients
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <returns></returns>
        Task<IList<EnhancedThreshold>> GetEThresholdListPatientId(int customerId, Guid patientId);

        /// <summary>
        /// Deletes the Enhanced threshold Patient.
        /// </summary> 
        /// <param name="etPatientId">The etPatient identifier.</param>
        /// <returns></returns>
        Task<GetDeleteEnhancedThresholdStatus> DeleteEnhancedThresholdPatient(Guid etPatientId);

        /// <summary>
        /// Creates Enhanced threshold alerts
        /// </summary>
        /// <param name="enthresholdAlert"></param>
        /// <returns></returns>
        Task<OperationResultDto<Guid, CreateUpdateEnhancedThresholdStatus>> CreateEnhancedThresholdAlert(EnhancedThresholdAlerts enthresholdAlert);

        /// <summary>
        /// Gets the Enhanced threshold Alert.
        /// </summary>
        /// <param name="etAlertId">The EnThreshold Alert ID.</param>
        /// <returns></returns>
        Task<EnhancedThresholdAlerts> GetEnhancedThresholdAlert(Guid etAlertId);

        /// <summary>
        /// Gets the Enhanced threshold Alerts.
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        Task<PagedResult<EnhancedThresholdAlerts>> GetEnhancedThresholdAlerts(int customerId);

        /// <summary>
        /// Deletes the Enhanced threshold Alert.
        /// </summary> 
        /// <param name="etAlertId">The et Alert identifier.</param>
        /// <returns></returns>
        Task<GetDeleteEnhancedThresholdStatus> DeleteEnhancedThresholdAlert(Guid etAlertId);

        /// <summary>
        /// Acknowledges the ETalerts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="acknowledgedBy">The acknowledged by.</param>
        /// <param name="etAlertsIds">The ETalerts ids.</param>
        /// <returns></returns>
        Task<CreateUpdateAlertStatus> AcknowledgeEnThAlerts(int customerId, Guid acknowledgedBy, IList<Guid> etAlertsIds);

        /// <summary>
        /// UnAcknowledges the ET alerts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="unacknowledgedBy">The acknowledged by.</param>
        /// <param name="etAlertsIds">The ET alerts ids.</param>
        /// <returns></returns>
        Task<CreateUpdateAlertStatus> UnAcknowledgeEnThAlerts(int customerId, Guid unacknowledgedBy, IList<Guid> etAlertsIds);

        /// <summary>
        /// Ignore the ET alerts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="etAlertsIds">The ET alerts ids.</param>
        /// <returns></returns>
        Task<CreateUpdateAlertStatus> IgnoreEnhancedThresholdAlert(int customerId, IList<Guid> etAlertsIds);


        /// <summary>
        /// Activate/Deactivate Customer/Condition Level Enhanced threshold for any patient.
        /// </summary>
        /// <param name="customerId">The customerId.</param>
        /// <param name="PatientActivateETRequestDto">The Patient Activate ET RequestDto.</param>
        Task<ActivateDeactivateETStatus> UpdateETActiveState(int customerId, PatientActiveETs patientActivateETRequestDto);

        /// <summary>
        /// Gets the Enhanced threshold Alerts.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <returns></returns>
        Task<PagedResult<PatientActiveETs>> GetAllETsPatient(Guid patientId);
    }
}
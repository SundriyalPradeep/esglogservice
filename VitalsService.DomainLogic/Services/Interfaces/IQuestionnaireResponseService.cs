﻿using System;
using System.Threading.Tasks;
using VitalsService.Domain.DbEntities;
using VitalsService.Domain.Dtos;
using VitalsService.Domain.Enums.Questionnaires;

namespace VitalsService.DomainLogic.Services.Interfaces
{
    public interface IQuestionnaireResponseService
    {
        /// <summary>
        /// Add a questionnaire response to the database.
        /// </summary>
        /// <param name="questionnaireResp"></param>
        /// <returns></returns>
        Task<OperationResultDto<QuestionnaireResponse, QuestionnaireStatus>> CreateAsync(QuestionnaireResponse questionnaireResp);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <param name="questionnaireResponseId"></param>
        /// <param name="answerChoiceId"></param>
        /// <param name="answerChoiceText"></param>
        /// <param name="freeText"></param>
        /// <param name="exacerbationAnswerChoiceId"></param>
        /// <param name="exacerbationAnswerChoiceText"></param>
        /// <returns></returns>
        Task<UpdateQuestionnaireResponseStatus> RecordActionTakenAsync(int customerId, Guid patientId, Guid questionnaireResponseId, Guid answerChoiceId, string answerChoiceText, string freeText, Guid exacerbationAnswerChoiceId, string exacerbationAnswerChoiceText);

        /// <summary>
        /// Get a collection of questionnaire responses.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<PagedResult<QuestionnaireResponse>> GetQuestionnaireResponsesAsync(int customerId, Guid patientId, BaseSearchDto request);

        /// <summary>
        /// Get a single questionnaire response for the given patient.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <param name="questionnaireResponseId"></param>
        /// <returns></returns>
        Task<QuestionnaireResponse> GetQuestionnaireResponseAsync(int customerId, Guid patientId, Guid questionnaireResponseId);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VitalsService.Domain.DbEntities;

namespace VitalsService.DomainLogic.Services.Interfaces
{
    public interface IMessageLogService
    {
        Task<Guid> LogMessage(ESGMessageTransaction message);
        Task<Guid> LogMessageAck(ESGMessageAcknowledgement message);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageBody"></param>
        /// <returns></returns>
        Task<Guid> StoreMessageBody(ESGMessageContent messageBody);
        Task<Guid> GetTransactionId(string MessageControlid);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ICN"></param>
        /// <param name="DFN"></param>
        /// <param name="MessageType"></param>
        /// <returns></returns>
        Task<ESGMessageContent> GetMessageContent(string ICN, string DFN, string MessageType);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ICN"></param>
        /// <param name="SSN"></param>
        /// <param name="MessageType"></param>
        /// <returns></returns>
        Task<string> GetDFN(string ICN, string SSN, string MessageType);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ICN"></param>
        /// <returns></returns>
        Task<string> GetEnrollmentDate(string ICN);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="QBPMessageControlId"></param>
        /// <returns></returns>
        Task<ESGMessageContent> GetRegistrationMessage(string QBPMessageControlId);
    }
}
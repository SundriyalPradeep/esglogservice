﻿using AutoMapper;
using VitalsService.DomainLogic.Mappings;

namespace VitalsService.DomainLogic
{
    /// <summary>
    /// <para>NOTE: this only exists for the unit test projects. The Web app AutoMapper initialization takes care of
    /// registering all AutoMapper profiles it needs.</para>
    /// <para>Contains declaration of all mappings used in Domain Logic.</para>
    /// </summary>
    public static class DomainLogicAutomapperConfig
    {
        /// <summary>
        /// Initializes mappings with custom config.
        /// </summary>
        public static void RegisterRules()
        {
            Mapper.Initialize(config =>
            {
                config.AddProfile<PatientNotesMapping>();
                config.AddProfile<ThresholdMapping>(); 
                config.AddProfile<ConditionsMapping>();
            });
        }
    }
}
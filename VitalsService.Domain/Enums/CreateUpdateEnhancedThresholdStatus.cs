﻿using System;
using VitalsService.CustomAttributes;
using VitalsService.Domain.Resources;

namespace VitalsService.Domain.Enums
{
    /// <summary>
    /// CreateUpdateThresholdStatus.
    /// </summary>
    [Flags]
    public enum CreateUpdateEnhancedThresholdStatus
    {
        [DescriptionLocalized("EnhancedThresholdResult_Success", typeof(GlobalStrings))]
        Success = 1 << 1,

        [DescriptionLocalized("EnhancedThresholdResult_VitalThresholdAlreadyExists", typeof(GlobalStrings))]
        VitalThresholdAlreadyExists = 1 << 2,

        [DescriptionLocalized("EnhancedThresholdResult_VitalThresholdDoesNotExist", typeof(GlobalStrings))]
        VitalThresholdDoesNotExist = 1 << 3,

        [DescriptionLocalized("EnhancedThresholdResult_AlertSeverityDoesNotExist", typeof(GlobalStrings))]
        AlertSeverityDoesNotExist = 1 << 4,

        [DescriptionLocalized("EnhancedThresholdResult_ExistingAlertSeverityShouldBeUsed", typeof(GlobalStrings))]
        ExistingAlertSeverityShouldBeUsed = 1 << 5,

        [DescriptionLocalized("EnhancedThresholdResult_ExistingAlertSeverityShouldBeUsed", typeof(GlobalStrings))]
        RequestInvalid = 1 << 6
    }
}
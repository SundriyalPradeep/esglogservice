﻿namespace VitalsService.Domain.Enums
{
    /// <summary>
    /// EnhancedThresholdRangeType.
    /// </summary>
    public enum EnhancedThresholdRangeType
    {
        /// <summary>
        /// Measurements Range Type
        /// </summary>
        Measurements = 1,

        /// <summary>
        /// Minutes Range Type
        /// </summary>
        Minutes = 2,

        /// <summary>
        /// Days Range Type
        /// </summary>
        Days
    }
}
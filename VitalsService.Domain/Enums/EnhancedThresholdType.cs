﻿namespace VitalsService.Domain.Enums
{
    /// <summary>
    /// EnhancedThresholdType.
    /// </summary>
    public enum EnhancedThresholdType
    {
        /// <summary>
        /// All EThreshold Type
        /// </summary>
        All = 1,
        /// <summary>
        /// Organization EThreshold Type
        /// </summary>
        Organization,
        /// <summary>
        /// Condition EThreshold Type
        /// </summary>
        Condition,
        /// <summary>
        /// Patient EThreshold Type
        /// </summary>
        Patient

    }
}
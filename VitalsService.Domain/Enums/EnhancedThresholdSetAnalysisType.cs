﻿namespace VitalsService.Domain.Enums
{
    /// <summary>
    /// EnhancedThresholdSetAnalysisType.
    /// </summary>
    public enum EnhancedThresholdSetAnalysisType
    {
        /// <summary>
        /// Decrease Set Analysis Type
        /// </summary>
        the_decrease_in_values_is = 1,

        /// <summary>
        /// Increase Set Analysis Type
        /// </summary>
        the_increase_in_values_is,

        /// <summary>
        /// Count Set Analysis Type
        /// </summary>
        the_most_recent_value_is,

        /// <summary>
        /// Count Set Analysis Type
        /// </summary>
        two_or_more_of_the_values_are,

        /// <summary>
        /// Count Set Analysis Type
        /// </summary>
        three_or_more_of_the_values_are,

        /// <summary>
        /// Percentage Set Analysis Type
        /// </summary>
        all_of_the_values_are

    }
}
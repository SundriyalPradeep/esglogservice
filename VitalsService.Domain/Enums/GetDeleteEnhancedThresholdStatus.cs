﻿using VitalsService.CustomAttributes;
using VitalsService.Domain.Resources;

namespace VitalsService.Domain.Enums
{
    /// <summary>
    /// GetDeleteEnhancedThresholdStatus.
    /// </summary>
    public enum GetDeleteEnhancedThresholdStatus
    {
        [DescriptionLocalized("ThresholdResult_Success", typeof(GlobalStrings))]
        Success = 1,

        [DescriptionLocalized("ThresholdResult_ThresholdWithSuchIdDoesNotExist", typeof(GlobalStrings))]
        NotFound = 2,

        [DescriptionLocalized("ThresholdResult_UnableToDeleteET", typeof(GlobalStrings))]
        UnableToDelete = 3
    }
}
﻿namespace VitalsService.Domain.Enums
{
    /// <summary>
    /// These ids are based on information provided by Andrew Sawyers on the Home Sensing team. Add new ones here
    /// as new notification types need to be supported.
    /// </summary>
    public enum HomeSensingNotificationType
    {
        /// <summary>
        /// Count of &quot;interruptions&quot; during bedtime averaged over 7 day period. When average goes above threshold, notify.
        /// </summary>
        SleepDisturbanceHigherThanAverage = 3,

        /// <summary>
        /// Amount of activity averaged over rolling 7 day period. When average goes below threshold &gt; 2 days in a row, notify
        /// </summary>
        ActivityLevelLowerThanAverage = 2,

        /// <summary>
        /// Nighttime bathroom visits averaged over rolling 7 day period. When average violates threshold &gt; 2 days in a row, notify.
        /// </summary>
        NighttimeBathroomVisitsLowerThanAverage = 11,

        /// <summary>
        /// Nighttime bathroom visits averaged over rolling 7 day period. When average violates threshold &gt; 2 days in a row, notify.
        /// </summary>
        NighttimeBathroomVisitsHigherThanAverage = 12,

        /// <summary>
        /// Nighttime bathroom visits averaged over rolling 7 day period. When average violates threshold &gt; 2 days in a row, notify.
        /// </summary>
        NighttimeBathroomVisitsNone = 13,
    }
}

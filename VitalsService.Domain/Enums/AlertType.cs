﻿namespace VitalsService.Domain.Enums
{
    /// <summary>
    /// AlertType.
    /// </summary>
    public enum AlertType
    {
        Adherence = 1,

        VitalsViolation = 2,

        ResponseViolation = 3,

        Insight = 4,

        HomeSensing = 5
    }
}
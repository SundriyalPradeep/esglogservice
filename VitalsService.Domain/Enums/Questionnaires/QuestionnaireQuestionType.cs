﻿namespace VitalsService.Domain.Enums.Questionnaires
{
    public enum QuestionnaireQuestionType
    {
        Unknown = 0,

        AlertConfirmation = 1,

        General = 2,

        PreStratification = 3,

        EscalationAction = 4
    }
}

﻿using System;
using VitalsService.CustomAttributes;
using VitalsService.Domain.Resources;

namespace VitalsService.Domain.Enums.Questionnaires
{
    [Flags]
    public enum GetQuestionnaireReponseStatus
    {
        Success = 1,

        [DescriptionLocalized(nameof(GlobalStrings.QuestionnaireResponse_NotFound), typeof(GlobalStrings))]
        QuestionnaireResponseNotFound = 2
    }
}

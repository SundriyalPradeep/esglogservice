﻿using System;

namespace VitalsService.Domain.Enums.Questionnaires
{
    [Flags]
    public enum QuestionnaireStatus
    {
        Success = 1 << 1
    }
}

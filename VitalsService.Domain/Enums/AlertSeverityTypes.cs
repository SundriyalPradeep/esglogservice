﻿using System.ComponentModel;

namespace VitalsService.Domain.Enums
{
    /// <summary>
    /// AlertType.
    /// </summary>
    public enum AlertSeverityTypes
    {
        [Description("#0BA713")]
        Green = 1,
        [Description("#FFA500")]
        Yellow = 2,
        [Description("#B00707")]
        Red = 3
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VitalsService.CustomAttributes;
using VitalsService.Domain.Resources;

namespace VitalsService.Domain.Enums
{
    /// <summary>
    /// CreateUpdateAlertStatus.
    /// </summary>
    [Flags]
    public enum ActivateDeactivateETStatus
    {
        [DescriptionLocalized("ActivateResult_Success", typeof(GlobalStrings))]
        Success = 1 << 1,
    }
}

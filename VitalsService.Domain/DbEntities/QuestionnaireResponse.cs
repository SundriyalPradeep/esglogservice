﻿using System;
using System.Collections.Generic;

namespace VitalsService.Domain.DbEntities
{
    public class QuestionnaireResponse : Entity, IAuditable
    {
        /// <summary>
        /// The customer identifier.
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Which questionnaire the user took.
        /// </summary>
        public Guid QuestionnaireId { get; set; }

        /// <summary>
        /// The name of the questionnaire the user took. This exists both to capture the name of the queestionnaire
        /// at the time it was administered, and to make it easier on the caller to display it at a later time. Otherwise,
        /// they'd have to do lookups to the health service to get the display text.
        /// </summary>
        public string QuestionnaireName { get; set; }

        /// <summary>
        /// The subject patient.
        /// </summary>
        public Guid PatientId { get; set; }

        /// <summary>
        /// The clinician who administered the questionnaire.
        /// </summary>
        public Guid ClinicianId { get; set; }

        /// <summary>
        /// The total pre-stratification score, computed by summing the scores of each pre-stratification question.
        /// </summary>
        public int TotalPreStratificationRiskScore { get; set; }

        /// <summary>
        /// Questions and their answers. This excludes the &quot;action taken&quot; answer, which is stored in separate fields.
        /// </summary>
        public List<QuestionnaireQuestionAnswer> QuestionAnswers { get; private set; } = new List<QuestionnaireQuestionAnswer>();


        //
        // Action Taken. We store this here for ease of retrieving recent questionnaire responses. The alternative is to 
        //   both get this questionnaire record, and then search its answers collection for an action taken response. What
        //   happens if the client were to accidently submit two?
        // 
        // This way, we know there is at most one.
        //

        public Guid? ActionTakenAnswerChoiceId { get; set; }
        public string ActionTakenAnswerChoiceText { get; set; }
        public string ActionTakenFreeText { get; set; }


        //
        // Exacerbation. We return the calculated pre-stratification score to the caller, who then uses that score to select
        //   the appropriate exacerbation level (Mild, Severe, or Very Severe). It doesn't make sense for the vitals service
        //   to make the selection because it knows nothing about the Health Library, which is where the questionnaires are
        //   defined. The Vitals Service is just a dumb data store.
        //

        public Guid? ExacerbationAnswerChoiceId { get; set; }
        public string ExacerbationAnswerChoiceText { get; set; }


        public DateTime CreatedUtc { get; set; }

        public DateTime UpdatedUtc { get; set; }

        public override string ToString() => $"(Score: {TotalPreStratificationRiskScore})";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VitalsService.Domain.DbEntities
{
    public class NotesMeasurement : Entity
    {
        
        /// <summary>
        /// Gets or sets the measurement identifier.
        /// </summary>
        /// <value>
        /// The measurement identifier.
        /// </value>
        public Guid MeasurementId { get; set; }


        ///// <summary>
        ///// For tracking measurement notes.
        ///// </summary>
        //public virtual Measurement Measurement { get; set; }


        /// <summary>
        /// Gets or sets the note identifier.
        /// </summary>
        /// <value>
        /// The note identifier.
        /// </value>
        public Guid NoteId { get; set; }

        /// <summary>
        /// Gets or sets the note.
        /// </summary>
        /// <value>
        /// The note.
        /// </value>
        public virtual Note Note { get; set; }

       

    }
}

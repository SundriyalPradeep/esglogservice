﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VitalsService.Domain.Enums;

namespace VitalsService.Domain.DbEntities
{
    public class ESGMessageContent : Entity
    {

        public string MessageBody { get; set; }
        public int MessageType { get; set; }

        public Guid TransactionId { get; set; }
        public ESGMessageTransaction ESGMessageTransaction  { get; set; }

       // public virtual ICollection<ESGMessageTransaction> ESGMessageTransaction { get; set; }
        //public Guid Id { get; set; }
        //public int ESGMessageTransactionId { get; set; }
        //public virtual ESGMessageTransaction ESGMessageTransaction { get; set; }
        //public string MessageBody { get; set; }
    }
}

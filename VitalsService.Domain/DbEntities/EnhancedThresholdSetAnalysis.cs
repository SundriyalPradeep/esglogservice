﻿using System;
using System.Collections.Generic;

namespace VitalsService.Domain.DbEntities
{
    /// <summary>
    /// Threshold.
    /// </summary>
    public class EnhancedThresholdSetAnalysis : Entity
    {
        ///// <summary>
        ///// Gets or sets the Id.
        ///// </summary>
        ///// <value>
        ///// The Id.
        ///// </value>
        //public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the SetAnalysisCount.
        /// </summary>
        /// <value>
        /// The SetAnalysisCount.
        /// </value>
        public int SetAnalysisCount { get; set; }

        /// <summary>
        /// Gets or sets the Percentage.
        /// </summary>
        /// <value>
        /// The Percentage.
        /// </value>
        public int Percentage { get; set; }

        /// <summary>
        /// Gets or sets the SetAnalysisText.
        /// </summary>
        /// <value>
        /// The SetAnalysisText.
        /// </value>
        public string SetAnalysisText { get; set; }

        /// <summary>
        /// Gets or sets the SetAnalysisFunctionType.
        /// </summary>
        /// <value>
        /// The SetAnalysisFunctionType.
        /// </value>
        public string SetAnalysisFunctionType { get; set; }

        /// <summary>
        /// Gets or sets the Weight
        /// </summary>
        /// <value>
        /// The Weight
        /// </value>
        public int Weight { get; set; }


    }
}
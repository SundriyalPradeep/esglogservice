﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VitalsService.Domain.DbEntities
{
    public class ESGMessageAcknowledgement : Entity
    {

        public string AckType { get; set; }
        public string AckSender { get; set; }
        public string AckReceiver { get; set; }
        public string MessageDate { get; set; }
        public DateTime? CreatedOn { get; set; }
       // public Guid MessageBodyId { get; set; }
        public Guid ESGMessageTransactionId { get; set; }
        public  ESGMessageTransaction ESGMessageTransaction { get; set; }
        
        //public virtual ESGMessageTransaction ESGMessageTransaction { get; set; }
        //public Guid Id { get; set; }
        //public Guid ESGMessageContentId { get; set; }
        //public virtual ESGMessageContent ESGMessageContent { get; set; }
        //public string AckType { get; set; }
        //public string AckSender { get; set; }
        //public string AckReceiver { get; set; }
        //public DateTime? MessageDate { get; set; }
        //public DateTime? InsertedOn { get; set; }
        //public string MessageBody { get; set; }
    }
}

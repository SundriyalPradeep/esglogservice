﻿using System;
using System.Collections.Generic;

namespace VitalsService.Domain.DbEntities
{
    /// <summary>
    /// Threshold.
    /// </summary>
    public class EnhancedThresholdRange : Entity
    {
        ///// <summary>
        ///// Gets or sets the Id.
        ///// </summary>
        ///// <value>
        ///// The Id.
        ///// </value>
        //public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the Range.
        /// </summary>
        /// <value>
        /// The Range.
        /// </value>
        public string RangeText { get; set; }

        /// <summary>
        /// Gets or sets the Range.
        /// </summary>
        /// <value>
        /// The Range.
        /// </value>
        public string MaxRangeCount { get; set; }


    }
}
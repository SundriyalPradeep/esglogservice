﻿using System;
using System.Collections.Generic;

namespace VitalsService.Domain.DbEntities
{
    /// <summary>
    /// EnhancedThresholdAlerts.
    /// </summary>
    public class EnhancedThresholdAlerts : Entity
    {
        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the Enhanced Threshold ID
        /// </summary>
        /// <value>
        /// The Enhanced Threshold ID.
        /// </value>
        public Guid EnhancedThresholdId { get; set; }

        /// <summary>
        /// Gets or sets the EnhancedThreshold.
        /// </summary>
        /// <value>
        /// The EnhancedThreshold.
        /// </value>
        public virtual EnhancedThreshold EnhancedThreshold { get; set; }

        /// <summary>
        /// Gets or sets the Patient ID
        /// </summary>
        /// <value>
        /// The Patient ID.
        /// </value>
        public Guid PatientId { get; set; }

        /// <summary>
        /// Gets or sets the Enhanced thresholds status.
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="EnhancedThresholdAlerts"/> is acknowledged.
        /// </summary>
        /// <value>
        ///   <c>true</c> if acknowledged; otherwise, <c>false</c>.
        /// </value>
        public bool Acknowledged { get; set; }

        /// <summary>
        /// Gets or sets the acknowledged UTC.
        /// </summary>
        /// <value>
        /// The acknowledged UTC.
        /// </value>
        public DateTime? AcknowledgedUtc { get; set; }

        /// <summary>
        /// Gets or sets the acknowledged by.
        /// </summary>
        /// <value>
        /// The acknowledged by.
        /// </value>
        public Guid? AcknowledgedBy { get; set; }

        /// <summary>
        /// Gets or sets the Alert Title.
        /// </summary>
        public string AlertTitle { get; set; }

        /// <summary>
        /// Gets or sets the MeasurementIds.
        /// </summary>
        public string MeasurementIds { get; set; }

        /// <summary>
        /// Gets or sets the Updated date.
        /// </summary>
        public Guid ViolatedMeasurementId { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        public DateTime CreatedDateUTC { get; set; }

        /// <summary>
        /// Gets or sets the Updated date.
        /// </summary>
        public DateTime? UpdatedDateUTC { get; set; }

        /// <summary>
        /// Gets or sets the etDescription.
        /// </summary>
        public string ETDescription { get; set; }

        /// <summary>
        /// Gets or sets the alert severity identifier
        /// </summary>
        /// <value>
        /// The alert severity identifier
        /// </value>
        public Guid? AlertSeverityId { get; set; }

        /// <summary>
        /// Gets or sets the alert severity
        /// </summary>
        /// <value>
        /// The alert severity
        /// </value>
        public virtual AlertSeverity AlertSeverity { get; set; }
    }
}
﻿using System;
using VitalsService.Domain.Enums;

namespace VitalsService.Domain.DbEntities
{
    /// <summary>
    /// Notifications received from the Home Sensing application.
    /// </summary>
    public class HomeSensingAlert : Alert
    {
        /// <summary>
        /// The type of Home Sensing notification. It maps to internal ids, which should never change. Stored as ints 
        /// so that we can also accept new, unknown (to Maestro) Home Sensing notifications before we've had a chance
        /// to update Maestro.
        /// </summary>
        public HomeSensingNotificationType NotificationType { get; set; }

        /// <summary>
        /// The limiting value, above or below which the observed value violates a threshold and triggers a notification.
        /// </summary>
        public double Threshold { get; set; }

        /// <summary>
        /// The actual observed value that violated a threshold and triggered a notification.
        /// </summary>
        public double Observed { get; set; }

        /// <summary>
        /// Optional. If specified, points to a page in Home Sensing where a Maestro user can view more information.
        /// </summary>
        public string MoreInfoUrl { get; set; }

        /// <summary>
        /// <para>On the off chance that Home Sensing sends us properties that we don't expect in the Request JSON (e.g., 
        /// it's a new type of notification that sends new information), we want to capture those property names and
        /// values, and store them so that later on we can add columns for them and populate them with the correct 
        /// data.</para>
        /// <para>This will be a simple JSON object with property names and values, or null if all properties in the request were known by Maestro.</para>
        /// <para>Not likely, but we don't want to lose data.</para>
        /// </summary>
        public string UnknownPropertiesJson { get; set; }

        /// <summary>
        /// Set to true if the user wants to ignore this home sensing alert.
        /// </summary>
        public bool IsIgnored { get; set; }

        /// <summary>
        /// The Maestro user who ignored the alert.
        /// </summary>
        public Guid? IgnoredBy { get; set; }

        /// <summary>
        /// When the Maestro user ignored the alert.
        /// </summary>
        public DateTime? IgnoredUtc { get; set; }

        /// <summary>
        /// For debugging.
        /// </summary>
        /// <returns></returns>
        public override string ToString() => $"{NotificationType} on {OccurredUtc}: {Title}";
    }
}

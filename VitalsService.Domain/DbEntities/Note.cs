﻿using System;
using System.Collections.Generic;

namespace VitalsService.Domain.DbEntities
{
    /// <summary>
    /// Note.
    /// </summary>
    public class Note : Entity, IAuditable
    {
       
        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the patient identifier.
        /// </summary>
        public Guid PatientId { get; set; }

        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the created UTC.
        /// </summary>
        public DateTime CreatedUtc { get; set; }

        /// <summary>
        /// Gets or sets the updated UTC.
        /// </summary>
        /// <value>
        /// The updated UTC.
        /// </value>
        public DateTime UpdatedUtc { get; set; }
        
        /// <summary>
        /// Gets or sets the vital.
        /// </summary>
        /// <value>
        /// The vital.
        /// </value>
        public virtual ICollection<NotesMeasurement> NotesMeasurements { get; set; }

        /// <summary>
        /// Gets or sets the health session element identifier.
        /// </summary>
        public Guid? HealthSessionElementId { get; set; }

        /// <summary>
        /// Gets or sets the health session element.
        /// </summary>
        public virtual HealthSessionElement HealthSessionElement { get; set; }

      
        /// <summary>
        /// For tracking alert notes.
        /// </summary>
        public virtual ICollection<NotesAlert> NotesAlerts { get; set; }

        /// <summary>
        /// Gets or sets the notables.
        /// </summary>
        public virtual ICollection<NoteNotable> Notables { get; set; }


    }
}
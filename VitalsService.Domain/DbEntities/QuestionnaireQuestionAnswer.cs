﻿using System;
using VitalsService.Domain.Enums.Questionnaires;

namespace VitalsService.Domain.DbEntities
{
    public class QuestionnaireQuestionAnswer : Entity, IAuditable
    {
        public Guid QuestionnaireResponseId { get; set; }
        public QuestionnaireResponse QuestionnaireResponse { get; set; }

        /// <summary>
        /// The id of the question as defined in the health library.
        /// </summary>
        public Guid QuestionId { get; set; }

        /// <summary>
        /// Record the question text as displayed to the clinician at the time the survey was taken.
        /// </summary>
        public string QuestionText { get; set; }

        /// <summary>
        /// The type of question.
        /// </summary>
        public QuestionnaireQuestionType QuestionType { get; set; }

        /// <summary>
        /// The type of answer recorded.
        /// </summary>
        public QuestionnaireAnswerType AnswerType { get; set; }

        /// <summary>
        /// If it's a single-select, this will contain the answer choice id. Otherwise, null.
        /// </summary>
        public Guid? AnswerChoiceId { get; set; }

        /// <summary>
        /// If it's a single-select, record the answer choice text as displayed to the clinician at the time
        /// the survey was taken.
        /// </summary>
        public string AnswerChoiceText { get; set; }

        /// <summary>
        /// If it's a measurement, this will contain the type of measurement. Otherwise, null.
        /// </summary>
        public QuestionnaireMeasurementType? MeasurementType { get; set; }

        /// <summary>
        /// If it's a measurement, this will contain the measurement value. Otherwise, null.
        /// </summary>
        public decimal? MeasurementValue { get; set; }

        /// <summary>
        /// <para>If this is an answer to a pre-stratification single-select question, this will contain the score of the
        /// of the selected answer choice.</para>
        /// <para>If this is an answer to a pre-stratification measurement question, this will be calculated based on the 
        /// raw measurement value, and possibly based on comparison with previous values of the same measurement type.</para>
        /// <para>If this is not an answer to a pre-stratification question, this value is null.</para>
        /// </summary>
        public int? Score { get; set; }

        public DateTime CreatedUtc { get; set; }

        public DateTime UpdatedUtc { get; set; }

        public override string ToString() => $"{QuestionType}: Q: {QuestionText}; A: {AnswerChoiceText ?? MeasurementValue?.ToString()}; Score: {Score}";
    }
}

﻿using System;
using System.Collections.Generic;

namespace VitalsService.Domain.DbEntities
{
    /// <summary>
    /// Threshold.
    /// </summary>
    public class EnhancedThresholdPatient : Entity
    {
        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the Patient ID
        /// </summary>
        /// <value>
        /// The Patient ID.
        /// </value>
        public Guid PatientId { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        public DateTime CreatedDateUTC { get; set; }

        /// <summary>
        /// Gets or sets the Updated date.
        /// </summary>
        public DateTime? UpdatedDateUTC { get; set; }
    }
}
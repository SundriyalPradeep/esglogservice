﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VitalsService.Domain.DbEntities
{
    public class ESGMessageTransaction : Entity
    {

        public string MessageControlId { get; set; }
        public string MessageEvent { get; set; }
        public string MessageType { get; set; }
        public string ICN { get; set; }
        public string DFN { get; set; }
        public string SSN { get; set; }
        public string SendingFacilityID { get; set; }
        public string SendingFacility { get; set; }
        public string ReceivingFacility { get; set; }
        public string MessageDate { get; set; }
        public string EnrollmentDate { get; set; }
        public string DisEnrollmentDate { get; set; }
        public DateTime? InsertedOn { get; set; }
        public Guid RegistrationId { get; set; }
        //public virtual ICollection<ESGMessageContent> ESGMessageContent { get; set; }
        public virtual ICollection<ESGMessageAcknowledgement> ESGMessageAcknowledgement { get; set; }

        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        // public Guid Id { get; set; }

        //public int Id { get; set; }
        //public string MessageControlId { get; set; }
        //public string MessageEvent { get; set; }
        //public string MessageType { get; set; }
        //public string ICN { get; set; }
        //public string DFN { get; set; }
        //public string SendingFacility { get; set; }
        //public string RaceivingFacility { get; set; }
        //public DateTime? MessageDate { get; set; }
        //public DateTime? InsertedOn { get; set; }
        // public string MessageBody { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using VitalsService.Domain.Enums;

namespace VitalsService.Domain.DbEntities
{
    /// <summary>
    /// EnhancedThreshold.
    /// </summary>
    public class EnhancedThreshold : Entity
    {
        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public int CustomerId { get; set; }
                
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Vital name.
        /// </summary>
        /// <value>
        /// The Vital name.
        /// </value>
        public string VitalName { get; set; }

        /// <summary>
        /// Gets or sets the EnhancedThresholdType .
        /// </summary>
        /// <value>
        /// The EnhancedThresholdType.
        /// </value>
        public EnhancedThresholdType Type { get; set; }

        /// <summary>
        /// Gets or sets the SetAnalysis ID
        /// </summary>
        /// <value>
        /// The SetAnalysis ID.
        /// </value>
        public virtual EnhancedThresholdSetAnalysis EThresholdSetAnalysis { get; set; }

        /// <summary>
        /// Gets or sets the EThresholdRange ID
        /// </summary>
        /// <value>
        /// The EThresholdRange ID.
        /// </value>
        public virtual EnhancedThresholdRange EThresholdRanges { get; set; }

        /// <summary>
        /// Gets or sets the unit.
        /// </summary>
        /// <value>
        /// The unit.
        /// </value>
        public string Unit { get; set; }

        /// <summary>
        /// Gets or sets the EThresholdRange ID
        /// </summary>
        /// <value>
        /// The EThresholdRange ID.
        /// </value>
        public int TimeOrValuesCount { get; set; }

        /// <summary>
        /// Gets or sets the EThresholdRange ID
        /// </summary>
        /// <value>
        /// The EThresholdRange ID.
        /// </value>
        public string ComparisonOperator { get; set; }

        /// <summary>
        /// Gets or sets the EThresholdRange ID
        /// </summary>
        /// <value>
        /// The EThresholdRange ID.
        /// </value>
        public Decimal ComparisonValue { get; set; }

        /// <summary>
        /// Gets or sets the thresholds status.
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets the Active status.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        public DateTime CreatedDateUTC { get; set; }

        /// <summary>
        /// Gets or sets the Updated date.
        /// </summary>
        public DateTime? UpdatedDateUTC { get; set; }

        /// <summary>
        /// Gets or sets the EnhancedThreshold Conditions.
        /// </summary>
        /// <value>
        /// The EnhancedThreshold Conditions.
        /// </value>
        public virtual EnhancedThresholdCondition ETConditions { get; set; }

        /// <summary>
        /// Gets or sets the EnhancedThreshold Conditions.
        /// </summary>
        /// <value>
        /// The EnhancedThreshold Conditions.
        /// </value>
        public virtual EnhancedThresholdPatient ETPatients { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        //public DateTime ArchievedDateUTC { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;

namespace VitalsService.Domain.DbEntities
{
    /// <summary>
    /// Threshold.
    /// </summary>
    public class EnhancedThresholdCondition : Entity
    {
        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public int CustomerId { get; set; }  

        /// <summary>
        /// Gets or sets the ConditionId.
        /// </summary>
        /// <value>
        /// The ConditionId.
        /// </value>
        public Guid ConditionId { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        public DateTime CreatedDateUTC { get; set; }

        /// <summary>
        /// Gets or sets the Updated date.
        /// </summary>
        public DateTime? UpdatedDateUTC { get; set; }

    }
}
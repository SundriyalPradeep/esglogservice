﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Isg.EntityFramework;
using VitalsService.DataAccess.EF.Helpers;
using VitalsService.Domain.DbEntities;
using System;

namespace VitalsService.DataAccess.EF.Contexts
{
    /// <summary>
    /// VitalsDataContext.
    /// </summary>
    public class VitalsDataContext : DbContextBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VitalsDataContext"/> class.
        /// </summary>
        public VitalsDataContext()
        {
        }

        public VitalsDataContext(string connectionString)
            : base(connectionString)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="VitalsDataContext"/> class.
        /// </summary>
        /// <param name="customerContext">The customer context.</param>
        public VitalsDataContext(ICustomerContext customerContext)
            : base(ConnectionStringsHelper.GetConnectionString(customerContext.CustomerId))
        {
        }

        #region DbSets

        public DbSet<MeasurementNote> MeasurementNotes { get; set; }

        public DbSet<Vital> Vitals { get; set; }

        public DbSet<Measurement> Measurements { get; set; }

        public DbSet<Device> Devices { get; set; }

        public DbSet<HealthSession> HealthSessions { get; set; }

        public DbSet<HealthSessionElement> HealthSessionElements { get; set; }

        public DbSet<HealthSessionElementValue> HealthSessionElementValues { get; set; }

        public DbSet<Threshold> Thresholds { get; set; }

        public DbSet<Alert> Alerts { get; set; }

        public DbSet<HealthSessionElementAlert> HealthSessionElementAlerts { get; set; }

        public DbSet<HomeSensingAlert> HomeSensingAlerts { get; set; }

        public DbSet<VitalAlert> VitalAlerts { get; set; }

        public DbSet<AlertSeverity> AlertSeverities { get; set; }

        public DbSet<AssessmentMedia> AssessmentMedias { get; set; }

        public DbSet<Condition> Conditions { get; set; }

        public DbSet<Tag> Tags { get; set; }

        public DbSet<EnhancedThreshold> EnhancedThresholds { get; set; }

        public DbSet<EnhancedThresholdCondition> EnhancedThresholdConditions { get; set; }

        public DbSet<EnhancedThresholdPatient> EnhancedThresholdPatients { get; set; }

        public DbSet<EnhancedThresholdRange> EnhancedThresholdRanges { get; set; }

        public DbSet<EnhancedThresholdSetAnalysis> EnhancedThresholdSetAnalysis { get; set; }

        public DbSet<EnhancedThresholdAlerts> EnhancedThresholdAlerts { get; set; }
		
        public DbSet<PatientThresholdType> PatientThresholdTypes { get; set; }

        public DbSet<PatientActiveETs> PatientActiveETs { get; set; }
		
        public DbSet<QuestionnaireResponse> QuestionnaireResponses { get; set; }
        public DbSet<QuestionnaireQuestionAnswer> QuestionnaireQuestionAnswers { get; set; }
        public DbSet<ESGMessageTransaction> ESGMessageTransaction { get; set; }
        public DbSet<ESGMessageContent> ESGMessageContent { get; set; }
        public DbSet<ESGMessageAcknowledgement> ESGMessageAcknowledgement { get; set; }



        #endregion

        /// <summary>
        /// This method is called when the model for a derived context has been initialized, but
        /// before the model has been locked down and used to initialize the context.  The default
        /// implementation of this method does nothing, but it can be overridden in a derived class
        /// such that the model can be further configured before it is locked down.
        /// </summary>
        /// <param name="modelBuilder">The builder that defines the model for the context being created.</param>
        /// <remarks>
        /// Typically, this method is called only once when the first instance of a derived context
        /// is created.  The model for that context is then cached and is for all further instances of
        /// the context in the app domain.  This caching can be disabled by setting the ModelCaching
        /// property on the given ModelBuidler, but note that this can seriously degrade performance.
        /// More control over caching is provided through use of the DbModelBuilder and DbContextFactory
        /// classes directly.
        /// </remarks>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetExecutingAssembly());

     //       Guid NewGuid = Guid.NewGuid();
     //       modelBuilder.Entity<ESGMessageTransaction>()
     //.Map(map =>
     //{
     //    map.Properties(p => new
     //    {
     //        p.Id,
     //        p.MessageControlId,
     //        p.MessageEvent,
     //        p.MessageType,
     //        p.ICN,
     //        p.DFN,
     //        p.SendingFacility,
     //        p.RaceivingFacility,
     //        p.MessageDate,
     //        p.InsertedOn
     //    });
     //    map.ToTable("ESGMessageTransaction");
     //})
     //// Map to the Users table  
     //.Map(map =>
     //{
     //    map.Properties(p => new
     //    {
             
     //        p.Id,
     //        p.MessageBody
     //    });
     //    map.ToTable("ESGMessageContent");
     //});

            //2

     //       modelBuilder.Entity<ESGMessageAcknowledgement>()
     //.Map(map =>
     //{
     //    map.Properties(p => new
     //    {
     //        p.Id,
     //       // p.ESGMessageContentId,
     //        p.AckType,
     //        p.AckSender,
     //        p.AckReceiver,
     //        p.MessageDate,
     //        p.InsertedOn
             
     //    });
     //    map.ToTable("ESGMessageAcknowledgement");
     //})
     //// Map to the Users table  
     //.Map(map =>
     //{
     //    map.Properties(p => new
     //    {
     //        p.Id,
     //        p.ESGMessageContentId,
     //        p.MessageBody
     //    });
     //    map.ToTable("ESGMessageContents1");
     //});

#if DEBUG
            LogGeneratedSql();
#endif
            base.OnModelCreating(modelBuilder);
        }


        /// <summary>
        /// The stock db entity validation exception is useless. Here, we add more context.
        /// </summary>
        /// <returns></returns>
        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException devEx)
            {
                throw CreateUsefulDbEntityValidationException(devEx);
            }
        }

        /// <summary>
        /// Note: we don't need to override the regular SaveChangesAsync method because all it does
        /// is call this one.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            try
            {
                return base.SaveChangesAsync(cancellationToken);
            }
            catch (DbEntityValidationException devEx)
            {
                throw CreateUsefulDbEntityValidationException(devEx);
            }
        }

        private void LogGeneratedSql()
        {
            Database.Log = log => Debug.Write(log);
        }

        /// <summary>
        /// Makes any DbEntityValidationExceptions useful. The stock error message is useless. 
        /// See http://stackoverflow.com/a/15820506/731.
        /// </summary>
        /// <param name="originalEx"></param>
        /// <returns></returns>
        private DbEntityValidationException CreateUsefulDbEntityValidationException(DbEntityValidationException originalEx)
        {
            // Retrieve the error messages as a list of strings.
            var errorMessages = new List<string>();

            // Get each validation error message, along with the type and property names.
            foreach (var dbeValRes in originalEx.EntityValidationErrors)
            {
                var entityName = dbeValRes.Entry.Entity.GetType().Name;
                foreach (var error in dbeValRes.ValidationErrors)
                {
                    errorMessages.Add($"{entityName}.{error.PropertyName}: {error.ErrorMessage}");
                }
            }

            // Join the list of error messages to create a single error string.
            var fullErrorMessage = string.Join("; ", errorMessages);

            // Use the original exception's message as part of the new exception's message.
            //  NOTE: the original message ends with a period, hence no period between it and the new message.
            var errorMessage = $"{originalEx.Message} The validation errors are: {fullErrorMessage}";

            return new DbEntityValidationException(errorMessage, originalEx.EntityValidationErrors, originalEx);
        }
    }
}

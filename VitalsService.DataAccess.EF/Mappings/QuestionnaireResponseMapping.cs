﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using VitalsService.Domain.DbEntities;

namespace VitalsService.DataAccess.EF.Mappings
{
    public class QuestionnaireResponseMapping : EntityTypeConfiguration<QuestionnaireResponse>
    {
        public QuestionnaireResponseMapping()
        {
            // Primary key
            HasKey(e => e.Id);
            Property(e => e.Id)
                .HasColumnName("Id")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(e => e.ActionTakenAnswerChoiceText)
                .HasMaxLength(1024);

            Property(e => e.ActionTakenFreeText)
                .HasColumnType("nvarchar(max)");

            Property(e => e.ExacerbationAnswerChoiceText)
                .HasMaxLength(1024);

            Property(e => e.QuestionnaireName)
                .IsRequired()
                .HasMaxLength(100);
        }
    }
}

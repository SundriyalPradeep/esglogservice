﻿using System.Data.Entity.ModelConfiguration;
using VitalsService.Domain.Constants;
using VitalsService.Domain.DbEntities;

namespace VitalsService.DataAccess.EF.Mappings
{
    internal class HomeSensingAlertMapping : EntityTypeConfiguration<HomeSensingAlert>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AlertMapping" /> class.
        /// </summary>
        public HomeSensingAlertMapping()
        {
            // Table name
            this.ToTable("HomeSensingAlerts");

            this.Property(hsa => hsa.MoreInfoUrl)
                .IsOptional()
                .HasMaxLength(DbConstraints.MaxLength.HomeSensingAlertMoreInfoUrl);

            this.Property(hsa => hsa.UnknownPropertiesJson)
                .IsOptional()
                .HasMaxLength(DbConstraints.MaxLength.HomeSensingAlertUnknownJsonProperties);
        }
    }
}

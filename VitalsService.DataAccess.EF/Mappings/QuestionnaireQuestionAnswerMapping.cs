﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using VitalsService.Domain.DbEntities;

namespace VitalsService.DataAccess.EF.Mappings
{
    public class QuestionnaireQuestionAnswerMapping : EntityTypeConfiguration<QuestionnaireQuestionAnswer>
    {
        public QuestionnaireQuestionAnswerMapping()
        {
            // Primary key
            HasKey(e => e.Id);
            Property(e => e.Id)
                .HasColumnName("Id")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(e => e.QuestionText)
                .HasMaxLength(1024);

            Property(e => e.AnswerChoiceText)
                .HasMaxLength(1024);

            //
            // Relationships
            //

            // One-to-Many: QuestionnaireResponse-to-QuestionnaireQuestionAnswers
            HasRequired(m => m.QuestionnaireResponse)
                .WithMany(qr => qr.QuestionAnswers)
                .WillCascadeOnDelete(false);
        }
    }
}

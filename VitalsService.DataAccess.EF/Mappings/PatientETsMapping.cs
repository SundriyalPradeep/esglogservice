﻿using System.Data.Entity.ModelConfiguration;
using VitalsService.Domain.DbEntities;

namespace VitalsService.DataAccess.EF.Mappings
{
    internal class PatientETsMapping : EntityTypeConfiguration<PatientActiveETs>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="PatientETsMapping" /> class.
        /// </summary>
        public PatientETsMapping()
        {
            // Table name
            this.ToTable("PatientActiveETs");
        }
    }
}

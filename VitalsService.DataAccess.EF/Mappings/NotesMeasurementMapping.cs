﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VitalsService.Domain.Constants;
using VitalsService.Domain.DbEntities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace VitalsService.DataAccess.EF.Mappings
{
    internal class NotesMeasurementMapping : EntityTypeConfiguration<NotesMeasurement>
    {

        public NotesMeasurementMapping()
        {
            // Table name
            this.ToTable("NotesMeasurement");

            // Primary Key
            this.HasKey(e => e.Id);
            this.Property(e => e.Id)
                .HasColumnName("Id")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Relationships
            // One-to-Many /Note - NoteNotables/
            this.HasRequired(nn => nn.Note)
                .WithMany(n => n.NotesMeasurements)
                .HasForeignKey(nn => nn.NoteId)
                .WillCascadeOnDelete(true);
        }

    }
}

namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedcustomeridpatientthresholdtype : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PatientThresholdTypes", "CustomerId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PatientThresholdTypes", "CustomerId");
        }
    }
}

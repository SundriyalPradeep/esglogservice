using System.Data.Entity.Migrations;
using VitalsService.DataAccess.EF.Contexts;

namespace VitalsService.DataAccess.EF.Migrations
{
    /// <summary>
    /// Configuration.
    /// </summary>
    internal sealed class Configuration : DbMigrationsConfiguration<VitalsDataContext>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Configuration"/> class.
        /// </summary>
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        /// <summary>
        /// Seeds the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        protected override void Seed(VitalsDataContext context)
        {
            //  This method will be called after migrating to the latest version.

            //#region EnhancedThresholdRange Seed

            //var Range1 = new Domain.DbEntities.EnhancedThresholdRange
            //{ Id = new System.Guid(), RangeText = "measurements", MaxRangeCount = "121" };

            //var Range2 = new Domain.DbEntities.EnhancedThresholdRange
            //{ Id = new System.Guid(), RangeText = "hours", MaxRangeCount = "2880" };

            //var Range3 = new Domain.DbEntities.EnhancedThresholdRange
            //{ Id = new System.Guid(), RangeText = "days", MaxRangeCount = "120" };

            //context.EnhancedThresholdRanges.Add(Range1);
            //context.EnhancedThresholdRanges.Add(Range2);
            //context.EnhancedThresholdRanges.Add(Range3);

            //#endregion

            //#region EnhancedThresholdSetAnalysis Seed

            //var SetAnalysis1 = new Domain.DbEntities.EnhancedThresholdSetAnalysis
            //{
            //    Id = new System.Guid(),
            //    SetAnalysisCount = 1,
            //    Percentage = 0,
            //    SetAnalysisText = "the decrease in values is",
            //    SetAnalysisFunctionType = "Decrease",
            //    Weight = 1
            //};


            //var SetAnalysis2 = new Domain.DbEntities.EnhancedThresholdSetAnalysis
            //{
            //    Id = new System.Guid(),
            //    SetAnalysisCount = 1,
            //    Percentage = 0,
            //    SetAnalysisText = "the increase in values is",
            //    SetAnalysisFunctionType = "Increase",
            //    Weight = 2
            //};


            //var SetAnalysis3 = new Domain.DbEntities.EnhancedThresholdSetAnalysis
            //{
            //    Id = new System.Guid(),
            //    SetAnalysisCount = 1,
            //    Percentage = 0,
            //    SetAnalysisText = "the most recent value is",
            //    SetAnalysisFunctionType = "Count",
            //    Weight = 3
            //};


            //var SetAnalysis4 = new Domain.DbEntities.EnhancedThresholdSetAnalysis
            //{
            //    Id = new System.Guid(),
            //    SetAnalysisCount = 2,
            //    Percentage = 0,
            //    SetAnalysisText = "two or more of the values are",
            //    SetAnalysisFunctionType = "Count",
            //    Weight = 4
            //};


            //var SetAnalysis5 = new Domain.DbEntities.EnhancedThresholdSetAnalysis
            //{
            //    Id = new System.Guid(),
            //    SetAnalysisCount = 3,
            //    Percentage = 0,
            //    SetAnalysisText = "three or more of the values are",
            //    SetAnalysisFunctionType = "Count",
            //    Weight = 5
            //};


            //var SetAnalysis6 = new Domain.DbEntities.EnhancedThresholdSetAnalysis
            //{
            //    Id = new System.Guid(),
            //    SetAnalysisCount = 0,
            //    Percentage = 100,
            //    SetAnalysisText = "all of the values are",
            //    SetAnalysisFunctionType = "Percentage",
            //    Weight = 6
            //};

            //context.EnhancedThresholdSetAnalysis.Add(SetAnalysis1);
            //context.EnhancedThresholdSetAnalysis.Add(SetAnalysis2);
            //context.EnhancedThresholdSetAnalysis.Add(SetAnalysis3);
            //context.EnhancedThresholdSetAnalysis.Add(SetAnalysis4);
            //context.EnhancedThresholdSetAnalysis.Add(SetAnalysis5);
            //context.EnhancedThresholdSetAnalysis.Add(SetAnalysis6);

            //#endregion

            //context.SaveChanges();
        }

    }
}
namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ESGLogService2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ESGMessageAcknowledgements", "MessageBodyId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ESGMessageAcknowledgements", "MessageBodyId", c => c.Guid(nullable: false));
        }
    }
}

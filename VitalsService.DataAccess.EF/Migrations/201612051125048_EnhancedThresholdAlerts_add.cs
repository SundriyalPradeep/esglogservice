namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EnhancedThresholdAlerts_add : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EnhancedThresholdAlerts",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CustomerId = c.Int(nullable: false),
                        EnhancedThresholdId = c.Guid(nullable: false),
                        MeasurementIds = c.String(),
                        ViolatedMeasurementId = c.Guid(nullable: false),
                        CreatedDateUTC = c.DateTime(nullable: false),
                        UpdatedDateUTC = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.EnhancedThresholdAlerts");
        }
    }
}

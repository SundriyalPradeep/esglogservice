namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class boolenvalueisdeleted : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Thresholds", "IsDeleted", c => c.Boolean(nullable: false));
            DropColumn("dbo.Thresholds", "isDelete");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Thresholds", "isDelete", c => c.Boolean(nullable: false));
            DropColumn("dbo.Thresholds", "IsDeleted");
        }
    }
}

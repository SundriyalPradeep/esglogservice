namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TweakQuestionnaireResponses : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.QuestionnaireResponses", "QuestionnaireName", c => c.String(nullable: false, maxLength: 100));
            AddColumn("dbo.QuestionnaireResponses", "ExacerbationAnswerChoiceId", c => c.Guid());
            AddColumn("dbo.QuestionnaireResponses", "ExacerbationAnswerChoiceText", c => c.String(maxLength: 1024));
        }
        
        public override void Down()
        {
            DropColumn("dbo.QuestionnaireResponses", "ExacerbationAnswerChoiceText");
            DropColumn("dbo.QuestionnaireResponses", "ExacerbationAnswerChoiceId");
            DropColumn("dbo.QuestionnaireResponses", "QuestionnaireName");
        }
    }
}

namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AllNoteschanges : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Notes", "AlertId", "dbo.Alerts");
            DropForeignKey("dbo.Notes", "MeasurementId", "dbo.Measurements");
            DropIndex("dbo.Notes", new[] { "MeasurementId" });
            DropIndex("dbo.Notes", new[] { "AlertId" });
            CreateTable(
                "dbo.NotesAlert",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        AlertId = c.Guid(nullable: false),
                        NoteId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Notes", t => t.NoteId, cascadeDelete: true)
                .Index(t => t.NoteId);
            
            CreateTable(
                "dbo.NotesMeasurement",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        MeasurementId = c.Guid(nullable: false),
                        NoteId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Notes", t => t.NoteId, cascadeDelete: true)
                .Index(t => t.NoteId);
            
            DropColumn("dbo.Notes", "MeasurementId");
            DropColumn("dbo.Notes", "AlertId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Notes", "AlertId", c => c.Guid());
            AddColumn("dbo.Notes", "MeasurementId", c => c.Guid());
            DropForeignKey("dbo.NotesMeasurement", "NoteId", "dbo.Notes");
            DropForeignKey("dbo.NotesAlert", "NoteId", "dbo.Notes");
            DropIndex("dbo.NotesMeasurement", new[] { "NoteId" });
            DropIndex("dbo.NotesAlert", new[] { "NoteId" });
            DropTable("dbo.NotesMeasurement");
            DropTable("dbo.NotesAlert");
            CreateIndex("dbo.Notes", "AlertId");
            CreateIndex("dbo.Notes", "MeasurementId");
            AddForeignKey("dbo.Notes", "MeasurementId", "dbo.Measurements", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Notes", "AlertId", "dbo.Alerts", "Id");
        }
    }
}

namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EnThresholds_AlertSeverity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EnhancedThresholdAlerts", "AlertSeverityId", c => c.Guid());
            CreateIndex("dbo.EnhancedThresholdAlerts", "AlertSeverityId");
            AddForeignKey("dbo.EnhancedThresholdAlerts", "AlertSeverityId", "dbo.AlertSeverities", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EnhancedThresholdAlerts", "AlertSeverityId", "dbo.AlertSeverities");
            DropIndex("dbo.EnhancedThresholdAlerts", new[] { "AlertSeverityId" });
            DropColumn("dbo.EnhancedThresholdAlerts", "AlertSeverityId");
        }
    }
}

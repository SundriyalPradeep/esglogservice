// <auto-generated />
namespace VitalsService.DataAccess.EF.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class EnrollmentDateColumn : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(EnrollmentDateColumn));
        
        string IMigrationMetadata.Id
        {
            get { return "201706210509124_EnrollmentDateColumn"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

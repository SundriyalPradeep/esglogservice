namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Vitals_addcol_IsInvalidated : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Vitals", "IsInvalidated", c => c.Boolean(nullable: false));
            DropColumn("dbo.Measurements", "IsInvalidated");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Measurements", "IsInvalidated", c => c.Boolean(nullable: false));
            DropColumn("dbo.Vitals", "IsInvalidated");
        }
    }
}

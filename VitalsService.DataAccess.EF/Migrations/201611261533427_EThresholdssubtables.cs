namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EThresholdssubtables : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.EnhancedThresholdRanges", "CustomerId");
            DropColumn("dbo.EnhancedThresholdSetAnalysis", "CustomerId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EnhancedThresholdSetAnalysis", "CustomerId", c => c.Int(nullable: false));
            AddColumn("dbo.EnhancedThresholdRanges", "CustomerId", c => c.Int(nullable: false));
        }
    }
}

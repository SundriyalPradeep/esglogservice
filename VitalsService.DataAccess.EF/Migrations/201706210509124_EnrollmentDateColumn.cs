namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EnrollmentDateColumn : DbMigration
    {
        //
        public override void Up()
        {
            AddColumn("dbo.ESGMessageTransactions", "EnrollmentDate", c => c.String());
            AddColumn("dbo.ESGMessageTransactions", "DisEnrollmentDate", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ESGMessageTransactions", "DisEnrollmentDate");
            DropColumn("dbo.ESGMessageTransactions", "EnrollmentDate");
        }
    }
}

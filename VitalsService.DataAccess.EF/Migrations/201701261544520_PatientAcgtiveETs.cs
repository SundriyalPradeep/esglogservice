namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PatientAcgtiveETs : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PatientActiveETs",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PatientId = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        EnhancedThresholdId = c.Guid(nullable: false),
                        isActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PatientActiveETs");
        }
    }
}

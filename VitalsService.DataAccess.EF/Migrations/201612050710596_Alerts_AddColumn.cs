namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Alerts_AddColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Alerts", "IsDeleted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Alerts", "IsDeleted");
        }
    }
}

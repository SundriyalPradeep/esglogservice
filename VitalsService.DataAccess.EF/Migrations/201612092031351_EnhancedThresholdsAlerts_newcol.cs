namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EnhancedThresholdsAlerts_newcol : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EnhancedThresholdAlerts", "PatientId", c => c.Guid(nullable: false));
            AddColumn("dbo.EnhancedThresholdAlerts", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.EnhancedThresholdAlerts", "AlertTitle", c => c.String());
            AddColumn("dbo.EnhancedThresholds", "VitalName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.EnhancedThresholds", "VitalName");
            DropColumn("dbo.EnhancedThresholdAlerts", "AlertTitle");
            DropColumn("dbo.EnhancedThresholdAlerts", "IsDeleted");
            DropColumn("dbo.EnhancedThresholdAlerts", "PatientId");
        }
    }
}

namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddHomeSensingAlerts : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HomeSensingAlerts",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        NotificationType = c.Int(nullable: false),
                        Threshold = c.Int(nullable: false),
                        Observed = c.Int(nullable: false),
                        MoreInfoUrl = c.String(maxLength: 500),
                        UnknownPropertiesJson = c.String(maxLength: 2000),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Alerts", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HomeSensingAlerts", "Id", "dbo.Alerts");
            DropIndex("dbo.HomeSensingAlerts", new[] { "Id" });
            DropTable("dbo.HomeSensingAlerts");
        }
    }
}

namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIgnoreAuditFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HomeSensingAlerts", "IgnoredBy", c => c.Guid());
            AddColumn("dbo.HomeSensingAlerts", "IgnoredUtc", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.HomeSensingAlerts", "IgnoredUtc");
            DropColumn("dbo.HomeSensingAlerts", "IgnoredBy");
        }
    }
}

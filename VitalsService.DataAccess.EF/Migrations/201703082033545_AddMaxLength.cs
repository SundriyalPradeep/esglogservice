namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMaxLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.QuestionnaireResponses", "ActionTakenFreeText", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.QuestionnaireResponses", "ActionTakenFreeText", c => c.String(maxLength: 1024));
        }
    }
}

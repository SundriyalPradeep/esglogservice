namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MinAndMaxUpdateThreshold : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Thresholds", "IsMinDefault", c => c.Boolean());
            AddColumn("dbo.Thresholds", "IsMaxDefault", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Thresholds", "IsMaxDefault");
            DropColumn("dbo.Thresholds", "IsMinDefault");
        }
    }
}

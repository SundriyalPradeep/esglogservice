namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ESGLogService : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ESGMessageAcknowledgements",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        AckType = c.String(),
                        AckSender = c.String(),
                        AckReceiver = c.String(),
                        MessageDate = c.DateTime(),
                        CreatedOn = c.DateTime(),
                        MessageBodyId = c.Guid(nullable: false),
                        ESGMessageTransactionId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ESGMessageTransactions", t => t.ESGMessageTransactionId, cascadeDelete: true)
                .Index(t => t.ESGMessageTransactionId);
            
            CreateTable(
                "dbo.ESGMessageTransactions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        MessageControlId = c.String(),
                        MessageEvent = c.String(),
                        MessageType = c.String(),
                        ICN = c.String(),
                        DFN = c.String(),
                        SendingFacility = c.String(),
                        ReceivingFacility = c.String(),
                        MessageDate = c.DateTime(),
                        InsertedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ESGMessageContents",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        MessageBody = c.String(),
                        MessageType = c.Int(nullable: false),
                        TransactionId = c.Guid(nullable: false),
                        ESGMessageTransaction_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ESGMessageTransactions", t => t.ESGMessageTransaction_Id)
                .Index(t => t.ESGMessageTransaction_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ESGMessageContents", "ESGMessageTransaction_Id", "dbo.ESGMessageTransactions");
            DropForeignKey("dbo.ESGMessageAcknowledgements", "ESGMessageTransactionId", "dbo.ESGMessageTransactions");
            DropIndex("dbo.ESGMessageContents", new[] { "ESGMessageTransaction_Id" });
            DropIndex("dbo.ESGMessageAcknowledgements", new[] { "ESGMessageTransactionId" });
            DropTable("dbo.ESGMessageContents");
            DropTable("dbo.ESGMessageTransactions");
            DropTable("dbo.ESGMessageAcknowledgements");
        }
    }
}

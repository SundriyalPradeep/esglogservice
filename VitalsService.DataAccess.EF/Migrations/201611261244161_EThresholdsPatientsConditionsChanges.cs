namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EThresholdsPatientsConditionsChanges : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Conditions", "EnhancedThresholdCondition_Id", "dbo.EnhancedThresholdConditions");
            DropIndex("dbo.Conditions", new[] { "EnhancedThresholdCondition_Id" });
            AddColumn("dbo.EnhancedThresholdConditions", "ConditionId", c => c.Guid(nullable: false));
            DropColumn("dbo.Conditions", "EnhancedThresholdCondition_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Conditions", "EnhancedThresholdCondition_Id", c => c.Guid());
            DropColumn("dbo.EnhancedThresholdConditions", "ConditionId");
            CreateIndex("dbo.Conditions", "EnhancedThresholdCondition_Id");
            AddForeignKey("dbo.Conditions", "EnhancedThresholdCondition_Id", "dbo.EnhancedThresholdConditions", "Id");
        }
    }
}

namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class A04Changes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ESGMessageTransactions", "RegistrationId", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ESGMessageTransactions", "RegistrationId");
        }
    }
}

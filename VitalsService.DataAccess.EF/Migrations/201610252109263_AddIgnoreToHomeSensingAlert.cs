namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIgnoreToHomeSensingAlert : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HomeSensingAlerts", "IsIgnored", c => c.Boolean(nullable: false, defaultValue: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.HomeSensingAlerts", "IsIgnored");
        }
    }
}

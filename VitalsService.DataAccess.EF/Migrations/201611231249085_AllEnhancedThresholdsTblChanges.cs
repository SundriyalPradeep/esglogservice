namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AllEnhancedThresholdsTblChanges : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.EnhancedThresholdEnhancedThresholdConditions", "EnhancedThreshold_Id", "dbo.EnhancedThresholds");
            DropForeignKey("dbo.EnhancedThresholdEnhancedThresholdConditions", "EnhancedThresholdCondition_Id", "dbo.EnhancedThresholdConditions");
            DropForeignKey("dbo.EnhancedThresholdPatientEnhancedThresholds", "EnhancedThresholdPatient_Id", "dbo.EnhancedThresholdPatients");
            DropForeignKey("dbo.EnhancedThresholdPatientEnhancedThresholds", "EnhancedThreshold_Id", "dbo.EnhancedThresholds");
            DropIndex("dbo.EnhancedThresholdEnhancedThresholdConditions", new[] { "EnhancedThreshold_Id" });
            DropIndex("dbo.EnhancedThresholdEnhancedThresholdConditions", new[] { "EnhancedThresholdCondition_Id" });
            DropIndex("dbo.EnhancedThresholdPatientEnhancedThresholds", new[] { "EnhancedThresholdPatient_Id" });
            DropIndex("dbo.EnhancedThresholdPatientEnhancedThresholds", new[] { "EnhancedThreshold_Id" });
            AddColumn("dbo.EnhancedThresholds", "ETConditions_Id", c => c.Guid());
            AddColumn("dbo.EnhancedThresholds", "ETPatients_Id", c => c.Guid());
            CreateIndex("dbo.EnhancedThresholds", "ETConditions_Id");
            CreateIndex("dbo.EnhancedThresholds", "ETPatients_Id");
            AddForeignKey("dbo.EnhancedThresholds", "ETConditions_Id", "dbo.EnhancedThresholdConditions", "Id");
            AddForeignKey("dbo.EnhancedThresholds", "ETPatients_Id", "dbo.EnhancedThresholdPatients", "Id");
            DropTable("dbo.EnhancedThresholdEnhancedThresholdConditions");
            DropTable("dbo.EnhancedThresholdPatientEnhancedThresholds");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.EnhancedThresholdPatientEnhancedThresholds",
                c => new
                    {
                        EnhancedThresholdPatient_Id = c.Guid(nullable: false),
                        EnhancedThreshold_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.EnhancedThresholdPatient_Id, t.EnhancedThreshold_Id });
            
            CreateTable(
                "dbo.EnhancedThresholdEnhancedThresholdConditions",
                c => new
                    {
                        EnhancedThreshold_Id = c.Guid(nullable: false),
                        EnhancedThresholdCondition_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.EnhancedThreshold_Id, t.EnhancedThresholdCondition_Id });
            
            DropForeignKey("dbo.EnhancedThresholds", "ETPatients_Id", "dbo.EnhancedThresholdPatients");
            DropForeignKey("dbo.EnhancedThresholds", "ETConditions_Id", "dbo.EnhancedThresholdConditions");
            DropIndex("dbo.EnhancedThresholds", new[] { "ETPatients_Id" });
            DropIndex("dbo.EnhancedThresholds", new[] { "ETConditions_Id" });
            DropColumn("dbo.EnhancedThresholds", "ETPatients_Id");
            DropColumn("dbo.EnhancedThresholds", "ETConditions_Id");
            CreateIndex("dbo.EnhancedThresholdPatientEnhancedThresholds", "EnhancedThreshold_Id");
            CreateIndex("dbo.EnhancedThresholdPatientEnhancedThresholds", "EnhancedThresholdPatient_Id");
            CreateIndex("dbo.EnhancedThresholdEnhancedThresholdConditions", "EnhancedThresholdCondition_Id");
            CreateIndex("dbo.EnhancedThresholdEnhancedThresholdConditions", "EnhancedThreshold_Id");
            AddForeignKey("dbo.EnhancedThresholdPatientEnhancedThresholds", "EnhancedThreshold_Id", "dbo.EnhancedThresholds", "Id", cascadeDelete: true);
            AddForeignKey("dbo.EnhancedThresholdPatientEnhancedThresholds", "EnhancedThresholdPatient_Id", "dbo.EnhancedThresholdPatients", "Id", cascadeDelete: true);
            AddForeignKey("dbo.EnhancedThresholdEnhancedThresholdConditions", "EnhancedThresholdCondition_Id", "dbo.EnhancedThresholdConditions", "Id", cascadeDelete: true);
            AddForeignKey("dbo.EnhancedThresholdEnhancedThresholdConditions", "EnhancedThreshold_Id", "dbo.EnhancedThresholds", "Id", cascadeDelete: true);
        }
    }
}

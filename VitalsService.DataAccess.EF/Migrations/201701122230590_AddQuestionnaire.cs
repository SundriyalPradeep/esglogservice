namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddQuestionnaire : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.QuestionnaireQuestionAnswers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        QuestionnaireResponseId = c.Guid(nullable: false),
                        QuestionId = c.Guid(nullable: false),
                        QuestionText = c.String(maxLength: 1024),
                        QuestionType = c.Int(nullable: false),
                        AnswerType = c.Int(nullable: false),
                        AnswerChoiceId = c.Guid(),
                        AnswerChoiceText = c.String(maxLength: 1024),
                        MeasurementType = c.Int(),
                        MeasurementValue = c.Decimal(precision: 18, scale: 2),
                        Score = c.Int(),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QuestionnaireResponses", t => t.QuestionnaireResponseId)
                .Index(t => t.QuestionnaireResponseId);
            
            CreateTable(
                "dbo.QuestionnaireResponses",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CustomerId = c.Int(nullable: false),
                        PatientId = c.Guid(nullable: false),
                        Name = c.String(maxLength: 100),
                        TotalPreStratificationRiskScore = c.Int(nullable: false),
                        ActionTakenAnswerChoiceId = c.Guid(),
                        ActionTakenAnswerChoiceText = c.String(maxLength: 1024),
                        ActionTakenFreeText = c.String(maxLength: 1024),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.QuestionnaireQuestionAnswers", "QuestionnaireResponseId", "dbo.QuestionnaireResponses");
            DropIndex("dbo.QuestionnaireQuestionAnswers", new[] { "QuestionnaireResponseId" });
            DropTable("dbo.QuestionnaireResponses");
            DropTable("dbo.QuestionnaireQuestionAnswers");
        }
    }
}

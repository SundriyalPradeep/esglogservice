namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class limitbasedthresholdchanges : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PatientThresholdTypes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PatientId = c.Guid(nullable: false),
                        ThresholdType = c.String(),
                        ConditionId = c.Guid(),
                        VitalType = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.DefaultThresholds", "IsConditional", c => c.Boolean());
            DropColumn("dbo.Thresholds", "IsMinDefault");
            DropColumn("dbo.Thresholds", "IsMaxDefault");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Thresholds", "IsMaxDefault", c => c.Boolean());
            AddColumn("dbo.Thresholds", "IsMinDefault", c => c.Boolean());
            DropColumn("dbo.DefaultThresholds", "IsConditional");
            DropTable("dbo.PatientThresholdTypes");
        }
    }
}

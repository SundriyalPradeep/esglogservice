namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HealthSessionElementValue_addColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HealthSessionElementValues", "Alert", c => c.Boolean());
            AddColumn("dbo.HealthSessionElementValues", "AlertSeverityId", c => c.Guid());
        }
        
        public override void Down()
        {
            DropColumn("dbo.HealthSessionElementValues", "AlertSeverityId");
            DropColumn("dbo.HealthSessionElementValues", "Alert");
        }
    }
}

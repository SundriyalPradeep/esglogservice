namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AllEnhancedThresholdsRelatedTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EnhancedThresholdRanges",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CustomerId = c.Int(nullable: false),
                        RangeText = c.String(),
                        MaxRangeCount = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EnhancedThresholdSetAnalysis",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CustomerId = c.Int(nullable: false),
                        SetAnalysisCount = c.Int(nullable: false),
                        Percentage = c.Int(nullable: false),
                        SetAnalysisText = c.String(),
                        SetAnalysisFunctionType = c.String(),
                        Weight = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EnhancedThresholdEnhancedThresholdConditions",
                c => new
                    {
                        EnhancedThreshold_Id = c.Guid(nullable: false),
                        EnhancedThresholdCondition_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.EnhancedThreshold_Id, t.EnhancedThresholdCondition_Id })
                .ForeignKey("dbo.EnhancedThresholds", t => t.EnhancedThreshold_Id, cascadeDelete: true)
                .ForeignKey("dbo.EnhancedThresholdConditions", t => t.EnhancedThresholdCondition_Id, cascadeDelete: true)
                .Index(t => t.EnhancedThreshold_Id)
                .Index(t => t.EnhancedThresholdCondition_Id);
            
            CreateTable(
                "dbo.EnhancedThresholdPatientEnhancedThresholds",
                c => new
                    {
                        EnhancedThresholdPatient_Id = c.Guid(nullable: false),
                        EnhancedThreshold_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.EnhancedThresholdPatient_Id, t.EnhancedThreshold_Id })
                .ForeignKey("dbo.EnhancedThresholdPatients", t => t.EnhancedThresholdPatient_Id, cascadeDelete: true)
                .ForeignKey("dbo.EnhancedThresholds", t => t.EnhancedThreshold_Id, cascadeDelete: true)
                .Index(t => t.EnhancedThresholdPatient_Id)
                .Index(t => t.EnhancedThreshold_Id);
            
            AddColumn("dbo.Conditions", "EnhancedThresholdCondition_Id", c => c.Guid());
            AddColumn("dbo.EnhancedThresholdConditions", "CreatedDateUTC", c => c.DateTime(nullable: false));
            AddColumn("dbo.EnhancedThresholdConditions", "UpdatedDateUTC", c => c.DateTime());
            AddColumn("dbo.EnhancedThresholdPatients", "UpdatedDateUTC", c => c.DateTime());
            AddColumn("dbo.EnhancedThresholds", "Unit", c => c.String());
            AddColumn("dbo.EnhancedThresholds", "TimeOrValuesCount", c => c.Int(nullable: false));
            AddColumn("dbo.EnhancedThresholds", "ComparisonOperator", c => c.String());
            AddColumn("dbo.EnhancedThresholds", "ComparisonValue", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.EnhancedThresholds", "UpdatedDateUTC", c => c.DateTime());
            AddColumn("dbo.EnhancedThresholds", "EThresholdRanges_Id", c => c.Guid());
            AddColumn("dbo.EnhancedThresholds", "EThresholdSetAnalysis_Id", c => c.Guid());
            CreateIndex("dbo.Conditions", "EnhancedThresholdCondition_Id");
            CreateIndex("dbo.EnhancedThresholds", "EThresholdRanges_Id");
            CreateIndex("dbo.EnhancedThresholds", "EThresholdSetAnalysis_Id");
            AddForeignKey("dbo.Conditions", "EnhancedThresholdCondition_Id", "dbo.EnhancedThresholdConditions", "Id");
            AddForeignKey("dbo.EnhancedThresholds", "EThresholdRanges_Id", "dbo.EnhancedThresholdRanges", "Id");
            AddForeignKey("dbo.EnhancedThresholds", "EThresholdSetAnalysis_Id", "dbo.EnhancedThresholdSetAnalysis", "Id");
            DropColumn("dbo.EnhancedThresholdConditions", "EThresholdId");
            DropColumn("dbo.EnhancedThresholdConditions", "ConditionId");
            DropColumn("dbo.EnhancedThresholdConditions", "SetAnalysisID");
            DropColumn("dbo.EnhancedThresholdConditions", "EThresholdRangeID");
            DropColumn("dbo.EnhancedThresholdConditions", "Unit");
            DropColumn("dbo.EnhancedThresholdConditions", "TimeOrValuesCount");
            DropColumn("dbo.EnhancedThresholdConditions", "ComparisonOperator");
            DropColumn("dbo.EnhancedThresholdConditions", "ComparisonValue");
            DropColumn("dbo.EnhancedThresholdPatients", "EThresholdPatientId");
            DropColumn("dbo.EnhancedThresholdPatients", "EThresholdId");
            DropColumn("dbo.EnhancedThresholds", "EThresholdId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EnhancedThresholds", "EThresholdId", c => c.Guid(nullable: false));
            AddColumn("dbo.EnhancedThresholdPatients", "EThresholdId", c => c.Guid(nullable: false));
            AddColumn("dbo.EnhancedThresholdPatients", "EThresholdPatientId", c => c.Guid(nullable: false));
            AddColumn("dbo.EnhancedThresholdConditions", "ComparisonValue", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.EnhancedThresholdConditions", "ComparisonOperator", c => c.String());
            AddColumn("dbo.EnhancedThresholdConditions", "TimeOrValuesCount", c => c.Int(nullable: false));
            AddColumn("dbo.EnhancedThresholdConditions", "Unit", c => c.String());
            AddColumn("dbo.EnhancedThresholdConditions", "EThresholdRangeID", c => c.Guid(nullable: false));
            AddColumn("dbo.EnhancedThresholdConditions", "SetAnalysisID", c => c.Guid(nullable: false));
            AddColumn("dbo.EnhancedThresholdConditions", "ConditionId", c => c.Guid(nullable: false));
            AddColumn("dbo.EnhancedThresholdConditions", "EThresholdId", c => c.Guid(nullable: false));
            DropForeignKey("dbo.EnhancedThresholds", "EThresholdSetAnalysis_Id", "dbo.EnhancedThresholdSetAnalysis");
            DropForeignKey("dbo.EnhancedThresholds", "EThresholdRanges_Id", "dbo.EnhancedThresholdRanges");
            DropForeignKey("dbo.EnhancedThresholdPatientEnhancedThresholds", "EnhancedThreshold_Id", "dbo.EnhancedThresholds");
            DropForeignKey("dbo.EnhancedThresholdPatientEnhancedThresholds", "EnhancedThresholdPatient_Id", "dbo.EnhancedThresholdPatients");
            DropForeignKey("dbo.EnhancedThresholdEnhancedThresholdConditions", "EnhancedThresholdCondition_Id", "dbo.EnhancedThresholdConditions");
            DropForeignKey("dbo.EnhancedThresholdEnhancedThresholdConditions", "EnhancedThreshold_Id", "dbo.EnhancedThresholds");
            DropForeignKey("dbo.Conditions", "EnhancedThresholdCondition_Id", "dbo.EnhancedThresholdConditions");
            DropIndex("dbo.EnhancedThresholdPatientEnhancedThresholds", new[] { "EnhancedThreshold_Id" });
            DropIndex("dbo.EnhancedThresholdPatientEnhancedThresholds", new[] { "EnhancedThresholdPatient_Id" });
            DropIndex("dbo.EnhancedThresholdEnhancedThresholdConditions", new[] { "EnhancedThresholdCondition_Id" });
            DropIndex("dbo.EnhancedThresholdEnhancedThresholdConditions", new[] { "EnhancedThreshold_Id" });
            DropIndex("dbo.EnhancedThresholds", new[] { "EThresholdSetAnalysis_Id" });
            DropIndex("dbo.EnhancedThresholds", new[] { "EThresholdRanges_Id" });
            DropIndex("dbo.Conditions", new[] { "EnhancedThresholdCondition_Id" });
            DropColumn("dbo.EnhancedThresholds", "EThresholdSetAnalysis_Id");
            DropColumn("dbo.EnhancedThresholds", "EThresholdRanges_Id");
            DropColumn("dbo.EnhancedThresholds", "UpdatedDateUTC");
            DropColumn("dbo.EnhancedThresholds", "ComparisonValue");
            DropColumn("dbo.EnhancedThresholds", "ComparisonOperator");
            DropColumn("dbo.EnhancedThresholds", "TimeOrValuesCount");
            DropColumn("dbo.EnhancedThresholds", "Unit");
            DropColumn("dbo.EnhancedThresholdPatients", "UpdatedDateUTC");
            DropColumn("dbo.EnhancedThresholdConditions", "UpdatedDateUTC");
            DropColumn("dbo.EnhancedThresholdConditions", "CreatedDateUTC");
            DropColumn("dbo.Conditions", "EnhancedThresholdCondition_Id");
            DropTable("dbo.EnhancedThresholdPatientEnhancedThresholds");
            DropTable("dbo.EnhancedThresholdEnhancedThresholdConditions");
            DropTable("dbo.EnhancedThresholdSetAnalysis");
            DropTable("dbo.EnhancedThresholdRanges");
        }
    }
}

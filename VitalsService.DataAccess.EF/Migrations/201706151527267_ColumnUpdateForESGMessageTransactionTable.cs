namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ColumnUpdateForESGMessageTransactionTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ESGMessageTransactions", "SSN", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ESGMessageTransactions", "SSN");
        }
    }
}

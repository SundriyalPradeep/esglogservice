namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAlertToNote : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Notes", "AlertId", c => c.Guid());
            CreateIndex("dbo.Notes", "AlertId");
            AddForeignKey("dbo.Notes", "AlertId", "dbo.Alerts", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Notes", "AlertId", "dbo.Alerts");
            DropIndex("dbo.Notes", new[] { "AlertId" });
            DropColumn("dbo.Notes", "AlertId");
        }
    }
}

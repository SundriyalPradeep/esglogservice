namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class messagedatedatatypechnage : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ESGMessageAcknowledgements", "MessageDate", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ESGMessageAcknowledgements", "MessageDate", c => c.DateTime());
        }
    }
}

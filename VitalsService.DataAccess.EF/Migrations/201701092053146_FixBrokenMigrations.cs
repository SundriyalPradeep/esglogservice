namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixBrokenMigrations : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EnhancedThresholdAlerts", "Acknowledged", c => c.Boolean(nullable: false));
            AddColumn("dbo.EnhancedThresholdAlerts", "AcknowledgedUtc", c => c.DateTime());
            AddColumn("dbo.EnhancedThresholdAlerts", "AcknowledgedBy", c => c.Guid());
            AddColumn("dbo.EnhancedThresholds", "Type", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EnhancedThresholds", "Type");
            DropColumn("dbo.EnhancedThresholdAlerts", "AcknowledgedBy");
            DropColumn("dbo.EnhancedThresholdAlerts", "AcknowledgedUtc");
            DropColumn("dbo.EnhancedThresholdAlerts", "Acknowledged");
        }
    }
}

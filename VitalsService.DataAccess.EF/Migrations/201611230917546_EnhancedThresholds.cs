namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EnhancedThresholds : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EnhancedThresholdConditions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CustomerId = c.Int(nullable: false),
                        EThresholdId = c.Guid(nullable: false),
                        ConditionId = c.Guid(nullable: false),
                        SetAnalysisID = c.Guid(nullable: false),
                        EThresholdRangeID = c.Guid(nullable: false),
                        Unit = c.String(),
                        TimeOrValuesCount = c.Int(nullable: false),
                        ComparisonOperator = c.String(),
                        ComparisonValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EnhancedThresholdPatients",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CustomerId = c.Int(nullable: false),
                        EThresholdPatientId = c.Guid(nullable: false),
                        EThresholdId = c.Guid(nullable: false),
                        PatientId = c.Guid(nullable: false),
                        CreatedDateUTC = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EnhancedThresholds",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CustomerId = c.Int(nullable: false),
                        EThresholdId = c.Guid(nullable: false),
                        Name = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsArchived = c.Boolean(nullable: false),
                        CreatedDateUTC = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.EnhancedThresholds");
            DropTable("dbo.EnhancedThresholdPatients");
            DropTable("dbo.EnhancedThresholdConditions");
        }
    }
}

namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateQuestionnaireResponse : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.QuestionnaireResponses", "QuestionnaireId", c => c.Guid(nullable: false));
            AddColumn("dbo.QuestionnaireResponses", "ClinicianId", c => c.Guid(nullable: false));
            DropColumn("dbo.QuestionnaireResponses", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.QuestionnaireResponses", "Name", c => c.String(maxLength: 100));
            DropColumn("dbo.QuestionnaireResponses", "ClinicianId");
            DropColumn("dbo.QuestionnaireResponses", "QuestionnaireId");
        }
    }
}

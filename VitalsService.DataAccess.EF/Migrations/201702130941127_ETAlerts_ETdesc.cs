namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ETAlerts_ETdesc : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EnhancedThresholdAlerts", "ETDescription", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.EnhancedThresholdAlerts", "ETDescription");
        }
    }
}

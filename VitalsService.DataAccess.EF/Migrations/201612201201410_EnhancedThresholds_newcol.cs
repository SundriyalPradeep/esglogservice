namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EnhancedThresholds_newcol : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EnhancedThresholds", "IsActive", c => c.Boolean(nullable: false));
            //CreateIndex("dbo.EnhancedThresholdAlerts", "EnhancedThresholdId");
            //AddForeignKey("dbo.EnhancedThresholdAlerts", "EnhancedThresholdId", "dbo.EnhancedThresholds", "Id", cascadeDelete: true);
            DropColumn("dbo.EnhancedThresholds", "IsArchived");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EnhancedThresholds", "IsArchived", c => c.Boolean(nullable: false));
            //DropForeignKey("dbo.EnhancedThresholdAlerts", "EnhancedThresholdId", "dbo.EnhancedThresholds");
            //DropIndex("dbo.EnhancedThresholdAlerts", new[] { "EnhancedThresholdId" });
            DropColumn("dbo.EnhancedThresholds", "IsActive");
        }
    }
}

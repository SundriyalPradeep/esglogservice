namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IsDeletedBoolValueThresholds : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Thresholds", "isDelete", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Thresholds", "MinValue", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Thresholds", "MaxValue", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Thresholds", "MaxValue", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Thresholds", "MinValue", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.Thresholds", "isDelete");
        }
    }
}

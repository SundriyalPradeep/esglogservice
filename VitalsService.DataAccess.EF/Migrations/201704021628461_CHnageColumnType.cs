namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CHnageColumnType : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ESGMessageTransactions", "MessageDate", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ESGMessageTransactions", "MessageDate", c => c.DateTime());
        }
    }
}

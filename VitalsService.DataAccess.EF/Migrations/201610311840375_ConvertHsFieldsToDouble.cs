namespace VitalsService.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ConvertHsFieldsToDouble : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.HomeSensingAlerts", "Threshold", c => c.Double(nullable: false));
            AlterColumn("dbo.HomeSensingAlerts", "Observed", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.HomeSensingAlerts", "Observed", c => c.Int(nullable: false));
            AlterColumn("dbo.HomeSensingAlerts", "Threshold", c => c.Int(nullable: false));
        }
    }
}

﻿using System;

namespace VitalsService.Web.Api.DataAnnotations
{
    /// <summary>
    /// Required to add possibility to upload files in swagger.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class SwaggerAddFileUploadAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public bool Required { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public string ParameterName { get; private set; }

        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="parameterName"></param>
        /// <param name="description"></param>
        /// <param name="required"></param>
        public SwaggerAddFileUploadAttribute(string parameterName, string description = null, bool required = true)
        {
            this.ParameterName = parameterName;
            this.Description = description;
            this.Required = required;
        }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using VitalsService.Domain.Enums.Questionnaires;

namespace VitalsService.Web.Api.DataAnnotations.Questionnaires
{
    /// <summary>
    /// Ensures the passed answer type is a valid enum value.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class QuestionnaireAnswerTypeIsValidAttribute : ValidationAttribute
    {
        /// <summary>
        /// Validator
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public override bool IsValid(object value)
        {
            var enumVal = (QuestionnaireAnswerType)value;

            return enumVal == QuestionnaireAnswerType.SingleSelect
                || enumVal == QuestionnaireAnswerType.Measurement;
        }
    }
}
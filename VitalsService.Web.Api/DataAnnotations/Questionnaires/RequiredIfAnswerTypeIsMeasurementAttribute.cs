﻿using System;
using System.ComponentModel.DataAnnotations;
using VitalsService.Domain.Enums.Questionnaires;
using VitalsService.Web.Api.Models.Questionnaires;

namespace VitalsService.Web.Api.DataAnnotations.Questionnaires
{
    /// <summary>
    /// Used to mark properties that are required if the answer type is Measurement.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class RequiredIfAnswerTypeIsMeasurementAttribute : ValidationAttribute
    {
        private readonly RequiredAttribute _requiredAttr = new RequiredAttribute();

        /// <summary>
        /// Validator
        /// </summary>
        /// <param name="value"></param>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var dto = (QuestionnaireQuestionAnswerDto)validationContext.ObjectInstance;

            if (dto.AnswerType != QuestionnaireAnswerType.Measurement)
            {
                return ValidationResult.Success;
            }

            if (_requiredAttr.IsValid(value))
            {
                return ValidationResult.Success;
            }
            
            return new ValidationResult($"{validationContext.MemberName} on question {dto.QuestionId} is required for measurement answer types. Question text: {dto.QuestionText}", new[] { validationContext.MemberName });
        }
    }
}
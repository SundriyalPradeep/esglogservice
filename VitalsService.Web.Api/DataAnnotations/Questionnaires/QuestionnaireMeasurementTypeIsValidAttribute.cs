﻿using System;
using System.ComponentModel.DataAnnotations;
using VitalsService.Domain.Enums.Questionnaires;

namespace VitalsService.Web.Api.DataAnnotations.Questionnaires
{
    /// <summary>
    /// Ensures the passed measurement type is a valid enum value.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class QuestionnaireMeasurementTypeIsValidAttribute : ValidationAttribute
    {
        /// <summary>
        /// Validator
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                // Not a required field, and this is not a required validator.
                return true;
            }

            var enumVal = (QuestionnaireMeasurementType)value;

            return enumVal == QuestionnaireMeasurementType.Temperature
                || enumVal == QuestionnaireMeasurementType.SpO2
                || enumVal == QuestionnaireMeasurementType.FEV1
                || enumVal == QuestionnaireMeasurementType.SYS
                || enumVal == QuestionnaireMeasurementType.HR
                || enumVal == QuestionnaireMeasurementType.Weight;
        }
    }
}
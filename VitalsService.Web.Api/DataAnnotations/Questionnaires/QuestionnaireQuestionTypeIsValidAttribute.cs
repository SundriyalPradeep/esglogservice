﻿using System;
using System.ComponentModel.DataAnnotations;
using VitalsService.Domain.Enums.Questionnaires;

namespace VitalsService.Web.Api.DataAnnotations.Questionnaires
{
    /// <summary>
    /// Ensures the passed question type is a valid enum value.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class QuestionnaireQuestionTypeIsValidAttribute : ValidationAttribute
    {
        /// <summary>
        /// Validator
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public override bool IsValid(object value)
        {
            var enumVal = (QuestionnaireQuestionType)value;

            return enumVal == QuestionnaireQuestionType.AlertConfirmation
                || enumVal == QuestionnaireQuestionType.General
                || enumVal == QuestionnaireQuestionType.PreStratification
                || enumVal == QuestionnaireQuestionType.EscalationAction;
        }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using VitalsService.Domain.Enums.Questionnaires;
using VitalsService.Web.Api.Models.Questionnaires;

namespace VitalsService.Web.Api.DataAnnotations.Questionnaires
{
    /// <summary>
    /// Used to mark properties that are required if the answer type is single select and the question 
    /// type is pre-stratification.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class RequiredIfQuestionTypeIsPreStratificationAndAnswerTypeIsSingleSelectAttribute : ValidationAttribute
    {
        private readonly RequiredAttribute _requiredAttr = new RequiredAttribute();

        /// <summary>
        /// Validator
        /// </summary>
        /// <param name="value"></param>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var dto = (QuestionnaireQuestionAnswerDto)validationContext.ObjectInstance;

            if (dto.QuestionType != QuestionnaireQuestionType.PreStratification)
            {
                return ValidationResult.Success;
            }

            if (dto.AnswerType != QuestionnaireAnswerType.SingleSelect)
            {
                return ValidationResult.Success;
            }

            if (_requiredAttr.IsValid(value))
            {
                return ValidationResult.Success;
            }

            return new ValidationResult($"{validationContext.MemberName} on question {dto.QuestionId} is required for pre-stratification questions with single-select answer types. Question text: {dto.QuestionText}", new[] { validationContext.MemberName });
        }
    }
}
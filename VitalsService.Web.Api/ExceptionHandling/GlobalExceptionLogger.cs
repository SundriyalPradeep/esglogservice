﻿using System.Text;
using System.Web.Http.ExceptionHandling;
using NLog;

namespace VitalsService.Web.Api.ExceptionHandling
{
    /// <summary>
    /// Why an exception logger instead of an exception handler? Exception loggers always get called, handlers
    /// not so much. Plus, we'll let the handler take care of error responses.
    /// See: https://www.asp.net/web-api/overview/error-handling/web-api-global-error-handling
    /// </summary>
    public class GlobalExceptionLogger : ExceptionLogger
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Log the exception.
        /// </summary>
        /// <param name="context"></param>
        public override void Log(ExceptionLoggerContext context)
        {
            Logger.Error(context.Exception, BuildSlightlyInformativeErrorMessage(context));
        }

        private string BuildSlightlyInformativeErrorMessage(ExceptionLoggerContext context)
        {
            var msg = new StringBuilder().AppendLine("*** Unhandled exception caught and propagated by GlobalExceptionLogger ***");

            string controllerFullName = "(controller context unvailable)";
            string action = "(action context unvailable)";

            if (context.ExceptionContext?.ControllerContext != null)
            {
                controllerFullName = context.ExceptionContext.ControllerContext.Controller.GetType().FullName;
            }

            if (context.ExceptionContext?.ActionContext != null)
            {
                action = context.ExceptionContext.ActionContext.ActionDescriptor.ActionName;
            }

            msg.AppendLine($"   Location: {controllerFullName}.{action}");

            if (context.ExceptionContext?.Request != null)
            {
                msg.AppendLine($"   Method: {context.ExceptionContext.Request.Method?.ToString()}");
                msg.AppendLine($"   RequestUri: {context.Request.RequestUri}");
            }

            return msg.ToString();
        }
    }
}
﻿using System.Configuration;
using NLog;
using StackExchange.Redis;

namespace VitalsService.Web.Api.Caching
{
    /// <summary>
    /// Provides a wrapper around StackExchange.Redis.
    /// </summary>
    public static class RedisCache
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The one and only instance of the redis connection multiplexer.
        /// </summary>
        public static readonly ConnectionMultiplexer Multiplexer;

        static RedisCache()
        {
            // Set abortConnect=false so that a connection failure doesn't cause type initialization to fail.
            Multiplexer = ConnectionMultiplexer.Connect(ConfigurationManager.ConnectionStrings["RedisCache"].ConnectionString);

            Multiplexer.ConnectionFailed += (o, cfea) =>
            {
                Logger.Trace(cfea.Exception, $"ConnectionFailed: connection of type {cfea.ConnectionType} on EndPoint {cfea.EndPoint} reported ConnectionFailureType {cfea.FailureType}");
            };

            Multiplexer.ConnectionRestored += (o, cfea) =>
            {
                Logger.Trace(cfea.Exception, $"ConnectionRestored: connection of type {cfea.ConnectionType} on EndPoint {cfea.EndPoint} reported ConnectionFailureType {cfea.FailureType}");
            };

            Multiplexer.ErrorMessage += (o, reea) =>
            {
                Logger.Trace($"Server {reea.EndPoint.ToString()} raised an ErrorMessage event with error message: {reea.Message}");
            };
        }
    }
}
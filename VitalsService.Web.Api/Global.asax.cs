﻿using System;
using System.Web.Http;
using HibernatingRhinos.Profiler.Appender.EntityFramework;
using Isg.EntityFramework.Interceptors;
using Microsoft.Practices.ServiceLocation;
using NLog;

namespace VitalsService.Web.Api
{
    /// <summary>
    /// WebApiApplication.
    /// </summary>
    /// <seealso cref="System.Web.HttpApplication" />
    public class WebApiApplication : System.Web.HttpApplication
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The UTC date/time when the application started.
        /// </summary>
        public readonly static DateTime ApplicationStartedUtc = DateTime.UtcNow;

        /// <summary>
        /// Application_s the start.
        /// </summary>
        protected void Application_Start()
        {
            try
            {
#if DEBUG
                EntityFrameworkProfiler.Initialize();
#endif

                GlobalConfiguration.Configure(WebApiConfig.Register);
                DependencyInjectionConfig.RegistedDependencies();
                FilterConfig.RegisterGlobalFilters(GlobalConfiguration.Configuration.Filters);
                InterceptorProvider.SetInterceptorProvider(ServiceLocator.Current.GetInstance<IInterceptorProvider>());
            }
            catch (Exception ex)
            {
                Logger.Fatal(ex, "Unhandled exception in Application_Start");
                throw;
            }
        }
    }
}
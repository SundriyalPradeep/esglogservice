﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AutoMapper;

namespace VitalsService.Web.Api.Helpers
{
    /// <summary>
    /// AutoMapper helper methods.
    /// </summary>
    public static class AutoMapperHelper
    {
        /// <summary>
        /// Configure AutoMapper, and assert that the configuration is valid.
        /// </summary>
        /// <param name="configAction"></param>
        /// <returns></returns>
        public static MapperConfiguration BuildAutoMapperConfiguration(Action<IMapperConfigurationExpression> configAction)
        {
            if (configAction == null)
            {
                throw new ArgumentNullException(nameof(configAction));
            }

            var config = new MapperConfiguration(configAction);

            config.AssertConfigurationIsValid();

            return config;
        }

        /// <summary>
        /// Find all value resolvers in the specified assemblies and register them with the DI container.
        /// </summary>
        /// <param name="register"></param>
        /// <param name="assemblies"></param>
        /// <returns></returns>
        public static void RegisterTypesImplementingIValueResolver(Action<Type> register, params Assembly[] assemblies)
        {
            if (register == null)
            {
                throw new ArgumentNullException(nameof(register));
            }

            if (assemblies == null || assemblies.Length == 0)
            {
                return;
            }

            var typesImplementingIValueResolver = GetTypesImplementingIValueResolver(assemblies);

            foreach (var valResolverType in typesImplementingIValueResolver)
            {
                register(valResolverType);
            }
        }

        /// <summary>
        /// Find all value resolvers in the specified assemblies.
        /// </summary>
        /// <param name="assemblies"></param>
        /// <returns></returns>
        private static List<Type> GetTypesImplementingIValueResolver(params Assembly[] assemblies)
        {
            if (assemblies == null || assemblies.Length == 0)
            {
                return Enumerable.Empty<Type>().ToList();
            }

            var typesImplementingIValueResolver = assemblies
                .SelectMany(a => a.GetTypes().Where(t => !t.IsInterface && !t.IsAbstract))
                .Where(t => t.GetInterfaces().Any(ti => ti.IsGenericType && ti.GetGenericTypeDefinition() == typeof(IValueResolver<,,>)))
                .ToList();

            return typesImplementingIValueResolver;
        }

        /// <summary>
        /// Find all type converters in the specified assemblies and register them with the DI container.
        /// </summary>
        /// <param name="register"></param>
        /// <param name="assemblies"></param>
        /// <returns></returns>
        public static void RegisterTypesImplementingITypeConverter(Action<Type> register, params Assembly[] assemblies)
        {
            if (register == null)
            {
                throw new ArgumentNullException(nameof(register));
            }

            if (assemblies == null || assemblies.Length == 0)
            {
                return;
            }

            var typesImplementingITypeConverter = GetTypesImplementingITypeConverter(assemblies);

            foreach (var typeConverterType in typesImplementingITypeConverter)
            {
                register(typeConverterType);
            }
        }

        /// <summary>
        /// Find all type converters in the specified assemblies.
        /// </summary>
        /// <param name="assemblies"></param>
        /// <returns></returns>
        private static List<Type> GetTypesImplementingITypeConverter(params Assembly[] assemblies)
        {
            if (assemblies == null || assemblies.Length == 0)
            {
                return Enumerable.Empty<Type>().ToList();
            }

            var typesImplementingITypeConverter = assemblies
                .SelectMany(a => a.GetTypes().Where(t => !t.IsInterface && !t.IsAbstract))
                .Where(t => t.GetInterfaces().Any(ti => ti.IsGenericType && ti.GetGenericTypeDefinition() == typeof(ITypeConverter<,>)))
                .ToList();

            return typesImplementingITypeConverter;
        }
    }
}
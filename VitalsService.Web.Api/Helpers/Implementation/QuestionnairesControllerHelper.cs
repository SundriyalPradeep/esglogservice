﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using VitalsService.Domain.DbEntities;
using VitalsService.Domain.Dtos;
using VitalsService.Domain.Enums;
using VitalsService.Domain.Enums.Questionnaires;
using VitalsService.DomainLogic.Services.Interfaces;
using VitalsService.Web.Api.Helpers.Interfaces;
using VitalsService.Web.Api.Models;
using VitalsService.Web.Api.Models.Questionnaires;
using VitalsService.Web.Api.Models.Thresholds;

namespace VitalsService.Web.Api.Helpers.Implementation
{
    /// <summary>
    /// Functionality for working with questionnaire responses.
    /// </summary>
    public class QuestionnairesControllerHelper : IQuestionnairesControllerHelper
    {
        private readonly IQuestionnaireResponseService _questionnaireResponseService;
        private readonly IThresholdsControllerHelper _thresholdsControllerHelper;
        private readonly IMapper _mapper;

        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="questionnaireResponseService"></param>
        /// <param name="thresholdsControllerHelper"></param>
        /// <param name="mapper"></param>
        public QuestionnairesControllerHelper(IQuestionnaireResponseService questionnaireResponseService, IThresholdsControllerHelper thresholdsControllerHelper, IMapper mapper)
        {
            _questionnaireResponseService = questionnaireResponseService;
            _thresholdsControllerHelper = thresholdsControllerHelper;
            _mapper = mapper;
        }


        /// <summary>
        /// Search for questionnaire responses for the given patient.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<QuestionnaireResponseResponseDto>> GetQuestionnaireResponsesAsync(int customerId, Guid patientId, BaseSearchDto request)
        {
            var questionnaireResponses = await _questionnaireResponseService.GetQuestionnaireResponsesAsync(customerId, patientId, request);

            return _mapper.Map<PagedResult<QuestionnaireResponse>, PagedResultDto<QuestionnaireResponseResponseDto>>(questionnaireResponses);
        }

        /// <summary>
        /// Returns a single questionnaire response for the given patient.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <param name="questionnaireResponseId"></param>
        /// <returns></returns>
        public async Task<OperationResultDto<QuestionnaireResponseResponseDto, GetQuestionnaireReponseStatus>> GetQuestionnaireResponseAsync(int customerId, Guid patientId, Guid questionnaireResponseId)
        {
            var questionnaireResponse = await _questionnaireResponseService.GetQuestionnaireResponseAsync(customerId, patientId, questionnaireResponseId);

            if (questionnaireResponse == null)
            {
                return new OperationResultDto<QuestionnaireResponseResponseDto, GetQuestionnaireReponseStatus>()
                {
                    Status = GetQuestionnaireReponseStatus.QuestionnaireResponseNotFound
                };
            }

            return new OperationResultDto<QuestionnaireResponseResponseDto, GetQuestionnaireReponseStatus>()
            {
                Status = GetQuestionnaireReponseStatus.Success,
                Content = _mapper.Map<QuestionnaireResponseResponseDto>(questionnaireResponse)
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <param name="questionnaireResponseId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<UpdateQuestionnaireResponseStatus> RecordActionTakenAsync(int customerId, Guid patientId, Guid questionnaireResponseId, QuestionnaireResponseActionTakenDto request)
        {
            return await _questionnaireResponseService.RecordActionTakenAsync(
                customerId,
                patientId,
                questionnaireResponseId,
                request.ActionTakenAnswerChoiceId,
                request.ActionTakenAnswerChoiceText,
                request.ActionTakenFreeText,
                request.ExacerbationAnswerChoiceId,
                request.ExacerbationAnswerChoiceText
                );
        }

        /// <summary>
        /// Add a questionnaire response to the database.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<OperationResultDto<QuestionnaireResponsePostResponseDto, CreateQuestionnaireResponseStatus>> CreateQuestionnaireResponseAsync(int customerId, Guid patientId, QuestionnaireResponseRequestDto request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var questionnaireResp = _mapper.Map<QuestionnaireResponse>(request);
            questionnaireResp.CustomerId = customerId;
            questionnaireResp.PatientId = patientId;
            //ThresholdsSearchDto searchDto = new ThresholdsSearchDto();
            //searchDto.Mode = ThresholdSearchType.Aggregate;
            //var patientThresholds = await _thresholdsControllerHelper.GetThresholds(customerId, patientId, searchDto);
            //if (patientThresholds.Results.Count == 0)
            //{
            //    throw new InvalidOperationException($"Unable to calculate questionnaire pre-stratification score. Patient {patientId} for customer {customerId} does not have any thresholds");
            //}

            //questionnaireResp.TotalPreStratificationRiskScore = CalculateTotalPreStratificationScore(customerId, patientId, request, patientThresholds.Results);

            var result = await _questionnaireResponseService.CreateAsync(questionnaireResp);

            return new OperationResultDto<QuestionnaireResponsePostResponseDto, CreateQuestionnaireResponseStatus>(
                CreateQuestionnaireResponseStatus.Success,
                new QuestionnaireResponsePostResponseDto()
                {
                    Id = result.Content.Id,
                    TotalPreStratificationRiskScore =request.TotalPreStratificationRiskScore
                }
            );
        }

        /// <summary>
        /// Public so that we can test it.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <param name="request"></param>
        /// <param name="patientThresholds"></param>
        /// <returns></returns>
        public int CalculateTotalPreStratificationScore(int customerId, Guid patientId, QuestionnaireResponseRequestDto request, IList<BaseThresholdDto> patientThresholds)
        {
            // Single-select pre-stratification answers are easy to score - we have the score itself right on each answer.
            var singleSelectScore = request.QuestionAnswers
                .Where(qa => qa.QuestionType == QuestionnaireQuestionType.PreStratification && qa.AnswerType == QuestionnaireAnswerType.SingleSelect)
                .Select(qa => qa.AnswerChoiceScore.Value)
                .DefaultIfEmpty(0)
                .Sum();


            //
            // Non-relative pre-stratification answers are the next easiest to score. We have an absolute range to compare 
            //   against for each measurement type.
            //

            var absoluteMeasurementsScore = 0;

            // Temperature in degrees C
            var temperatureAnswers = request.QuestionAnswers
                .Where(qa => qa.QuestionType == QuestionnaireQuestionType.PreStratification && qa.AnswerType == QuestionnaireAnswerType.Measurement && qa.MeasurementType == QuestionnaireMeasurementType.Temperature)
                .ToArray();

            foreach (var temperatureAnswer in temperatureAnswers)
            {
                // Validation should have required a value.
                var temperature = temperatureAnswer.MeasurementValue.Value;

                //Convert temperature to Celcius if entered in Fereinheigh on questionnaire 
                if (temperatureAnswer.MeasurementUnit.ToLower() == "F"
                  || temperatureAnswer.MeasurementUnit.ToLower() == "f")
                {
                    //Formula : Deduct 32, then multiply by 5, then divide by 9
                    temperature = temperature - 32;
                    temperature = decimal.Multiply(temperature, 5);
                    temperature = decimal.Round(temperature / 9,1,MidpointRounding.AwayFromZero);
                }

                // Temperature <= 37 C is 0 points.
                if (temperature > 37m && temperature < 37.5m)
                {
                    absoluteMeasurementsScore += 1;
                }
                else if (temperature >= 37.5m && temperature < 38m)
                {
                    absoluteMeasurementsScore += 2;
                }
                else if (temperature >= 38m)
                {
                    absoluteMeasurementsScore += 3;
                }
            }

            // Heart Rate in beats per minute
            var heartRateAnswers = request.QuestionAnswers
                .Where(qa => qa.QuestionType == QuestionnaireQuestionType.PreStratification && qa.AnswerType == QuestionnaireAnswerType.Measurement && qa.MeasurementType == QuestionnaireMeasurementType.HR)
                .ToArray();

            foreach (var heartRateAnswer in heartRateAnswers)
            {
                // Validation should have required a value.
                var heartRate = heartRateAnswer.MeasurementValue.Value;

                // 46 <= Heart rate <= 100 is 0 points.

                if (heartRate > 100m && heartRate <= 110m)
                {
                    absoluteMeasurementsScore += 1;
                }
                else if (heartRate > 110m && heartRate <= 115m)
                {
                    absoluteMeasurementsScore += 2;
                }
                else if (heartRate > 115m || heartRate < 46)
                {
                    absoluteMeasurementsScore += 3;
                }
            }


            //
            // Relative pre-stratification answer score calculations are based on the related Green Low threshold value. The UI is 
            //   supposed to require these patient thresholds to exist, but if we're trying to compute a score and the threshold 
            //   doesn't yet exist, throw an exception.
            //
            // We're assuming the customer is configured to use RYG thresholds.
            //

            var relativeMeasurementsScore = 0;

            // SpO2
            var spo2Threshold = GetPatientThresholdForVitalType(customerId, patientId, VitalType.OxygenSaturation, patientThresholds);
            if (spo2Threshold != null && spo2Threshold.MaxValue != null)
                relativeMeasurementsScore += ScoreSpO2Answers(request, spo2Threshold);

            // FEV1
            var fev1Threshold = GetPatientThresholdForVitalType(customerId, patientId, VitalType.ForcedExpiratoryVolume, patientThresholds);
            if (fev1Threshold != null && fev1Threshold.MaxValue != null)
                relativeMeasurementsScore += ScoreFev1Answers(request, fev1Threshold);

            // SYS
            var sysThreshold = GetPatientThresholdForVitalType(customerId, patientId, VitalType.SystolicBloodPressure, patientThresholds);
            if (sysThreshold != null && sysThreshold.MaxValue != null)
                relativeMeasurementsScore += ScoreSysAnswers(request, sysThreshold);

            // Weight
            var weightThreshold = GetPatientThresholdForVitalType(customerId, patientId, VitalType.Weight, patientThresholds);
            if (weightThreshold != null && weightThreshold.MaxValue !=null)
                relativeMeasurementsScore += ScoreWeightAnswers(request, weightThreshold);


            return (singleSelectScore + absoluteMeasurementsScore + relativeMeasurementsScore);
        }

        /// <summary>
        /// Pulling out this methid from create questionnaire as we dont want to create questionnaire before user confirms
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<OperationResultDto<QuestionnaireResponsePostResponseDto, CreateQuestionnaireResponseStatus>> CalculatePreStratificationScore(int customerId, Guid patientId, QuestionnaireResponseRequestDto request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var questionnaireResp = _mapper.Map<QuestionnaireResponse>(request);
            questionnaireResp.CustomerId = customerId;
            questionnaireResp.PatientId = patientId;
            ThresholdsSearchDto searchDto = new ThresholdsSearchDto();
            searchDto.Mode = ThresholdSearchType.Aggregate;
            var patientThresholds = await _thresholdsControllerHelper.GetThresholds(customerId, patientId, searchDto);
            if (patientThresholds.Results.Count == 0)
            {
                throw new InvalidOperationException($"Unable to calculate questionnaire pre-stratification score. Patient {patientId} for customer {customerId} does not have any thresholds");
            }

            int score = CalculateTotalPreStratificationScore(customerId, patientId, request, patientThresholds.Results);

            return new OperationResultDto<QuestionnaireResponsePostResponseDto, CreateQuestionnaireResponseStatus>(
                CreateQuestionnaireResponseStatus.Success,
                new QuestionnaireResponsePostResponseDto()
                {
                    Id = Guid.NewGuid(),
                    TotalPreStratificationRiskScore = score
                }
                );
        }



        private BaseThresholdDto GetPatientThresholdForVitalType(int customerId, Guid patientId, VitalType vitalType, IList<BaseThresholdDto> patientThresholds)
        {
            // If configured properly, there must be three thresholds per vital type, one each for Red, Yellow, and Green.
            var vitalTypeThresholds = patientThresholds.Where(pt => pt.Name == vitalType && pt.AlertSeverity != null && pt.AlertSeverity.Severity == 2);
            if (vitalTypeThresholds != null && vitalTypeThresholds.Count() > 0)
                vitalTypeThresholds = vitalTypeThresholds.ToArray();
            
            if (vitalTypeThresholds.Count() == 1)
            {
                var vitalTypeThreshold  =  vitalTypeThresholds.ToArray()[0];
                return vitalTypeThreshold;
            }

            return null;
        }

        private int ScoreSpO2Answers(QuestionnaireResponseRequestDto request, BaseThresholdDto spo2Threshold)
        {
            var spo2Score = 0;
            var spo2ReferenceValue = spo2Threshold.MaxValue.Value;
            var spo2Answers = GetPreStratificationMeasurementAnswers(request, QuestionnaireMeasurementType.SpO2);

            foreach (var spo2Answer in spo2Answers)
            {
                var spo2 = spo2Answer.MeasurementValue.Value;
                var spo2Diff = spo2ReferenceValue - spo2;

                if (spo2Diff >= 3m && spo2Diff <= 5m)
                {
                    spo2Score += 1;
                }
                else if (spo2Diff > 5m && spo2Diff <= 7m)
                {
                    spo2Score += 2;
                }
                else if (spo2Diff > 7m)
                {
                    spo2Score += 3;
                }
            }

            return spo2Score;
        }

        private int ScoreFev1Answers(QuestionnaireResponseRequestDto request, BaseThresholdDto fev1Threshold)
        {
            // 2017-02-02: per an email from Sarat, scoring has changed from time-based comparisons with previous 
            //   measurements to simple comparison with a relatively static Green Low threshold value. Jessica also
            //   noted that FEV1 >= 15 below the reference value gives you a score of 3.

            var fev1Score = 0;
            var fev1ReferenceValue = fev1Threshold.MaxValue.Value;
            var fev1Answers = GetPreStratificationMeasurementAnswers(request, QuestionnaireMeasurementType.FEV1);

            foreach (var fev1Answer in fev1Answers)
            {
                var fev1 = fev1Answer.MeasurementValue.Value;
                var fev1Diff = fev1ReferenceValue - fev1;

                if (fev1Diff >= 10m && fev1Diff < 15m)
                {
                    fev1Score += 2;
                }
                else if (fev1Diff >= 15m)
                {
                    fev1Score += 3;
                }
            }

            return fev1Score;
        }

        private int ScoreSysAnswers(QuestionnaireResponseRequestDto request, BaseThresholdDto sysThreshold)
        {
            var sysScore = 0;
            var sysReferenceValue = sysThreshold.MaxValue.Value;
            var sysAnswers = GetPreStratificationMeasurementAnswers(request, QuestionnaireMeasurementType.SYS);

            foreach (var sysAnswer in sysAnswers)
            {
                var sys = sysAnswer.MeasurementValue.Value;
                var sysDiff = sys - sysReferenceValue;

                if (sysDiff >= 16m && sysDiff <= 25m)
                {
                    sysScore += 1;
                }
                else if (sysDiff > 25m && sysDiff <= 30m)
                {
                    sysScore += 2;
                }
                else if (sysDiff > 30m)
                {
                    sysScore += 3;
                }
            }

            return sysScore;
        }

        private int ScoreWeightAnswers(QuestionnaireResponseRequestDto request, BaseThresholdDto weightThreshold)
        {
            // 2017-02-02: per an email from Sarat, scoring has changed completely. It used to be a time-base comparison
            //   with previous measurements. Now, it's a simple comparison with a relatively static Green Low threshold
            //   value.

            var weightScore = 0;
            var weightReferenceValue = weightThreshold.MaxValue.Value;
            var weightAnswers = GetPreStratificationMeasurementAnswers(request, QuestionnaireMeasurementType.Weight);

            foreach (var weightAnswer in weightAnswers)
            {
                var weight = weightAnswer.MeasurementValue.Value;
                //Convert weight to lbs if entered in kgs on questionnaire because thresholds will always be in lbs
                if (weightAnswer.MeasurementUnit.ToLower() == "kgs"
                   || weightAnswer.MeasurementUnit.ToLower() == "kg")
                {
                    weight = decimal.Round(decimal.Multiply(weight , 2.2046m),2,MidpointRounding.AwayFromZero);
                }

                var weightDiff = weight - weightReferenceValue;

                if (weightDiff >= 0.5m && weightDiff < 0.75m)
                {
                    weightScore += 1;
                }
                else if (weightDiff >= 0.75m && weightDiff <= 1m)
                {
                    weightScore += 2;
                }
                else if (weightDiff > 1m)
                {
                    weightScore += 3;
                }
            }

            return weightScore;
        }

        private QuestionnaireQuestionAnswerDto[] GetPreStratificationMeasurementAnswers(QuestionnaireResponseRequestDto request, QuestionnaireMeasurementType measurementType)
        {
            return request.QuestionAnswers
                .Where(qa => qa.QuestionType == QuestionnaireQuestionType.PreStratification && qa.AnswerType == QuestionnaireAnswerType.Measurement && qa.MeasurementType == measurementType)
                .ToArray();
        }
    }
}
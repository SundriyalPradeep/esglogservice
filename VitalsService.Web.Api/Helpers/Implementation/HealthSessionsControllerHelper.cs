﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using NLog;
using VitalsService.Domain.DbEntities;
using VitalsService.Domain.Dtos;
using VitalsService.Domain.Enums;
using VitalsService.DomainLogic.Services.Interfaces;
using VitalsService.Extensions;
using VitalsService.Helpers;
using VitalsService.Web.Api.Helpers.Interfaces;
using VitalsService.Web.Api.Models;
using VitalsService.Web.Api.Models.HealthSessions;

namespace VitalsService.Web.Api.Helpers.Implementation
{
    /// <summary>
    /// Contains health sessions models mapping and presentation
    /// layer logic.
    /// </summary>
    public class HealthSessionsControllerHelper : IHealthSessionsControllerHelper
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private readonly IHealthSessionsService _healthSessionsService;
        private readonly IMeasurementsService _measurementsService;
        private readonly IEsb _esbService;
        private readonly IAlertSeveritiesService _alertSeveritiesService;
        private readonly IAlertsService _alertsService;
        private readonly IThresholdAggregator _thresholdAggregator;
        private readonly IDefaultThresholdsService _defaultThresholdsService;
        private readonly IThresholdsService _thresholdsService;
        private readonly IPatientConditionsService _patientConditionsService;
        private readonly IMapper _mapper;
        private readonly IEnhancedThresholdsService _enhancedThresholdsService;

        /// <summary>
        /// Initializes a new instance of the <see cref="HealthSessionsControllerHelper" /> class.
        /// </summary>
        /// <param name="healthSessionsService">The health sessions service.</param>
        /// <param name="measurementsService">The measurements service.</param>
        /// <param name="esbService">The esb service.</param>
        /// <param name="alertSeveritiesService">The alert severities service.</param>
        /// <param name="alertsService">The alerts service.</param>
        /// <param name="thresholdsService">The thresholds service.</param>
        /// <param name="patientConditionsService">The patient Conditions Service.</param>
        /// <param name="defaultThresholdsService">The default thresholds service.</param>
        /// <param name="thresholdAggregator">The threshold aggregator.</param>
        /// <param name="enhancedThresholdsService">The enhanced threshold.</param>
        /// <param name="mapper"></param>
        public HealthSessionsControllerHelper(
            IHealthSessionsService healthSessionsService,
            IMeasurementsService measurementsService,
            IEsb esbService,
            IAlertSeveritiesService alertSeveritiesService,
            IAlertsService alertsService,
            IThresholdsService thresholdsService,
            IPatientConditionsService patientConditionsService,
            IDefaultThresholdsService defaultThresholdsService,
            IThresholdAggregator thresholdAggregator,
            IEnhancedThresholdsService enhancedThresholdsService,
            IMapper mapper
        )
        {
            _healthSessionsService = healthSessionsService;
            _measurementsService = measurementsService;
            _esbService = esbService;
            _alertSeveritiesService = alertSeveritiesService;
            _alertsService = alertsService;
            _thresholdAggregator = thresholdAggregator;
            _defaultThresholdsService = defaultThresholdsService;
            _thresholdsService = thresholdsService;
            _patientConditionsService = patientConditionsService;
            _enhancedThresholdsService = enhancedThresholdsService;
            _mapper = mapper;
        }

        /// <summary>
        /// Creates entity from request model and save data.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <returns></returns>
        public async Task<OperationResultDto<PostResponseDto<Guid>, CreateHealthSessionStatus>> Create(
            HealthSessionRequestDto model,
            int customerId,
            Guid patientId
        )
        {
            var isHealthSessionClientIdValid = await ValidateHealthSessionClientId(customerId, patientId, model);

            if (!isHealthSessionClientIdValid)
            {
                return new OperationResultDto<PostResponseDto<Guid>, CreateHealthSessionStatus>(
                    CreateHealthSessionStatus.HealthSessionWithClientIdAlreadyExists);
            }

            var healthSession = _mapper.Map<HealthSession>(model);
            healthSession.CustomerId = customerId;
            healthSession.PatientId = patientId;

            var buildHealthSessionElementsResult = await BuildHealthSessionElements(
                customerId,
                patientId,
                model.Elements,
                healthSession);

            if (buildHealthSessionElementsResult.Status != CreateHealthSessionStatus.Success)
            {
                return new OperationResultDto<PostResponseDto<Guid>, CreateHealthSessionStatus>(
                    buildHealthSessionElementsResult.Status
                );
            }

            healthSession.Elements.AddRange(buildHealthSessionElementsResult.Content);

            var result = await this._healthSessionsService.Create(healthSession);

            if (result.Status != CreateHealthSessionStatus.Success)
            {
                return new OperationResultDto<PostResponseDto<Guid>, CreateHealthSessionStatus>(result.Status);
            }

            try
            {
                var esbModel = _mapper.Map<HealthSessionEsbDto>(model);
                esbModel.Id = result.Content.Id;
                esbModel.CustomerId = customerId;
                esbModel.PatientId = patientId;

                foreach (var hserDto in esbModel.Elements)
                {
                    ResetRawJson(hserDto);
                }

                await this._esbService.PublishHealthSession(esbModel, customerId);
            }
            catch (Exception e)
            {
                logger.Error(e, "An error occured when try to publish health session to esb");
            }

            var resultModel = new PostResponseDto<Guid>
            {
                Id = result.Content.Id
            };

            return new OperationResultDto<PostResponseDto<Guid>, CreateHealthSessionStatus>()
            {
                Content = resultModel,
                Status = CreateHealthSessionStatus.Success
            };
        }

        /// <summary>
        /// Builds the list of health session elements.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="healthSessionElementDtos">The health session elements dtos.</param>
        /// <param name="healthSession">The health session instance.</param>
        /// <returns></returns>
        public async Task<OperationResultDto<IList<HealthSessionElement>, CreateHealthSessionStatus>>
            BuildHealthSessionElements(
                int customerId,
                Guid patientId,
                IList<HealthSessionElementRequestDto> healthSessionElementDtos,
                HealthSession healthSession)
        {
            var resultHealthSessionElements = new List<HealthSessionElement>();
            var customerAlertSeverities = (await _alertSeveritiesService
                            .GetAlertSeverities(customerId, new AlertSeveritiesSearchDto() { Take = int.MaxValue }))
                            .Results;

            foreach (var elementModel in healthSessionElementDtos)
            {
                var hlElement = await InitSessionElement(elementModel, customerId, patientId);

                if (elementModel.Alert && hlElement.Values.Any(el => el.Type == HealthSessionElementValueType.SelectionAnswer || el.Type == HealthSessionElementValueType.OpenEndedAnswer ||
                                                                     el.Type == HealthSessionElementValueType.ScaleAnswer))
                {

                    if (elementModel.AlertSeverityId.HasValue)
                    {
                        if (customerAlertSeverities.Any())
                        {
                            var requestedAlertSeverity = customerAlertSeverities.FirstOrDefault(s => s.Id == elementModel.AlertSeverityId.Value);

                            if (requestedAlertSeverity == null)
                            {
                                // Using Highest severity
                                elementModel.AlertSeverityId = customerAlertSeverities.OrderBy(s => s.Severity).Last().Id;
                            }
                        }
                        else
                        {
                            // Ignoring provided invalid alert severity
                            resultHealthSessionElements.Add(hlElement);

                            continue;
                        }
                    }
                    else
                    {
                        if (customerAlertSeverities.Any())
                        {
                            // Using Highest severity
                            elementModel.AlertSeverityId = customerAlertSeverities.OrderBy(s => s.Severity).Last().Id;
                        }
                    }
                    //Begin  : Code changes for Ms-3079
                    var alertTitle = "";
                    if (elementModel != null && elementModel.Values.Count != 0 && elementModel.Values[0].Type.ToString() == "OpenEndedAnswer")
                    {
                        var answersText = ((FreeFormAnswerDto)(elementModel.Values[0])).Value;
                        alertTitle = string.Format("{0} {1}", elementModel.Text, answersText);
                    }
                    else
                    { //Code changes for Ms-3079
                        var answersText = hlElement
                               .Values
                               .Select(v => v is SelectionAnswer ? ((SelectionAnswer)v).Text : ((ScaleAnswer)v).Value.ToString())
                               .Aggregate((s1, s2) => string.Format("{0}, {1}", s1, s2));
                        alertTitle = string.Format("{0} {1}", elementModel.Text, answersText);
                    } //end  : Code changes for Ms-3079


                    hlElement.HealthSessionElementAlert =
                        new HealthSessionElementAlert()
                        {
                            Id = SequentialGuidGenerator.Generate(),
                            HealthSessionElement = hlElement,
                            CustomerId = customerId,
                            PatientId = patientId,
                            Type = AlertType.ResponseViolation,
                            AlertSeverityId = elementModel.AlertSeverityId,
                            Title = alertTitle,
                            Body = string.Empty,
                            OccurredUtc = elementModel.AnsweredUtc ?? DateTime.MinValue,
                            ExpiresUtc = null,
                            Weight = 0
                        };
                }

                resultHealthSessionElements.Add(hlElement);
            }

            return new OperationResultDto<IList<HealthSessionElement>, CreateHealthSessionStatus>()
            {
                Status = CreateHealthSessionStatus.Success,
                Content = resultHealthSessionElements
            };
        }

        /// <summary>
        /// Returns all health sessions for patient.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="searchDto">The search dto.</param>
        /// <returns></returns>
        public async Task<PagedResultDto<HealthSessionResponseDto>> Find(
            int customerId,
            Guid patientId,
            HealthSessionsSearchDto searchDto
        )
        {
            var healthSessions = await this._healthSessionsService.Find(customerId, patientId, searchDto);

            return _mapper.Map<PagedResultDto<HealthSessionResponseDto>>(healthSessions);
        }

        /// <summary>
        /// Returns health session with specified id.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <param name="healthSessionId"></param>
        /// <returns></returns>
        public async Task<OperationResultDto<HealthSessionResponseDto, GetHealthSessionStatus>> GetById(int customerId, Guid patientId, Guid healthSessionId)
        {
            var resultHealthSession = await this._healthSessionsService.GetById(customerId, patientId, healthSessionId);

            if (resultHealthSession == null)
            {
                return new OperationResultDto<HealthSessionResponseDto, GetHealthSessionStatus>()
                {
                    Status = GetHealthSessionStatus.HealthSessionNotFound,
                    Content = null
                };
            }

            return new OperationResultDto<HealthSessionResponseDto, GetHealthSessionStatus>()
            {
                Status = GetHealthSessionStatus.Success,
                Content = _mapper.Map<HealthSessionResponseDto>(resultHealthSession)
            };
        }

        #region private methods

        /// <summary>
        /// Initializes health session element by model.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <returns></returns>
        private async Task<HealthSessionElement> InitSessionElement(HealthSessionElementRequestDto model, int customerId, Guid patientId)
        {
            var elementEntity = _mapper.Map<HealthSessionElementRequestDto, HealthSessionElement>(model);

            if (model.Values == null)
            {
                return elementEntity;
            }

            foreach (var elementValueModel in model.Values)
            {
                var measurementValueModel = elementValueModel as MeasurementValueRequestDto;

                if (measurementValueModel == null)
                {
                    elementEntity.Values.Add(_mapper.Map<HealthSessionElementValueDto, HealthSessionElementValue>(elementValueModel));
                }
                else
                {
                    elementEntity.Values.Add(
                        await CreateMeasurementValue(measurementValueModel.Value, customerId, patientId, elementValueModel));
                }
            }

            return elementEntity;
        }

        /// <summary>
        /// Creates new measurement using model.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <param name="elementValueModel"></param>
        private async Task<MeasurementValue> CreateMeasurementValue(MeasurementRequestDto model, int customerId, Guid patientId, HealthSessionElementValueDto elementValueModel)
        {
            var measurement = _mapper.Map<MeasurementRequestDto, Measurement>(model);
            measurement.CustomerId = customerId;
            measurement.PatientId = patientId;
            var defaultThresholds = (await _defaultThresholdsService.GetDefaultThresholds(customerId, null)).Results;
            var patientThresholds = (await _thresholdsService.GetThresholds(customerId, patientId, null)).Results;

            //MS-2841 & MS-2834 fix
            //Author: Shajudheen S
            #region # Patient Conditions threshold block #
            var patientConditions = (await _patientConditionsService.GetPatientConditions(customerId, patientId));
            var patientThresholdTypes = (await _thresholdsService.GetPatientThresholdsTypes(customerId, patientId));

            var customerAlertSeverities = (await _alertSeveritiesService
                            .GetAlertSeverities(customerId, new AlertSeveritiesSearchDto() { Take = int.MaxValue }))
                            .Results;

            IList<DefaultThreshold> filterdThresholds = new List<DefaultThreshold>();
            IList<Threshold> aggregatedThresholds = new List<Threshold>();
            if (customerAlertSeverities.Count > 0)
            {
                if (patientConditions.Count == 0)
                {
                    filterdThresholds =
                                 defaultThresholds.Where(t => t.ConditionId == null).ToList();

                    //MS-3465 Fix
                    aggregatedThresholds = _thresholdAggregator.CalculateAggregateThresholds(filterdThresholds, patientThresholds, patientThresholdTypes);
                }
                else
                {
                    //MS-3465 Fix
                    aggregatedThresholds = _thresholdAggregator.CalculateAggregateThresholds(defaultThresholds, patientThresholds, patientThresholdTypes);
                }
            }
            else
            {
                aggregatedThresholds = _thresholdAggregator.AggregateThresholds(filterdThresholds, patientThresholds);
            }
            #endregion         

            //var aggregatedThresholds = _thresholdAggregator.AggregateThresholds(defaultThresholds, patientThresholds);

            await _alertsService.CreateViolationAlerts(measurement, aggregatedThresholds);

            var createdMeasurement = await _measurementsService.Create(measurement, model.RawJson, false);
            var createETAlerts = await CheckEnhancedThresholdViolation(createdMeasurement, customerId, patientId);
            return new MeasurementValue
            {
                Measurement = createdMeasurement,
                Type = HealthSessionElementValueType.MeasurementAnswer,
                Alert = elementValueModel.Alert,
                AlertSeverityId = elementValueModel.AlertSeverityId
            };
        }

        /// <summary>
        /// Initializes health session element by model.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <returns></returns>
        private async Task<EnhancedThresholdAlerts> CheckEnhancedThresholdViolation(Measurement model, int customerId, Guid patientId)
        {
            var vitals = model.Vitals.ToList();

            #region Patient - EnhancedThreshold
            IList<EnhancedThreshold> filteredET_PatientList = new List<EnhancedThreshold>();
            var enthreshold_PatientList = (await _enhancedThresholdsService.GetEThresholdListPatientId(customerId, patientId));

            if (vitals.Count > 1)
            {
                var vitalNamelist = vitals.Select(v => v.Name);

                foreach (var vname in vitalNamelist)
                {
                    var filteredet_Pat = enthreshold_PatientList.Where(p => p.VitalName == vname).FirstOrDefault();
                    if (filteredet_Pat != null)
                        filteredET_PatientList.Add(filteredet_Pat);
                }
            }
            else
            {
                var vital = model.Vitals.FirstOrDefault();
                filteredET_PatientList = enthreshold_PatientList.Where(p => p.VitalName == vital.Name.ToString()).ToList();
            }


            if (filteredET_PatientList.Count > 0)
            {
                #region Patient ET Alert
                foreach (var enthreshold in filteredET_PatientList)
                {
                    await CreateViolatedEnhancedThresholdAlert(model, enthreshold, customerId, patientId);
                }
                #endregion
            }
            #endregion

            #region Condition - EnhancedThreshold
            var patientConditions = (await _patientConditionsService.GetPatientConditions(customerId, patientId));
            if (patientConditions.Count > 0)
            {
                foreach (var Condition in patientConditions)
                {
                    IList<EnhancedThreshold> filteredET_ConditionList = new List<EnhancedThreshold>();
                    var enthreshold_ConditionList = (await _enhancedThresholdsService.GetEThresholdListConditionId(customerId, Condition.Id, patientId));

                    if (vitals.Count > 1)
                    {
                        var vitalNamelist = vitals.Select(v => v.Name);
                        foreach (var vname in vitalNamelist)
                        {
                            var filteredET_Condn = enthreshold_ConditionList.Where(p => p.VitalName == vname).FirstOrDefault();
                            if (filteredET_Condn != null)
                                filteredET_ConditionList.Add(filteredET_Condn);
                        }

                    }
                    else
                    {
                        var vital = model.Vitals.FirstOrDefault();
                        filteredET_ConditionList = enthreshold_ConditionList.Where(p => p.VitalName == vital.Name.ToString()).ToList();
                    }

                    if (filteredET_ConditionList.Count > 0)
                    {
                        #region Conditions ET Alert
                        foreach (var enthreshold in filteredET_ConditionList)
                        {
                            await CreateViolatedEnhancedThresholdAlert(model, enthreshold, customerId, patientId);
                        }
                        #endregion
                    }
                }
            }
            #endregion


            #region Organization - EnhancedThreshold
            IList<EnhancedThreshold> filteredET_OrgList = new List<EnhancedThreshold>();
            var enthreshold_Org = (await _enhancedThresholdsService.GetActiveEnhancedThresholds(customerId, null, EnhancedThresholdType.Organization, patientId)).Results;

            if (vitals.Count > 1)
            {
                var vitalNamelist = vitals.Select(v => v.Name);
                foreach (var vname in vitalNamelist)
                {
                    var filteredET_Org = enthreshold_Org.Where(p => p.VitalName == vname).FirstOrDefault();
                    if (filteredET_Org != null)
                        filteredET_OrgList.Add(enthreshold_Org.Where(p => p.VitalName == vname).FirstOrDefault());
                }
            }
            else
            {
                var vital = model.Vitals.FirstOrDefault();
                filteredET_OrgList = enthreshold_Org.Where(p => p.VitalName == vital.Name.ToString()).ToList();
            }

            if (filteredET_OrgList.Count > 0)
            {
                #region Organization ET Alert
                foreach (var enthreshold in filteredET_OrgList)
                {
                    await CreateViolatedEnhancedThresholdAlert(model, enthreshold, customerId, patientId);
                }
                #endregion
            }
            #endregion

            return new EnhancedThresholdAlerts
            {
                CustomerId = customerId
            };
        }

        private async Task<EnhancedThresholdAlerts> CreateViolatedEnhancedThresholdAlert(
            Measurement measModel,
            EnhancedThreshold enthModel, int customerId, Guid patientId)
        {

            decimal _comparisonValue = enthModel.ComparisonValue;
            string _comparisonOperator = enthModel.ComparisonOperator;
            int _timeOrcount = enthModel.TimeOrValuesCount;
            string _rangeText = enthModel.EThresholdRanges.RangeText; /*days,hours,meas*/
            int _setAnalysisCount = enthModel.EThresholdSetAnalysis.SetAnalysisCount; /*To pick list count for comparison*/
            string _setAnalysisText = enthModel.EThresholdSetAnalysis.SetAnalysisText;
            string _setAnalysisFunctionType = enthModel.EThresholdSetAnalysis.SetAnalysisFunctionType;
            DateTime MeasobservedUtc = Convert.ToDateTime(measModel.ObservedUtc);
            _comparisonOperator = _comparisonOperator.Replace("< =", "<=").Replace("> =", ">=");
            string vitalname = enthModel.VitalName;
            var vitalList = measModel.Vitals.ToList();
            string setAnalysisCondText = string.Empty;
            foreach (var vital in vitalList)
            {
                if (vital.Name.ToLower() == vitalname.ToLower())
                {

                    var measurements_info = (await _measurementsService.Search
                                                            (customerId,
                                                             patientId,
                                                              new MeasurementsSearchDto()
                                                              {
                                                                  Q = vital.Name.ToString(),
                                                                  Skip = 0,
                                                                  Take = int.MaxValue
                                                              },
                                                             null)
                                            ).Results;
                    IList<Measurement> filterdMeasurement = new List<Measurement>();



                    if (_rangeText.ToLower() == "days")
                    {
                        DateTime dtCurrent = Convert.ToDateTime(MeasobservedUtc);
                        DateTime dtFromDays = Convert.ToDateTime(MeasobservedUtc).AddDays(-_timeOrcount);

                        var getMeasurementInfo = from mI in measurements_info
                                                 where mI.CreatedUtc >= dtFromDays &&
                                                 mI.CreatedUtc <= dtCurrent
                                                 orderby mI.CreatedUtc descending
                                                 select mI;

                        filterdMeasurement = measurements_info.Where(mI => mI.ObservedUtc >= dtFromDays).ToList();

                    }
                    else if (_rangeText.ToLower() == "minutes")
                    {
                        DateTime dtCurrent = Convert.ToDateTime(MeasobservedUtc);
                        DateTime dtFromMinutes = Convert.ToDateTime(MeasobservedUtc).AddMinutes(-_timeOrcount);

                        var getMeasurementInfo = from mI in measurements_info
                                                 where mI.ObservedUtc >= dtFromMinutes &&
                                                 mI.CreatedUtc <= dtCurrent &&
                                                 mI.Vitals == from vit in mI.Vitals
                                                              where vit.Name == vital.Name.ToString()
                                                              select vit
                                                 orderby mI.CreatedUtc descending
                                                 select mI;
                        filterdMeasurement = measurements_info.Where(mI => mI.ObservedUtc >= dtFromMinutes).ToList();
                    }
                    else
                    {
                        var getMeasurementInfo = from mI in measurements_info
                                                 orderby mI.CreatedUtc descending
                                                 select mI;
                        if (_timeOrcount > 1)
                        {
                            filterdMeasurement = getMeasurementInfo.OrderByDescending(c => c.CreatedUtc).Take(_timeOrcount).ToList();
                        }
                        else
                        {
                            if (_setAnalysisCount == 0)
                            {
                                if (_timeOrcount > 0)
                                {
                                    filterdMeasurement = getMeasurementInfo.OrderByDescending(c => c.CreatedUtc).Take(_timeOrcount).ToList();
                                }
                                else
                                    filterdMeasurement = getMeasurementInfo.OrderByDescending(c => c.CreatedUtc).Take(int.MaxValue).ToList();
                            }
                            else if (_setAnalysisCount == 1)
                            {
                                filterdMeasurement = getMeasurementInfo.OrderByDescending(c => c.CreatedUtc).Take(_setAnalysisCount + 1).ToList();
                            }
                            else
                                filterdMeasurement = getMeasurementInfo.OrderByDescending(c => c.CreatedUtc).Take(_setAnalysisCount).ToList();
                        }
                    }

                    if (filterdMeasurement.Count > 0)
                    {
                        if (_setAnalysisText.ToLower() == "the most recent value is")
                        {
                            filterdMeasurement = filterdMeasurement.OrderByDescending(c => c.CreatedUtc).Take(1).ToList();
                        }
                        else
                        {
                            filterdMeasurement = filterdMeasurement.OrderByDescending(c => c.CreatedUtc).Take(filterdMeasurement.Count).ToList();
                        }
                        //else if (_setAnalysisText.ToLower() == "two or more of the values are" ||
                        //         _setAnalysisText.ToLower() == "three or more of the values are")
                        //{
                        //    //filterdMeasurement = filterdMeasurement.OrderByDescending(c => c.CreatedUtc).Take(_timeOrcount).ToList();
                        //}
                    }
                    else
                    {
                        if (_setAnalysisCount == 0)
                        {
                            filterdMeasurement = filterdMeasurement.OrderByDescending(c => c.CreatedUtc).Take(int.MaxValue).ToList();
                        }
                        else if (_setAnalysisCount == 1)
                        {
                            filterdMeasurement = filterdMeasurement.OrderByDescending(c => c.CreatedUtc).Take(_setAnalysisCount + 1).ToList();
                        }
                        else
                            filterdMeasurement = filterdMeasurement.OrderByDescending(c => c.CreatedUtc).Take(_setAnalysisCount).ToList();
                    }

                    for (int i = 0; i < filterdMeasurement.Count; i++)
                    {
                        filterdMeasurement[i].Vitals = filterdMeasurement[i].Vitals.Where(v => v.Name == vital.Name.ToString()).ToList();
                    }

                    string violatedMeasurementId = string.Empty;
                    List<string> violatedMeasurementIdsCollection = new List<string> { };
                    string measurementIds = string.Empty;
                    List<string> alertTitles = new List<string> { };
                    if (filterdMeasurement.Count > 0)
                    {
                        //decimal vitalCompare = 0;
                        List<decimal> vitalVals = new List<decimal> { };
                        Dictionary<Guid, decimal> vitalValsDic = new Dictionary<Guid, decimal> { };
                        bool isIncrease = true;
                        var filterdMeasurementID = filterdMeasurement[0].Id;

                        if (_setAnalysisText.ToLower() == "the decrease in values is" ||
                            _setAnalysisText.ToLower() == "the increase in values is")
                        {
                            foreach (var measurement in filterdMeasurement)
                            {
                                measurementIds = measurement.Id + (string.IsNullOrEmpty(measurementIds) ? "" : "|" + measurementIds);
                                if (measurement.Vitals.Count > 0)
                                {
                                    vitalVals.Add(measurement.Vitals.FirstOrDefault().Value);
                                    vitalValsDic.Add(measurement.Id, measurement.Vitals.FirstOrDefault().Value);
                                }
                            }
                            filterdMeasurementID = filterdMeasurement[0].Id;

                            //vitalCompare = GetVitalsDifferences(vitalVals);
                            Dictionary<Guid, decimal> vitalValsCompare = GetVitalsDifferencesWithID(vitalValsDic);


                            if (vitalValsCompare.Count > 0)
                            {
                                var recentReading = vitalValsCompare.FirstOrDefault();
                                switch (_comparisonOperator)
                                {
                                    case ">":
                                        if (recentReading.Value > _comparisonValue)
                                        {
                                            violatedMeasurementId = Convert.ToString(recentReading.Key);
                                            violatedMeasurementIdsCollection.Add(Convert.ToString(recentReading.Key));
                                        }
                                        break;
                                    case "<":

                                        if (recentReading.Value < _comparisonValue)
                                        {
                                            violatedMeasurementId = Convert.ToString(recentReading.Key);
                                            violatedMeasurementIdsCollection.Add(Convert.ToString(recentReading.Key));
                                        }
                                        break;
                                    case "<=":
                                        if (recentReading.Value <= _comparisonValue)
                                        {
                                            violatedMeasurementId = Convert.ToString(recentReading.Key);
                                            violatedMeasurementIdsCollection.Add(Convert.ToString(recentReading.Key));
                                        }
                                        break;
                                    case ">=":
                                        if (recentReading.Value >= _comparisonValue)
                                        {
                                            violatedMeasurementId = Convert.ToString(recentReading.Key);
                                            violatedMeasurementIdsCollection.Add(Convert.ToString(recentReading.Key));
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }

                            isIncrease = isListIncreasing(vitalVals);

                            if (_setAnalysisText.ToLower() == "the increase in values is")
                            {
                                if (!isIncrease) //decrease true
                                {
                                    //violatedMeasurementId = string.Empty;
                                    violatedMeasurementIdsCollection.Clear();
                                    violatedMeasurementIdsCollection = new List<string>() { };
                                }
                                setAnalysisCondText = "increases in value";
                            }
                            else if (_setAnalysisText.ToLower() == "the decrease in values is")
                            {
                                if (isIncrease) //decrease false
                                {
                                    //violatedMeasurementId = string.Empty;
                                    violatedMeasurementIdsCollection.Clear();
                                    violatedMeasurementIdsCollection = new List<string>() { };
                                }
                                setAnalysisCondText = "decreases in value";
                            }

                        }
                        else
                        {
                            if (_setAnalysisText.ToLower() == "the most recent value is")
                            {
                                filterdMeasurement = filterdMeasurement.OrderByDescending(c => c.CreatedUtc).Take(1).ToList();
                                setAnalysisCondText = "has the most recent value";
                            }
                            else if (_setAnalysisText.ToLower() == "two or more of the values are")
                            {
                                setAnalysisCondText = "has two or more of the values are";
                            }
                            else if (_setAnalysisText.ToLower() == "three or more of the values are")
                            {
                                setAnalysisCondText = "has three or more of the values are";
                            }
                            else
                            {
                                setAnalysisCondText = "have all of the values are";
                            }

                            foreach (var measurement in filterdMeasurement)
                            {
                                if (measurement.Vitals.Count > 0)
                                {
                                    vitalVals.Add(measurement.Vitals.FirstOrDefault().Value);
                                    vitalValsDic.Add(measurement.Id, measurement.Vitals.FirstOrDefault().Value);
                                }
                            }

                            measurementIds = GetVitalsValuestoCompare(filterdMeasurement, _comparisonOperator, _comparisonValue, _setAnalysisText);
                            filterdMeasurementID = filterdMeasurement[0].Id;
                            if (!string.IsNullOrEmpty(measurementIds))
                            {
                                //violatedMeasurementId = Convert.ToString(filterdMeasurementID);
                                violatedMeasurementIdsCollection.Add(Convert.ToString(filterdMeasurementID));
                            }
                        }

                        var etDescription = string.Format(
                            "{0} {1} {2} {3} {4} during in last {5} {6}",
                            vital.Name,
                            setAnalysisCondText,
                            _comparisonOperator,
                            _comparisonValue,
                            vital.Unit,
                            _timeOrcount,
                            _rangeText.ToLower()
                        );


                        if (violatedMeasurementIdsCollection.Count > 0)
                        {
                            foreach (var violatedMeasurement in violatedMeasurementIdsCollection)
                            {
                                var vitalValues = vitalValsDic.Where(a => a.Key == Guid.Parse(violatedMeasurement));
                                var vitalValue = vitalValues.Select(a => a.Value).FirstOrDefault();
                                var alertTitle = string.Format(
                                                   "{0}: {1} {2} violated enhanced threshold of {3} {4} on {5}",
                                                   vital.Name,
                                                   vitalValue,
                                                   vital.Unit,
                                                   _comparisonValue,
                                                   enthModel.Unit,
                                                   filterdMeasurement[0].ObservedUtc
                                               );



                                EnhancedThresholdAlerts enhancedthresholdAlerts =
                               new EnhancedThresholdAlerts()
                               {
                                   Id = SequentialGuidGenerator.Generate(),
                                   CustomerId = customerId,
                                   PatientId = patientId,
                                   EnhancedThresholdId = enthModel.Id,
                                   CreatedDateUTC = DateTime.UtcNow,
                                   MeasurementIds = measurementIds,
                                   AlertTitle = alertTitle,
                                   ETDescription = etDescription,
                                   ViolatedMeasurementId = Guid.Parse(violatedMeasurement)
                               };

                                var result = await _enhancedThresholdsService.CreateEnhancedThresholdAlert(enhancedthresholdAlerts);
                            }
                        }
                    }
                }
            }

            return new EnhancedThresholdAlerts
            {
                CustomerId = customerId
            };
        }

        /// <summary>
        /// Get Vitals Values to Compare
        /// </summary>
        /// <param name="Measurements"></param>
        /// <param name="_comparisonOperator"></param>
        /// <param name="_comparisonValue"></param>
        /// <param name="_setAnalysisText"></param>
        public static string GetVitalsValuestoCompare(IList<Measurement> Measurements, string _comparisonOperator, decimal _comparisonValue, string _setAnalysisText)
        {
            int count = 0;
            decimal vitalCompare;
            string measurementIds = string.Empty;
            foreach (var Measurement in Measurements)
            {
                var vital = Measurement.Vitals.FirstOrDefault();
                vitalCompare = vital.Value;

                switch (_comparisonOperator)
                {
                    case ">":
                        if (vitalCompare > _comparisonValue)
                        {
                            count = count + 1;
                            measurementIds = Measurement.Id + (string.IsNullOrEmpty(measurementIds) ? "" : "|" + measurementIds);
                        }
                        continue;
                    case "<":
                        if (vitalCompare < _comparisonValue)
                        {
                            count = count + 1;
                            measurementIds = Measurement.Id + (string.IsNullOrEmpty(measurementIds) ? "" : "|" + measurementIds);
                        }
                        continue;
                    case "<=":
                        if (vitalCompare <= _comparisonValue)
                        {
                            count = count + 1;
                            measurementIds = Measurement.Id + (string.IsNullOrEmpty(measurementIds) ? "" : "|" + measurementIds);
                        }
                        continue;
                    case ">=":
                        if (vitalCompare >= _comparisonValue)
                        {
                            count = count + 1;
                            measurementIds = Measurement.Id + (string.IsNullOrEmpty(measurementIds) ? "" : "|" + measurementIds);
                        }
                        continue;

                    default:
                        break;

                }
            }

            if (_setAnalysisText == "the most recent value is")
            {
                return measurementIds;
            }
            else if (_setAnalysisText.ToLower() == "two or more of the values are")
            {
                if (count >= 2)
                    return measurementIds;
                else
                    return string.Empty;
            }
            else if (_setAnalysisText.ToLower() == "three or more of the values are")
            {
                if (count >= 3)
                    return measurementIds;
                else
                    return string.Empty;
            }
            else if (_setAnalysisText.ToLower() == "all of the values are")
            {
                if (count == Measurements.Count)
                    return measurementIds;
                else
                    return string.Empty;
            }
            else
                return string.Empty;

        }

        /// <summary>
        /// Get Vitals Differences
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static decimal GetVitalsDifferences(List<decimal> input)
        {
            var differences = new List<decimal>();

            decimal val = 0;

            bool isIncrease = isListIncreasing(input);
            if (isIncrease)
            {
                decimal minInput = input.Min(a => a);
                for (int i = 0; i < input.Count - 1; i++)
                {
                    val = val + (input[i] - minInput);
                    differences.Add((input[i] - minInput));
                }
                //for (int i = 0; i < input.Count - 1; i++)
                //{
                //    val = val + (input[i] - input[i + 1]);
                //    differences.Add((input[i] - input[i + 1]));
                //}
            }
            else
            {
                decimal maxInput = input.Max(a => a);
                for (int i = 0; i < input.Count - 1; i++)
                {
                    val = val + (maxInput - input[i]);
                    differences.Add((maxInput - input[i]));
                }
                //for (int i = 1; i < input.Count; i++)
                //{
                //    val = val + (input[i] - input[i - 1]);
                //    differences.Add((input[i] - input[i - 1]));
                //}
            }

            return val;
        }

        /// <summary>
        /// Get Vitals Differences with meas ID
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static Dictionary<Guid, decimal> GetVitalsDifferencesWithID(Dictionary<Guid, decimal> input)
        {
            Dictionary<Guid, decimal> differences = new Dictionary<Guid, decimal>();

            decimal val = 0;

            bool isIncrease = isListIncreasing(input.Values.ToList());
            if (isIncrease)
            {
                decimal minInput = input.Values.Min(a => a);
                for (int i = 0; i < input.Count - 1; i++)
                {
                    val = (input.Values.ToList()[i] - minInput);
                    differences.Add(input.Keys.ToList()[i], (input.Values.ToList()[i] - minInput));
                }
                //for (int i = 0; i < input.Count - 1; i++)
                //{
                //    val = val + (input[i] - input[i + 1]);
                //    differences.Add((input[i] - input[i + 1]));
                //}
            }
            else
            {
                decimal maxInput = input.Values.Max(a => a);
                for (int i = 0; i < input.Count - 1; i++)
                {
                    val = (maxInput - input.Values.ToList()[i]);
                    differences.Add(input.Keys.ToList()[i], (maxInput - input.Values.ToList()[i]));
                }
                //for (int i = 1; i < input.Count; i++)
                //{
                //    val = val + (input[i] - input[i - 1]);
                //    differences.Add((input[i] - input[i - 1]));
                //}
            }

            return differences;
        }

        /// <summary>
        /// Get Vitals Differences List
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static List<decimal> GetVitalsDifferencesList(List<decimal> input)
        {
            var differences = new List<decimal>();
            for (int i = 1; i < input.Count; i++)
            {
                differences.Add((input[i] - input[i - 1]));
            }
            return differences;
        }

        /// <summary>
        /// Get bool isIncreasing
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool isListIncreasing(List<decimal> input)
        {
            int count = 0;
            if (input.Count > 1)
            {
                if (input[1] < input[0])
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }


            //for (int i = 1; i < input.Count; i++)
            //{
            //    c
            //    {
            //        count += 1; // for inc                   
            //    }
            //}
            //if (input.Count > 2)
            //{
            //    if (count > Math.Abs(input.Count / 2))
            //    {
            //        return true;
            //    }
            //    else
            //    {
            //        return false;
            //    }
            //}
            //else
            //{
            //    if (count >= 1)
            //    {
            //        return true;
            //    }
            //    else
            //        return false;
            //}
        }

        private static bool ComputeEThresholdExpression(Dictionary<Measurement, DateTime> oldMeasurements, EnhancedThreshold enthModel, DateTime newMeasurementDate, Measurement newMeasurement)
        {
            return true;
        }

        private static Dictionary<Measurement, DateTime> GetMeasurementsInRange(IList<Measurement> _measurementInfo,
                                                                                EnhancedThreshold enthModel,
                                                                                Dictionary<Measurement, DateTime> measurements,
                                                                                DateTime newMeasurementDate)
        {
            int _timeOrcount = enthModel.TimeOrValuesCount;
            string _rangeText = enthModel.EThresholdRanges.RangeText; /*days,hours,meas*/

            switch (_rangeText.ToLower())
            {
                case "days":
                    if (measurements == null)
                        return null;

                    DateTime sinceDays = newMeasurementDate.AddDays(-_timeOrcount);

                    // intentionally used greater than or equals instead of greater than
                    // though in practice it probably won't make any difference
                    // it is more correct to include a measurement from exactly x hours ago
                    // given that the time resolution on many of these devices is 1 minute
                    var resultDays = (from m in measurements
                                      where m.Value >= sinceDays
                                      select m);

                    return resultDays.ToDictionary(item => item.Key, item => item.Value);

                case "minutes":
                    if (measurements == null)
                        return null;

                    DateTime sinceHours = newMeasurementDate.AddHours(-_timeOrcount);

                    // intentionally used greater than or equals instead of greater than
                    // though in practice it probably won't make any difference
                    // it is more correct to include a measurement from exactly x hours ago
                    // given that the time resolution on many of these devices is 1 minute
                    var resultHours = (from m in measurements
                                       where m.Value >= sinceHours
                                       select m);

                    return resultHours.ToDictionary(item => item.Key, item => item.Value);

                case "measurements":
                    if (measurements == null)
                        return null;

                    int above = measurements.Count - _timeOrcount;
                    // if I need more than there is then get everything above -1 (which includes 0)
                    if (above < 0)
                        above = -1;

                    // the result should be that if someone askes for the last 3 measurements
                    // we should return 2 here because a new one has come in which isn't in the array
                    var resultMeasurements = measurements.OrderBy(item => item.Value).Where((item, index) => index > above);

                    return resultMeasurements.ToDictionary(item => item.Key, item => item.Value);
            }

            return null;
        }


        /// <summary>
        /// Resets RawJson for measurement element if exists.
        /// </summary>
        /// <param name="model"></param>
        private static void ResetRawJson(HealthSessionElementRequestDto model)
        {
            if (model.Values == null)
            {
                return;
            }

            foreach (var hsevDto in model.Values)
            {
                var measurementVal = hsevDto as MeasurementValueRequestDto;

                if (measurementVal != null)
                {
                    measurementVal.Value.RawJson = null;
                }
            }
        }

        /// <summary>
        /// Uppdate the health session.
        /// </summary>
        /// <param name="model">The update model.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="healthSessionId">The health session identifier to be updated</param>
        /// <returns></returns>
        public async Task<UpdateHealthSessionStatus> Update(UpdateHealthSessionRequestDto model, int customerId, Guid patientId, Guid healthSessionId)
        {
            var existedHealthSession = await _healthSessionsService.GetById(customerId, patientId, healthSessionId);

            if (existedHealthSession == null)
            {
                return UpdateHealthSessionStatus.HealthSessionNotFound;
            }

            existedHealthSession.IsIncomplete = model.IsIncomplete;
            existedHealthSession.CompletedUtc = model.CompletedUtc ?? existedHealthSession.CompletedUtc;

            var buildHealthSessionElementsResult = await BuildHealthSessionElements(
                customerId,
                patientId,
                model.Elements,
                existedHealthSession);

            if (buildHealthSessionElementsResult.Status != CreateHealthSessionStatus.Success)
            {
                return (UpdateHealthSessionStatus)buildHealthSessionElementsResult.Status;
            }

            existedHealthSession.Elements.AddRange(buildHealthSessionElementsResult.Content);

            await _healthSessionsService.Update(existedHealthSession);

            try
            {
                var esbModel = _mapper.Map<HealthSessionEsbDto>(existedHealthSession);
                esbModel.Elements = model.Elements;

                foreach (var hserDto in esbModel.Elements)
                {
                    ResetRawJson(hserDto);
                }

                await this._esbService.PublishHealthSession(esbModel, existedHealthSession.CustomerId);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "An error occured when try to publish health session to Esb.");
            }

            return UpdateHealthSessionStatus.Success;
        }

        private async Task<bool> ValidateHealthSessionClientId(int customerId, Guid patientId, HealthSessionRequestDto request)
        {
            if (!string.IsNullOrEmpty(request.ClientId))
            {
                var existingHealthSession = (await _healthSessionsService.Find(
                    customerId,
                    patientId,
                    new HealthSessionsSearchDto()
                    {
                        Skip = 0,
                        Take = int.MaxValue,
                        IncludePrivate = true
                    },
                    request.ClientId
                ))
                .Results
                .FirstOrDefault();

                if (existingHealthSession != null)
                {
                    return false;
                }
            }

            return true;
        }

        #endregion
    }
}
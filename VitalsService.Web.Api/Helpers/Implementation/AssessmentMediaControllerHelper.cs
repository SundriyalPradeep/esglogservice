﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using VitalsService.Domain.DbEntities;
using VitalsService.Domain.Dtos;
using VitalsService.Domain.Enums;
using VitalsService.DomainLogic.Services.Interfaces;
using VitalsService.Web.Api.Helpers.Interfaces;
using VitalsService.Web.Api.Models.AssessmentMedias;

namespace VitalsService.Web.Api.Helpers.Implementation
{
    /// <summary>
    /// Contains help methods for assessment media controller.
    /// </summary>
    public class AssessmentMediaControllerHelper : IAssessmentMediaControllerHelper
    {
        private readonly IAssessmentMediaService _assessmentMediaService;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="AssessmentMediaControllerHelper"/> class.
        /// </summary>
        /// <param name="assessmentMediaService">The assessment media service.</param>
        /// <param name="mapper"></param>
        public AssessmentMediaControllerHelper(IAssessmentMediaService assessmentMediaService, IMapper mapper)
        {
            _assessmentMediaService = assessmentMediaService;
            _mapper = mapper;
        }

        /// <summary>
        /// Creates new instance of AssessmentMedia using request model.
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="request"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public async Task<Guid> CreateAssessmentMedia(int customerId, Guid patientId, CreateAssessmentMediaRequestDto request)
        {
            var entity = _mapper.Map<AssessmentMedia>(request);
            entity.PatientId = patientId;
            entity.CustomerId = customerId;

            var result = await this._assessmentMediaService.Create(entity);

            return result.Id;
        }

        /// <summary>
        /// Saves file content for specified media.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <param name="assessmentMediaId"></param>
        /// <param name="fileContent"></param>
        /// <returns></returns>
        public async Task<OperationResultDto<AssessmentMedia, UpdateAssessmentMediaStatus>> UpdateAssessmentMedia(
            int customerId, Guid patientId, Guid assessmentMediaId, HttpContent fileContent)
        {
            var fileBytes = await fileContent.ReadAsByteArrayAsync();

            var result = await this._assessmentMediaService.Update(customerId, patientId, assessmentMediaId, fileBytes);

            return result;
        }

        /// <summary>
        /// Returns required assessment media by identifier.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <param name="assessmentMediaId"></param>
        /// <returns></returns>
        public async Task<AssessmentMediaResponseDto> GetAssessmentMediaById(int customerId, Guid patientId,
            Guid assessmentMediaId)
        {
            var assessmentMedia = await this._assessmentMediaService.GetById(customerId, patientId, assessmentMediaId);

            return _mapper.Map<AssessmentMediaResponseDto>(assessmentMedia);
        }

        /// <summary>
        /// Returns all assessment media records for specified patient.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <param name="searchDto"></param>
        /// <returns></returns>
        public async Task<PagedResult<AssessmentMediaResponseDto>> GetAllAssessmentMedia(int customerId, Guid patientId,
            AssessmentMediaSearchDto searchDto)
        {
            var searchResult = await this._assessmentMediaService.GetAll(customerId, patientId, searchDto);

            return new PagedResult<AssessmentMediaResponseDto>
            {
                Results = _mapper.Map<IList<AssessmentMediaResponseDto>>(searchResult.Results),
                Total = searchResult.Total
            };
        }
    }
}
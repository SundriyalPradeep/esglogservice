﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using VitalsService.Domain.DbEntities;
using VitalsService.Domain.Dtos;
using VitalsService.Domain.Enums;
using VitalsService.DomainLogic.Services.Interfaces;
using VitalsService.Web.Api.Helpers.Interfaces;
using VitalsService.Web.Api.Models;
using VitalsService.Web.Api.Models.PatientNotes;
using VitalsService.Web.Api.Models.Alerts;

namespace VitalsService.Web.Api.Helpers.Implementation
{
    /// <summary>
    /// NotesControllerHelper.
    /// </summary>
    public class NotesControllerHelper : INotesControllerHelper
    {
        private readonly IPatientNotesService _patientNotesService;
        private readonly IMeasurementsService _measurementsService;
        private readonly IAlertsService _alertService;
        private IEnhancedThresholdsService _enhancedThresholdsService;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="NotesControllerHelper"/> class.
        /// </summary>
        /// <param name="patientNotesService">The patient notes service.</param>
        /// <param name="measurementsService">The measurement service.</param>
        /// <param name="alertService">The alert service.</param>
        /// <param name="mapper"></param>
        public NotesControllerHelper(IPatientNotesService patientNotesService, IMeasurementsService measurementsService, IAlertsService alertService, IMapper mapper, IEnhancedThresholdsService enhancedThresholdsService)
        {
            _patientNotesService = patientNotesService;
            _measurementsService = measurementsService;
            _alertService = alertService;
            _mapper = mapper;
            _enhancedThresholdsService = enhancedThresholdsService;
        }

        /// <summary>
        /// Creates the note.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId"></param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public async Task<OperationResultDto<BaseNoteDto, NoteStatus>> CreateNote(
            int customerId,
            Guid patientId,
            NoteRequestDto request
        )
        {
            var patientNote = _mapper.Map<NoteRequestDto, Note>(request);
            patientNote.CustomerId = customerId;
            patientNote.PatientId = patientId;

            var createNoteResult = await _patientNotesService.CreateNote(patientNote);

            var noteDto = createNoteResult.Status == NoteStatus.Success ? _mapper.Map<NoteDetailedResponseDto>(createNoteResult.Content) : null;

            var result = new OperationResultDto<BaseNoteDto, NoteStatus>()
            {
                Content = noteDto,
                Status = createNoteResult.Status
            };
            return result;
        }

        /// <summary>
        /// Gets the note.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="noteId">The note identifier.</param>
        /// <returns></returns>
        public async Task<OperationResultDto<NoteBriefResponseDto, NoteStatus>> GetNote(int customerId, Guid patientId, Guid noteId)
        {
            var result = await _patientNotesService.GetNote(customerId, patientId, noteId);

            if (result == null)
            {
                return await Task.FromResult(
                    new OperationResultDto<NoteBriefResponseDto, NoteStatus>()
                    {
                        Status = NoteStatus.NotFound
                    }
                );
            }

            return await Task.FromResult(
                new OperationResultDto<NoteBriefResponseDto, NoteStatus>()
                {
                    Status = NoteStatus.Success,
                    Content = _mapper.Map<Note, NoteBriefResponseDto>(result)
                }
            );
        }

        /// <summary>
        /// Gets the notes.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public async Task<PagedResultDto<BaseNoteDto>> GetNotes(int customerId, Guid patientId, NotesSearchDto request)
        {
            var getNotesResult = await _patientNotesService.GetNotes(customerId, patientId, request);


            if (request.IsBrief)
            {
                var result = _mapper.Map<PagedResult<Note>, PagedResultDto<NoteBriefResponseDto>>(getNotesResult);

                if (result.Results.Count > 0)
                    for (var r = 0; r < result.Results.Count; r++)
                    {
                        for (var i = 0; i < result.Results[r].NotesMeasurements.Count; i++)
                        {
                            var meas = await _measurementsService.GetById(customerId, patientId, result.Results[r].NotesMeasurements[i].MeasurementId);
                            result.Results[r].NotesMeasurements[i].Measurement = _mapper.Map<Measurement, MeasurementResponseDto>(meas);
                        }

                        if (result.Results.Count > 0)
                            for (var i = 0; i < result.Results[r].NotesAlerts.Count; i++)
                            {
                                var alertObject = await _alertService.GetById(customerId, patientId, result.Results[r].NotesAlerts[i].AlertId);
                                if (alertObject == null)
                                {
                                    var enThresholdAlert = await _enhancedThresholdsService.GetEnhancedThresholdAlert(result.Results[r].NotesAlerts[i].AlertId);
                                    if (enThresholdAlert != null)
                                    {

                                        result.Results[r].NotesAlerts[i].Alert = new BaseAlertResponseDto
                                        {
                                            Acknowledged = enThresholdAlert.Acknowledged,
                                            AcknowledgedBy = enThresholdAlert.AcknowledgedBy,
                                            AcknowledgedUtc = enThresholdAlert.AcknowledgedUtc,
                                            Id = enThresholdAlert.Id,
                                            Title = string.IsNullOrEmpty(enThresholdAlert.ETDescription) ? enThresholdAlert.AlertTitle : enThresholdAlert.ETDescription,
                                            //PatientId = enThresholdAlert.PatientId,
                                            IsDeleted = enThresholdAlert.IsDeleted,
                                            AlertSeverity = (enThresholdAlert.AlertSeverity == null) ? new Models.AlertSeverities.AlertSeverityResponseDto() : new Models.AlertSeverities.AlertSeverityResponseDto { ColorCode = enThresholdAlert.AlertSeverity.ColorCode, CustomerId = enThresholdAlert.AlertSeverity.CustomerId, Id = enThresholdAlert.AlertSeverity.Id, Name = enThresholdAlert.AlertSeverity.Name, Severity = enThresholdAlert.AlertSeverity.Severity },
                                            CustomerId = enThresholdAlert.CustomerId,
                                            OccurredUtc = enThresholdAlert.CreatedDateUTC,
                                            Body = string.Empty,
                                            Weight = 0,
                                            Type = AlertType.VitalsViolation,
                                            ExpiresUtc = DateTime.MaxValue

                                        };
                                    }
                                }
                                else
                                {
                                    result.Results[r].NotesAlerts[i].Alert = _mapper.Map<Alert, BaseAlertResponseDto>(alertObject);
                                }

                            }
                    }

                return new PagedResultDto<BaseNoteDto>()
                {
                    Total = result.Total,
                    Results = result.Results.Cast<BaseNoteDto>().ToList()
                };
            }
            else
            {
                var result = _mapper.Map<PagedResult<Note>, PagedResultDto<NoteDetailedResponseDto>>(getNotesResult);
                if (result.Results.Count > 0)
                {
                    for (var r = 0; r < result.Results.Count; r++)
                    {
                        for (var i = 0; i < result.Results[r].NotesMeasurements.Count; i++)
                        {
                            var meas = await _measurementsService.GetById(customerId, patientId, result.Results[r].NotesMeasurements[i].MeasurementId);
                            result.Results[r].NotesMeasurements[i].Measurement = _mapper.Map<Measurement, MeasurementResponseDto>(meas);
                        }
                        for (var i = 0; i < result.Results[r].NotesAlerts.Count; i++)
                        {
                            var alertObject = await _alertService.GetById(customerId, patientId, result.Results[r].NotesAlerts[i].AlertId);
                            if (alertObject == null)
                            {
                                var enThresholdAlert = await _enhancedThresholdsService.GetEnhancedThresholdAlert(result.Results[r].NotesAlerts[i].AlertId);
                                if (enThresholdAlert != null)
                                {
                                    result.Results[r].NotesAlerts[i].Alert = new BaseAlertResponseDto
                                    {
                                        Acknowledged = enThresholdAlert.Acknowledged,
                                        AcknowledgedBy = enThresholdAlert.AcknowledgedBy,
                                        AcknowledgedUtc = enThresholdAlert.AcknowledgedUtc,
                                        Id = enThresholdAlert.Id,
                                        Title = string.IsNullOrEmpty(enThresholdAlert.ETDescription) ? enThresholdAlert.AlertTitle : enThresholdAlert.ETDescription,
                                        //PatientId = enThresholdAlert.PatientId,
                                        IsDeleted = enThresholdAlert.IsDeleted,
                                        AlertSeverity = (enThresholdAlert.AlertSeverity == null) ? new Models.AlertSeverities.AlertSeverityResponseDto() : new Models.AlertSeverities.AlertSeverityResponseDto { ColorCode = enThresholdAlert.AlertSeverity.ColorCode, CustomerId = enThresholdAlert.AlertSeverity.CustomerId, Id = enThresholdAlert.AlertSeverity.Id, Name = enThresholdAlert.AlertSeverity.Name, Severity = enThresholdAlert.AlertSeverity.Severity },
                                        CustomerId = enThresholdAlert.CustomerId,
                                        OccurredUtc = enThresholdAlert.CreatedDateUTC,
                                        Body = string.Empty,
                                        Weight = 0,
                                        Type = AlertType.VitalsViolation,
                                        ExpiresUtc = DateTime.MaxValue

                                    };
                                }
                            }
                            else
                            {
                                result.Results[r].NotesAlerts[i].Alert = _mapper.Map<Alert, BaseAlertResponseDto>(alertObject);
                            }

                        }
                    }
                }

                return new PagedResultDto<BaseNoteDto>()
                {
                    Total = result.Total,
                    Results = result.Results.Cast<BaseNoteDto>().ToList()
                };
            }
        }

        /// <summary>
        /// Gets the patient note notables.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns></returns>
        public async Task<IList<string>> GetPatientNoteNotables(int customerId, Guid patientId)
        {
            var result = await _patientNotesService.GetPatientNoteNotables(customerId, patientId);

            return _mapper.Map<IList<NoteNotable>, IList<string>>(result)
                .Distinct()
                .ToList();
        }
    }
}
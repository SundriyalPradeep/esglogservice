﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using VitalsService.Domain.DbEntities;
using VitalsService.Domain.Dtos;
using VitalsService.Domain.Enums;
using VitalsService.DomainLogic.Services.Interfaces;
using VitalsService.Web.Api.Helpers.Interfaces;
using VitalsService.Web.Api.Models;
using VitalsService.Helpers;

namespace VitalsService.Web.Api.Helpers.Implementation
{
    /// <summary>
    /// VitalsControllerHelper.
    /// </summary>
    public class VitalsControllerHelper : IVitalsControllerHelper
    {
        private readonly IMeasurementsService _measurementsService;
        private readonly IAlertSeveritiesService _alertSeveritiesService;
        private readonly IAlertsService _alertsService;
        private readonly IThresholdAggregator _thresholdAggregator;
        private readonly IDefaultThresholdsService _defaultThresholdsService;
        private readonly IThresholdsService _thresholdsService;
        private readonly IPatientConditionsService _patientsConditionsService;
        private readonly IEnhancedThresholdsService _enhancedThresholdsService;
        private readonly IMapper _mapper;


        /// <summary>
        /// Initializes a new instance of the <see cref="VitalsControllerHelper" /> class.
        /// </summary>
        /// <param name="measurementsService">The measurements service.</param>
        /// <param name="thresholdsService">The thresholds service.</param>
        /// <param name="defaultThresholdsService">The default thresholds service.</param>
        /// <param name="thresholdAggregator">The threshold aggregator.</param>
        /// <param name="alertsService">The alerts service.</param>
        /// <param name="alertSeveritiesService">The alerts severities service</param>
        /// <param name="patientsConditionsService">The patients condition service.</param>
        /// <param name="enhancedThresholdsService">The enhancedThresholds service.</param>
        /// <param name="mapper"></param>
        /// <summary>        
        public VitalsControllerHelper(
            IMeasurementsService measurementsService,
            IThresholdsService thresholdsService,
            IDefaultThresholdsService defaultThresholdsService,
            IThresholdAggregator thresholdAggregator,
            IAlertSeveritiesService alertSeveritiesService,
            IAlertsService alertsService,
            IPatientConditionsService patientsConditionsService,
            IEnhancedThresholdsService enhancedThresholdsService,
            IMapper mapper
        )
        {
            _measurementsService = measurementsService;
            _thresholdsService = thresholdsService;
            _defaultThresholdsService = defaultThresholdsService;
            _thresholdsService = thresholdsService;
            _thresholdAggregator = thresholdAggregator;
            _alertSeveritiesService = alertSeveritiesService;
            _alertsService = alertsService;
            _patientsConditionsService = patientsConditionsService;
            _enhancedThresholdsService = enhancedThresholdsService;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets the vitals.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public async Task<PagedResultDto<MeasurementResponseDto>> GetVitals(
            int customerId,
            Guid patientId,
            MeasurementsSearchDto request
        )
        {
            var result = await _measurementsService.Search(customerId, patientId, request);
            List<decimal> input = new List<decimal>() { 119, 115, 105, 102, 110, 100 };
            var temp = isListIncreasing(input);
            return _mapper.Map<PagedResult<Measurement>, PagedResultDto<MeasurementResponseDto>>(result);
        }

        /// <summary>
        /// Gets the vital.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="measurementId">The measurement identifier.</param>
        /// <returns></returns>
        public async Task<MeasurementResponseDto> GetVital(int customerId, Guid patientId, Guid measurementId)
        {
            var vital = await _measurementsService.GetById(customerId, patientId, measurementId);
            List<decimal> input = new List<decimal>() { 100, 110 };
            var temp = isListIncreasing(input);

            return _mapper.Map<Measurement, MeasurementResponseDto>(vital);
        }

        /// <summary>
        /// Creates the vital.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public async Task<OperationResultDto<PostResponseDto<Guid>, CreateMeasurementStatus>> CreateVital(int customerId, Guid patientId, MeasurementRequestDto request)
        {
            var customerAlertSeverities = (await _alertSeveritiesService
                           .GetAlertSeverities(customerId, new AlertSeveritiesSearchDto() { Take = int.MaxValue }))
                           .Results;

            var isMeasurementClientIdValid = await ValidateMeasurementClientId(customerId, patientId, request);

            if (!isMeasurementClientIdValid)
            {
                return new OperationResultDto<PostResponseDto<Guid>, CreateMeasurementStatus>(
                    CreateMeasurementStatus.MeasurementWithClientIdAlreadyExists
                );
            }

            var measurement = _mapper.Map<Measurement>(request);
            measurement.CustomerId = customerId;
            measurement.PatientId = patientId;

            var patientConditions = await _patientsConditionsService.GetPatientConditions(customerId, patientId);
            var patientConditionIds = patientConditions.Select(pc => pc.Id).ToList();

            List<DefaultThreshold> conditionDefaultThresholds = new List<DefaultThreshold>();
            if (patientConditionIds.Any())
            {
                conditionDefaultThresholds = (await _defaultThresholdsService.GetDefaultThresholds(customerId, new DefaultThresholdsSearchDto()
                {
                    DefaultType = ThresholdDefaultType.Condition,
                    ConditionIds = patientConditionIds.ToList()
                })).Results.ToList();
            }

            var customerDefaultThresholds = (await _defaultThresholdsService.GetDefaultThresholds(customerId, new DefaultThresholdsSearchDto()
            {
                DefaultType = ThresholdDefaultType.Customer
            })).Results;

            var defaultThresholds = new List<DefaultThreshold>(customerDefaultThresholds);
            defaultThresholds.AddRange(conditionDefaultThresholds);

            var patientThresholds = (await _thresholdsService.GetThresholds(customerId, patientId, null)).Results;
            var patientThresholdTypes = (await _thresholdsService.GetPatientThresholdsTypes(customerId, patientId));

            IList<Threshold> aggregatedThresholds = null;
            if (customerAlertSeverities.Count > 0)
            {
                //MS-3465 Fix
                aggregatedThresholds = _thresholdAggregator.CalculateAggregateThresholds(defaultThresholds, patientThresholds, patientThresholdTypes);
            }else
            {
                aggregatedThresholds = _thresholdAggregator.AggregateThresholds(defaultThresholds, patientThresholds);
            }

            await _alertsService.CreateViolationAlerts(measurement, aggregatedThresholds);

            var createdMeasurement = await _measurementsService.Create(measurement, request.RawJson, true);

            var createETAlerts = await CheckEnhancedThresholdViolation(createdMeasurement, customerId, patientId);

            return new OperationResultDto<PostResponseDto<Guid>, CreateMeasurementStatus>(
                CreateMeasurementStatus.Success,
                new PostResponseDto<Guid>()
                {
                    Id = createdMeasurement.Id
                }
            );
        }

        /// <summary>
        /// Updates the vital.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="measurementId">The measurement identifier.</param>
        /// <param name="vitalId">The vitalId.</param>
        /// <returns></returns>
        public async Task<UpdateMeasurementStatus> UpdateVital(
            int customerId,
            Guid patientId,
            Guid measurementId,
            Guid vitalId
        )
        {
            var existingvital = await _measurementsService.GetVitalById(vitalId);
            existingvital.IsInvalidated = true;
            return (await _measurementsService.UpdateIsInvalidated(customerId, existingvital)).Status;
        }

        private async Task<bool> ValidateMeasurementClientId(int customerId, Guid patientId, MeasurementRequestDto request)
        {
            if (!string.IsNullOrEmpty(request.ClientId))
            {
                var existingMeasurement = (await _measurementsService.Search(
                    customerId,
                    patientId,
                    new MeasurementsSearchDto()
                    {
                        Skip = 0,
                        Take = int.MaxValue
                    },
                    request.ClientId
                ))
                .Results
                .FirstOrDefault();

                if (existingMeasurement != null)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Initializes health session element by model.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <returns></returns>
        private async Task<EnhancedThresholdAlerts> CheckEnhancedThresholdViolation(Measurement model, int customerId, Guid patientId)
        {
            var vitals = model.Vitals.ToList();

            #region Patient - EnhancedThreshold
            IList<EnhancedThreshold> filteredET_PatientList = new List<EnhancedThreshold>();
            var enthreshold_PatientList = (await _enhancedThresholdsService.GetEThresholdListPatientId(customerId, patientId));

            if (vitals.Count > 1)
            {
                var vitalNamelist = vitals.Select(v => v.Name);

                foreach (var vname in vitalNamelist)
                {
                    var filteredet_Pat = enthreshold_PatientList.Where(p => p.VitalName == vname).FirstOrDefault();
                    if (filteredet_Pat != null)
                        filteredET_PatientList.Add(filteredet_Pat);
                }
            }
            else
            {
                var vital = model.Vitals.FirstOrDefault();
                filteredET_PatientList = enthreshold_PatientList.Where(p => p.VitalName == vital.Name.ToString()).ToList();
            }


            if (filteredET_PatientList.Count > 0)
            {
                #region Patient ET Alert
                foreach (var enthreshold in filteredET_PatientList)
                {
                    await CreateViolatedEnhancedThresholdAlert(model, enthreshold, customerId, patientId);
                }
                #endregion
            }
            #endregion

            #region Condition - EnhancedThreshold
            var patientConditions = (await _patientsConditionsService.GetPatientConditions(customerId, patientId));
            if (patientConditions.Count > 0)
            {
                foreach (var Condition in patientConditions)
                {
                    IList<EnhancedThreshold> filteredET_ConditionList = new List<EnhancedThreshold>();
                    var enthreshold_ConditionList = (await _enhancedThresholdsService.GetEThresholdListConditionId(customerId, Condition.Id, patientId));

                    if (vitals.Count > 1)
                    {
                        var vitalNamelist = vitals.Select(v => v.Name);
                        foreach (var vname in vitalNamelist)
                        {
                            var filteredET_Condn = enthreshold_ConditionList.Where(p => p.VitalName == vname).FirstOrDefault();
                            if (filteredET_Condn != null)
                                filteredET_ConditionList.Add(filteredET_Condn);
                        }

                    }
                    else
                    {
                        var vital = model.Vitals.FirstOrDefault();
                        filteredET_ConditionList = enthreshold_ConditionList.Where(p => p.VitalName == vital.Name.ToString()).ToList();
                    }

                    if (filteredET_ConditionList.Count > 0)
                    {
                        #region Conditions ET Alert
                        foreach (var enthreshold in filteredET_ConditionList)
                        {
                            await CreateViolatedEnhancedThresholdAlert(model, enthreshold, customerId, patientId);
                        }
                        #endregion
                    }
                }
            }
            #endregion


            #region Organization - EnhancedThreshold
            IList<EnhancedThreshold> filteredET_OrgList = new List<EnhancedThreshold>();
            var enthreshold_Org = (await _enhancedThresholdsService.GetActiveEnhancedThresholds(customerId, null, EnhancedThresholdType.Organization, patientId)).Results;

            if (vitals.Count > 1)
            {
                var vitalNamelist = vitals.Select(v => v.Name);
                foreach (var vname in vitalNamelist)
                {
                    var filteredET_Org = enthreshold_Org.Where(p => p.VitalName == vname).FirstOrDefault();
                    if (filteredET_Org != null)
                        filteredET_OrgList.Add(enthreshold_Org.Where(p => p.VitalName == vname).FirstOrDefault());
                }
            }
            else
            {
                var vital = model.Vitals.FirstOrDefault();
                filteredET_OrgList = enthreshold_Org.Where(p => p.VitalName == vital.Name.ToString()).ToList();
            }

            if (filteredET_OrgList.Count > 0)
            {
                #region Organization ET Alert
                foreach (var enthreshold in filteredET_OrgList)
                {
                    await CreateViolatedEnhancedThresholdAlert(model, enthreshold, customerId, patientId);
                }
                #endregion
            }
            #endregion

            return new EnhancedThresholdAlerts
            {
                CustomerId = customerId
            };
        }

        private async Task<EnhancedThresholdAlerts> CreateViolatedEnhancedThresholdAlert(
            Measurement measModel,
            EnhancedThreshold enthModel, int customerId, Guid patientId)
        {

            decimal _comparisonValue = enthModel.ComparisonValue;
            string _comparisonOperator = enthModel.ComparisonOperator;
            int _timeOrcount = enthModel.TimeOrValuesCount;
            string _rangeText = enthModel.EThresholdRanges.RangeText; /*days,hours,meas*/
            int _setAnalysisCount = enthModel.EThresholdSetAnalysis.SetAnalysisCount; /*To pick list count for comparison*/
            string _setAnalysisText = enthModel.EThresholdSetAnalysis.SetAnalysisText;
            string _setAnalysisFunctionType = enthModel.EThresholdSetAnalysis.SetAnalysisFunctionType;
            DateTime MeasobservedUtc = Convert.ToDateTime(measModel.ObservedUtc);
            _comparisonOperator = _comparisonOperator.Replace("< =", "<=").Replace("> =", ">=");
            string vitalname = enthModel.VitalName;
            var vitalList = measModel.Vitals.ToList();
            string setAnalysisCondText = string.Empty;
            foreach (var vital in vitalList)
            {
                if (vital.Name.ToLower() == vitalname.ToLower())
                {

                    var measurements_info = (await _measurementsService.Search
                                                            (customerId,
                                                             patientId,
                                                              new MeasurementsSearchDto()
                                                              {
                                                                  Q = vital.Name.ToString(),
                                                                  Skip = 0,
                                                                  Take = int.MaxValue
                                                              },
                                                             null)
                                            ).Results;
                    IList<Measurement> filterdMeasurement = new List<Measurement>();



                    if (_rangeText.ToLower() == "days")
                    {
                        DateTime dtCurrent = Convert.ToDateTime(MeasobservedUtc);
                        DateTime dtFromDays = Convert.ToDateTime(MeasobservedUtc).AddDays(-_timeOrcount);

                        var getMeasurementInfo = from mI in measurements_info
                                                 where mI.CreatedUtc >= dtFromDays &&
                                                 mI.CreatedUtc <= dtCurrent
                                                 orderby mI.CreatedUtc descending
                                                 select mI;

                        filterdMeasurement = measurements_info.Where(mI => mI.ObservedUtc >= dtFromDays).ToList();

                    }
                    else if (_rangeText.ToLower() == "minutes")
                    {
                        DateTime dtCurrent = Convert.ToDateTime(MeasobservedUtc);
                        DateTime dtFromMinutes = Convert.ToDateTime(MeasobservedUtc).AddMinutes(-_timeOrcount);

                        var getMeasurementInfo = from mI in measurements_info
                                                 where mI.ObservedUtc >= dtFromMinutes &&
                                                 mI.CreatedUtc <= dtCurrent &&
                                                 mI.Vitals == from vit in mI.Vitals
                                                              where vit.Name == vital.Name.ToString()
                                                              select vit
                                                 orderby mI.CreatedUtc descending
                                                 select mI;
                        filterdMeasurement = measurements_info.Where(mI => mI.ObservedUtc >= dtFromMinutes).ToList();
                    }
                    else
                    {
                        var getMeasurementInfo = from mI in measurements_info
                                                 orderby mI.CreatedUtc descending
                                                 select mI;
                        if (_timeOrcount > 1)
                        {
                            filterdMeasurement = getMeasurementInfo.OrderByDescending(c => c.CreatedUtc).Take(_timeOrcount).ToList();
                        }
                        else
                        {
                            if (_setAnalysisCount == 0)
                            {
                                if (_timeOrcount > 0)
                                {
                                    filterdMeasurement = getMeasurementInfo.OrderByDescending(c => c.CreatedUtc).Take(_timeOrcount).ToList();
                                }
                                else
                                    filterdMeasurement = getMeasurementInfo.OrderByDescending(c => c.CreatedUtc).Take(int.MaxValue).ToList();
                            }
                            else if (_setAnalysisCount == 1)
                            {
                                filterdMeasurement = getMeasurementInfo.OrderByDescending(c => c.CreatedUtc).Take(_setAnalysisCount + 1).ToList();
                            }
                            else
                                filterdMeasurement = getMeasurementInfo.OrderByDescending(c => c.CreatedUtc).Take(_setAnalysisCount).ToList();
                        }
                    }

                    if (filterdMeasurement.Count > 0)
                    {
                        if (_setAnalysisText.ToLower() == "the most recent value is")
                        {
                            filterdMeasurement = filterdMeasurement.OrderByDescending(c => c.CreatedUtc).Take(1).ToList();
                        }
                        else
                        {
                            filterdMeasurement = filterdMeasurement.OrderByDescending(c => c.CreatedUtc).Take(filterdMeasurement.Count).ToList();
                        }
                        //else if (_setAnalysisText.ToLower() == "two or more of the values are" ||
                        //         _setAnalysisText.ToLower() == "three or more of the values are")
                        //{
                        //    //filterdMeasurement = filterdMeasurement.OrderByDescending(c => c.CreatedUtc).Take(_timeOrcount).ToList();
                        //}
                    }
                    else
                    {
                        if (_setAnalysisCount == 0)
                        {
                            filterdMeasurement = filterdMeasurement.OrderByDescending(c => c.CreatedUtc).Take(int.MaxValue).ToList();
                        }
                        else if (_setAnalysisCount == 1)
                        {
                            filterdMeasurement = filterdMeasurement.OrderByDescending(c => c.CreatedUtc).Take(_setAnalysisCount + 1).ToList();
                        }
                        else
                            filterdMeasurement = filterdMeasurement.OrderByDescending(c => c.CreatedUtc).Take(_setAnalysisCount).ToList();
                    }

                    for (int i = 0; i < filterdMeasurement.Count; i++)
                    {
                        filterdMeasurement[i].Vitals = filterdMeasurement[i].Vitals.Where(v => v.Name == vital.Name.ToString()).ToList();
                    }

                    string violatedMeasurementId = string.Empty;
                    List<string> violatedMeasurementIdsCollection = new List<string> { };
                    string measurementIds = string.Empty;
                    List<string> alertTitles = new List<string> { };
                    if (filterdMeasurement.Count > 0)
                    {
                        //decimal vitalCompare = 0;
                        List<decimal> vitalVals = new List<decimal> { };
                        Dictionary<Guid, decimal> vitalValsDic = new Dictionary<Guid, decimal> { };
                        bool isIncrease = true;
                        var filterdMeasurementID = filterdMeasurement[0].Id;

                        if (_setAnalysisText.ToLower() == "the decrease in values is" ||
                            _setAnalysisText.ToLower() == "the increase in values is")
                        {
                            foreach (var measurement in filterdMeasurement)
                            {
                                measurementIds = measurement.Id + (string.IsNullOrEmpty(measurementIds) ? "" : "|" + measurementIds);
                                if (measurement.Vitals.Count > 0)
                                {
                                    vitalVals.Add(measurement.Vitals.FirstOrDefault().Value);
                                    vitalValsDic.Add(measurement.Id, measurement.Vitals.FirstOrDefault().Value);
                                }
                            }
                            filterdMeasurementID = filterdMeasurement[0].Id;

                            //vitalCompare = GetVitalsDifferences(vitalVals);
                            Dictionary<Guid, decimal> vitalValsCompare = GetVitalsDifferencesWithID(vitalValsDic);


                            if (vitalValsCompare.Count > 0)
                            {
                                var recentReading = vitalValsCompare.FirstOrDefault();
                                switch (_comparisonOperator)
                                {
                                    case ">":
                                        if (recentReading.Value > _comparisonValue)
                                        {
                                            violatedMeasurementId = Convert.ToString(recentReading.Key);
                                            violatedMeasurementIdsCollection.Add(Convert.ToString(recentReading.Key));
                                        }
                                        break;
                                    case "<":

                                        if (recentReading.Value < _comparisonValue)
                                        {
                                            violatedMeasurementId = Convert.ToString(recentReading.Key);
                                            violatedMeasurementIdsCollection.Add(Convert.ToString(recentReading.Key));
                                        }
                                        break;
                                    case "<=":
                                        if (recentReading.Value <= _comparisonValue)
                                        {
                                            violatedMeasurementId = Convert.ToString(recentReading.Key);
                                            violatedMeasurementIdsCollection.Add(Convert.ToString(recentReading.Key));
                                        }
                                        break;
                                    case ">=":
                                        if (recentReading.Value >= _comparisonValue)
                                        {
                                            violatedMeasurementId = Convert.ToString(recentReading.Key);
                                            violatedMeasurementIdsCollection.Add(Convert.ToString(recentReading.Key));
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }                           

                            isIncrease = isListIncreasing(vitalVals);

                            if (_setAnalysisText.ToLower() == "the increase in values is")
                            {
                                if (!isIncrease) //decrease true
                                {
                                    //violatedMeasurementId = string.Empty;
                                    violatedMeasurementIdsCollection.Clear();
                                    violatedMeasurementIdsCollection = new List<string>() { };
                                }
                                setAnalysisCondText = "increases in value";
                            }
                            else if (_setAnalysisText.ToLower() == "the decrease in values is")
                            {
                                if (isIncrease) //decrease false
                                {
                                    //violatedMeasurementId = string.Empty;
                                    violatedMeasurementIdsCollection.Clear();
                                    violatedMeasurementIdsCollection = new List<string>() { };
                                }
                                setAnalysisCondText = "decreases in value";
                            }

                        }
                        else
                        {
                            if (_setAnalysisText.ToLower() == "the most recent value is")
                            {
                                filterdMeasurement = filterdMeasurement.OrderByDescending(c => c.CreatedUtc).Take(1).ToList();
                                setAnalysisCondText = "has the most recent value";
                            }
                            else if (_setAnalysisText.ToLower() == "two or more of the values are")
                            {
                                setAnalysisCondText = "has two or more of the values are";
                            }
                            else if (_setAnalysisText.ToLower() == "three or more of the values are")
                            {
                                setAnalysisCondText = "has three or more of the values are";
                            }
                            else
                            {
                                setAnalysisCondText = "have all of the values are";
                            }

                            foreach (var measurement in filterdMeasurement)
                            {
                                if (measurement.Vitals.Count > 0)
                                {
                                    vitalVals.Add(measurement.Vitals.FirstOrDefault().Value);
                                    vitalValsDic.Add(measurement.Id, measurement.Vitals.FirstOrDefault().Value);
                                }
                            }

                            measurementIds = GetVitalsValuestoCompare(filterdMeasurement, _comparisonOperator, _comparisonValue, _setAnalysisText);
                            filterdMeasurementID = filterdMeasurement[0].Id;
                            if (!string.IsNullOrEmpty(measurementIds))
                            {
                                //violatedMeasurementId = Convert.ToString(filterdMeasurementID);
                                violatedMeasurementIdsCollection.Add(Convert.ToString(filterdMeasurementID));
                            }
                        }

                        var etDescription = string.Format(
                            "{0} {1} {2} {3} {4} during in last {5} {6}",
                            vital.Name,
                            setAnalysisCondText,
                            _comparisonOperator,
                            _comparisonValue,
                            vital.Unit,
                            _timeOrcount,
                            _rangeText.ToLower()
                        );


                        if (violatedMeasurementIdsCollection.Count > 0)
                        {
                            foreach (var violatedMeasurement in violatedMeasurementIdsCollection)
                            {
                                var vitalValues = vitalValsDic.Where(a => a.Key == Guid.Parse(violatedMeasurement));
                                var vitalValue = vitalValues.Select(a => a.Value).FirstOrDefault();
                                var alertTitle = string.Format(
                                                   "{0}: {1} {2} violated enhanced threshold of {3} {4} on {5}",
                                                   vital.Name,
                                                   vitalValue,
                                                   vital.Unit,
                                                   _comparisonValue,
                                                   enthModel.Unit,
                                                   filterdMeasurement[0].ObservedUtc
                                               );



                                EnhancedThresholdAlerts enhancedthresholdAlerts =
                               new EnhancedThresholdAlerts()
                               {
                                   Id = SequentialGuidGenerator.Generate(),
                                   CustomerId = customerId,
                                   PatientId = patientId,
                                   EnhancedThresholdId = enthModel.Id,
                                   CreatedDateUTC = DateTime.UtcNow,
                                   MeasurementIds = measurementIds,
                                   AlertTitle = alertTitle,
                                   ETDescription = etDescription,
                                   ViolatedMeasurementId = Guid.Parse(violatedMeasurement)
                               };

                                var result = await _enhancedThresholdsService.CreateEnhancedThresholdAlert(enhancedthresholdAlerts);
                            }
                        }
                    }
                }
            }

            return new EnhancedThresholdAlerts
            {
                CustomerId = customerId
            };
        }

        /// <summary>
        /// Get Vitals Values to Compare
        /// </summary>
        /// <param name="Measurements"></param>
        /// <param name="_comparisonOperator"></param>
        /// <param name="_comparisonValue"></param>
        /// <param name="_setAnalysisText"></param>
        public static string GetVitalsValuestoCompare(IList<Measurement> Measurements, string _comparisonOperator, decimal _comparisonValue, string _setAnalysisText)
        {
            int count = 0;
            decimal vitalCompare;
            string measurementIds = string.Empty;
            foreach (var Measurement in Measurements)
            {
                var vital = Measurement.Vitals.FirstOrDefault();
                vitalCompare = vital.Value;

                switch (_comparisonOperator)
                {
                    case ">":
                        if (vitalCompare > _comparisonValue)
                        {
                            count = count + 1;
                            measurementIds = Measurement.Id + (string.IsNullOrEmpty(measurementIds) ? "" : "|" + measurementIds);
                        }
                        continue;
                    case "<":
                        if (vitalCompare < _comparisonValue)
                        {
                            count = count + 1;
                            measurementIds = Measurement.Id + (string.IsNullOrEmpty(measurementIds) ? "" : "|" + measurementIds);
                        }
                        continue;
                    case "<=":
                        if (vitalCompare <= _comparisonValue)
                        {
                            count = count + 1;
                            measurementIds = Measurement.Id + (string.IsNullOrEmpty(measurementIds) ? "" : "|" + measurementIds);
                        }
                        continue;
                    case ">=":
                        if (vitalCompare >= _comparisonValue)
                        {
                            count = count + 1;
                            measurementIds = Measurement.Id + (string.IsNullOrEmpty(measurementIds) ? "" : "|" + measurementIds);
                        }
                        continue;

                    default:
                        break;

                }
            }

            if (_setAnalysisText == "the most recent value is")
            {
                return measurementIds;
            }
            else if (_setAnalysisText.ToLower() == "two or more of the values are")
            {
                if (count >= 2)
                    return measurementIds;
                else
                    return string.Empty;
            }
            else if (_setAnalysisText.ToLower() == "three or more of the values are")
            {
                if (count >= 3)
                    return measurementIds;
                else
                    return string.Empty;
            }
            else if (_setAnalysisText.ToLower() == "all of the values are")
            {
                if (count == Measurements.Count)
                    return measurementIds;
                else
                    return string.Empty;
            }
            else
                return string.Empty;

        }

        /// <summary>
        /// Get Vitals Differences
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static decimal GetVitalsDifferences(List<decimal> input)
        {
            var differences = new List<decimal>();

            decimal val = 0;

            bool isIncrease = isListIncreasing(input);
            if (isIncrease)
            {
                decimal minInput = input.Min(a => a);
                for (int i = 0; i < input.Count - 1; i++)
                {
                    val = val + (input[i] - minInput);
                    differences.Add((input[i] - minInput));
                }
                //for (int i = 0; i < input.Count - 1; i++)
                //{
                //    val = val + (input[i] - input[i + 1]);
                //    differences.Add((input[i] - input[i + 1]));
                //}
            }
            else
            {
                decimal maxInput = input.Max(a => a);
                for (int i = 0; i < input.Count - 1; i++)
                {
                    val = val + (maxInput - input[i]);
                    differences.Add((maxInput - input[i]));
                }
                //for (int i = 1; i < input.Count; i++)
                //{
                //    val = val + (input[i] - input[i - 1]);
                //    differences.Add((input[i] - input[i - 1]));
                //}
            }

            return val;
        }

        /// <summary>
        /// Get Vitals Differences with meas ID
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static Dictionary<Guid, decimal> GetVitalsDifferencesWithID(Dictionary<Guid, decimal> input)
        {
            Dictionary<Guid, decimal> differences = new Dictionary<Guid, decimal>();

            decimal val = 0;

            bool isIncrease = isListIncreasing(input.Values.ToList());
            if (isIncrease)
            {
                decimal minInput = input.Values.Min(a => a);
                for (int i = 0; i < input.Count - 1; i++)
                {
                    val = (input.Values.ToList()[i] - minInput);
                    differences.Add(input.Keys.ToList()[i], (input.Values.ToList()[i] - minInput));
                }
            }
            else
            {
                decimal maxInput = input.Values.Max(a => a);
                for (int i = 0; i < input.Count - 1; i++)
                {
                    val = (maxInput - input.Values.ToList()[i]);
                    differences.Add(input.Keys.ToList()[i], (maxInput - input.Values.ToList()[i]));
                }
            }

            return differences;
        }

        /// <summary>
        /// Get Vitals Differences List
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static List<decimal> GetVitalsDifferencesList(List<decimal> input)
        {
            var differences = new List<decimal>();
            for (int i = 1; i < input.Count; i++)
            {
                differences.Add((input[i] - input[i - 1]));
            }
            return differences;
        }

        /// <summary>
        /// Get bool isIncreasing
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool isListIncreasing(List<decimal> input)
        {
            int count = 0;
            if (input.Count > 1)
            {
                if (input[1] < input[0])
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }


            //for (int i = 1; i < input.Count; i++)
            //{
            //    c
            //    {
            //        count += 1; // for inc                   
            //    }
            //}
            //if (input.Count > 2)
            //{
            //    if (count > Math.Abs(input.Count / 2))
            //    {
            //        return true;
            //    }
            //    else
            //    {
            //        return false;
            //    }
            //}
            //else
            //{
            //    if (count >= 1)
            //    {
            //        return true;
            //    }
            //    else
            //        return false;
            //}
        }

        private static bool ComputeEThresholdExpression(Dictionary<Measurement, DateTime> oldMeasurements, EnhancedThreshold enthModel, DateTime newMeasurementDate, Measurement newMeasurement)
        {
            return true;
        }

        private static Dictionary<Measurement, DateTime> GetMeasurementsInRange(IList<Measurement> _measurementInfo,
                                                                                EnhancedThreshold enthModel,
                                                                                Dictionary<Measurement, DateTime> measurements,
                                                                                DateTime newMeasurementDate)
        {
            int _timeOrcount = enthModel.TimeOrValuesCount;
            string _rangeText = enthModel.EThresholdRanges.RangeText; /*days,hours,meas*/

            switch (_rangeText.ToLower())
            {
                case "days":
                    if (measurements == null)
                        return null;

                    DateTime sinceDays = newMeasurementDate.AddDays(-_timeOrcount);

                    // intentionally used greater than or equals instead of greater than
                    // though in practice it probably won't make any difference
                    // it is more correct to include a measurement from exactly x hours ago
                    // given that the time resolution on many of these devices is 1 minute
                    var resultDays = (from m in measurements
                                      where m.Value >= sinceDays
                                      select m);

                    return resultDays.ToDictionary(item => item.Key, item => item.Value);

                case "minutes":
                    if (measurements == null)
                        return null;

                    DateTime sinceHours = newMeasurementDate.AddHours(-_timeOrcount);

                    // intentionally used greater than or equals instead of greater than
                    // though in practice it probably won't make any difference
                    // it is more correct to include a measurement from exactly x hours ago
                    // given that the time resolution on many of these devices is 1 minute
                    var resultHours = (from m in measurements
                                       where m.Value >= sinceHours
                                       select m);

                    return resultHours.ToDictionary(item => item.Key, item => item.Value);

                case "measurements":
                    if (measurements == null)
                        return null;

                    int above = measurements.Count - _timeOrcount;
                    // if I need more than there is then get everything above -1 (which includes 0)
                    if (above < 0)
                        above = -1;

                    // the result should be that if someone askes for the last 3 measurements
                    // we should return 2 here because a new one has come in which isn't in the array
                    var resultMeasurements = measurements.OrderBy(item => item.Value).Where((item, index) => index > above);

                    return resultMeasurements.ToDictionary(item => item.Key, item => item.Value);
            }

            return null;
        }

    }
}
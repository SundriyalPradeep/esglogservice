﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using NLog;
using VitalsService.Domain.DbEntities;
using VitalsService.Domain.Dtos;
using VitalsService.Domain.Dtos.MessagingHub;
using VitalsService.Domain.Enums;
using VitalsService.Domain.Enums.MessagingHub;
using VitalsService.DomainLogic.Services.Interfaces;
using VitalsService.Web.Api.Helpers.Interfaces;
using VitalsService.Web.Api.Models;
using VitalsService.Web.Api.Models.Thresholds;
using VitalsService.Web.Api.Models.Conditions;

namespace VitalsService.Web.Api.Helpers.Implementation
{
    /// <summary>
    /// EnhancedThresholdsControllerHelper.
    /// </summary>
    public class EnhancedThresholdsControllerHelper : IEnhancedThresholdsControllerHelper
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IEnhancedThresholdsService _enhancedthresholdsService;
        private readonly IDefaultThresholdsService _defaultThresholdsService;
        private readonly IAlertSeveritiesService _alertSeveritiesService;
        private readonly IThresholdAggregator _thresholdAggregator;
        private readonly IMessagingHubService _messagingHubService;
        private readonly IPatientConditionsService _patientsConditionsService;
        private readonly IConditionsService _conditionsService;
        private readonly IMapper _mapper;
        ///// <summary>
        ///// 
        ///// </summary>
        //public EnhancedThresholdsControllerHelper()
        //{

        //}

        /// <summary>
        /// Initializes a new instance of the <see cref="EnhancedThresholdsControllerHelper" /> class.
        /// </summary>
        /// <param name="enhancedThresholdsService">The Enhanced thresholds service.</param>
        /// <param name="defaultThresholdsService">The default thresholds service.</param>
        /// <param name="alertSeveritiesService">The alert severities service.</param>
        /// <param name="thresholdAggregator"></param>
        /// <param name="messagingHubService"></param>
        /// <param name="patientsConditionsService"></param>
        /// <param name="conditionsService"></param>
        /// <param name="mapper"></param>
        public EnhancedThresholdsControllerHelper(
            IEnhancedThresholdsService enhancedThresholdsService,
            IDefaultThresholdsService defaultThresholdsService,
            IAlertSeveritiesService alertSeveritiesService,
            IThresholdAggregator thresholdAggregator,
            IMessagingHubService messagingHubService,
            IPatientConditionsService patientsConditionsService,
            IConditionsService conditionsService,
            IMapper mapper
        )
        {
            _enhancedthresholdsService = enhancedThresholdsService;
            _defaultThresholdsService = defaultThresholdsService;
            _alertSeveritiesService = alertSeveritiesService;
            _thresholdAggregator = thresholdAggregator;
            _messagingHubService = messagingHubService;
            _patientsConditionsService = patientsConditionsService;
            _conditionsService = conditionsService;
            _mapper = mapper;
        }


        /// <summary>
        /// Creates the threshold.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public async Task<OperationResultDto<Guid, CreateUpdateEnhancedThresholdStatus>>
            CreateEnhancedThreshold(int customerId, EnhancedThresholdRequestDto request)
        {
            var enthreshold = _mapper.Map<EnhancedThresholdRequestDto, EnhancedThreshold>(request);
            enthreshold.CustomerId = customerId;

            var enthresholdRange = await _enhancedthresholdsService.GetEnhancedThresholdRange(request.RangeType.ToString());

            var setAnalysis = await _enhancedthresholdsService.GetEnhancedThresholdSetAnalysis(request.SetAnalysisType.GetHashCode());
            if (enthresholdRange != null && setAnalysis != null)
            {
                enthreshold.EThresholdRanges = enthresholdRange;
                enthreshold.EThresholdSetAnalysis = setAnalysis;
            }

            if (request.Type == EnhancedThresholdType.Organization || request.Type == EnhancedThresholdType.All)
            {
                var result = await _enhancedthresholdsService.CreateEnhancedThreshold(enthreshold);
                return result;
            }
            else if (request.Type == EnhancedThresholdType.Condition)
            {
                var enthresholdCondition = _mapper.Map<EnhancedThresholdRequestDto, EnhancedThresholdCondition>(request);
                enthresholdCondition.CustomerId = customerId;

                var enthresholdConditionResult = await _enhancedthresholdsService.CreateEnhancedThresholdCondition(enthresholdCondition);

                var etCondition = await _enhancedthresholdsService.GetEnhancedThresholdCondition(enthresholdConditionResult.Content);
                if (etCondition != null)
                {
                    enthreshold.ETConditions = etCondition;
                }

                var result = await _enhancedthresholdsService.CreateEnhancedThreshold(enthreshold);
                return result;
            }
            else if (request.Type == EnhancedThresholdType.Patient)
            {
                if (request.ConditionId != null)
                {
                    var enthresholdCondition = _mapper.Map<EnhancedThresholdRequestDto, EnhancedThresholdCondition>(request);
                    enthresholdCondition.CustomerId = customerId;

                    var enthresholdConditionResult = await _enhancedthresholdsService.CreateEnhancedThresholdCondition(enthresholdCondition);

                    var etCondition = await _enhancedthresholdsService.GetEnhancedThresholdCondition(enthresholdConditionResult.Content);
                    if (etCondition != null)
                    {
                        enthreshold.ETConditions = etCondition;
                    }
                }

                var enthresholdPatient = _mapper.Map<EnhancedThresholdRequestDto, EnhancedThresholdPatient>(request);
                enthresholdPatient.CustomerId = customerId;

                var enthresholdPatientResult = await _enhancedthresholdsService.CreateEnhancedThresholdPatient(enthresholdPatient);

                var etPatient = await _enhancedthresholdsService.GetEnhancedThresholdPatient(enthresholdPatientResult.Content);
                if (etPatient != null)
                {
                    enthreshold.ETPatients = etPatient;
                }


                var result = await _enhancedthresholdsService.CreateEnhancedThreshold(enthreshold);
                return result;
            }
            else
            {
                return await Task.FromResult(
                  new OperationResultDto<Guid, CreateUpdateEnhancedThresholdStatus>()
                  {
                      Status = CreateUpdateEnhancedThresholdStatus.RequestInvalid,
                      Content = enthreshold.Id
                  }
              );
            }
        }

        /// <summary>
        /// Updates the threshold.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="enhancedthresholdId">The Enhancedthreshold identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public async Task<CreateUpdateEnhancedThresholdStatus> UpdateEnhancedThreshold(
            int customerId,
            Guid enhancedthresholdId,
            EnhancedThresholdRequestDto request
        )
        {

            var enthreshold = _mapper.Map<EnhancedThresholdRequestDto, EnhancedThreshold>(request);
            enthreshold.CustomerId = customerId;
            enthreshold.Id = enhancedthresholdId;

            var enthresholdRange = await _enhancedthresholdsService.GetEnhancedThresholdRange(request.RangeType.ToString());

            var setAnalysis = await _enhancedthresholdsService.GetEnhancedThresholdSetAnalysis(request.SetAnalysisType.GetHashCode());
            if (enthresholdRange != null && setAnalysis != null)
            {
                enthreshold.EThresholdRanges = enthresholdRange;
                enthreshold.EThresholdSetAnalysis = setAnalysis;
            }

            var existingenthreshold = await _enhancedthresholdsService.GetEnhancedThreshold(customerId, enhancedthresholdId);

            enthreshold.ETConditions = existingenthreshold.ETConditions;
            enthreshold.ETPatients = existingenthreshold.ETPatients;
            enthreshold.CreatedDateUTC = existingenthreshold.CreatedDateUTC;

            _mapper.Map(enthreshold, existingenthreshold, typeof(EnhancedThreshold), typeof(EnhancedThreshold));

            var result = await _enhancedthresholdsService.UpdateEnhancedThreshold(enthreshold);

            return result;

        }

        /// <summary>
        /// Deletes the enhanced threshold.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="enhancedThresholdId">The Enhancedthreshold identifier.</param>
        /// <returns></returns>
        public async Task<GetDeleteEnhancedThresholdStatus> DeleteEnhancedThreshold(int customerId, Guid enhancedThresholdId)
        {
            var enthreshold = await _enhancedthresholdsService.GetEnhancedThreshold(customerId, enhancedThresholdId);

            var deleteEThresholdResult = await _enhancedthresholdsService.DeleteEnhancedThreshold(customerId, enhancedThresholdId);

            if (deleteEThresholdResult != GetDeleteEnhancedThresholdStatus.Success)
                return deleteEThresholdResult;

            if (enthreshold.ETConditions != null)
            {
                var deleteEThresholdConditionResult = await _enhancedthresholdsService.DeleteEnhancedThresholdCondition(enthreshold.ETConditions.Id);
            }
            else if (enthreshold.ETPatients != null)
            {
                var deleteEThresholdPatientResult = await _enhancedthresholdsService.DeleteEnhancedThresholdPatient(enthreshold.ETPatients.Id);
            }

            return deleteEThresholdResult;
        }

        /// <summary>
        /// Gets the Enhanced threshold.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="EnhancedthresholdId">The Enhancedthreshold identifier.</param>
        /// <returns></returns>
        public async Task<OperationResultDto<BaseEnhancedThresholdDto, GetDeleteEnhancedThresholdStatus>>
            GetEnhancedThreshold(int customerId, Guid EnhancedthresholdId)
        {
            var result = await _enhancedthresholdsService.GetEnhancedThreshold(customerId, EnhancedthresholdId);
            var ETContent = _mapper.Map<EnhancedThreshold, BaseEnhancedThresholdDto>(result);
            if (ETContent.ETConditions != null)
            {
                if (ETContent.ETConditions.ConditionId != null)
                {
                    var conditions_ET = await _conditionsService.GetCondition(customerId, ETContent.ETConditions.ConditionId);
                    ETContent.Conditions = _mapper.Map<Condition, ConditionResponseDto>(conditions_ET);
                }

            }


            if (result == null)
            {
                return await Task.FromResult(
                    new OperationResultDto<BaseEnhancedThresholdDto, GetDeleteEnhancedThresholdStatus>()
                    {
                        Status = GetDeleteEnhancedThresholdStatus.NotFound
                    }
                );
            }

            return await Task.FromResult(
                new OperationResultDto<BaseEnhancedThresholdDto, GetDeleteEnhancedThresholdStatus>()
                {
                    Status = GetDeleteEnhancedThresholdStatus.Success,
                    Content = ETContent
                }
            );
        }

        /// <summary>
        /// Gets the Enhanced thresholds.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public async Task<PagedResultDto<BaseEnhancedThresholdDto>> GetEnhancedThresholds(int customerId, EnhancedThresholdsSearchDto request)
        {
            //var customerAlertSeverities = (await _alertSeveritiesService.GetAlertSeverities(customerId, null)).Results;
            if (request.Mode == null)
            {
                request.Mode = EnhancedThresholdType.All;
            }
            var enhancedThresholdsPagedResult = await _enhancedthresholdsService.GetEnhancedThresholds(customerId, request, request.Mode.Value);



            //var filteredPatientThresholds = FilterUnusedThresholds(
            //    _mapper.Map<IList<EnhancedThreshold>, IList<BaseEnhancedThresholdDto>>(enhancedThresholdsPagedResult.Results).Cast<BaseEnhancedThresholdDto>().ToList());
            var baseEnhancedThresholdsPagedResult = _mapper.Map<IList<EnhancedThreshold>, IList<BaseEnhancedThresholdDto>>(enhancedThresholdsPagedResult.Results).Cast<BaseEnhancedThresholdDto>().ToList();
            //var temp = from baseList in baseEnhancedThresholdsPagedResult
            //           join listET in enhancedThresholdsPagedResult.Results on
            //               new { baseList.Id }
            //           equals
            //               new { listET.Id }
            //           into list2Joined
            //           from l2 in list2Joined.DefaultIfEmpty()
            //           select new BaseEnhancedThresholdDto
            //           {
            //              Conditions = await _conditionsService.GetCondition(customerId, l2.ETConditions.ConditionId); 
            //           }).ToList()
            foreach (var enThreshold in baseEnhancedThresholdsPagedResult)
            {
                if (enThreshold.ETConditions != null)
                {
                    if (enThreshold.ETConditions.ConditionId != null)
                    {
                        var conditions_ET = await _conditionsService.GetCondition(customerId, enThreshold.ETConditions.ConditionId);
                        enThreshold.Conditions = _mapper.Map<Condition, ConditionResponseDto>(conditions_ET);
                    }
                }
            }

            return new PagedResultDto<BaseEnhancedThresholdDto>
            {
                Results = baseEnhancedThresholdsPagedResult,
                Total = enhancedThresholdsPagedResult.Total
            };


        }

        /// <summary>
        /// Gets the Enhanced thresholds Alerts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        public async Task<PagedResultDto<EnhancedThresholdAlertDto>> GetEnhancedThresholdAlerts(int customerId)
        {

            var enhancedThresholdsPagedResult = await _enhancedthresholdsService.GetEnhancedThresholdAlerts(customerId);
                      
            var baseEnhancedThresholdsPagedResult = _mapper.Map<IList<EnhancedThresholdAlerts>, IList<EnhancedThresholdAlertDto>>(enhancedThresholdsPagedResult.Results).Cast<EnhancedThresholdAlertDto>().ToList();
            foreach (var enThresholdAlert in baseEnhancedThresholdsPagedResult)
            {
                if (enThresholdAlert.EnhancedThreshold.ETConditions != null)
                {
                    if (enThresholdAlert.EnhancedThreshold.ETConditions.ConditionId != null)
                    {
                        var conditions_ET = await _conditionsService.GetCondition(customerId, enThresholdAlert.EnhancedThreshold.ETConditions.ConditionId);
                        enThresholdAlert.EnhancedThreshold.Conditions = _mapper.Map<Condition, ConditionResponseDto>(conditions_ET);
                    }
                }
            }

            return new PagedResultDto<EnhancedThresholdAlertDto>
            {
                Results = baseEnhancedThresholdsPagedResult,
                Total = enhancedThresholdsPagedResult.Total
            };

        }

        /// <summary>
        /// Acknowledges the alerts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public async Task<CreateUpdateAlertStatus> AcknowledgeEnThAlerts(int customerId, AcknowledgeETAlertsRequestDto request)
        {
            var distinctAlertsIds = request.ETAlertIds.Distinct().ToList();

            return await _enhancedthresholdsService.AcknowledgeEnThAlerts(
                customerId,
                request.AcknowledgedBy ?? Guid.Empty,
                distinctAlertsIds
            );
        }


        /// <summary>
        /// UnAcknowledges the alerts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public async Task<CreateUpdateAlertStatus> UnAcknowledgeEnThAlerts(int customerId, AcknowledgeETAlertsRequestDto request)
        {
            var distinctAlertsIds = request.ETAlertIds.Distinct().ToList();

            return await _enhancedthresholdsService.UnAcknowledgeEnThAlerts(
                customerId,
                request.AcknowledgedBy ?? Guid.Empty,
                distinctAlertsIds
            );
        }

        /// <summary>
        /// Ignore the ET alerts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public async Task<CreateUpdateAlertStatus> IgnoreEnThresholdAlerts(int customerId, IgnoreETAlertsRequestDto request)
        {
            var distinctAlertsIds = request.ETAlertIds.Distinct().ToList();

            return await _enhancedthresholdsService.IgnoreEnhancedThresholdAlert(
                customerId,
                distinctAlertsIds
            );
        }

        /// <summary>
        /// Activate/Deactivate Customer/Condition Level Enhanced threshold for any patient.
        /// </summary>
        /// <param name="customerId">The customerId.</param>
        /// <param name="patientActivateETRequestDto">The Patient Activate ET RequestDto.</param>
        /// <returns></returns>
        public async Task<ActivateDeactivateETStatus> UpdateETActiveState(int customerId, PatientActivateETRequestDto patientActivateETRequestDto)
        {
            var patientActiveEt = _mapper.Map<PatientActivateETRequestDto, PatientActiveETs>(patientActivateETRequestDto);
            return await _enhancedthresholdsService.UpdateETActiveState(customerId, patientActiveEt);
        }

        ///<summary>
        /// Get Active/Inactive Customer/Condition Level ET's for  any patient.
        /// </summary>
        /// <param name="customerId">The customerId.</param>
        /// <param name="patientId">The Patient identifier</param>
        /// <returns></returns>
        public async Task<PagedResultDto<PatientActiveETResponseDto>> GetAllETsPatient(int customerId, Guid patientId)
        {
            var patientActiveETPageResult = await _enhancedthresholdsService.GetAllETsPatient(patientId);
            var patientActiveETResponseDtoResult = _mapper.Map<IList<PatientActiveETs>, IList<PatientActiveETResponseDto>>(patientActiveETPageResult.Results).Cast<PatientActiveETResponseDto>().ToList();

            return new PagedResultDto<PatientActiveETResponseDto>
            {
                Results = patientActiveETResponseDtoResult,
                Total = patientActiveETPageResult.Total
            };
        }

    }
}
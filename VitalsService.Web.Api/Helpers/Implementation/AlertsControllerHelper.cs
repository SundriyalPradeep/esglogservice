﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using Newtonsoft.Json;
using NLog;
using VitalsService.Domain.DbEntities;
using VitalsService.Domain.DomainObjects;
using VitalsService.Domain.Dtos;
using VitalsService.Domain.Enums;
using VitalsService.DomainLogic.Services.Interfaces;
using VitalsService.Web.Api.Helpers.Interfaces;
using VitalsService.Web.Api.Models;
using VitalsService.Web.Api.Models.Alerts;

namespace VitalsService.Web.Api.Helpers.Implementation
{
    /// <summary>
    /// AlertsControllerHelper.
    /// </summary>
    public class AlertsControllerHelper : IAlertsControllerHelper
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly IAlertsService _alertsService;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="AlertsControllerHelper"/> class.
        /// </summary>
        /// <param name="alertsService">The alerts service.</param>
        /// <param name="mapper"></param>
        public AlertsControllerHelper(IAlertsService alertsService, IMapper mapper)
        {
            _alertsService = alertsService;
            _mapper = mapper;
        }

        /// <summary>
        /// Creates the alert.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public async Task<OperationResultDto<Guid, CreateUpdateAlertStatus>> CreateAlert(int customerId, AlertRequestDto request)
        {
            var alert = _mapper.Map<AlertRequestDto, Alert>(request);
            alert.CustomerId = customerId;

            return await _alertsService.CreateAlert(alert);
        }

        /// <summary>
        /// Create an alert from a Home Sensing notification.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="requestDto"></param>
        /// <param name="requestMsg"></param>
        /// <returns></returns>
        public async Task<OperationResultDto<Guid, CreateUpdateAlertStatus>> CreateHomeSensingAlert(int customerId, HomeSensingAlertRequestDto requestDto, HttpRequestMessage requestMsg)
        {
            var homeSensingAlert = _mapper.Map<HomeSensingAlertRequestDto, HomeSensingAlert>(requestDto);
            homeSensingAlert.CustomerId = customerId;
            homeSensingAlert.UnknownPropertiesJson = await GetUnknownHomeSensingRequestPropertiesAndValuesAsJsonAsync(requestDto, requestMsg);
            
            return await _alertsService.CreateAlert(homeSensingAlert);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<CreateUpdateAlertStatus> IgnoreHomeSensingAlert(int customerId, IgnoreHomeSensingAlertRequestDto request)
        {
            return await _alertsService.IgnoreHomeSensingAlert(customerId, request.IgnoredBy ?? Guid.Empty, request.AlertId ?? Guid.Empty);
        }

        /// <summary>
        /// Acknowledges the alerts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public async Task<CreateUpdateAlertStatus> AcknowledgeAlerts(int customerId, AcknowledgeAlertsRequestDto request)
        {
            var distinctAlertsIds = request.AlertIds.Distinct().ToList();

            return await _alertsService.AcknowledgeAlerts(
                customerId, 
                request.AcknowledgedBy ?? Guid.Empty,
                distinctAlertsIds
            );
        }


        /// <summary>
        /// UnAcknowledges the alerts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public async Task<CreateUpdateAlertStatus> UnAcknowledgeAlerts(int customerId, AcknowledgeAlertsRequestDto request)
        {
            var distinctAlertsIds = request.AlertIds.Distinct().ToList();

            return await _alertsService.UnAcknowledgeAlerts(
                customerId,
                request.AcknowledgedBy ?? Guid.Empty,
                distinctAlertsIds
            );
        }

        /// <summary>
        /// Gets the alerts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public PagedResultDto<PatientAlertsDto> GetAlerts(int customerId, AlertsSearchDto request)
        {
            if (request != null && request.Take <= 0)
            {
                request.Take = 1000;
            }

            var result = _alertsService.GetAlerts(customerId, request);

            return _mapper.Map<PagedResult<PatientAlerts>, PagedResultDto<PatientAlertsDto>>(result,
                o => o.Items.Add("isBrief", request == null || request.IsBrief));
        }


        /// <summary>
        /// We need to capture Home Sensing Notification properties that may be present, but are currently unknown to 
        /// Maestro. On initial integration, we're only expecting notificationType, threshold, observed, and moreInfoUrl.
        /// </summary>
        /// <param name="requestDto"></param>
        /// <param name="requestMsg"></param>
        /// <returns></returns>
        private async Task<string> GetUnknownHomeSensingRequestPropertiesAndValuesAsJsonAsync(HomeSensingAlertRequestDto requestDto, HttpRequestMessage requestMsg)
        {
            try
            {
                var unknownRequestPropertiesAndValues = new Dictionary<string, object>();

                // Get the public properties of the Home Sensing alert request. These are the known properties, and we can ignore matching
                //   properties in the json.
                // An early version of the API design mistakenly listed customerId as a required property of the request body. This is unnecessary
                //   because customerId is already in the URL, but in the off chance that Home Sensing sends it anyway, exclude it by adding it to
                //   the "known properties" list.
                var knownProperties = requestDto.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance).Select(pi => pi.Name).ToList();
                knownProperties.Add("customerId");

                using (var contentStream = await requestMsg.Content.ReadAsStreamAsync())
                {
                    // The stream has already been read during model binding. Reset it so that we can read the body contents ourselves.
                    contentStream.Seek(0, SeekOrigin.Begin);

                    using (var sr = new StreamReader(contentStream))
                    {
                        // Get the JSON request as a string.
                        var jsonBody = await sr.ReadToEndAsync();

                        // Deserialize it into a dynamic object. Since we could receive unexpected/unknown home sensing notification properties, 
                        //   we can't use a statically-typed object.
                        dynamic requestObjDyn = JsonConvert.DeserializeObject(jsonBody);

                        // Get the names of the properties submitted via JSON.
                        var tTarget = (IDynamicMetaObjectProvider)requestObjDyn;
                        var dynamicMemberNames = tTarget.GetMetaObject(Expression.Constant(tTarget)).GetDynamicMemberNames().ToArray();

                        // If the property name is not currently known to Maestro, we need to store it and its value so that we populate
                        //   the database columns when we add them later. I don't think this is likely, but we don't want to lose any data.
                        //   Maestro and Home Sensing are orthogonal products with different release cycles, so it's possible that they will add 
                        //   notifications that send properties other than those we're expecting.
                        // Again, I expect they would let us know ahead of time, and I don't think this is likely, but we don't want to lose any data.
                        foreach (var dynamicMemberName in dynamicMemberNames)
                        {
                            if (knownProperties.Contains(dynamicMemberName, StringComparer.OrdinalIgnoreCase))
                            {
                                // We already know this property, and we have a way to store it. Ignore it.
                                continue;
                            }

                            // In the event of the same property appearing twice in the JSON request body, the last value wins.
                            unknownRequestPropertiesAndValues.Add(dynamicMemberName, requestObjDyn[dynamicMemberName]);
                        }
                    }
                }

                if (unknownRequestPropertiesAndValues.Count > 0)
                {
                    // Convert the properties/values to a JSON object, serialized as a string.
                    return JsonConvert.SerializeObject(unknownRequestPropertiesAndValues);
                }

                // No unknown properties found.
                return null;
            }
            catch(Exception ex)
            {
                Logger.Error(ex, "Failure when trying to retrieve potentially unknown Home Sensing Notification properties from the request body");

                // We want to know about this, but we don't want it to cause an application failure.
                return null;
            }
        }
    }
}
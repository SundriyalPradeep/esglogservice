﻿using System;
using VitalsService.Web.Api.Helpers.Interfaces;
using System.Threading.Tasks;
using AutoMapper;
using VitalsService.Web.Api.Models.ESGLogger;
using VitalsService.DomainLogic.Services.Interfaces;
using VitalsService.Domain.DbEntities;

namespace VitalsService.Web.Api.Helpers.Implementation
{

    /// <summary>
    /// 
    /// </summary>
    public class EsgMessageControllerHelper : IEsgMessageControllerHelper
    {
        private readonly IMessageLogService _messageService;
        private readonly IMapper _mapper;
        //IMessageLogManager iManager, IMapper iMapper
        /// <summary>
        /// 
        /// </summary>
        public EsgMessageControllerHelper() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iMapper"></param>
        /// <param name="messageService"></param>
        public EsgMessageControllerHelper(IMapper iMapper, IMessageLogService messageService)
        {
            if (iMapper == null)
                throw new ArgumentNullException("Mapper cannot be null");

            if (messageService == null)
                throw new ArgumentNullException("MessageLogDal cannot be null");

            _mapper = iMapper;
            _messageService = messageService;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>

        public async Task<Guid> LogMessage(TransactionMessageDTO message)
        {
            //return await _messageSer
            var entity = _mapper.Map<ESGMessageTransaction>(message);
            // var entity1 = _mapper.Map<ESGMessageContent>(message);

            var result = await _messageService.LogMessage(entity);
            //var result1 = await _messageService.LogMessage(entity1);

            ESGMessageContent esgMessageContent = new ESGMessageContent()
            { TransactionId = result, MessageBody = message.MessageBody };
            var result1 = await _messageService.StoreMessageBody(esgMessageContent);

            return result;
            //return Guid.NewGuid();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task<Guid> LogMessageAck(ESGAcknowledgementDTO message)
        {
            //return await _messageSer
            var entity = _mapper.Map<ESGMessageAcknowledgement>(message);
            var result = await _messageService.LogMessageAck(entity);

            ESGMessageContent esgMessageContent = new ESGMessageContent()
            { TransactionId = result, MessageBody = message.MessageBody };
            var result1 = await _messageService.StoreMessageBody(esgMessageContent);

            return result;
            //return Guid.NewGuid();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="MessageControlid"></param>
        /// <returns></returns>
        public async Task<Guid> GetTransactionId(string MessageControlid)
        {
            var resultset = await _messageService.GetTransactionId(MessageControlid);
            return resultset;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ICN"></param>
        /// <param name="DFN"></param>
        /// <param name="MessageType"></param>
        /// <returns></returns>
        //public async Task<ESGMessageContent> GetMessageContent(string ICN, string DFN, string MessageType)
        //{
        //    var result = await _messageService.GetMessageContent(ICN, DFN, MessageType);
        //    return result;

        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ICN"></param>
        /// <param name="SSN"></param>
        /// <param name="MessageType"></param>
        /// <returns></returns>
        public async Task<string> GetDFN(string ICN, string SSN, string MessageType)
        {
            var result = await _messageService.GetDFN(ICN, SSN, MessageType);
            return result;

        }
        public async Task<string> GetEnrollmentDate(string ICN)
        {
            var result = await _messageService.GetEnrollmentDate(ICN);
            return result;

        }

        public async Task<ESGMessageContent> GetRegistrationMessage(string QBPMessageControlId)
        {
            var result = await _messageService.GetRegistrationMessage(QBPMessageControlId);
            return result;
        }
    }
}
﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using VitalsService.Domain.DbEntities;
using VitalsService.Domain.Dtos;
using VitalsService.Domain.Enums;
using VitalsService.DomainLogic.Services.Interfaces;
using VitalsService.Web.Api.Helpers.Interfaces;
using VitalsService.Web.Api.Models;
using VitalsService.Web.Api.Models.AlertSeverities;

namespace VitalsService.Web.Api.Helpers.Implementation
{
    /// <summary>
    /// AlertSeveritiesControllerHelper.
    /// </summary>
    /// <seealso cref="VitalsService.Web.Api.Helpers.Interfaces.IAlertSeveritiesControllerHelper" />
    public class AlertSeveritiesControllerHelper : IAlertSeveritiesControllerHelper
    {
        private readonly IAlertSeveritiesService _alertSeveritiesService;
        private readonly IMapper _mapper;


        /// <summary>
        /// Initializes a new instance of the <see cref="AlertSeveritiesControllerHelper" /> class.
        /// </summary>
        /// <param name="alertSeveritiesService">The alert severities service.</param>
        /// <param name="mapper"></param>
        public AlertSeveritiesControllerHelper(IAlertSeveritiesService alertSeveritiesService, IMapper mapper)
        {
            _alertSeveritiesService = alertSeveritiesService;
            _mapper = mapper;
        }

        /// <summary>
        /// Creates the alert severity.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public async Task<Guid> CreateAlertSeverity(int customerId, AlertSeverityRequestDto request)
        {
            var alertSeverityEntity = _mapper.Map<AlertSeverity>(request);
            alertSeverityEntity.CustomerId = customerId;

            var result = await _alertSeveritiesService.CreateAlertSeverity(alertSeverityEntity);

            return result.Id;
        }

        /// <summary>
        /// Updates the alert severity.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public async Task<CreateUpdateDeleteAlertSeverityStatus> UpdateAlertSeverity(Guid id, int customerId, AlertSeverityRequestDto request)
        {
            var alertSeverityEntity = _mapper.Map<AlertSeverity>(request);
            alertSeverityEntity.Id = id;
            alertSeverityEntity.CustomerId = customerId;

            return await this._alertSeveritiesService.UpdateAlertSeverity(alertSeverityEntity);
        }

        /// <summary>
        /// Deletes the alert severity.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<CreateUpdateDeleteAlertSeverityStatus> DeleteAlertSeverity(int customerId, Guid id)
        {
            return await _alertSeveritiesService.DeleteAlertSeverity(customerId, id);
        }

        /// <summary>
        /// Gets the alert severity.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<OperationResultDto<AlertSeverityResponseDto, GetAlertSeverityStatus>> GetAlertSeverity(int customerId, Guid id)
        {
            var operationResult = await _alertSeveritiesService.GetAlertSeverity(customerId, id);

            return new OperationResultDto<AlertSeverityResponseDto, GetAlertSeverityStatus>()
            {
                Content = _mapper.Map<AlertSeverityResponseDto>(operationResult.Content),
                Status = operationResult.Status
            };
        }

        /// <summary>
        /// Gets the alert severities.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="searchRequest">The search request.</param>
        /// <returns></returns>
        public async Task<PagedResultDto<AlertSeverityResponseDto>> GetAlertSeverities(int customerId, AlertSeveritiesSearchDto searchRequest)
        {
            var alertSeverities = await _alertSeveritiesService.GetAlertSeverities(customerId, searchRequest);

            return _mapper.Map<PagedResult<AlertSeverity>, PagedResultDto<AlertSeverityResponseDto>>(alertSeverities);
        }
    }
}
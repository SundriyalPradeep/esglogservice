﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using NLog;
using VitalsService.Domain.DbEntities;
using VitalsService.Domain.Dtos;
using VitalsService.Domain.Dtos.MessagingHub;
using VitalsService.Domain.Enums;
using VitalsService.Domain.Enums.MessagingHub;
using VitalsService.DomainLogic.Services.Interfaces;
using VitalsService.Web.Api.Helpers.Interfaces;
using VitalsService.Web.Api.Models;
using VitalsService.Web.Api.Models.Thresholds;
using VitalsService.Web.Api.Models.AlertSeverities;

namespace VitalsService.Web.Api.Helpers.Implementation
{
    /// <summary>
    /// ThresholdsControllerHelper.
    /// </summary>
    public class ThresholdsControllerHelper : IThresholdsControllerHelper
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private readonly IThresholdsService _thresholdsService;
        private readonly IDefaultThresholdsService _defaultThresholdsService;
        private readonly IAlertSeveritiesService _alertSeveritiesService;
        private readonly IThresholdAggregator _thresholdAggregator;
        private readonly IMessagingHubService _messagingHubService;
        private readonly IPatientConditionsService _patientsConditionsService;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="ThresholdsControllerHelper" /> class.
        /// </summary>
        /// <param name="thresholdsService">The thresholds service.</param>
        /// <param name="defaultThresholdsService">The default thresholds service.</param>
        /// <param name="alertSeveritiesService">The alert severities service.</param>
        /// <param name="thresholdAggregator"></param>
        /// <param name="messagingHubService"></param>
        /// <param name="patientsConditionsService"></param>
        /// <param name="mapper"></param>
        public ThresholdsControllerHelper(
            IThresholdsService thresholdsService,
            IDefaultThresholdsService defaultThresholdsService,
            IAlertSeveritiesService alertSeveritiesService,
            IThresholdAggregator thresholdAggregator,
            IMessagingHubService messagingHubService,
            IPatientConditionsService patientsConditionsService,
            IMapper mapper
        )
        {
            _thresholdsService = thresholdsService;
            _defaultThresholdsService = defaultThresholdsService;
            _alertSeveritiesService = alertSeveritiesService;
            _thresholdAggregator = thresholdAggregator;
            _messagingHubService = messagingHubService;
            _patientsConditionsService = patientsConditionsService;
            _mapper = mapper;
        }

        /// <summary>
        /// Creates the threshold.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public async Task<OperationResultDto<Guid, CreateUpdateThresholdStatus>>
            CreateThreshold(int customerId, Guid patientId, ThresholdRequestDto request)
        {
            var threshold = _mapper.Map<ThresholdRequestDto, PatientThreshold>(request);
            threshold.CustomerId = customerId;
            threshold.PatientId = patientId;

            var patientthresholdtype = _mapper.Map<ThresholdRequestDto, PatientThresholdType>(request);
            patientthresholdtype.CustomerId = customerId;
            patientthresholdtype.PatientId = patientId;
            patientthresholdtype.IsDeleted = false;

            try
            {
                var patientthresholdtyperesult = await _thresholdsService.UpdatePatientThresholdType(patientthresholdtype);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Failure under create threshold helper for UpdatePatientThresholdType");
            }
            var result = await _thresholdsService.CreateThreshold(threshold);

            if (result.Status == CreateUpdateThresholdStatus.Success)
            {
                //if (patientthresholdtyperesult == CreateUpdateThresholdStatus.Success)
                //{
                //    await _messagingHubService.SendPushNotification(new NotificationDto()
                //    {
                //        AllTags = true,
                //        Data = new
                //        {
                //            PatientDeviceNotification = new
                //            {
                //                Action = "PatientThresholdsChanged",
                //                CustmerId = threshold.CustomerId,
                //                PatientId = threshold.Id
                //            }
                //        },
                //        Message = null,
                //        Sender = string.Format("maestro-customer-{0}", threshold.CustomerId),
                //        Tags = new List<string> { string.Format("maestro-customer-{0}", threshold.CustomerId),
                //                              string.Format("maestro-patientId-{0}", threshold.PatientId)
                //                            },
                //        Types = new[] { RegistrationType.APN, RegistrationType.GCM }
                //    });
                //}
            }

            return result;
        }

        /// <summary>
        /// Updates the threshold.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="thresholdId">The threshold identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public async Task<CreateUpdateThresholdStatus> UpdateThreshold(
            int customerId,
            Guid patientId,
            Guid thresholdId,
            ThresholdRequestDto request
        )
        {
            var threshold = _mapper.Map<ThresholdRequestDto, PatientThreshold>(request);
            threshold.Id = thresholdId;
            threshold.CustomerId = customerId;
            threshold.PatientId = patientId;

            var patientthresholdtype = _mapper.Map<ThresholdRequestDto, PatientThresholdType>(request);
            patientthresholdtype.CustomerId = customerId;
            patientthresholdtype.PatientId = patientId;
            patientthresholdtype.IsDeleted = false;

            try
            {
                var patientthresholdtyperesult = await _thresholdsService.UpdatePatientThresholdType(patientthresholdtype);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Failure under create threshold helper for UpdatePatientThresholdType");
            }
          

            var result = await _thresholdsService.UpdateThreshold(threshold);

            if (result == CreateUpdateThresholdStatus.Success)
            {
                //var patientthresholdtyperesult = await _thresholdsService.UpdatePatientThresholdType(patientthresholdtype);
                //if (patientthresholdtyperesult == CreateUpdateThresholdStatus.Success)
                //{
                //    try
                //    {
                //        await _messagingHubService.SendPushNotification(new NotificationDto()
                //        {
                //            AllTags = true,
                //            Data = new
                //            {
                //                PatientDeviceNotification = new
                //                {
                //                    Action = "PatientThresholdsChanged",
                //                    CustmerId = threshold.CustomerId,
                //                    PatientId = threshold.PatientId
                //                }
                //            },
                //            Message = null,
                //            Sender = string.Format("maestro-customer-{0}", threshold.CustomerId),
                //            Tags = new List<string> { string.Format("maestro-customer-{0}", threshold.CustomerId),
                //                                  string.Format("maestro-patientId-{0}", threshold.PatientId)
                //                                },
                //            Types = new[] { RegistrationType.APN, RegistrationType.GCM }
                //        });
                //    }
                //    catch (Exception ex)
                //    {
                //        logger.Error(ex, "An error occured when try to SendPushNotification: PatientThresholdsChanged");
                //    }
                //}

            }

            return result;
        }

        /// <summary>
        /// Deletes the threshold.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="thresholdId">The threshold identifier.</param>
        /// <returns></returns>
        public async Task<GetDeleteThresholdStatus> DeleteThreshold(int customerId, Guid patientId, Guid thresholdId)
        {
            var deleteThresholdResult = await _thresholdsService.DeleteThreshold(customerId, patientId, thresholdId);

            if (deleteThresholdResult == GetDeleteThresholdStatus.Success)
            {
                await _messagingHubService.SendPushNotification(new NotificationDto()
                {
                    AllTags = true,
                    Data = new
                    {
                        PatientDeviceNotification = new
                        {
                            Action = "PatientThresholdsChanged",
                            CustmerId = customerId,
                            PatientId = patientId
                        }
                    },
                    Message = null,
                    Sender = string.Format("maestro-customer-{0}", customerId),
                    Tags = new List<string> { string.Format("maestro-customer-{0}", customerId),
                                              string.Format("maestro-patientId-{0}", patientId)
                                            },
                    Types = new[] { RegistrationType.APN, RegistrationType.GCM }
                });
            }

            return deleteThresholdResult;
        }

        /// <summary>
        /// Gets the threshold.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="thresholdId">The threshold identifier.</param>
        /// <returns></returns>
        public async Task<OperationResultDto<PatientThresholdDto, GetDeleteThresholdStatus>>
            GetThreshold(int customerId, Guid patientId, Guid thresholdId)
        {
            var result = await _thresholdsService.GetThreshold(customerId, patientId, thresholdId);

            if (result == null)
            {
                return await Task.FromResult(
                    new OperationResultDto<PatientThresholdDto, GetDeleteThresholdStatus>()
                    {
                        Status = GetDeleteThresholdStatus.NotFound
                    }
                );
            }

            return await Task.FromResult(
                new OperationResultDto<PatientThresholdDto, GetDeleteThresholdStatus>()
                {
                    Status = GetDeleteThresholdStatus.Success,
                    Content = _mapper.Map<PatientThreshold, PatientThresholdDto>(result)
                }
            );
        }

        /// <summary>
        /// Gets the thresholds.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public async Task<PagedResultDto<BaseThresholdDto>> GetThresholds(int customerId, Guid patientId, ThresholdsSearchDto request)
        {
            var customerAlertSeverities = (await _alertSeveritiesService.GetAlertSeverities(customerId, null)).Results;

            if (request != null && request.Mode.HasValue)
            {
                if (customerAlertSeverities.Count > 0)
                {
                    switch (request.Mode.Value)
                    {
                        case ThresholdSearchType.Patient:
                            {
                                var patientThresholdsPagedResult = await _thresholdsService.GetThresholds(customerId, patientId, request);

                                var filteredPatientThresholds = FilterUnusedThresholds(
                                    _mapper.Map<IList<PatientThreshold>, IList<PatientThresholdDto>>(patientThresholdsPagedResult.Results).Cast<BaseThresholdDto>().ToList(),
                                    customerAlertSeverities
                                );

                                var patientThresholdTypes = await _thresholdsService.GetPatientThresholdsTypes(customerId, patientId);
                                var patientThresholdTypeDtos = _mapper.Map<IList<PatientThresholdType>, IList<PatientThresholdTypeResponseDto>>(patientThresholdTypes);

                                var filteredPatientthresholds = PopulatePatientThresholdTypes(filteredPatientThresholds, patientThresholdTypeDtos);

                                return new PagedResultDto<BaseThresholdDto>
                                {
                                    Results = filteredPatientThresholds,
                                    Total = patientThresholdsPagedResult.Total
                                };
                            }
                        case ThresholdSearchType.Defaults:
                            {
                                var customerDefaultThresholdsPagedResult = await _defaultThresholdsService.GetDefaultThresholds(customerId, new DefaultThresholdsSearchDto()
                                {
                                    DefaultType = ThresholdDefaultType.Customer,
                                    Q = request.Q
                                });

                                var conditionDefaultThresholdsPagedResult = await _defaultThresholdsService.GetDefaultThresholds(customerId, new DefaultThresholdsSearchDto()
                                {
                                    DefaultType = ThresholdDefaultType.Condition,
                                    ConditionIds = request.ConditionIds ?? new List<Guid>(),
                                    Q = request.Q
                                });

                                var defaultThresholds = new List<DefaultThreshold>(customerDefaultThresholdsPagedResult.Results);
                                defaultThresholds.AddRange(conditionDefaultThresholdsPagedResult.Results);

                                var filteredDefaultThresholds = FilterUnusedThresholds(
                                    _mapper.Map<IList<DefaultThreshold>, IList<DefaultThresholdDto>>(defaultThresholds).Cast<BaseThresholdDto>().ToList(),
                                    customerAlertSeverities
                                );

                                var defaultPatientThresholdTypes = await _thresholdsService.GetPatientThresholdsTypes(customerId, patientId);
                                var defaultPatientThresholdTypeDtos = _mapper.Map<IList<PatientThresholdType>, IList<PatientThresholdTypeResponseDto>>(defaultPatientThresholdTypes);

                                filteredDefaultThresholds = PopulatePatientThresholdTypes(filteredDefaultThresholds, defaultPatientThresholdTypeDtos);

                                return new PagedResultDto<BaseThresholdDto>
                                {
                                    Results = filteredDefaultThresholds.Skip(request.Skip).Take(request.Take).ToList(),
                                    Total = filteredDefaultThresholds.Count
                                };
                            }
                        case ThresholdSearchType.All:
                            {
                                var patientThresholdsPagedResult = await _thresholdsService.GetThresholds(customerId, patientId, new BaseSearchDto
                                {
                                    Q = request.Q
                                });

                                var customerDefaultThresholdsPagedResult = await _defaultThresholdsService.GetDefaultThresholds(customerId, new DefaultThresholdsSearchDto()
                                {
                                    DefaultType = ThresholdDefaultType.Customer,
                                    Q = request.Q
                                });

                                var conditionsDefaultThresholdsPagedResult = await _defaultThresholdsService.GetDefaultThresholds(customerId, new DefaultThresholdsSearchDto()
                                {
                                    DefaultType = ThresholdDefaultType.Condition,
                                    ConditionIds = request.ConditionIds ?? new List<Guid>(),
                                    Q = request.Q
                                });

                                var allThresholds = new List<BaseThresholdDto>();
                                allThresholds.AddRange(_mapper.Map<IList<PatientThresholdDto>>(patientThresholdsPagedResult.Results));
                                allThresholds.AddRange(_mapper.Map<IList<DefaultThresholdDto>>(customerDefaultThresholdsPagedResult.Results));
                                allThresholds.AddRange(_mapper.Map<IList<DefaultThresholdDto>>(conditionsDefaultThresholdsPagedResult.Results));

                                var allFilteredThresholds = FilterUnusedThresholds(allThresholds, customerAlertSeverities);

                                var allPatientThresholdTypes = await _thresholdsService.GetPatientThresholdsTypes(customerId, patientId);
                                var allPatientThresholdTypeDtos = _mapper.Map<IList<PatientThresholdType>, IList<PatientThresholdTypeResponseDto>>(allPatientThresholdTypes);

                                allFilteredThresholds = PopulatePatientThresholdTypes(allFilteredThresholds, allPatientThresholdTypeDtos);


                                return new PagedResultDto<BaseThresholdDto>
                                {
                                    Results = allFilteredThresholds.Skip(request.Skip).Take(request.Take).ToList(),
                                    Total = allFilteredThresholds.Count
                                };
                            }
                        case ThresholdSearchType.Aggregate:
                            {
                                var patientThresholdsPagedResult = await _thresholdsService.GetThresholds(customerId, patientId, new BaseSearchDto
                                {
                                    Q = request.Q
                                });

                                var patientConditions = await _patientsConditionsService.GetPatientConditions(customerId, patientId);


                                // Author : Shajudheen S
                                #region Patient ThresholdTypes conditions
                                var patientThresholdTypes = await _thresholdsService.GetPatientThresholdsTypes(customerId, patientId);
                                var filteredConditionalThresholdTypes = patientThresholdTypes.Where(t => t.ThresholdType == "Condition").ToList();
                                var assignedConditionIds = filteredConditionalThresholdTypes.Select(pc => pc.ConditionId).ToList();
                                //var patientConditionIds_Actual = assignedConditionIds.Select(pc => pc.Id).ToList();
                                var conditionIds = assignedConditionIds.Where(x => assignedConditionIds.Any(y => y == x)).ToList();

                                var conditionDefaultThresholdsPagedResult = new PagedResult<DefaultThreshold>();
                                if (conditionIds.Any())
                                {
                                    conditionDefaultThresholdsPagedResult = await _defaultThresholdsService.GetDefaultThresholds(customerId, new DefaultThresholdsSearchDto()
                                    {
                                        DefaultType = ThresholdDefaultType.Condition,
                                        ConditionIds = conditionIds.OfType<Guid>().ToList(),
                                        Q = request.Q
                                    });
                                }

                                #endregion

                                var customerDefaultThresholdsPagedResult = await _defaultThresholdsService.GetDefaultThresholds(customerId, new DefaultThresholdsSearchDto()
                                {
                                    DefaultType = ThresholdDefaultType.Customer,
                                    Q = request.Q
                                });

                                var defaultThresholds = new List<DefaultThreshold>(customerDefaultThresholdsPagedResult.Results);
                                if (conditionDefaultThresholdsPagedResult.Results != null)
                                {
                                    defaultThresholds.AddRange(conditionDefaultThresholdsPagedResult.Results);
                                }

                                var aggregatedThresholds = _thresholdAggregator.CalculateAggregateThresholds(defaultThresholds, patientThresholdsPagedResult.Results, patientThresholdTypes);

                                var filteredAggregatedThresholds = FilterUnusedThresholds(_mapper.Map<IList<BaseThresholdDto>>(aggregatedThresholds), customerAlertSeverities);

                                var aggregatePatientThresholdTypes = await _thresholdsService.GetPatientThresholdsTypes(customerId, patientId);
                                var aggregatePatientThresholdTypeDtos = _mapper.Map<IList<PatientThresholdType>, IList<PatientThresholdTypeResponseDto>>(aggregatePatientThresholdTypes);

                                filteredAggregatedThresholds = PopulatePatientThresholdTypes(filteredAggregatedThresholds, aggregatePatientThresholdTypeDtos);

                                return new PagedResultDto<BaseThresholdDto>
                                {
                                    Results = filteredAggregatedThresholds.Skip(request.Skip).Take(request.Take).ToList(),
                                    Total = filteredAggregatedThresholds.Count
                                };
                            }
                    }
                } else
                {
                    switch (request.Mode.Value)
                    {
                        case ThresholdSearchType.Patient:
                            {
                                var patientThresholdsPagedResult = await _thresholdsService.GetThresholds(customerId, patientId, request);

                                var filteredPatientThresholds = FilterUnusedThresholds(
                                    _mapper.Map<IList<PatientThreshold>, IList<PatientThresholdDto>>(patientThresholdsPagedResult.Results).Cast<BaseThresholdDto>().ToList(),
                                    customerAlertSeverities
                                );

                                return new PagedResultDto<BaseThresholdDto>
                                {
                                    Results = filteredPatientThresholds,
                                    Total = patientThresholdsPagedResult.Total
                                };
                            }
                        case ThresholdSearchType.Defaults:
                            {
                                var customerDefaultThresholdsPagedResult = await _defaultThresholdsService.GetDefaultThresholds(customerId, new DefaultThresholdsSearchDto()
                                {
                                    DefaultType = ThresholdDefaultType.Customer,
                                    Q = request.Q
                                });

                                var conditionDefaultThresholdsPagedResult = await _defaultThresholdsService.GetDefaultThresholds(customerId, new DefaultThresholdsSearchDto()
                                {
                                    DefaultType = ThresholdDefaultType.Condition,
                                    ConditionIds = request.ConditionIds ?? new List<Guid>(),
                                    Q = request.Q
                                });

                                var defaultThresholds = new List<DefaultThreshold>(customerDefaultThresholdsPagedResult.Results);
                                defaultThresholds.AddRange(conditionDefaultThresholdsPagedResult.Results);

                                var filteredDefaultThresholds = FilterUnusedThresholds(
                                    _mapper.Map<IList<DefaultThreshold>, IList<DefaultThresholdDto>>(defaultThresholds).Cast<BaseThresholdDto>().ToList(),
                                    customerAlertSeverities
                                );

                                return new PagedResultDto<BaseThresholdDto>
                                {
                                    Results = filteredDefaultThresholds.Skip(request.Skip).Take(request.Take).ToList(),
                                    Total = filteredDefaultThresholds.Count
                                };
                            }
                        case ThresholdSearchType.All:
                            {
                                var patientThresholdsPagedResult = await _thresholdsService.GetThresholds(customerId, patientId, new BaseSearchDto
                                {
                                    Q = request.Q
                                });

                                var customerDefaultThresholdsPagedResult = await _defaultThresholdsService.GetDefaultThresholds(customerId, new DefaultThresholdsSearchDto()
                                {
                                    DefaultType = ThresholdDefaultType.Customer,
                                    Q = request.Q
                                });

                                var conditionsDefaultThresholdsPagedResult = await _defaultThresholdsService.GetDefaultThresholds(customerId, new DefaultThresholdsSearchDto()
                                {
                                    DefaultType = ThresholdDefaultType.Condition,
                                    ConditionIds = request.ConditionIds ?? new List<Guid>(),
                                    Q = request.Q
                                });

                                var allThresholds = new List<BaseThresholdDto>();
                                allThresholds.AddRange(_mapper.Map<IList<PatientThresholdDto>>(patientThresholdsPagedResult.Results));
                                allThresholds.AddRange(_mapper.Map<IList<DefaultThresholdDto>>(customerDefaultThresholdsPagedResult.Results));
                                allThresholds.AddRange(_mapper.Map<IList<DefaultThresholdDto>>(conditionsDefaultThresholdsPagedResult.Results));

                                var allFilteredThresholds = FilterUnusedThresholds(allThresholds, customerAlertSeverities);

                                return new PagedResultDto<BaseThresholdDto>
                                {
                                    Results = allFilteredThresholds.Skip(request.Skip).Take(request.Take).ToList(),
                                    Total = allFilteredThresholds.Count
                                };
                            }
                        case ThresholdSearchType.Aggregate:
                            {
                                var patientThresholdsPagedResult = await _thresholdsService.GetThresholds(customerId, patientId, new BaseSearchDto
                                {
                                    Q = request.Q
                                });

                                var patientConditions = await _patientsConditionsService.GetPatientConditions(customerId, patientId);
                                var patientConditionIds = patientConditions.Select(pc => pc.Id).ToList();

                                var conditionDefaultThresholdsPagedResult = new PagedResult<DefaultThreshold>();
                                if (patientConditionIds.Any())
                                {
                                    conditionDefaultThresholdsPagedResult = await _defaultThresholdsService.GetDefaultThresholds(customerId, new DefaultThresholdsSearchDto()
                                    {
                                        DefaultType = ThresholdDefaultType.Condition,
                                        ConditionIds = patientConditionIds.ToList(),
                                        Q = request.Q
                                    });
                                }

                                var customerDefaultThresholdsPagedResult = await _defaultThresholdsService.GetDefaultThresholds(customerId, new DefaultThresholdsSearchDto()
                                {
                                    DefaultType = ThresholdDefaultType.Customer,
                                    Q = request.Q
                                });

                                var defaultThresholds = new List<DefaultThreshold>(customerDefaultThresholdsPagedResult.Results);
                                if (conditionDefaultThresholdsPagedResult.Results != null)
                                {
                                    defaultThresholds.AddRange(conditionDefaultThresholdsPagedResult.Results);
                                }

                                var aggregatedThresholds = _thresholdAggregator.AggregateThresholds(defaultThresholds, patientThresholdsPagedResult.Results);

                                var filteredAggregatedThresholds = FilterUnusedThresholds(_mapper.Map<IList<BaseThresholdDto>>(aggregatedThresholds), customerAlertSeverities);

                                return new PagedResultDto<BaseThresholdDto>
                                {
                                    Results = filteredAggregatedThresholds.Skip(request.Skip).Take(request.Take).ToList(),
                                    Total = filteredAggregatedThresholds.Count
                                };
                            }
                    }
                }
            }

            var pagedResult = await _thresholdsService.GetThresholds(customerId, patientId, request);

            var filteredResult = FilterUnusedThresholds(
                _mapper.Map<IList<PatientThreshold>, IList<PatientThresholdDto>>(pagedResult.Results).Cast<BaseThresholdDto>().ToList(),
                customerAlertSeverities
            );

            //If two level severities
            if (customerAlertSeverities.Count > 0)
            {
                var pagedPatientThresholdTypes = await _thresholdsService.GetPatientThresholdsTypes(customerId, patientId);
                var pagedPatientThresholdTypeDtos = _mapper.Map<IList<PatientThresholdType>, IList<PatientThresholdTypeResponseDto>>(pagedPatientThresholdTypes);

                filteredResult = PopulatePatientThresholdTypes(filteredResult, pagedPatientThresholdTypeDtos);
            }

            return new PagedResultDto<BaseThresholdDto>
            {
                Results = filteredResult,
                Total = pagedResult.Total
            };
        }

        /// <summary>
        /// Filters the unused thresholds.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="customersAlertSeverities">The customers alert severities.</param>
        /// <returns></returns>
        private IList<BaseThresholdDto> FilterUnusedThresholds(
            IList<BaseThresholdDto> input,
            IList<AlertSeverity> customersAlertSeverities
        )
        {
            if (customersAlertSeverities.Any())
            {
                return input.Where(t => t.AlertSeverity != null).ToList();
            }

            return input;
        }

        private IList<BaseThresholdDto> PopulatePatientThresholdTypes(
            IList<BaseThresholdDto> patientThresholds,
            IList<PatientThresholdTypeResponseDto> patientThresholdTypes
        )
        {
            foreach (BaseThresholdDto item in patientThresholds)
            {
                item.PatientThresholdType = patientThresholdTypes.SingleOrDefault(p => p.VitalType == item.Name);
            }
            return patientThresholds;
        }

        /// <summary>
        /// Gets the patient threshold types for a patient and customer id
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<PatientThresholdTypeResponseDto>> GetPatientThresholdTypes(int customerId, Guid patientId)
        {
            var patientthresholdtypes = await _thresholdsService.GetPatientThresholdsTypes(customerId, patientId);
            var pagedPatientThresholdTypeDtos = _mapper.Map<IList<PatientThresholdType>, IList<PatientThresholdTypeResponseDto>>(patientthresholdtypes);
            return new PagedResultDto<PatientThresholdTypeResponseDto>
            {
                Results = pagedPatientThresholdTypeDtos,
                Total = pagedPatientThresholdTypeDtos.Count
            };
        }

        /// <summary>
        /// Gets the alert severities.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        public async Task<IList<AlertSeverity>> GetAlertSeverities(int customerId)
        {
            var alertSeverities = (await _alertSeveritiesService.GetAlertSeverities(customerId, null)).Results;

            return alertSeverities;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VitalsService.Domain.DbEntities;
using VitalsService.Web.Api.Models.ESGLogger;

namespace VitalsService.Web.Api.Helpers.Interfaces
{
    /// <summary>
    /// Pradeep Sundriyal
    /// </summary>
    public interface IEsgMessageControllerHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        Task<Guid> LogMessage(TransactionMessageDTO message);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        Task<Guid> LogMessageAck(ESGAcknowledgementDTO message);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="MessageControlid"></param>
        /// <returns></returns>
        Task<Guid> GetTransactionId(string MessageControlid);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ICN"></param>
        /// <param name="DFN"></param>
        /// <param name="MessageType"></param>
        /// <returns></returns>
        //Task<ESGMessageContent> GetMessageContent(string ICN, string DFN, string MessageType);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ICN"></param>
        /// <param name="SSN"></param>
        /// <param name="MessageType"></param>
        /// <returns></returns>
        Task<string> GetDFN(string ICN, string SSN, string MessageType);
        Task<string> GetEnrollmentDate(string ICN);
        /// <summary>
        /// Gets the A04 message and QBP messge
        /// </summary>
        /// <param name="MessageControlId"></param>
        /// <returns></returns>
        Task<ESGMessageContent> GetRegistrationMessage(string QBPMessageControlId);

    }
}
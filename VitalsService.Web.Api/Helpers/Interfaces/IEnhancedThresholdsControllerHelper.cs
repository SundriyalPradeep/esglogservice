﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VitalsService.Domain.DbEntities;
using VitalsService.Domain.Dtos;
using VitalsService.Domain.Enums;
using VitalsService.Web.Api.Models;
using VitalsService.Web.Api.Models.Thresholds;

namespace VitalsService.Web.Api.Helpers.Interfaces
{
    /// <summary>
    /// IThresholdsControllerHelper.
    /// </summary>
    public interface IEnhancedThresholdsControllerHelper
    {
        /// <summary>
        /// Creates the threshold.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        Task<OperationResultDto<Guid, CreateUpdateEnhancedThresholdStatus>>
            CreateEnhancedThreshold(int customerId, EnhancedThresholdRequestDto request);

        /// <summary>
        /// Updates the threshold.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="EnhancedthresholdId">The Enhancedthreshold identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        Task<CreateUpdateEnhancedThresholdStatus>
            UpdateEnhancedThreshold(int customerId,  Guid EnhancedthresholdId, EnhancedThresholdRequestDto request);

        /// <summary>
        /// Deletes the enhanced threshold.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="enhancedThresholdId">The Enhancedthreshold identifier.</param>
        /// <returns></returns>
        Task<GetDeleteEnhancedThresholdStatus> DeleteEnhancedThreshold(int customerId, Guid enhancedThresholdId);

        /// <summary>
        /// Gets the Enhanced threshold.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="EnhancedthresholdId">The Enhancedthreshold identifier.</param>
        /// <returns></returns>
        Task<OperationResultDto<BaseEnhancedThresholdDto, GetDeleteEnhancedThresholdStatus>>
            GetEnhancedThreshold(int customerId, Guid EnhancedthresholdId);

        /// <summary>
        /// Gets the Enhanced thresholds.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        Task<PagedResultDto<BaseEnhancedThresholdDto>> GetEnhancedThresholds(int customerId, EnhancedThresholdsSearchDto request);

        /// <summary>
        /// Enhanced Threshold Alerts
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        Task<PagedResultDto<EnhancedThresholdAlertDto>> GetEnhancedThresholdAlerts(int customerId);

        /// <summary>
        /// Acknowledges the ET alerts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        Task<CreateUpdateAlertStatus> AcknowledgeEnThAlerts(int customerId, AcknowledgeETAlertsRequestDto request);

        /// <summary>
        /// Acknowledges the ET alerts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        Task<CreateUpdateAlertStatus> UnAcknowledgeEnThAlerts(int customerId, AcknowledgeETAlertsRequestDto request);

        /// <summary>
        /// Ignore the ET alerts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        Task<CreateUpdateAlertStatus> IgnoreEnThresholdAlerts(int customerId, IgnoreETAlertsRequestDto request);


        /// <summary>
        /// Activate/Deactivate Customer/Condition Level Enhanced threshold for any patient.
        /// </summary>
        /// <param name="customerId">The customerId.</param>
        /// <param name="patientActivateETRequestDto">The Patient Activate ET RequestDto.</param>
        /// <returns></returns>
        Task<ActivateDeactivateETStatus> UpdateETActiveState(int customerId, PatientActivateETRequestDto patientActivateETRequestDto);

        /// <summary>
        /// Get Active/Inactive Customer/Condition Level ET's for  any patient.
        /// </summary>
        /// <param name="customerId">The customerId.</param>
        /// <param name="patientId">The Patient identifier</param>
        /// <returns></returns>
        Task<PagedResultDto<PatientActiveETResponseDto>> GetAllETsPatient(int customerId, Guid patientId);

    }
}
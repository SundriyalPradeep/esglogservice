﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using VitalsService.Domain.Dtos;
using VitalsService.Domain.Enums;
using VitalsService.Web.Api.Models;
using VitalsService.Web.Api.Models.Alerts;

namespace VitalsService.Web.Api.Helpers.Interfaces
{
    /// <summary>
    /// IAlertsControllerHelper.
    /// </summary>
    public interface IAlertsControllerHelper
    {
        /// <summary>
        /// Creates the alert.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        Task<OperationResultDto<Guid, CreateUpdateAlertStatus>> CreateAlert(int customerId, AlertRequestDto request);

        /// <summary>
        /// Create an alert from a Home Sensing notification.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="requestDto"></param>
        /// <param name="requestMsg"></param>
        /// <returns></returns>
        Task<OperationResultDto<Guid, CreateUpdateAlertStatus>> CreateHomeSensingAlert(int customerId, HomeSensingAlertRequestDto requestDto, HttpRequestMessage requestMsg);

        /// <summary>
        /// Ignore the home sensing alert.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        Task<CreateUpdateAlertStatus> IgnoreHomeSensingAlert(int customerId, IgnoreHomeSensingAlertRequestDto request);

        /// <summary>
        /// Acknowledges the alerts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        Task<CreateUpdateAlertStatus> AcknowledgeAlerts(int customerId, AcknowledgeAlertsRequestDto request);

       

        /// <summary>
        /// Gets the alerts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        PagedResultDto<PatientAlertsDto> GetAlerts(int customerId, AlertsSearchDto request);


        /// <summary>
        ///UnAcknowledges the alerts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        Task<CreateUpdateAlertStatus> UnAcknowledgeAlerts(int customerId, AcknowledgeAlertsRequestDto request);
    }
}
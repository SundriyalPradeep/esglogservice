﻿using System;
using System.Threading.Tasks;
using VitalsService.Domain.Dtos;
using VitalsService.Domain.Enums;
using VitalsService.Domain.Enums.Questionnaires;
using VitalsService.Web.Api.Models;
using VitalsService.Web.Api.Models.Questionnaires;

namespace VitalsService.Web.Api.Helpers.Interfaces
{
    /// <summary>
    /// Functionality for working with questionnaire responses.
    /// </summary>
    public interface IQuestionnairesControllerHelper
    {
        /// <summary>
        /// Create a QuestionnaireResponse
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<OperationResultDto<QuestionnaireResponsePostResponseDto, CreateQuestionnaireResponseStatus>> CreateQuestionnaireResponseAsync(int customerId, Guid patientId, QuestionnaireResponseRequestDto request);

        /// <summary>
        /// After the clinician reviews the pre-stratification score, s/he will pick an action. This method records that action,
        /// and also any optional free text that the clinician enters.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <param name="questionnaireResponseId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<UpdateQuestionnaireResponseStatus> RecordActionTakenAsync(int customerId, Guid patientId, Guid questionnaireResponseId, QuestionnaireResponseActionTakenDto request);

        /// <summary>
        /// Get a list of the patient's questionnaire responses.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<PagedResultDto<QuestionnaireResponseResponseDto>> GetQuestionnaireResponsesAsync(int customerId, Guid patientId, BaseSearchDto request);

        /// <summary>
        /// Get a single questionnaire response for the given patient.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <param name="questionnaireResponseId"></param>
        /// <returns></returns>
        Task<OperationResultDto<QuestionnaireResponseResponseDto, GetQuestionnaireReponseStatus>> GetQuestionnaireResponseAsync(int customerId, Guid patientId, Guid questionnaireResponseId);

        /// <summary>
        /// Pulling out this methid from create questionnaire as we dont want to create questionnaire before user confirms
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <param name="request"></param>
        /// <returns>int</returns>
        Task<OperationResultDto<QuestionnaireResponsePostResponseDto, CreateQuestionnaireResponseStatus>> CalculatePreStratificationScore(int customerId, Guid patientId, QuestionnaireResponseRequestDto request);
    }
}
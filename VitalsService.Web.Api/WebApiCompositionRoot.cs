﻿using AutoMapper;
using LightInject;
using NLog;
using StackExchange.Redis;
using VitalsService.Web.Api.Caching;
using VitalsService.Web.Api.Helpers;
using VitalsService.Web.Api.Helpers.Implementation;
using VitalsService.Web.Api.Helpers.Interfaces;
using VitalsService.Web.Api.Models.Mappings;
using VitalsService.Web.Api.Models.Mappings.Converters;
using VitalsService.Web.Api.Models.Mappings.Resolvers;
using VitalsService.Web.Api.Security;
using WebApi2.RedisOutputCache.Caching;

namespace VitalsService.Web.Api
{
    /// <summary>
    /// WebApiCompositionRoot.
    /// </summary>
    public class WebApiCompositionRoot : ICompositionRoot
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Composes the specified service registry.
        /// </summary>
        /// <param name="serviceRegistry">The service registry.</param>
        public void Compose(IServiceRegistry serviceRegistry)
        {
            #region Helpers
            
            serviceRegistry.Register<IThresholdsControllerHelper, ThresholdsControllerHelper>();
            serviceRegistry.Register<IDefaultThresholdsControllerHelper, DefaultThresholdsControllerHelper>();
            serviceRegistry.Register<IHealthSessionsControllerHelper, HealthSessionsControllerHelper>();
            serviceRegistry.Register<IVitalsControllerHelper, VitalsControllerHelper>();
            serviceRegistry.Register<IAlertsControllerHelper, AlertsControllerHelper>();
            serviceRegistry.Register<IAlertSeveritiesControllerHelper, AlertSeveritiesControllerHelper>();
            serviceRegistry.Register<ISuggestedNotablesControllerHelper, SuggestedNotablesControllerHelper>();
            serviceRegistry.Register<INotesControllerHelper, NotesControllerHelper>();
            serviceRegistry.Register<IAssessmentMediaControllerHelper, AssessmentMediaControllerHelper>();
            serviceRegistry.Register<IConditionsControllerHelper, ConditionsControllerHelper>();
            serviceRegistry.Register<IPatientConditionsControllerHelper, PatientConditionsControllerHelper>();
            serviceRegistry.Register<IEnhancedThresholdsControllerHelper, EnhancedThresholdsControllerHelper>();
            serviceRegistry.Register<IQuestionnairesControllerHelper, QuestionnairesControllerHelper>();
            serviceRegistry.Register<IEsgMessageControllerHelper, EsgMessageControllerHelper>();
            #endregion

            #region Contexts

            serviceRegistry.Register<ICustomerContext, CustomerContext>(new PerScopeLifetime());
            serviceRegistry.Register<IUserContext, UserContext>(new PerScopeLifetime());
            serviceRegistry.Register<ICertificateAuthContext, DefaultCertificateAuthContext>(new PerScopeLifetime());

            #endregion

            #region Redis cache

            // For output caching.
            serviceRegistry.Register<ConnectionMultiplexer>(factory => RedisCache.Multiplexer, new PerContainerLifetime());
            serviceRegistry.Register<IDatabase>(factory => factory.GetInstance<ConnectionMultiplexer>().GetDatabase(-1, null));
            serviceRegistry.Register<ISubscriber>(factory => factory.GetInstance<ConnectionMultiplexer>().GetSubscriber(null));
            serviceRegistry.Register<IApiOutputCache, RedisApiOutputCache>();

            #endregion

            #region AutoMapper

            // AutoMapper
            var autoMapperCfg = AutoMapperHelper.BuildAutoMapperConfiguration(cfg =>
            {
                cfg.AddProfiles(typeof(AlertMapping).Assembly, typeof(DomainLogic.Mappings.PatientNotesMapping).Assembly);
            });
            serviceRegistry.Register<MapperConfiguration>(factory => autoMapperCfg, new PerContainerLifetime());
            serviceRegistry.Register<IMapper>(factory => factory.GetInstance<MapperConfiguration>().CreateMapper(factory.GetInstance), new PerContainerLifetime());

            // Since we're using DI to inject IMapper, we need to make our IValueResolvers and ITypeConverters
            //   available for DI as well.
            AutoMapperHelper.RegisterTypesImplementingIValueResolver(t => serviceRegistry.Register(t), typeof(AssessmentMediaUrlResolver).Assembly);
            AutoMapperHelper.RegisterTypesImplementingITypeConverter(t => serviceRegistry.Register(t), typeof(AlertsConverter).Assembly);

            #endregion
        }
    }
}
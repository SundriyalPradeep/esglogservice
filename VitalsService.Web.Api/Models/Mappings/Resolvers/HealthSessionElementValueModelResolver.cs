﻿using System;
using System.Collections.Generic;
using AutoMapper;
using VitalsService.Domain.DbEntities;
using VitalsService.Domain.Enums;
using VitalsService.Web.Api.Models.HealthSessions;

namespace VitalsService.Web.Api.Models.Mappings.Resolvers
{
    /// <summary>
    /// Selects appropriate model to map HealthSessionElementValue entity.
    /// </summary>
    public class HealthSessionElementValueModelResolver : IValueResolver<HealthSessionElement, HealthSessionElementResponseDto, IList<HealthSessionElementValueDto>>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        /// <param name="destMember"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public IList<HealthSessionElementValueDto> Resolve(HealthSessionElement source, HealthSessionElementResponseDto destination, IList<HealthSessionElementValueDto> destMember, ResolutionContext context)
        {
            var result = new List<HealthSessionElementValueDto>();

            foreach (var elementValue in source.Values)
            {
                switch (elementValue.Type)
                {
                    case HealthSessionElementValueType.ScaleAnswer:
                        result.Add(context.Mapper.Map<ScaleAnswerDto>(elementValue));
                        break;

                    case HealthSessionElementValueType.SelectionAnswer:
                        result.Add(context.Mapper.Map<SelectionAnswerDto>(elementValue));
                        break;

                    case HealthSessionElementValueType.OpenEndedAnswer:
                        result.Add(context.Mapper.Map<FreeFormAnswerDto>(elementValue));
                        break;

                    case HealthSessionElementValueType.MeasurementAnswer:
                        result.Add(context.Mapper.Map<MeasurementValueResponseDto>(elementValue));
                        break;

                    case HealthSessionElementValueType.StethoscopeAnswer:
                        result.Add(context.Mapper.Map<AssessmentValueResponseDto>(elementValue));
                        break;

                    default:
                        throw new NotImplementedException();
                }
            }

            return result;
        }
    }
}
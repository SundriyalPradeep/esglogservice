﻿using AutoMapper;
using Microsoft.Practices.ServiceLocation;
using VitalsService.ContentStorage.Azure.Services.Interfaces;
using VitalsService.Domain.DbEntities;
using VitalsService.Web.Api.Models.AssessmentMedias;

namespace VitalsService.Web.Api.Models.Mappings.Resolvers
{
    /// <summary>
    /// Resolver to map url of assessment media correct.
    /// </summary>
    public class AssessmentMediaUrlResolver : IValueResolver<AssessmentMedia, AssessmentMediaResponseDto, string>
    {
        /// <summary>
        /// Resolves the core.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        /// <param name="destMember"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public string Resolve(AssessmentMedia source, AssessmentMediaResponseDto destination, string destMember, ResolutionContext context)
        {
            if (source == null || string.IsNullOrEmpty(source.StorageKey))
            {
                return string.Empty;
            }

            var contentStorage = ServiceLocator.Current.GetInstance<IContentStorage>();

            return contentStorage.GenerateContentSASUrl(source.StorageKey, source.OriginalFileName);
        }
    }
}
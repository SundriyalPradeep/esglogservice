﻿using System;
using System.Linq;
using AutoMapper;
using VitalsService.Domain.DbEntities;
using VitalsService.Domain.EsbEntities;

namespace VitalsService.Web.Api.Models.Mappings.Resolvers
{
    /// <summary>
    /// Resolves mapping for health session id in measurement.
    /// </summary>
    public class MeasurementSessionIdResolver : IValueResolver<Measurement, MeasurementResponseDto, Guid?>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        /// <param name="destMember"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public Guid? Resolve(Measurement source, MeasurementResponseDto destination, Guid? destMember, ResolutionContext context)
        {
            if (source.MeasurementValues == null || !source.MeasurementValues.Any())
            {
                return null;
            }

            return source.MeasurementValues.First().HealthSessionElement.HealthSessionId;
        }
    }

    /// <summary>
    /// Resolves mapping for health session id in measurement.
    /// </summary>
    public class MeasurementSessionIdFromMsgResolver : IValueResolver<Measurement, MeasurementMessage, Guid?>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        /// <param name="destMember"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public Guid? Resolve(Measurement source, MeasurementMessage destination, Guid? destMember, ResolutionContext context)
        {
            if (source.MeasurementValues == null || !source.MeasurementValues.Any())
            {
                return null;
            }

            return source.MeasurementValues.First().HealthSessionElement.HealthSessionId;
        }
    }
}
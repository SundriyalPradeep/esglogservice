﻿using AutoMapper;
using VitalsService.Domain.DbEntities;
using VitalsService.Web.Api.Models.AlertSeverities;

namespace VitalsService.Web.Api.Models.Mappings
{
    /// <summary>
    /// AlertMapping.
    /// </summary>
    public class AlertSeverityMapping : Profile
    {
        /// <summary>
        /// Override this method in a derived class and call the CreateMap method to associate that map with this profile.
        /// Avoid calling the <see cref="T:AutoMapper.Mapper" /> class from this method.
        /// </summary>
        public AlertSeverityMapping()
        {
            CreateMap<AlertSeverityRequestDto, AlertSeverity>()
                .ForMember(d => d.CustomerId, o => o.Ignore())
                .ForMember(d => d.Thresholds, o => o.Ignore())
                .ForMember(d => d.Alerts, o => o.Ignore())
                .ForMember(d => d.EnhancedThresholdAlerts, o => o.Ignore())
                .ForMember(d => d.Id, o => o.Ignore());

            CreateMap<AlertSeverity, AlertSeverityResponseDto>();
        }
    }
}
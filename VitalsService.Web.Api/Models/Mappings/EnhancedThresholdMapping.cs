﻿using System;
using AutoMapper;
using VitalsService.Domain.DbEntities;
using VitalsService.Domain.Enums;
using VitalsService.Web.Api.Models.Thresholds;

namespace VitalsService.Web.Api.Models.Mappings
{
    /// <summary>
    /// EnhancedThresholdMapping.
    /// </summary>
    public class EnhancedThresholdMapping : Profile
    {
        /// <summary>
        /// Override this method in a derived class and call the CreateMap method to associate that map with this profile.
        /// Avoid calling the <see cref="T:AutoMapper.Mapper" /> class from this method.
        /// </summary>
        public EnhancedThresholdMapping()
        {
            CreateMap<EnhancedThresholdRequestDto, EnhancedThreshold>()
                .ForMember(d => d.Name, m => m.MapFrom(s => s.Name.ToString()))
                .ForMember(d => d.Unit, m => m.MapFrom(s => s.Unit.ToString()))
                .ForMember(d => d.TimeOrValuesCount, m => m.MapFrom(s => s.TimeOrValuesCount))
                .ForMember(d => d.ComparisonOperator, m => m.MapFrom(s => s.ComparisonOperator))
                .ForMember(d => d.ComparisonValue, m => m.MapFrom(s => s.ComparisonValue))
                .ForMember(d => d.VitalName, m => m.MapFrom(s => s.VitalName.ToString()))
                .ForMember(d => d.CustomerId, m => m.Ignore())
                .ForMember(d => d.EThresholdRanges, m => m.Ignore())
                .ForMember(d => d.EThresholdSetAnalysis, m => m.Ignore())
                .ForMember(d => d.ETConditions, m => m.Ignore())
                .ForMember(d => d.ETPatients, m => m.Ignore())
                .ForMember(d => d.IsActive, m => m.MapFrom(s => s.IsActive))
                .ForMember(d => d.Type, m => m.MapFrom(s => s.Type.ToString()))
                .ForMember(d => d.CreatedDateUTC, m => m.MapFrom(s => s.CreatedDateUTC))
                .ForMember(d => d.UpdatedDateUTC, m => m.MapFrom(s => s.UpdatedDateUTC))
                .ForMember(d => d.Id, m => m.Ignore());

            CreateMap<EnhancedThreshold, BaseEnhancedThresholdDto>()
                .ForMember(d => d.Name, m => m.MapFrom(s => s.Name.ToString()))
                .ForMember(d => d.Unit, m => m.MapFrom(s => s.Unit.ToString()))
                .ForMember(d => d.TimeOrValuesCount, m => m.MapFrom(s => s.TimeOrValuesCount))
                .ForMember(d => d.ComparisonOperator, m => m.MapFrom(s => s.ComparisonOperator))
                .ForMember(d => d.ComparisonValue, m => m.MapFrom(s => s.ComparisonValue))
                .ForMember(d => d.VitalName, m => m.MapFrom(s => Enum.Parse(typeof(VitalType), s.VitalName, true)))
                .ForMember(d => d.Type, m => m.MapFrom(s => Enum.Parse(typeof(EnhancedThresholdType), s.Type.ToString(), true)))
                .ForMember(d => d.CustomerId, m => m.MapFrom(s => s.CustomerId))
                .ForMember(d => d.ETConditions, m => m.MapFrom(s => s.ETConditions))
                .ForMember(d => d.ETPatients, m => m.MapFrom(s => s.ETPatients))
                .ForMember(d => d.Conditions, m => m.Ignore())
                .ForMember(d => d.CreatedDateUTC, m => m.MapFrom(s => s.CreatedDateUTC))
                .ForMember(d => d.UpdatedDateUTC, m => m.MapFrom(s => s.UpdatedDateUTC))
                .ForMember(d => d.IsActive, m => m.MapFrom(s => s.IsActive))
                .ForMember(d => d.RangeType, m => m.MapFrom(s => Enum.Parse(typeof(EnhancedThresholdRangeType), s.EThresholdRanges.RangeText, true)))
               .ForMember(d => d.SetAnalysisType, m => m.MapFrom(s => Enum.Parse(typeof(EnhancedThresholdSetAnalysisType), s.EThresholdSetAnalysis.SetAnalysisText.Replace(" ", "_"), true)));

            CreateMap<EnhancedThresholdRequestDto, EnhancedThresholdPatient>()
                .ForMember(d => d.PatientId, m => m.MapFrom(s => s.PatientId))
                .ForMember(d => d.CustomerId, m => m.Ignore())
                .ForMember(d => d.Id, m => m.Ignore());

            CreateMap<EnhancedThresholdRequestDto, EnhancedThresholdCondition>()
                .ForMember(d => d.ConditionId, m => m.MapFrom(s => s.ConditionId))
                .ForMember(d => d.CustomerId, m => m.Ignore())
                .ForMember(d => d.Id, m => m.Ignore());

            CreateMap<EnhancedThresholdAlerts, EnhancedThresholdAlertDto>()
               .ForMember(d => d.CustomerId, m => m.MapFrom(s => s.CustomerId))
               .ForMember(d => d.EnhancedThresholdId, m => m.MapFrom(s => s.EnhancedThresholdId))
               .ForMember(d => d.PatientId, m => m.MapFrom(s => s.PatientId))
               .ForMember(d => d.MeasurementIds, m => m.MapFrom(s => s.MeasurementIds))
               .ForMember(d => d.ViolatedMeasurementId, m => m.MapFrom(s => s.ViolatedMeasurementId))
               .ForMember(d => d.AlertTitle, m => m.MapFrom(s => s.AlertTitle))
               .ForMember(d => d.IsDeleted, m => m.MapFrom(s => s.IsDeleted))
               .ForMember(d => d.Acknowledged, m => m.MapFrom(s => s.Acknowledged))
               .ForMember(d => d.AcknowledgedBy, m => m.MapFrom(s => s.AcknowledgedBy))
               .ForMember(d => d.AcknowledgedUtc, m => m.MapFrom(s => s.AcknowledgedUtc))
               .ForMember(d => d.CreatedDateUTC, m => m.MapFrom(s => s.CreatedDateUTC))
               .ForMember(d => d.UpdatedDateUTC, m => m.MapFrom(s => s.UpdatedDateUTC))
               .ForMember(d => d.EnhancedThreshold, m => m.MapFrom(s => s.EnhancedThreshold))
               .ForMember(d => d.ETDescription, m => m.MapFrom(s => s.ETDescription))
               .ForMember(d => d.AlertSeverityId, m => m.MapFrom(s => s.AlertSeverityId))
               .ForMember(d => d.AlertSeverity, m => m.MapFrom(s => s.AlertSeverity))               
               .ForMember(d => d.Id, m => m.MapFrom(s => s.Id));


            CreateMap<PatientActivateETRequestDto, PatientActiveETs>()
               .ForMember(d => d.PatientId, m => m.MapFrom(s => s.PatientId))
               .ForMember(d => d.EnhancedThresholdId, m => m.MapFrom(s => s.EnhancedThresholdId))
               .ForMember(d => d.Type, m => m.MapFrom(s => Enum.Parse(typeof(EnhancedThresholdType), s.Type.ToString(), true)))
               .ForMember(d => d.isActive, m => m.MapFrom(s => s.isActive))
               .ForMember(d => d.Id, m => m.Ignore());

            CreateMap<PatientActiveETs, PatientActiveETResponseDto>()
              .ForMember(d => d.PatientId, m => m.MapFrom(s => s.PatientId))
              .ForMember(d => d.EnhancedThresholdId, m => m.MapFrom(s => s.EnhancedThresholdId))
              .ForMember(d => d.Type, m => m.MapFrom(s => Enum.Parse(typeof(EnhancedThresholdType), s.Type.ToString(), true)))
              .ForMember(d => d.isActive, m => m.MapFrom(s => s.isActive));
        }
    }
}
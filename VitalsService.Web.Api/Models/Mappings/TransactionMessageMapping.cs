﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VitalsService.Domain.DbEntities;
using VitalsService.Web.Api.Models.ESGLogger;

namespace VitalsService.Web.Api.Models.Mappings
{
    /// <summary>
    /// 
    /// </summary>
    public class TransactionMessageMapping : Profile

    {

        /// <summary>
        /// 
        /// </summary>
        public TransactionMessageMapping()
        {

            CreateMap<TransactionMessageDTO, ESGMessageTransaction>()
               .ForMember(d => d.MessageControlId, m => m.MapFrom(s => s.MessageControlId.ToString()))
               .ForMember(d => d.MessageEvent, m => m.MapFrom(s => s.MessageEvent))
               .ForMember(d => d.MessageType, m => m.MapFrom(s => s.MessageType))
               .ForMember(d => d.ICN, m => m.MapFrom(s => s.ICN))
               .ForMember(d => d.SendingFacilityID, m => m.MapFrom(s => s.SendingFacilityID.ToString()))
               .ForMember(d => d.SendingFacility, m => m.MapFrom(s => s.SendingFacility.ToString()))
               .ForMember(d => d.ReceivingFacility, m => m.MapFrom(s => s.ReceivingFacility.ToString()))
               .ForMember(d => d.DFN, m => m.MapFrom(s => s.DFN))
               .ForMember(d => d.MessageDate, m => m.MapFrom(s => s.MessageDate))
               .ForMember(d => d.RegistrationId, m => m.MapFrom(s => s.RegistrationId))
               .ForMember(d => d.InsertedOn, m => m.Ignore())
               .ForMember(d => d.ESGMessageAcknowledgement, m => m.Ignore())
               .ForMember(d => d.Id, m => m.Ignore());

            CreateMap<TransactionMessageDTO, ESGMessageContent>()
               .ForMember(d => d.TransactionId, m => m.Ignore())
               .ForMember(d => d.Id, m => m.Ignore())
                .ForMember(d => d.ESGMessageTransaction, m => m.Ignore())
               .ForMember(d => d.MessageBody, m => m.MapFrom(S => S.MessageBody));

        }
    }
}
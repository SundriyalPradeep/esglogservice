﻿using System;
using AutoMapper;
using VitalsService.Domain.DbEntities;
using VitalsService.Domain.Enums;
using VitalsService.Web.Api.Models.Alerts;
using VitalsService.Web.Api.Models.AlertSeverities;

namespace VitalsService.Web.Api.Models.Mappings.Converters
{
    /// <summary>
    /// Contains logic to build alert dtos base on alert type and search criteria.
    /// </summary>
    public class AlertsConverter : ITypeConverter<Alert, BaseAlertResponseDto>
    {
        /// <summary>
        /// Performs conversion from source to destination type
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public BaseAlertResponseDto Convert(Alert source, BaseAlertResponseDto destination, ResolutionContext context)
        {
            if (source == null)
            {
                // It's possible for the Alert to be null when we're loading Notes that are not associated with an alert, in which
                //   case the alert id is null. In that case, the proper response is to return null.
                return null;
            }

            Alert alert = source;

            var options = context.Options.Items;
            object isBrief;

            if (options.TryGetValue("isBrief", out isBrief) && !(bool)isBrief)
            {
                switch (alert.Type)
                {
                    case AlertType.VitalsViolation:
                        var vitalAlert = alert as VitalAlert;

                        if (vitalAlert != null)
                        {
                            return context.Mapper.Map<VitalAlertResponseDto>(vitalAlert);
                        }

                        break;

                    case AlertType.ResponseViolation:
                        var healthSessionElementAlert = alert as HealthSessionElementAlert;

                        if (healthSessionElementAlert != null)
                        {
                            return context.Mapper.Map<HealthSessionElementAlertResponseDto>(healthSessionElementAlert);
                        }

                        break;

                    case AlertType.HomeSensing:
                        var homeSensingAlert = alert as HomeSensingAlert;

                        if (homeSensingAlert != null)
                        {
                            return context.Mapper.Map<HomeSensingAlertResponseDto>(homeSensingAlert);
                        }

                        break;
                }
            }

            return new BaseAlertResponseDto
            {
                Acknowledged = alert.Acknowledged,
                Type = alert.Type.Value,
                CustomerId = alert.CustomerId,
                Id = alert.Id,
                AlertSeverity = context.Mapper.Map<AlertSeverityResponseDto>(alert.AlertSeverity),
                AcknowledgedBy = alert.AcknowledgedBy,
                AcknowledgedUtc = alert.AcknowledgedUtc,
                Body = alert.Body,
                ExpiresUtc = alert.ExpiresUtc,
                OccurredUtc = alert.OccurredUtc,
                Title = alert.Title.Substring(0, alert.Title.LastIndexOf(" on") + 1),
                Weight = alert.Weight
            };
        }
    }
}
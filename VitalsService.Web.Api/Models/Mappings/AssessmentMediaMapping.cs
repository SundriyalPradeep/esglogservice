﻿using AutoMapper;
using VitalsService.Domain.DbEntities;
using VitalsService.Web.Api.Models.AssessmentMedias;
using VitalsService.Web.Api.Models.Mappings.Resolvers;

namespace VitalsService.Web.Api.Models.Mappings
{
    /// <summary>
    /// AlertMapping.
    /// </summary>
    public class AssessmentMediaMapping : Profile
    {
        /// <summary>
        /// Override this method in a derived class and call the CreateMap method to associate that map with this profile.
        /// Avoid calling the <see cref="T:AutoMapper.Mapper" /> class from this method.
        /// </summary>
        public AssessmentMediaMapping()
        {
            CreateMap<CreateAssessmentMediaRequestDto, AssessmentMedia>()
                .ForMember(d => d.CustomerId, o => o.Ignore())
                .ForMember(d => d.PatientId, o => o.Ignore())
                .ForMember(d => d.MediaTypeString, o => o.Ignore())
                .ForMember(d => d.StorageKey, o => o.Ignore())
                .ForMember(d => d.CreatedUtc, o => o.Ignore())
                .ForMember(d => d.UpdatedUtc, o => o.Ignore())
                .ForMember(d => d.AssessmentValues, o => o.Ignore())
                .ForMember(d => d.Id, o => o.Ignore());

            CreateMap<AssessmentMedia, AssessmentMediaResponseDto>()
                .ForMember(d => d.AssessmentMediaUrl, s => s.ResolveUsing<AssessmentMediaUrlResolver>());
        }
    }
}
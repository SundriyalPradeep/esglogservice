﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using VitalsService.Domain.DbEntities;
using VitalsService.Web.Api.Models.PatientNotes;

namespace VitalsService.Web.Api.Models.Mappings
{
    /// <summary>
    /// PatientNotesMapping.
    /// </summary>
    public class PatientNotesMapping : Profile
    {
        /// <summary>
        /// Override this method in a derived class and call the CreateMap method to associate that map with this profile.
        /// Avoid calling the <see cref="T:AutoMapper.Mapper" /> class from this method.
        /// </summary>
        public PatientNotesMapping()
        {
            CreateMap<SuggestedNotableRequestDto, SuggestedNotable>()
                .ForMember(d => d.CustomerId, o => o.Ignore())
                .ForMember(d => d.CreatedUtc, o => o.Ignore())
                .ForMember(d => d.UpdatedUtc, o => o.Ignore())
                .ForMember(d => d.Id, o => o.Ignore());

            CreateMap<SuggestedNotable, SuggestedNotableDto>();

            CreateMap<NoteRequestDto, Note>()
                .ForMember(d => d.Notables, m => m.MapFrom(s => s.Notables != null ? s.Notables.Select(n => new NoteNotable() { Name = n }) : new List<NoteNotable>()))
                .ForMember(d => d.CustomerId, o => o.Ignore())
                .ForMember(d => d.PatientId, o => o.Ignore())
                .ForMember(d => d.CreatedUtc, o => o.Ignore())
                .ForMember(d => d.UpdatedUtc, o => o.Ignore())
                .ForMember(d => d.NotesMeasurements, m => m.MapFrom(s => s.NotesMeasurements != null ? s.NotesMeasurements.Select(n => new NotesMeasurement() {MeasurementId = n.MeasurementId }) : new List<NotesMeasurement>()))
                .ForMember(d => d.HealthSessionElement, o => o.Ignore())
               .ForMember(d => d.NotesAlerts, m => m.MapFrom(s => s.NotesAlerts != null ? s.NotesAlerts.Select(n => new NotesAlert() { AlertId = n.AlertId }) : new List<NotesAlert>()))
                .ForMember(d => d.Id, o => o.Ignore());

            //CreateMap<Note, NoteRequestDto>()
            //    .ForMember(d => d.MeasurementIds, o => o.Ignore())
            //     .ForMember(d => d.AlertIds, o => o.Ignore())
            // .ForMember(d => d.NotesMeasurements, o => o.MapFrom(s => s.NotesMeasurements))
            //  .ForMember(d => d.NotesAlerts, o => o.MapFrom(s => s.NotesAlerts));


            CreateMap<Note, NoteBriefResponseDto>()
                .ForMember(d => d.MeasurementIds, o => o.Ignore())
                .ForMember(d => d.AlertIds, o => o.Ignore())
                .ForMember(d => d.Notables, m => m.MapFrom(s => s.Notables != null ? s.Notables.Select(n => n.Name) : new List<string>()));

            CreateMap<NoteNotable, string>()
                .ConvertUsing(source => source.Name);

            CreateMap<NotesMeasurement, NotesMeasurementDto>()
                .ForMember(d => d.MeasurementId, o => o.MapFrom(s => s.MeasurementId))
                .ForMember(d => d.Measurement, o => o.Ignore());

            CreateMap<NotesAlert, NotesAlertDto>()
                .ForMember(d => d.AlertId, o => o.MapFrom(s => s.AlertId))
                .ForMember(d => d.Alert, o => o.Ignore());

            CreateMap<Note, NoteDetailedResponseDto>();
        }
    }
}
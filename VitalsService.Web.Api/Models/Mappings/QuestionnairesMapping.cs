﻿using AutoMapper;
using VitalsService.Domain.DbEntities;
using VitalsService.Web.Api.Models.Questionnaires;

namespace VitalsService.Web.Api.Models.Mappings
{
    /// <summary>
    /// Questionnaire-related AutoMapper mapping.
    /// </summary>
    public class QuestionnairesMapping : Profile
    {
        /// <summary>
        /// .ctor
        /// </summary>
        public QuestionnairesMapping()
        {
            CreateMap<QuestionnaireResponseRequestDto, QuestionnaireResponse>()
                .ForMember(d => d.Id, o => o.Ignore())
                .ForMember(d => d.CreatedUtc, o => o.Ignore())
                .ForMember(d => d.UpdatedUtc, o => o.Ignore())
                .ForMember(d => d.CustomerId, o => o.Ignore())
                .ForMember(d => d.PatientId, o => o.Ignore())
                .ForMember(d => d.TotalPreStratificationRiskScore, o => o.MapFrom(s => s.TotalPreStratificationRiskScore))
                .ForMember(d => d.ActionTakenAnswerChoiceId, o => o.MapFrom(s => s.ActionTakenAnswerChoiceId))
                .ForMember(d => d.ActionTakenAnswerChoiceText, o => o.MapFrom(s => s.ActionTakenAnswerChoiceText))
                .ForMember(d => d.ActionTakenFreeText, o => o.MapFrom(s => s.ActionTakenFreeText))
                .ForMember(d => d.ExacerbationAnswerChoiceId, o => o.MapFrom(s => s.ExacerbationAnswerChoiceId))
                .ForMember(d => d.ExacerbationAnswerChoiceText, o => o.MapFrom(s => s.ExacerbationAnswerChoiceText));

            // We don't currently send the list of questions/answers back to the client.
            CreateMap<QuestionnaireResponse, QuestionnaireResponseResponseDto>();

            CreateMap<QuestionnaireQuestionAnswerDto, QuestionnaireQuestionAnswer>()
                .ForMember(d => d.Score, o => o.MapFrom(s => s.AnswerChoiceScore))
                .ForMember(d => d.QuestionnaireResponse, o => o.Ignore())
                .ForMember(d => d.QuestionnaireResponseId, o => o.Ignore())
                .ForMember(d => d.Id, o => o.Ignore())
                .ForMember(d => d.CreatedUtc, o => o.Ignore())
                .ForMember(d => d.UpdatedUtc, o => o.Ignore());

            CreateMap<QuestionnaireQuestionAnswer, QuestionnaireQuestionAnswerDto>()
                .ForMember(d => d.AnswerChoiceScore, o => o.MapFrom(s => s.Score))
                .ForMember(d => d.MeasurementUnit, o => o.Ignore());
        }
    }
}
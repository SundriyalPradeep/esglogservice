﻿using System;
using AutoMapper;
using VitalsService.Domain.DbEntities;
using VitalsService.Domain.Enums;
using VitalsService.Web.Api.Models.Thresholds;

namespace VitalsService.Web.Api.Models.Mappings
{
    /// <summary>
    /// ThresholdMapping.
    /// </summary>
    public class ThresholdMapping : Profile
    {
        /// <summary>
        /// Override this method in a derived class and call the CreateMap method to associate that map with this profile.
        /// Avoid calling the <see cref="T:AutoMapper.Mapper" /> class from this method.
        /// </summary>
        public ThresholdMapping()
        {
            CreateMap<ThresholdRequestDto, PatientThreshold>()
                .ForMember(d => d.Type, m => m.MapFrom(s => s.Type.ToString()))
                .ForMember(d => d.Name, m => m.MapFrom(s => s.Name.ToString()))
                .ForMember(d => d.Unit, m => m.MapFrom(s => s.Unit.ToString()))
                .ForMember(d => d.AlertSeverityId, m => m.MapFrom(s => s.AlertSeverityId))
                .ForMember(d => d.AlertSeverity, m => m.Ignore())
                .ForMember(d => d.PatientId, m => m.Ignore())
                .ForMember(d => d.CustomerId, m => m.Ignore())
                .ForMember(d => d.VitalAlerts, m => m.Ignore())
                .ForMember(d => d.Id, m => m.Ignore());

            CreateMap<ThresholdRequestDto, PatientThresholdType>()
                .ForMember(d => d.ConditionId, m => m.MapFrom(s => s.ConditionId))
                .ForMember(d => d.VitalType, o => o.MapFrom(s => Enum.Parse(typeof(VitalType), s.Name.ToString(), true)))
                .ForMember(d => d.ThresholdType, m => m.MapFrom(s => s.ThresholdType))
                .ForMember(d => d.CustomerId, m => m.Ignore())
                .ForMember(d => d.Id, m => m.Ignore())
                .ForMember(d => d.PatientId, m => m.Ignore());



            CreateMap<PatientThreshold, PatientThresholdDto>()
                .ForMember(d => d.Type, m => m.MapFrom(s => Enum.Parse(typeof(ThresholdType), s.Type, true)))
                .ForMember(d => d.Name, m => m.MapFrom(s => Enum.Parse(typeof(VitalType), s.Name, true)))
                .ForMember(d => d.Unit, m => m.MapFrom(s => Enum.Parse(typeof(UnitType), s.Unit, true)))
                .ForMember(d => d.PatientThresholdType, m => m.Ignore())
                .ForMember(d => d.ConditionId, m => m.Ignore());

            CreateMap<DefaultThresholdRequestDto, DefaultThreshold>()
                .ForMember(d => d.Type, m => m.MapFrom(s => s.Type.ToString()))
                .ForMember(d => d.Name, m => m.MapFrom(s => s.Name.ToString()))
                .ForMember(d => d.Unit, m => m.MapFrom(s => s.Unit.ToString()))
                .ForMember(d => d.AlertSeverityId, m => m.MapFrom(s => s.AlertSeverityId))
                .ForMember(d => d.AlertSeverity, m => m.Ignore())
                .ForMember(d => d.Condition, m => m.Ignore())
                .ForMember(d => d.CustomerId, m => m.Ignore())
                .ForMember(d => d.VitalAlerts, m => m.Ignore())
                .ForMember(d => d.Id, m => m.Ignore());

            CreateMap<DefaultThreshold, DefaultThresholdDto>()
                .ForMember(d => d.Type, m => m.MapFrom(s => Enum.Parse(typeof(ThresholdType), s.Type, true)))
                .ForMember(d => d.Name, m => m.MapFrom(s => Enum.Parse(typeof(VitalType), s.Name, true)))
                .ForMember(d => d.Unit, m => m.MapFrom(s => Enum.Parse(typeof(UnitType), s.Unit, true)))
                .ForMember(d => d.PatientThresholdType, m => m.Ignore())
                .ForMember(d => d.ConditionId, m => m.MapFrom(s => s.ConditionId));

            CreateMap<Threshold, BaseThresholdDto>()
                .ForMember(d => d.PatientThresholdType, m => m.Ignore())
                .ForMember(d => d.ConditionId, m => m.Ignore())
                .Include<DefaultThreshold, DefaultThresholdDto>()
                .Include<PatientThreshold, PatientThresholdDto>();

            CreateMap<PatientThresholdType, PatientThresholdTypeResponseDto>()
               .ForMember(d => d.ThresholdType, m => m.MapFrom(s => s.ThresholdType.ToString()))
               .ForMember(d => d.VitalType, m => m.MapFrom(s => s.VitalType))
               .ForMember(d => d.PatientId, m => m.MapFrom(s => s.PatientId))
               .ForMember(d => d.IsDeleted, m => m.MapFrom(s => s.IsDeleted))
               .ForMember(d => d.ConditionId, m => m.MapFrom(s => s.ConditionId));
        }
    }
}
﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VitalsService.Domain.DbEntities;
using VitalsService.Web.Api.Models.ESGLogger;

namespace VitalsService.Web.Api.Models.Mappings
{
    /// <summary>
    /// 
    /// </summary>
    public class AcknowledgementMessageMapping : Profile

    {

        /// <summary>
        /// 
        /// </summary>
        public AcknowledgementMessageMapping()
        {
            CreateMap<ESGAcknowledgementDTO, ESGMessageAcknowledgement>()
               .ForMember(d => d.ESGMessageTransactionId, m => m.MapFrom(s => s.TransactionId.ToString()))
                .ForMember(d => d.AckType, m => m.MapFrom(s => s.AckType))
                .ForMember(d => d.Id, m => m.Ignore())
                .ForMember(d => d.ESGMessageTransaction, m => m.Ignore())
                .ForMember(d => d.CreatedOn, m => m.Ignore())
               .ForMember(d => d.AckSender, m => m.MapFrom(s => s.AckSender))
               .ForMember(d => d.AckReceiver, m => m.MapFrom(s => s.AckReceiver))
               .ForMember(d => d.MessageDate, m => m.MapFrom(s => s.MessageDate));

            CreateMap<ESGAcknowledgementDTO, ESGMessageContent>()
              .ForMember(d => d.TransactionId, m => m.Ignore())
              .ForMember(d => d.Id, m => m.Ignore())
               .ForMember(d => d.ESGMessageTransaction, m => m.Ignore())
               .ForMember(d => d.MessageType, m => m.Ignore())

              .ForMember(d => d.MessageBody, m => m.MapFrom(S => S.MessageBody));

        }
    }
}
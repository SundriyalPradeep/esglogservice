﻿using System;
using AutoMapper;
using VitalsService.Domain.DbEntities;
using VitalsService.Domain.Enums;
using VitalsService.Domain.EsbEntities;
using VitalsService.Extensions;
using VitalsService.Web.Api.Models.Mappings.Resolvers;

namespace VitalsService.Web.Api.Models.Mappings
{
    /// <summary>
    /// Mapping of interfaces to implementations for vitals API.
    /// </summary>
    public class VitalsMapping : Profile
    {
        /// <summary>
        /// Override this method in a derived class and call the CreateMap method to associate that map with this profile.
        /// Avoid calling the <see cref="T:AutoMapper.Mapper" /> class from this method.
        /// </summary>
        public VitalsMapping()
        {
            CreateMap<VitalDto, Vital>()
                .ForMember(d => d.Name, o => o.MapFrom(s => s.Name.ToString()))
                .ForMember(d => d.Unit, o => o.MapFrom(s => s.Unit.ToString()))
                .ForMember(d => d.MeasurementId, o => o.Ignore())
                .ForMember(d => d.Measurement, o => o.Ignore())
                .ForMember(d => d.VitalAlert, o => o.Ignore())
                .ForMember(d => d.Id, o => o.Ignore());

            CreateMap<MeasurementNoteDto, MeasurementNote>()
                .ForMember(d => d.MeasurementId, o => o.Ignore())
                .ForMember(d => d.Measurement, o => o.Ignore())
                .ForMember(d => d.Id, o => o.Ignore());

            CreateMap<DeviceDto, Device>()
                .ForMember(d => d.MeasurementId, o => o.Ignore())
                .ForMember(d => d.Measurement, o => o.Ignore());

            CreateMap<MeasurementRequestDto, Measurement>()
                .ForMember(d => d.ProcessingType, o => o.MapFrom(s => s.ProcessingType ?? ProcessingType.Debugging))
                .ForMember(d => d.PatientId, o => o.Ignore())
                .ForMember(d => d.CustomerId, o => o.Ignore())
                .ForMember(d => d.CreatedUtc, o => o.Ignore())
                .ForMember(d => d.UpdatedUtc, o => o.Ignore())
                //.ForMember(d => d.IsInvalidated, o => o.Ignore())
                .ForMember(d => d.MeasurementValues, o => o.Ignore())
                //.ForMember(d => d.Notes, o => o.Ignore())
                .ForMember(d => d.Id, o => o.Ignore());

            // Updated upstream
            //CreateMap<UpdateMeasurementRequestDto, Measurement>()
            //    .ForMember(d => d.PatientId, o => o.Ignore())
            //    .ForMember(d => d.CustomerId, o => o.Ignore())
            //    .ForMember(d => d.CreatedUtc, o => o.Ignore())
            //    .ForMember(d => d.UpdatedUtc, o => o.Ignore())
            //    .ForMember(d => d.ObservedUtc, o => o.Ignore())
            //    .ForMember(d => d.ObservedTz, o => o.Ignore())
            //    .ForMember(d => d.IsAutomated, o => o.Ignore())
            //    .ForMember(d => d.ProcessingType, o => o.Ignore())
            //    .ForMember(d => d.ClientId, o => o.Ignore())
            //    .ForMember(d => d.Vitals, o => o.Ignore())
            //    .ForMember(d => d.MeasurementNotes, o => o.Ignore())
            //    .ForMember(d => d.Device, o => o.Ignore())
            //    .ForMember(d => d.MeasurementValues, o => o.Ignore())
            //    .ForMember(d => d.Notes, o => o.Ignore())
            //    .ForMember(d => d.Id, o => o.Ignore());

            CreateMap<UpdateVitalRequestDto, Vital>()
                .ForMember(d => d.Id, o => o.Ignore())
                .ForMember(d => d.Measurement, o => o.Ignore())
                .ForMember(d => d.MeasurementId, o => o.Ignore())
                .ForMember(d => d.Name, o => o.Ignore())
                .ForMember(d => d.Unit, o => o.Ignore())
                .ForMember(d => d.Value, o => o.Ignore())
                .ForMember(d => d.VitalAlert, o => o.Ignore())
                .ForMember(d => d.IsInvalidated, o => o.MapFrom(s => s.IsInvalidated));

            CreateMap<UpdateMeasurementRequestDto, Measurement>()
                .ForMember(d => d.PatientId, o => o.Ignore())
                .ForMember(d => d.CustomerId, o => o.Ignore())
                .ForMember(d => d.CreatedUtc, o => o.Ignore())
                .ForMember(d => d.UpdatedUtc, o => o.Ignore())
                .ForMember(d => d.ObservedUtc, o => o.Ignore())
                .ForMember(d => d.ObservedTz, o => o.Ignore())
                .ForMember(d => d.IsAutomated, o => o.Ignore())
                .ForMember(d => d.ProcessingType, o => o.Ignore())
                .ForMember(d => d.ClientId, o => o.Ignore())
                .ForMember(d => d.Vitals, o => o.Ignore())
                .ForMember(d => d.MeasurementNotes, o => o.Ignore())
                .ForMember(d => d.Device, o => o.Ignore())
                .ForMember(d => d.MeasurementValues, o => o.Ignore())
               // .ForMember(d => d.Notes, o => o.Ignore())
                .ForMember(d => d.Id, o => o.Ignore());


            CreateMap<Measurement, MeasurementResponseDto>()
                .ForMember(d => d.HealthSessionId, m => m.ResolveUsing<MeasurementSessionIdResolver>())
                .ForMember(d => d.RawJson, o => o.Ignore());

            CreateMap<Measurement, MeasurementBriefResponseDto>();

            CreateMap<Vital, VitalDto>()
                .ForMember(d => d.Name, o => o.MapFrom(s => Enum.Parse(typeof(VitalType), s.Name, true)))
                .ForMember(d => d.Unit, o => o.MapFrom(s => Enum.Parse(typeof(UnitType), s.Unit, true)))
                .ForMember(d => d.IsAutomated, o => o.Ignore());

            CreateMap<Vital, VitalResponseDto>()
                .ForMember(d => d.IsAutomated, o => o.MapFrom(s => s.Measurement.IsAutomated));

            CreateMap<Vital, VitalBriefResponseDto>()
                .ForMember(d => d.IsAutomated, o => o.MapFrom(s => s.Measurement.IsAutomated))
            .ForMember(d => d.IsInvalidated, o => o.MapFrom(s => s.IsInvalidated));

            CreateMap<MeasurementNote, MeasurementNoteDto>();

            CreateMap<Device, DeviceDto>();

            CreateMap<Vital, VitalMessage>();

            CreateMap<Measurement, MeasurementMessage>()
                .ForMember(d => d.ProcessingType, o => o.MapFrom(s => s.ProcessingType.Description()))
                .ForMember(d => d.HealthSessionId, o => o.ResolveUsing<MeasurementSessionIdFromMsgResolver>());

            CreateMap<MeasurementNote, NoteMessage>();

            CreateMap<Note, NoteMessage>()
                .ForMember(d => d.Title, o => o.Ignore());

            CreateMap<Device, DeviceMessage>();
        }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using VitalsService.Domain.Constants;
using VitalsService.Domain.Enums;
using VitalsService.Web.Api.Resources;

namespace VitalsService.Web.Api.Models.Alerts
{
    /// <summary>
    /// Additional properties that can be specified by home sensing notifications.
    /// </summary>
    public class HomeSensingAlertRequestDto : AlertRequestDto
    {
        /// <summary>
        /// <para>The type of home sensing notification.</para>
        /// <para>NOTE: We need to handle unknown notification types, so we can't do a &quot;if value not in enum, reject&quot; check. 
        /// Home Sensing is evolving, and we need to expect new notification types to come online at arbitrary times.</para>
        /// <para>This means that unknown values will be treated and stored as integers that don't yet map to a particular enum value.</para>
        /// </summary>
        public HomeSensingNotificationType NotificationType { get; set; }

        /// <summary>
        /// The number above or below which an observed value will trigger a notification.
        /// </summary>
        [Range(
            0,
            double.MaxValue,
            ErrorMessageResourceType = typeof(GlobalStrings),
            ErrorMessageResourceName = "GreaterThanOrEqualToZero_ValidationError",
            ErrorMessage = null
        )]
        public double Threshold { get; set; }

        /// <summary>
        /// The observed value that triggered the violation.
        /// </summary>
        [Range(
            0,
            double.MaxValue,
            ErrorMessageResourceType = typeof(GlobalStrings),
            ErrorMessageResourceName = "GreaterThanOrEqualToZero_ValidationError",
            ErrorMessage = null
        )]
        public double Observed { get; set; }

        /// <summary>
        /// An optional URL to a page in Home Sensing that contains more information about the notification.
        /// </summary>
        [MaxLength(
            DbConstraints.MaxLength.HomeSensingAlertMoreInfoUrl,
            ErrorMessageResourceType = typeof(GlobalStrings),
            ErrorMessageResourceName = "MaxLengthAttribute_ValidationError",
            ErrorMessage = null
        )]
        public string MoreInfoUrl { get; set; }
    }
}
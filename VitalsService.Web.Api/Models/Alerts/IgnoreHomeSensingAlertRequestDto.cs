﻿using System;
using System.ComponentModel.DataAnnotations;
using VitalsService.Web.Api.Resources;

namespace VitalsService.Web.Api.Models.Alerts
{
    /// <summary>
    /// DTO for setting a home sensing alert to ignored.
    /// </summary>
    public class IgnoreHomeSensingAlertRequestDto
    {
        /// <summary>
        /// The id of the home sensing alert to ignore.
        /// </summary>
        [Required(
            ErrorMessageResourceType = typeof(GlobalStrings),
            ErrorMessageResourceName = "RequiredAttribute_ValidationError",
            ErrorMessage = null
        )]
        public Guid? AlertId { get; set; }

        /// <summary>
        /// Gets or sets the ignored by.
        /// </summary>
        [Required(
            ErrorMessageResourceType = typeof(GlobalStrings),
            ErrorMessageResourceName = "RequiredAttribute_ValidationError",
            ErrorMessage = null
        )]
        public Guid? IgnoredBy { get; set; }
    }
}
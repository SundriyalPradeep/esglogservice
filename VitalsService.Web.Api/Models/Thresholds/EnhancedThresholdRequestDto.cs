﻿using System;
using System.ComponentModel.DataAnnotations;
using FoolproofWebApi;
using VitalsService.Domain.Enums;
using VitalsService.Web.Api.DataAnnotations;
using VitalsService.Web.Api.Resources;

namespace VitalsService.Web.Api.Models.Thresholds
{
    /// <summary>
    /// EnhancedThresholdRequestDto.
    /// </summary>
    public class EnhancedThresholdRequestDto
    {
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        [EnumMember(
            ErrorMessageResourceType = typeof(GlobalStrings),
            ErrorMessageResourceName = "EnumMemberAttribute_ValidationError",
            ErrorMessage = null
        )]
        public EnhancedThresholdType Type { get; set; }

        /// <summary>
        /// Gets or sets the Enhanced thresholds name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Vital name.
        /// </summary>
        /// <value>
        /// The Vital name.
        /// </value>
        [EnumMember(
           ErrorMessageResourceType = typeof(GlobalStrings),
           ErrorMessageResourceName = "EnumMemberAttribute_ValidationError",
           ErrorMessage = null
       )]
        public VitalType VitalName { get; set; }

        /// <summary>
        /// Gets or sets the Enhanced thresholds status.
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets the Active status.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the unit.
        /// </summary>
        /// <value>
        /// The unit.
        /// </value>
        [ValidateUnitForVital(
            "Name",
            ErrorMessageResourceType = typeof(GlobalStrings),
            ErrorMessageResourceName = "ValidateUnitForVitalAttribute_ValidationError",
            ErrorMessage = null
        )]
        [EnumMember(
            ErrorMessageResourceType = typeof(GlobalStrings),
            ErrorMessageResourceName = "EnumMemberAttribute_ValidationError",
            ErrorMessage = null
        )]
        public UnitType Unit { get; set; }

        /// <summary>
        /// Gets or sets the TimeOrValuesCount.
        /// </summary>
        /// <value>
        /// The TimeOrValuesCount.
        /// </value>
        [Range(
            0,
            double.MaxValue,
            ErrorMessageResourceType = typeof(GlobalStrings),
            ErrorMessageResourceName = "GreaterThanZero_ValidationError",
            ErrorMessage = null
        )]
        public int TimeOrValuesCount { get; set; }

        /// <summary>
        /// Gets or sets the ComparisonOperator.
        /// </summary>
        /// <value>
        /// The ComparisonOperator.
        /// </value>
        public string ComparisonOperator { get; set; }

        /// <summary>
        /// Gets or sets the ComparisonValue.
        /// </summary>
        /// <value>
        /// The ComparisonValue.
        /// </value>
        [Range(
           0,
           double.MaxValue,
           ErrorMessageResourceType = typeof(GlobalStrings),
           ErrorMessageResourceName = "GreaterThanZero_ValidationError",
           ErrorMessage = null
       )]
        public decimal ComparisonValue { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDateUTC.
        /// </summary>
        /// <value>
        /// The CreatedDateUTC.
        /// </value>
        public DateTime CreatedDateUTC { get; set; }

        /// <summary>
        /// Gets or sets the UpdatedDateUTC.
        /// </summary>
        /// <value>
        /// The UpdatedDateUTC.
        /// </value>
        public DateTime? UpdatedDateUTC { get; set; }

        /// <summary>
        /// Gets or sets the EnhancedThresholdRange Type.
        /// </summary>
        /// <value>
        /// The EnhancedThresholdRange Type.
        /// </value>
        [EnumMember(
            ErrorMessageResourceType = typeof(GlobalStrings),
            ErrorMessageResourceName = "EnumMemberAttribute_ValidationError",
            ErrorMessage = null
        )]
        public EnhancedThresholdRangeType RangeType { get; set; }

        /// <summary>
        /// Gets or sets the EnhancedThreshold SetAnalysisType.
        /// </summary>
        /// <value>
        /// The EnhancedThreshold SetAnalysisType.
        /// </value>
        [EnumMember(
            ErrorMessageResourceType = typeof(GlobalStrings),
            ErrorMessageResourceName = "EnumMemberAttribute_ValidationError",
            ErrorMessage = null
        )]
        public EnhancedThresholdSetAnalysisType SetAnalysisType { get; set; }

        /// <summary>
        /// Gets or sets the ConditionsId.
        /// </summary>
        /// <value>
        /// The ConditionsId.
        /// </value>
        public Guid? ConditionId { get; set; }

        /// <summary>
        /// Gets or sets the PatientsId.
        /// </summary>
        /// <value>
        /// The PatientsId.
        /// </value>
        public Guid? PatientId { get; set; }
    }
}
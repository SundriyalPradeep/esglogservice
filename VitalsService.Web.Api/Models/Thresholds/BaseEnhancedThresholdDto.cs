﻿using System;
using System.ComponentModel.DataAnnotations;
using FoolproofWebApi;
using VitalsService.Domain.Enums;
using VitalsService.Web.Api.DataAnnotations;
using VitalsService.Web.Api.Resources;
using VitalsService.Domain.DbEntities;
using VitalsService.Web.Api.Models.Conditions;

namespace VitalsService.Web.Api.Models.Thresholds
{
    /// <summary>
    /// BaseThresholdDto.
    /// </summary>
    public class BaseEnhancedThresholdDto
    {
        /// <summary>
        /// The Enhanced threshold id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        [EnumMember(
            ErrorMessageResourceType = typeof(GlobalStrings), 
            ErrorMessageResourceName = "EnumMemberAttribute_ValidationError", 
            ErrorMessage = null
        )]
        public EnhancedThresholdType Type { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [EnumMember(
            ErrorMessageResourceType = typeof(GlobalStrings), 
            ErrorMessageResourceName = "EnumMemberAttribute_ValidationError", 
            ErrorMessage = null
        )]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Vital name.
        /// </summary>
        /// <value>
        /// The Vital name.
        /// </value>
        [EnumMember(
          ErrorMessageResourceType = typeof(GlobalStrings),
          ErrorMessageResourceName = "EnumMemberAttribute_ValidationError",
          ErrorMessage = null
        )]
        public VitalType VitalName { get; set; }

        /// <summary>
        /// Gets or sets the Enhanced thresholds status.
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets the Archived status.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the unit.
        /// </summary>
        /// <value>
        /// The unit.
        /// </value>
        [ValidateUnitForVital(
            "Name",
            ErrorMessageResourceType = typeof(GlobalStrings),
            ErrorMessageResourceName = "ValidateUnitForVitalAttribute_ValidationError",
            ErrorMessage = null
        )]
        [EnumMember(
            ErrorMessageResourceType = typeof(GlobalStrings),
            ErrorMessageResourceName = "EnumMemberAttribute_ValidationError",
            ErrorMessage = null
        )]
        public UnitType Unit { get; set; }

        /// <summary>
        /// Gets or sets the TimeOrValuesCount.
        /// </summary>
        /// <value>
        /// The TimeOrValuesCount.
        /// </value>
        [Range(
            0,
            double.MaxValue,
            ErrorMessageResourceType = typeof(GlobalStrings),
            ErrorMessageResourceName = "GreaterThanZero_ValidationError",
            ErrorMessage = null
        )]
        public int TimeOrValuesCount { get; set; }

        /// <summary>
        /// Gets or sets the ComparisonOperator.
        /// </summary>
        /// <value>
        /// The ComparisonOperator.
        /// </value>
        public string ComparisonOperator { get; set; }

        /// <summary>
        /// Gets or sets the ComparisonValue.
        /// </summary>
        /// <value>
        /// The ComparisonValue.
        /// </value>
        [Range(
           0,
           double.MaxValue,
           ErrorMessageResourceType = typeof(GlobalStrings),
           ErrorMessageResourceName = "GreaterThanZero_ValidationError",
           ErrorMessage = null
       )]
        public decimal ComparisonValue { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDateUTC.
        /// </summary>
        /// <value>
        /// The CreatedDateUTC.
        /// </value>
        public DateTime CreatedDateUTC { get; set; }

        /// <summary>
        /// Gets or sets the UpdatedDateUTC.
        /// </summary>
        /// <value>
        /// The UpdatedDateUTC.
        /// </value>
        public DateTime? UpdatedDateUTC { get; set; }


        /// <summary>
        /// Gets or sets the EnhancedThresholdRange Type.
        /// </summary>
        /// <value>
        /// The EnhancedThresholdRange Type.
        /// </value>
        public EnhancedThresholdRangeType RangeType { get; set; }

        /// <summary>
        /// Gets or sets the EnhancedThreshold SetAnalysisType.
        /// </summary>
        /// <value>
        /// The EnhancedThreshold SetAnalysisType.
        /// </value>
        public EnhancedThresholdSetAnalysisType SetAnalysisType { get; set; }

        /// <summary>
        /// Gets or sets the ETConditionsId.
        /// </summary>
        /// <value>
        /// The ETConditionsId.
        /// </value>
        public EnhancedThresholdCondition ETConditions { get; set; }

        /// <summary>
        /// Gets or sets the ETPatientsId.
        /// </summary>
        /// <value>
        /// The ETPatientsId.
        /// </value>
        public EnhancedThresholdPatient ETPatients { get; set; }

        /// <summary>
        /// Gets or sets the Conditions.
        /// </summary>
        /// <value>
        /// The Conditions.
        /// </value>
        public ConditionResponseDto Conditions { get; set; }

    }
}
﻿using System;

using VitalsService.Domain.Enums;

namespace VitalsService.Web.Api.Models.Thresholds
{
    /// <summary>
    /// DefaultThresholdDto.
    /// </summary>
    public class DefaultThresholdDto : BaseThresholdDto
    {
        /// <summary>
        /// Gets or sets the default type.
        /// </summary>
        /// <value>
        /// The default type.
        /// </value>
        public ThresholdDefaultType DefaultType { get; set; }

        /// <summary>
        /// Gets or sets is conditional.
        /// </summary>
        /// <value>
        /// The conditional identifier.
        /// </value>
        public bool? IsConditional { get; set; }
    }
}
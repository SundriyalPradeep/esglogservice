﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using VitalsService.Web.Api.DataAnnotations;
using VitalsService.Web.Api.Resources;

namespace VitalsService.Web.Api.Models.Thresholds
{
    /// <summary>
    /// IgnoreETAlertsRequestDto.
    /// </summary>
    public class IgnoreETAlertsRequestDto
    {
        /// <summary>
        /// Gets or sets the ET alert ids.
        /// </summary>
        /// <value>
        /// The ET alert ids.
        /// </value>
        [ItemsRequired(
            ErrorMessageResourceType = typeof(GlobalStrings),
            ErrorMessageResourceName = "ItemsRequired_ValidationError",
            ErrorMessage = null
        )]
        public IList<Guid> ETAlertIds { get; set; }
    }
}
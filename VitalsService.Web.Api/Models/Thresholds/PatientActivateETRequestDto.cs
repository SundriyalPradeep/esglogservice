﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VitalsService.Domain.Enums;

namespace VitalsService.Web.Api.Models.Thresholds
{
    /// <summary>
    /// PatientActivateETRequestDto.
    /// </summary>
    public class PatientActivateETRequestDto
    {
        /// <summary>
        /// Gets or sets the patient identifier.
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
        public Guid PatientId { get; set; }

        /// <summary>
        /// ET Type.
        /// </summary>
        /// <value>
        /// The Enhanced threshold type Customer(Organization)/Condition.
        /// </value>
        public EnhancedThresholdType Type { get; set; }

        /// <summary>
        /// ET Id.
        /// </summary>
        /// <value>
        /// The Customer(Organization)/Condition level Enhanced threshold Id
        /// </value>
        public Guid EnhancedThresholdId { get; set; }

        /// <summary>
        /// IsActive
        /// </summary>
        /// <value>
        /// Set whether the Customer(Organization)/Condition ET is active or not
        /// </value>
        public bool isActive { get; set; }
    }
}
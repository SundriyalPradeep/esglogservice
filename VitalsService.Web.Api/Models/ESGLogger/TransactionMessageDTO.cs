﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VitalsService.Web.Api.Models.ESGLogger
{

    /// <summary>
    /// 
    /// </summary>
    public class TransactionMessageDTO
    {
        /// <summary>
        /// 15 digit unique id
        /// </summary>
        //public int Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string MessageControlId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string MessageEvent { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string MessageType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ICN { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DFN { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SSN { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EnrollmentDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DisEnrollmentDate { get; set; }
        /// <summary>
        /// <summary>
        /// <summary>
        /// 
        /// </summary>
        public string SendingFacilityID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string SendingFacility { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ReceivingFacility { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <summary>
        /// 
        /// </summary>
        public string MessageDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? InsertedOn { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string MessageBody { get; set; }

        /// <summary>
        /// This takes A04 messge control Id along with QBP message logging
        /// </summary>
        public Guid RegistrationId { get; set; }

    }
}
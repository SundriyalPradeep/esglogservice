﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VitalsService.Web.Api.Models.ESGLogger
{
    /// <summary>
    /// 
    /// </summary>
    public class ESGAcknowledgementDTO
    {
        /// <summary>
        /// 
        /// </summary>
        //public Guid Id { get; set; }
        /// <summary>
        /// ESG will send this ID. Integer of 15 digit
        /// </summary>
        //public Guid Id { get; set; }

        //public Guid ESGMessageContentId { get; set; }

        public Guid TransactionId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AckType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AckSender { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AckReceiver { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string MessageDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? InsertedOn { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string MessageBody { get; set; }

    }

}
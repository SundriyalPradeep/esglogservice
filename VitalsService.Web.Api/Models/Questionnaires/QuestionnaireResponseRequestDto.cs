﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VitalsService.Web.Api.Models.Questionnaires
{
    /// <summary>
    /// Dto for creating a QuestionnaireResponse.
    /// </summary>
    public class QuestionnaireResponseRequestDto : IValidatableObject
    {
        /// <summary>
        /// The questionnaire they took.
        /// </summary>
        public Guid QuestionnaireId { get; set; }

        /// <summary>
        /// The localized name of the questionnaire.
        /// </summary>
        [Required]
        [MaxLength(100)]
        public string QuestionnaireName { get; set; }

        /// <summary>
        /// The clinician who administered the questionnaire.
        /// </summary>
        public Guid ClinicianId { get; set; }

        /// <summary>
        /// Questions and their answers.
        /// </summary>
        public List<QuestionnaireQuestionAnswerDto> QuestionAnswers { get; private set; } = new List<QuestionnaireQuestionAnswerDto>();

        ///Added below properties as we are saving questionnaires and recored action in one go
        ///Calculation of PreStratification Score is done seperately.
        /// <summary>
        /// The answer chosen by the clinician.
        /// </summary>
        public Guid ActionTakenAnswerChoiceId { get; set; }

        /// <summary>
        /// The text value of answer chosen by the clinician. Captured for auditing purposes.
        /// </summary>]
        public string ActionTakenAnswerChoiceText { get; set; }

        /// <summary>
        /// Optional free text entered by the clinician.
        /// </summary>
        public string ActionTakenFreeText { get; set; }

        /// <summary>
        /// The id of the &quot;Exacerbation&quot; that the client determined by examining the pre-stratification score
        /// returned by the vitals service.
        /// </summary>
        public Guid ExacerbationAnswerChoiceId { get; set; }

        /// <summary>
        /// The localized text of the &quot;Exacerbation&quot; that the client determined. The client makes this determination
        /// because the vitals service has no knowledge of the Health Library service, where the questionnaires are defined.
        /// </summary>
        public string ExacerbationAnswerChoiceText { get; set; }

        /// <summary>
        /// The calculated pre-stratification risk score based on the answers supplied with the questionnaire response.
        /// </summary>
        public int TotalPreStratificationRiskScore { get; set; }

        /// <summary>
        /// ToString override.
        /// </summary>
        /// <returns></returns>
        public override string ToString() => $"{ActionTakenAnswerChoiceText} ({ActionTakenFreeText ?? "(no notes entered)"})";
        /// <summary>
        /// For validating the list of question/answers. We're using this instead of a custom validator attribute
        /// so that we can collect all QuestionAnswer errors at once instead of one at a time.
        /// </summary>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validationResults = new List<ValidationResult>();

            // There must be at least one question/answer.
            if (QuestionAnswers.Count == 0)
            {
                validationResults.Add(new ValidationResult($"{nameof(QuestionAnswers)} must not be empty", new[] { nameof(QuestionAnswers) }));
            }

            foreach (var questionAnswer in QuestionAnswers)
            {
                Validator.TryValidateObject(questionAnswer, new ValidationContext(questionAnswer), validationResults, validateAllProperties: true);
            }

            return validationResults;
        }
    }
}
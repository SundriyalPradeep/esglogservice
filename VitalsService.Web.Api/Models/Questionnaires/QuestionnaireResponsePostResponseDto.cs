﻿using System;

namespace VitalsService.Web.Api.Models.Questionnaires
{
    /// <summary>
    /// 
    /// </summary>
    public class QuestionnaireResponsePostResponseDto : PostResponseDto<Guid>
    {
        /// <summary>
        /// The calculated pre-stratification risk score based on the answers supplied with the questionnaire response.
        /// </summary>
        public int TotalPreStratificationRiskScore { get; set; }
    }
}
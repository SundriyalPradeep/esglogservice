﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VitalsService.Web.Api.Models.Questionnaires
{
    /// <summary>
    /// Records the action taken by a clinician after administering a questionnaire and reviewing
    /// the pre-stratification score.
    /// </summary>
    public class QuestionnaireResponseActionTakenDto
    {
        /// <summary>
        /// The answer chosen by the clinician.
        /// </summary>
        public Guid ActionTakenAnswerChoiceId { get; set; }

        /// <summary>
        /// The text value of answer chosen by the clinician. Captured for auditing purposes.
        /// </summary>
        [Required]
        [MaxLength(1024)]
        public string ActionTakenAnswerChoiceText { get; set; }

        /// <summary>
        /// Optional free text entered by the clinician.
        /// </summary>
        [MaxLength(1024)]
        public string ActionTakenFreeText { get; set; }

        /// <summary>
        /// The id of the &quot;Exacerbation&quot; that the client determined by examining the pre-stratification score
        /// returned by the vitals service.
        /// </summary>
        public Guid ExacerbationAnswerChoiceId { get; set; }

        /// <summary>
        /// The localized text of the &quot;Exacerbation&quot; that the client determined. The client makes this determination
        /// because the vitals service has no knowledge of the Health Library service, where the questionnaires are defined.
        /// </summary>
        [Required]
        [MaxLength(100)]
        public string ExacerbationAnswerChoiceText { get; set; }


        /// <summary>
        /// ToString override.
        /// </summary>
        /// <returns></returns>
        public override string ToString() => $"{ActionTakenAnswerChoiceText} ({ActionTakenFreeText ?? "(no notes entered)"})";
    }
}
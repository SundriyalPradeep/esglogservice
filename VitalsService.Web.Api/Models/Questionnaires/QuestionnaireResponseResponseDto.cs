﻿using System;
using System.Collections.Generic;

namespace VitalsService.Web.Api.Models.Questionnaires
{
    /// <summary>
    /// For reading responses from the service.
    /// </summary>
    public class QuestionnaireResponseResponseDto
    {
        /// <summary>
        /// The questionnaire response id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The questionnaire they took.
        /// </summary>
        public Guid QuestionnaireId { get; set; }

        /// <summary>
        /// Name of the Questionnaire 
        /// </summary>
        public string QuestionnaireName { get; set; }

        /// <summary>
        /// The clinician who administered the questionnaire.
        /// </summary>
        public Guid ClinicianId { get; set; }

        /// <summary>
        /// Questions and their answers.
        /// </summary>
        public List<QuestionnaireQuestionAnswerDto> QuestionAnswers { get; private set; } = new List<QuestionnaireQuestionAnswerDto>();

        /// <summary>
        /// The computed pre-stratification risk score, based on a subset of the questions/answers.
        /// </summary>
        public int TotalPreStratificationRiskScore { get; set; }

        /// <summary>
        /// The action chosen by the clinician.
        /// </summary>
        public Guid? ActionTakenAnswerChoiceId { get; set; }

        /// <summary>
        /// The action chosen by the clinician.
        /// </summary>
        public string ActionTakenAnswerChoiceText { get; set; }

        /// <summary>
        /// Any free text notes added by the clinician.
        /// </summary>
        public string ActionTakenFreeText { get; set; }

        /// <summary>
        /// When the questionnaire response was recorded in the database.
        /// </summary>
        public DateTime CreatedUtc { get; set; }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using VitalsService.Domain.Enums.Questionnaires;
using VitalsService.Web.Api.DataAnnotations.Questionnaires;

namespace VitalsService.Web.Api.Models.Questionnaires
{
    /// <summary>
    /// Information about the question/answer on a questionnaire.
    /// </summary>
    public class QuestionnaireQuestionAnswerDto
    {
        /// <summary>
        /// The question id.
        /// </summary>
        public Guid QuestionId { get; set; }

        /// <summary>
        /// Record the question text at the time the clinician administered the questionnaire.
        /// </summary>
        [Required]
        [StringLength(1024)]
        public string QuestionText { get; set; }

        /// <summary>
        /// The type of question, used for UI grouping and scoring.
        /// </summary>
        [QuestionnaireQuestionTypeIsValid]
        public QuestionnaireQuestionType QuestionType { get; set; }

        /// <summary>
        /// Single select or measurement.
        /// </summary>
        [QuestionnaireAnswerTypeIsValid]
        public QuestionnaireAnswerType AnswerType { get; set; }

        /// <summary>
        /// If it's a single select question, the id of the selected answer choice.
        /// </summary>
        [RequiredIfAnswerTypeIsSingleSelect]
        public Guid? AnswerChoiceId { get; set; }

        /// <summary>
        /// If it's a single select question, record the answer choice text at the time the clinician administered the questionnaire.
        /// </summary>
        [RequiredIfAnswerTypeIsSingleSelect]
        [StringLength(1024)]
        public string AnswerChoiceText { get; set; }

        /// <summary>
        /// If it's a single select, pre-stratification question, it must also have an accompanying pre-stratification score.
        /// </summary>
        [RequiredIfQuestionTypeIsPreStratificationAndAnswerTypeIsSingleSelect]
        [Range(0, 3)]
        public int? AnswerChoiceScore { get; set; }

        /// <summary>
        /// If it's a measurement question, the type of measurement the clinician collected.
        /// </summary>
        [RequiredIfAnswerTypeIsMeasurement]
        [QuestionnaireMeasurementTypeIsValid]
        public QuestionnaireMeasurementType? MeasurementType { get; set; }

        /// <summary>
        /// If it's a measurement question, the measurement value recorded by the clinician.
        /// </summary>
        [RequiredIfAnswerTypeIsMeasurement]
        public decimal? MeasurementValue { get; set; }

        public string MeasurementUnit { get; set; }

        /// <summary>
        /// For debugging
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return AnswerType == QuestionnaireAnswerType.SingleSelect
                ? $"({QuestionType} - {AnswerType}) {QuestionText}: {AnswerChoiceText}"
                : $"({QuestionType} - {AnswerType}) {QuestionText}: {MeasurementType} = {MeasurementValue}";
        }
    }
}
﻿using System;
using AutoMapper;
using VitalsService.Domain.DbEntities;
using VitalsService.Domain.Enums;
using VitalsService.Web.Api.Models.HealthSessions;

namespace VitalsService.Web.Api.Models.Converters
{
    /// <summary>
    /// Converts element value model to appropriate entity.
    /// </summary>
    public class HealthSessionElementValueTypeConverter : ITypeConverter<HealthSessionElementValueDto, HealthSessionElementValue>
    {
        /// <summary>
        /// Performs conversion from source to destination type
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException">Mapping is not supported for specified element type.</exception>
        public HealthSessionElementValue Convert(HealthSessionElementValueDto source, HealthSessionElementValue destination, ResolutionContext context)
        {
            if (source == null)
            {
                return null;
            }

            switch (source.Type)
            {
                case HealthSessionElementValueType.SelectionAnswer:
                    return context.Mapper.Map<SelectionAnswer>(source);

                case HealthSessionElementValueType.ScaleAnswer:
                    return context.Mapper.Map<ScaleAnswer>(source);

                case HealthSessionElementValueType.OpenEndedAnswer:
                    return context.Mapper.Map<FreeFormAnswer>(source);

                case HealthSessionElementValueType.StethoscopeAnswer:
                    return context.Mapper.Map<AssessmentValue>(source);

                default:
                    throw new NotSupportedException("Mapping is not supported for specified element type.");
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VitalsService.Web.Api.DataAnnotations;
using VitalsService.Web.Api.Resources;
using System.ComponentModel.DataAnnotations;
namespace VitalsService.Web.Api.Models.PatientNotes
{
    
    /// <summary>
    /// The base model for NotesMeasurement
    /// </summary>
    public class NotesMeasurementDto
    {
        /// <summary>
        /// Gets or sets the Measurements.
        /// </summary>
        /// <value>
        /// The Measurements.
        /// </value>
        public MeasurementResponseDto Measurement { get; set; }

        /// <summary>
        /// Gets or sets the measurement identifier.
        /// </summary>
        /// <value>
        /// The measurement identifier.
        /// </value>
        public Guid MeasurementId { get; set; }

    }
}
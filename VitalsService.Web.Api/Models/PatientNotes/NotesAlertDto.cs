﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VitalsService.Web.Api.DataAnnotations;
using VitalsService.Web.Api.Resources;
using System.ComponentModel.DataAnnotations;

namespace VitalsService.Web.Api.Models.PatientNotes
{
    /// <summary>
    /// The base model for NotesAlert
    /// </summary>
    public class NotesAlertDto
    {
        /// <summary>
        /// Gets or sets the Alerts.
        /// </summary>
        /// <value>
        /// The Alerts.
        /// </value>
        public Alerts.BaseAlertResponseDto Alert { get; set; }

        /// <summary>
        /// Gets or sets the Alerts identifier.
        /// </summary>
        /// <value>
        /// The Alert identifier.
        /// </value>
        public Guid AlertId { get; set; }
    }
}
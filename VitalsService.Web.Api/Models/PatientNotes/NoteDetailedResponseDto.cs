﻿using System;
using System.Collections.Generic;
using VitalsService.Web.Api.Models.Alerts;
using VitalsService.Web.Api.Models.HealthSessions;

namespace VitalsService.Web.Api.Models.PatientNotes
{
    /// <summary>
    /// The model for detailed note response
    /// </summary>
    public class NoteDetailedResponseDto : BaseNoteDto
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the created UTC.
        /// </summary>
        public DateTime CreatedUtc { get; set; }

        /// <summary>
        /// Gets or sets the Measurements.
        /// </summary>
        /// <value>
        /// The Measurements.
        /// </value>
        public IList<NotesMeasurementDto> NotesMeasurements { get; set; }
        /// <summary>
        /// Gets or sets the Alerts.
        /// </summary>
        /// <value>
        /// The Alerts.
        /// </value>
        public IList<NotesAlertDto> NotesAlerts { get; set; }

        /// <summary>
        /// Gets or sets the health session element reading assigned to the note.
        /// </summary>
        public HealthSessionElementResponseDto HealthSessionElement { get; set; }
        
    }
}
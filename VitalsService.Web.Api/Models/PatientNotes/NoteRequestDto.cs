﻿using System;
using System.Collections.Generic;

namespace VitalsService.Web.Api.Models.PatientNotes
{
    /// <summary>
    /// NoteRequestDto.
    /// </summary>
    public class NoteRequestDto: BaseNoteDto
    {
        
        /// <summary>
        /// Gets or sets the health session element identifier.
        /// </summary>
        public Guid? HealthSessionElementId { get; set; }

  

        /// <summary>
        /// Gets or sets the Measurements.
        /// </summary>
        /// <value>
        /// The Measurements.
        /// </value>
        public IList<Guid> MeasurementIds { get; set; }


        /// <summary>
        /// Gets or sets the NotesMeasurements.
        /// </summary>
        /// <value>
        /// The NotesMeasurements.
        /// </value>
        public IList<NotesMeasurementDto> NotesMeasurements { get; set; }

        /// <summary>
        /// Gets or sets the Alerts.
        /// </summary>
        /// <value>
        /// The Alerts.
        /// </value>
        public IList<Guid> AlertIds { get; set; }


        /// <summary>
        /// Gets or sets the NotesAlerts.
        /// </summary>
        /// <value>
        /// The NotesAlerts.
        /// </value>
        public IList<NotesAlertDto> NotesAlerts { get; set; }

    }
}
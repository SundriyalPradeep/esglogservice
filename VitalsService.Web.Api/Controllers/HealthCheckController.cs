﻿using System;
using System.Data.Entity;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Web.Http;
using VitalsService.DataAccess.EF.Contexts;
using WebApi2.RedisOutputCache;

namespace VitalsService.Web.Api.Controllers
{
    /// <summary>
    /// A simple health check for IT to monitor availability of the service.
    /// </summary>
    public class HealthCheckController : ApiController
    {
        /// <summary>
        /// Health check method.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("healthcheck/prtg")]
        [IgnoreOutputCache]
        public async Task<IHttpActionResult> Prtg()
        {
            var isOkay = true;
            var applicationUptime = DateTime.UtcNow - WebApiApplication.ApplicationStartedUtc;
            var timer = Stopwatch.StartNew();

            Exception ex = null;

            try
            {
                // Use of SqlAzureExecutionStrategy means we can't have user-initiated transactions like we do in
                //   all of the other health checks.
                using (var db = new VitalsDataContext("Vitals_Shared"))
                {
                    var reg = await db.AlertSeverities
                        .AsNoTracking()
                        .FirstOrDefaultAsync()
                        .ConfigureAwait(false);

                    timer.Stop();
                }
            }
            catch (Exception e)
            {
                timer.Stop();

                isOkay = false;
                ex = e;
            }

            return Ok(new
            {
                okay = isOkay,
                ms = timer.ElapsedMilliseconds,
                age = applicationUptime,
                machine = Environment.MachineName,
                e = ex
            });
        }
    }
}
﻿using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using VitalsService.Web.Api.Helpers.Interfaces;
using VitalsService.Web.Api.Models.ESGLogger;

namespace VitalsService.Web.Api.Controllers
{

    /// <summary>
    /// Pradeep Sundriyal
    /// </summary>
    public class EsgMessageController : ApiController
    {
        private readonly IEsgMessageControllerHelper esgMessageControllerHelper;

        public EsgMessageController()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageLogHelper"></param>
        public EsgMessageController(IEsgMessageControllerHelper messageLogHelper)
        {
            this.esgMessageControllerHelper = messageLogHelper;
        }

        [Route(@"api/Messsage")]
        [HttpGet]
        //[Route(@"api/{MessageControlId:string}")]
        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.OK, "Demo Response.", typeof(string))]
        public async Task<IHttpActionResult> GetMessageControlId(string MessageControlId)
        {

            var data = await esgMessageControllerHelper.GetTransactionId(MessageControlId);
            return Ok(data);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ICN"></param>
        /// <param name="DFN"></param>
        ///  <param name="MessageType"></param>
        /// <returns></returns>
        //[Route(@"api/MessageContent")]
        //[HttpGet]
        ////[Route(@"api/{MessageControlId:string}")]
        //[ResponseType(typeof(string))]
        //[SwaggerResponse(HttpStatusCode.OK, "Demo Response.", typeof(string))]
        //public async Task<IHttpActionResult> GetMessageContent(string ICN, string DFN, string MessageType)
        //{

        //    var data = await esgMessageControllerHelper.GetMessageContent(ICN, DFN, MessageType);
        //    return Ok(data);

        //}


        [Route(@"api/PatientDfn")]
        [HttpGet]
        //[Route(@"api/{MessageControlId:string}")]
        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.OK, "Demo Response.", typeof(string))]
        public async Task<IHttpActionResult> GetPatientDfn(string ICN, string SSN, string MessageType)
        {

            var data = await esgMessageControllerHelper.GetDFN(ICN, SSN, MessageType);
            return Ok(data);

        }

        //public async Task<IHttpActionResult> Get(Guid transactionId)
        //{
        //    //var data = await manager.LogMessage(null);
        //    return null;// Ok(data);
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route(@"api/Messsage")]
        [HttpPost]
        [ResponseType(typeof(Guid))]
        [SwaggerResponse(HttpStatusCode.OK, "Demo Response.", typeof(Guid))]
        public async Task<IHttpActionResult> Post(TransactionMessageDTO model)
        {
            if (model == null)
                return BadRequest();
            // var result = await messageLogControllerHelper.LogMessage(model);

            //var item = mapper.Map<TransactionMessage>(model);
            var data = await esgMessageControllerHelper.LogMessage(model);
            return Ok(data);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route(@"api/MesssagAck")]
        [HttpPost]
        [ResponseType(typeof(Guid))]
        [SwaggerResponse(HttpStatusCode.OK, "Demo Response.", typeof(Guid))]

        public async Task<IHttpActionResult> StoreAck(ESGAcknowledgementDTO model)
        {
            if (model == null)
                return BadRequest();
            // var result = await messageLogControllerHelper.LogMessage(model);

            //var item = mapper.Map<TransactionMessage>(model);
            var data = await esgMessageControllerHelper.LogMessageAck(model);
            return Ok(data);
        }

        /// <summary>
        /// PatientEnrollmentDate
        /// </summary>
        /// <param name="ICN"></param>
        /// <returns></returns>
        [Route(@"api/PatientEnrollmentDate")]
        [HttpGet]
        //[Route(@"api/{MessageControlId:string}")]
        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.OK, "Demo Response.", typeof(string))]
        public async Task<IHttpActionResult> PatientEnrollmentDate(string ICN)
        {

            var data = await esgMessageControllerHelper.GetEnrollmentDate(ICN);
            return Ok(data);

        }

        [Route(@"api/RegistrationMessage")]
        [HttpGet]
        [ResponseType(typeof(string))]
        [SwaggerResponse(HttpStatusCode.OK, "Demo Response.", typeof(string))]
        public async Task<IHttpActionResult> GetRegistrationMessage(string QBPMessageControlId)
        {
            var data = await esgMessageControllerHelper.GetRegistrationMessage(QBPMessageControlId);
            return Ok(data);
        }
    }
}
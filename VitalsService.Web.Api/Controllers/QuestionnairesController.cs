﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Swashbuckle.Swagger.Annotations;
using VitalsService.Domain.Dtos;
using VitalsService.Domain.Enums.Questionnaires;
using VitalsService.Extensions;
using VitalsService.Web.Api.Filters;
using VitalsService.Web.Api.Helpers.Interfaces;
using VitalsService.Web.Api.Models;
using VitalsService.Web.Api.Models.Enums;
using VitalsService.Web.Api.Models.Questionnaires;

namespace VitalsService.Web.Api.Controllers
{
    /// <summary>
    /// Exposes functionality for managing questionnaire responses.
    /// </summary>
   // [TokenAuthorize]
    public class QuestionnairesController : BaseApiController
    {
        private readonly IQuestionnairesControllerHelper _questionnairesControllerHelper;

        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="questionnairesControllerHelper"></param>
        public QuestionnairesController(IQuestionnairesControllerHelper questionnairesControllerHelper)
        {
            _questionnairesControllerHelper = questionnairesControllerHelper;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route(@"api/{customerId:regex(^[1-9]\d*)}/questionnaires/{patientId:guid}")]
        [ResponseType(typeof(QuestionnaireResponsePostResponseDto))]
        [InvalidateOutputCache(typeof(QuestionnairesController), nameof(QuestionnairesController.GetQuestionnaireResponsesForPatient), "patientId")]
        public async Task<IHttpActionResult> CreateQuestionnaireResponse(int customerId, Guid patientId, QuestionnaireResponseRequestDto request)
        {
            var result = await _questionnairesControllerHelper.CreateQuestionnaireResponseAsync(customerId, patientId, request);

            var location = new Uri(Request.RequestUri, result.Content.Id.ToString());
            return Created(location, result.Content);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route(@"api/{customerId:regex(^[1-9]\d*)}/questionnaires/score/{patientId:guid}")]
        [ResponseType(typeof(QuestionnaireResponsePostResponseDto))]
        public async Task<IHttpActionResult> CalculatePreStratificationScore(int customerId, Guid patientId, QuestionnaireResponseRequestDto request)
        {
            var result = await _questionnairesControllerHelper.CalculatePreStratificationScore(customerId, patientId, request);

            var location = new Uri(Request.RequestUri, result.Content.Id.ToString());
            return Created(location, result.Content);
        }

        /// <summary>
        /// After we calculate the pre-stratification score, the clinician has to pick an action, and can optionally provide free text
        /// before we consider the questionnaire finalized.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <param name="questionnaireResponseId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Route(@"api/{customerId:regex(^[1-9]\d*)}/questionnaires/{patientId:guid}/response/{questionnaireResponseId:guid}")]
        [InvalidateOutputCache(typeof(QuestionnairesController), nameof(QuestionnairesController.GetQuestionnaireResponsesForPatient), "patientId")]
        [InvalidateOutputCache(typeof(QuestionnairesController), nameof(QuestionnairesController.GetQuestionnaireResponseForPatient), "questionnaireResponseId")]
        public async Task<IHttpActionResult> RecordActionTaken(int customerId, Guid patientId, Guid questionnaireResponseId, [FromBody] QuestionnaireResponseActionTakenDto request)
        {
            var result = await _questionnairesControllerHelper.RecordActionTakenAsync(customerId, patientId, questionnaireResponseId, request);

            if (result.HasFlag(UpdateQuestionnaireResponseStatus.QuestionnaireResponseNotFound))
            {
                return Content(
                    HttpStatusCode.NotFound,
                    new ErrorResponseDto
                    {
                        Error = ErrorCode.InvalidRequest,
                        Message = ErrorCode.InvalidRequest.Description(),
                        Details = UpdateQuestionnaireResponseStatus.QuestionnaireResponseNotFound.Description()
                    }
                );
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Get questionnaire responses for a patient.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [Route(@"api/{customerId:regex(^[1-9]\d*)}/questionnaires/{patientId:guid}")]
        [ResponseType(typeof(PagedResultDto<QuestionnaireResponseResponseDto>))]
        [SwaggerResponse(HttpStatusCode.OK, "Response with questionnaire responses.", typeof(PagedResultDto<QuestionnaireResponseResponseDto>))]
        public async Task<IHttpActionResult> GetQuestionnaireResponsesForPatient(int customerId, Guid patientId, [FromUri] BaseSearchDto request)
        {
            var result = await _questionnairesControllerHelper.GetQuestionnaireResponsesAsync(customerId, patientId, request);

            return Ok(result);
        }

        /// <summary>
        /// Get a single questionnaire response for a patient.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <param name="questionnaireResponseId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route(@"api/{customerId:regex(^[1-9]\d*)}/questionnaires/{patientId:guid}/response/{questionnaireResponseId:guid}")]
        [ResponseType(typeof(QuestionnaireResponseResponseDto))]
        [SwaggerResponse(HttpStatusCode.OK, "Response with a questionnaire response.", typeof(QuestionnaireResponseResponseDto))]
        public async Task<IHttpActionResult> GetQuestionnaireResponseForPatient(int customerId, Guid patientId, Guid questionnaireResponseId)
        {
            var result = await _questionnairesControllerHelper.GetQuestionnaireResponseAsync(customerId, patientId, questionnaireResponseId);

            if (result.Status == GetQuestionnaireReponseStatus.QuestionnaireResponseNotFound)
            {
                return Content(
                    HttpStatusCode.NotFound,
                    new ErrorResponseDto
                    {
                        Error = ErrorCode.InvalidRequest,
                        Message = ErrorCode.InvalidRequest.Description(),
                        Details = result.Status.Description()
                    }
                );
            }

            return Ok(result.Content);
        }
    }
}
 
﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Swashbuckle.Swagger.Annotations;
using VitalsService.Domain.Dtos;
using VitalsService.Domain.Enums;
using VitalsService.Extensions;
using VitalsService.Web.Api.Filters;
using VitalsService.Web.Api.Helpers.Interfaces;
using VitalsService.Web.Api.Models;
using VitalsService.Web.Api.Models.Enums;
using VitalsService.Web.Api.Models.Thresholds;

namespace VitalsService.Web.Api.Controllers
{
    /// <summary>
    /// EnhancedThresholdsController.
    /// </summary>
    [TokenAuthorize]
    [RoutePrefix(@"api/{customerId:regex(^[1-9]\d*)}/enhancedthresholds")]
    public class EnhancedThresholdsController : ApiController
    {
        private readonly IEnhancedThresholdsControllerHelper enhancedthresholdsControllerHelper;

        /// <summary>
        /// Initializes a new instance of the <see cref="EnhancedThresholdsController" /> class.
        /// </summary>
        /// <param name="enhancedthresholdsControllerHelper">The thresholds controller helper.</param>
        public EnhancedThresholdsController(IEnhancedThresholdsControllerHelper enhancedthresholdsControllerHelper)
        {
            this.enhancedthresholdsControllerHelper = enhancedthresholdsControllerHelper;
        }


        /// <summary>
        /// Creates the Enhanced threshold.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(PostResponseDto<Guid>))]
        [InvalidateOutputCache(typeof(EnhancedThresholdsController), nameof(EnhancedThresholdsController.GetEnhancedThresholds), "customerId")]
        public async Task<IHttpActionResult> CreateEnhancedThreshold(
            int customerId,
            EnhancedThresholdRequestDto request
        )
        {
            var result = await enhancedthresholdsControllerHelper.CreateEnhancedThreshold(customerId, request);

            if (!result.Status.HasFlag(CreateUpdateEnhancedThresholdStatus.Success))
            {
                return Content(
                    HttpStatusCode.BadRequest,
                    new ErrorResponseDto()
                    {
                        Error = ErrorCode.InvalidRequest,
                        Message = ErrorCode.InvalidRequest.Description(),
                        Details = result.Status.GetConcatString()
                    }
                );
            }

            return Created(
                new Uri(Request.RequestUri, result.Content.ToString()),
                new PostResponseDto<Guid> { Id = result.Content }
            );
        }

        /// <summary>
        /// Updates the Enhanced threshold.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="enhancedthresholdId">The Enhanced threshold identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPut]
        [Route("{EnhancedthresholdId:guid}")]
        [SwaggerResponse(HttpStatusCode.OK, "Enhanced Threshold was updated.")]
        [SwaggerResponse(HttpStatusCode.NotFound, "Enhanced Threshold does not exist.")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Incorrect attributes provided in the request.")]
        [InvalidateOutputCache(typeof(EnhancedThresholdsController), nameof(EnhancedThresholdsController.GetEnhancedThreshold), "enhancedthresholdId")]
        [InvalidateOutputCache(typeof(EnhancedThresholdsController), nameof(EnhancedThresholdsController.GetEnhancedThresholds), "customerId")]
        public async Task<IHttpActionResult> UpdateEnhancedThreshold(
            int customerId,
            Guid enhancedthresholdId,
            EnhancedThresholdRequestDto request
        )
        {
            var status = await enhancedthresholdsControllerHelper.UpdateEnhancedThreshold(customerId, enhancedthresholdId, request);

            if (!status.HasFlag(CreateUpdateEnhancedThresholdStatus.Success))
            {
                return Content(
                    HttpStatusCode.BadRequest,
                    new ErrorResponseDto()
                    {
                        Error = ErrorCode.InvalidRequest,
                        Message = ErrorCode.InvalidRequest.Description(),
                        Details = status.GetConcatString()
                    }
                );
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Deletes the Enhanced threshold.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="enhancedthresholdId">The Enhancedthreshold identifier.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{EnhancedthresholdId:guid}")]
        [SwaggerResponse(HttpStatusCode.OK, "Enhanced Threshold was deleted.")]
        [SwaggerResponse(HttpStatusCode.NotFound, "Enhanced Threshold with such identifier does not exist.")]
        [SwaggerResponse(HttpStatusCode.Conflict, "Unable to delete enhanced threshold because it is active on one or more patients.")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Incorrent attributes provided in the request.")]
        [InvalidateOutputCache(typeof(EnhancedThresholdsController), nameof(EnhancedThresholdsController.GetEnhancedThresholds), "customerId")]
        [InvalidateOutputCache(typeof(EnhancedThresholdsController), nameof(EnhancedThresholdsController.GetEnhancedThreshold), "enhancedthresholdId")]
        public async Task<IHttpActionResult> DeleteEnhancedThreshold(
            int customerId,
            Guid enhancedthresholdId
        )
        {
            var status = await enhancedthresholdsControllerHelper.DeleteEnhancedThreshold(customerId, enhancedthresholdId);

            if (status == GetDeleteEnhancedThresholdStatus.NotFound)
            {
                return Content(
                    HttpStatusCode.NotFound,
                    new ErrorResponseDto()
                    {
                        Error = ErrorCode.InvalidRequest,
                        Message = ErrorCode.InvalidRequest.Description(),
                        Details = status.Description()
                    }
                );
            }

            if (status == GetDeleteEnhancedThresholdStatus.UnableToDelete)
            { 
                //    return Content(
                //        HttpStatusCode.Conflict,
                //        new ErrorResponseDto()
                //        {
                //            Error = ErrorCode.UnableToDelete,
                //            Message = ErrorCode.UnableToDelete.Description(),
                //            Details = status.Description()
                //        }
                //    );
                return StatusCode(HttpStatusCode.Conflict);
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Get the Enhanced threshold by Id.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="enhancedthresholdId">The Enhancedthreshold identifier.</param>
        /// <returns></returns>
        [HttpGet]
        [CertificateAuthorize]
        [Route("{EnhancedthresholdId:guid}")]
        [ResponseType(typeof(BaseEnhancedThresholdDto))]
        [SwaggerResponse(HttpStatusCode.OK, "Existing Enhanced threshold.", typeof(BaseEnhancedThresholdDto))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Threshold does not exist.")]
        public async Task<IHttpActionResult> GetEnhancedThreshold(
            int customerId,
            Guid enhancedthresholdId
        )
        {
            var result = await enhancedthresholdsControllerHelper.GetEnhancedThreshold(customerId, enhancedthresholdId);

            if (result.Status == GetDeleteEnhancedThresholdStatus.NotFound)
            {
                return Content(
                    HttpStatusCode.NotFound,
                    new ErrorResponseDto()
                    {
                        Error = ErrorCode.InvalidRequest,
                        Message = ErrorCode.InvalidRequest.Description(),
                        Details = result.Status.Description()
                    }
                );
            }

            return Ok(result.Content);
        }

        /// <summary>
        /// Gets the Enhanced thresholds.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpGet]
        [CertificateAuthorize]
        [Route("")]
        [ResponseType(typeof(PagedResultDto<BaseEnhancedThresholdDto>))]
        [SwaggerResponse(HttpStatusCode.OK, "Response with Enhanced thresholds.", typeof(PagedResultDto<BaseEnhancedThresholdDto>))]
        public async Task<IHttpActionResult> GetEnhancedThresholds(
            int customerId,
            [FromUri]EnhancedThresholdsSearchDto request
        )
        {
            var result = await enhancedthresholdsControllerHelper.GetEnhancedThresholds(customerId, request);
            return Ok(result);
        }

        /// <summary>
        ///  Enhanced thresholds Alerts
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        [HttpPost]
        [CertificateAuthorize]
        [Route("enhancedThresholdAlerts")]
        [ResponseType(typeof(PagedResultDto<EnhancedThresholdAlertDto>))]
        [SwaggerResponse(HttpStatusCode.OK, "Response with Enhanced thresholds Alerts.", typeof(PagedResultDto<EnhancedThresholdAlertDto>))]
        public async Task<IHttpActionResult> GetEnhancedThresholdAlerts(
            int customerId
        )
        {
            var result = await enhancedthresholdsControllerHelper.GetEnhancedThresholdAlerts(customerId);

            return Ok(result);
        }

        /// <summary>
        /// Acknowledges the ET alerts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("enhancedThresholdAlerts/acknowledge")]
        public async Task<IHttpActionResult> AcknowledgeEnThAlerts(int customerId, AcknowledgeETAlertsRequestDto request)
        {
            var result = await enhancedthresholdsControllerHelper.AcknowledgeEnThAlerts(customerId, request);

            if (!result.HasFlag(CreateUpdateAlertStatus.Success))
            {
                return Content(
                    HttpStatusCode.BadRequest,
                    new ErrorResponseDto()
                    {
                        Error = ErrorCode.InvalidRequest,
                        Message = ErrorCode.InvalidRequest.Description(),
                        Details = result.GetConcatString()
                    }
                );
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        /// <summary>
        /// UnAcknowledges the alerts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("enhancedThresholdAlerts/unacknowledge")]
        public async Task<IHttpActionResult> UnAcknowledgeEnThAlerts(int customerId, AcknowledgeETAlertsRequestDto request)
        {
            var result = await enhancedthresholdsControllerHelper.UnAcknowledgeEnThAlerts(customerId, request);

            if (!result.HasFlag(CreateUpdateAlertStatus.Success))
            {
                return Content(
                    HttpStatusCode.BadRequest,
                    new ErrorResponseDto()
                    {
                        Error = ErrorCode.InvalidRequest,
                        Message = ErrorCode.InvalidRequest.Description(),
                        Details = result.GetConcatString()
                    }
                );
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// ignore the ET alerts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("enhancedThresholdAlerts/ignore")]
        public async Task<IHttpActionResult> IgnoreEnThresholdAlerts(int customerId, IgnoreETAlertsRequestDto request)
        {
            var result = await enhancedthresholdsControllerHelper.IgnoreEnThresholdAlerts(customerId, request);

            if (!result.HasFlag(CreateUpdateAlertStatus.Success))
            {
                return Content(
                    HttpStatusCode.BadRequest,
                    new ErrorResponseDto()
                    {
                        Error = ErrorCode.InvalidRequest,
                        Message = ErrorCode.InvalidRequest.Description(),
                        Details = result.GetConcatString()
                    }
                );
            }

            return StatusCode(HttpStatusCode.NoContent);
        }



        /// <summary>
        /// Activate/Deactivate Customer/Condition Level Enhanced threshold for any patient.
        /// </summary>
        /// <param name="customerId">The customerId.</param>
        /// <param name="patientActivateETRequestDto">The Patient Activate ET RequestDto.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateETActiveState")]
        [SwaggerResponse(HttpStatusCode.OK, "Enhanced Threshold was updated.")]
        [SwaggerResponse(HttpStatusCode.NotFound, "Enhanced Threshold does not exist.")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Incorrect attributes provided in the request.")]
        [InvalidateOutputCache(typeof(EnhancedThresholdsController), nameof(EnhancedThresholdsController.GetAllETsPatient), "patientId")]
        public async Task<IHttpActionResult> UpdateETActiveState(int customerId, PatientActivateETRequestDto patientActivateETRequestDto)
        {
            await enhancedthresholdsControllerHelper.UpdateETActiveState(customerId, patientActivateETRequestDto);

            return Json(string.Empty);
        }

        /// <summary>
        /// Get Active/Inactive Customer/Condition Level ET's for  any patient.
        /// </summary>
        /// <param name="customerId">The customerId.</param>
        /// <param name="patientId">The Patient identifier</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllETsPatient")]
        [ResponseType(typeof(PagedResultDto<PatientActiveETResponseDto>))]
        [CertificateAuthorize]
        [SwaggerResponse(HttpStatusCode.OK, "Response with active/inactive enhanced thresholds", typeof(PagedResultDto<PatientActiveETResponseDto>))]
        [InvalidateOutputCache(typeof(EnhancedThresholdsController), nameof(EnhancedThresholdsController.GetAllETsPatient), "patientId")]
        public async Task<IHttpActionResult> GetAllETsPatient(int customerId,
             [FromUri] Guid patientId)
        {
            var result = await enhancedthresholdsControllerHelper.GetAllETsPatient(customerId, patientId);
            return Ok(result);
        }
    }
}
﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Swashbuckle.Swagger.Annotations;
using VitalsService.Domain.Dtos;
using VitalsService.Domain.Enums;
using VitalsService.Extensions;
using VitalsService.Web.Api.Filters;
using VitalsService.Web.Api.Helpers.Interfaces;
using VitalsService.Web.Api.Models;
using VitalsService.Web.Api.Models.Enums;
using VitalsService.Web.Api.Models.Thresholds;
using System.Web;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using VitalsService.Web.Api.Models.AlertSeverities;
using VitalsService.Domain.DbEntities;
using NLog;

namespace VitalsService.Web.Api.Controllers
{
    /// <summary>
    /// ThresholdsController.
    /// </summary>
    [TokenAuthorize]
    [RoutePrefix(@"api/{customerId:regex(^[1-9]\d*)}/thresholds/{patientId:guid}")]
    public class ThresholdsController : ApiController
    {
        private readonly IThresholdsControllerHelper thresholdsControllerHelper;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="ThresholdsController" /> class.
        /// </summary>
        /// <param name="thresholdsControllerHelper">The thresholds controller helper.</param>
        public ThresholdsController(IThresholdsControllerHelper thresholdsControllerHelper)
        {
            this.thresholdsControllerHelper = thresholdsControllerHelper;
        }

        /// <summary>
        /// Creates the threshold.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(PostResponseDto<Guid>))]
        [InvalidateOutputCache(typeof(ThresholdsController), nameof(ThresholdsController.GetThresholds), "patientId")]
        [InvalidateOutputCache(typeof(ThresholdsController), nameof(ThresholdsController.GetThreshold), "patientId")]
        public async Task<IHttpActionResult> CreateThreshold(
            int customerId,
            Guid patientId,
            ThresholdRequestDto request
        )
        {
            try
            {
                var result = await thresholdsControllerHelper.CreateThreshold(customerId, patientId, request);

                if (!result.Status.HasFlag(CreateUpdateThresholdStatus.Success))
                {
                    return Content(
                        HttpStatusCode.BadRequest,
                        new ErrorResponseDto()
                        {
                            Error = ErrorCode.InvalidRequest,
                            Message = ErrorCode.InvalidRequest.Description(),
                            Details = result.Status.GetConcatString()
                        }
                    );
                }

                return Created(
                    new Uri(Request.RequestUri, result.Content.ToString()),
                    new PostResponseDto<Guid> { Id = result.Content }
                );
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Failure Create Thresholds controller");
                // We want to know about this, but we don't want it to cause an application failure.
                return null;
            }
        }

        /// <summary>
        /// Updates the threshold.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="thresholdId">The threshold identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPut]
        [Route("{thresholdId:guid}")]
        [SwaggerResponse(HttpStatusCode.OK, "Threshold was updated.")]
        [SwaggerResponse(HttpStatusCode.NotFound, "Threshold does not exist.")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Incorrent attributes provided in the request.")]
        [InvalidateOutputCache(typeof(ThresholdsController), nameof(ThresholdsController.GetThresholds), "patientId")]
        [InvalidateOutputCache(typeof(ThresholdsController), nameof(ThresholdsController.GetThreshold), "thresholdId")]
        [InvalidateOutputCache(typeof(VitalsController), nameof(VitalsController.GetVital), "patientId")]
        [InvalidateOutputCache(typeof(VitalsController), nameof(VitalsController.GetVitals), "patientId")]
        public async Task<IHttpActionResult> UpdateThreshold(
            int customerId,
            Guid patientId,
            Guid thresholdId,
            ThresholdRequestDto request
        )
        {
            try
            {
                var status = await thresholdsControllerHelper.UpdateThreshold(customerId, patientId, thresholdId, request);

                if (!status.HasFlag(CreateUpdateThresholdStatus.Success))
                {
                    return Content(
                        HttpStatusCode.BadRequest,
                        new ErrorResponseDto()
                        {
                            Error = ErrorCode.InvalidRequest,
                            Message = ErrorCode.InvalidRequest.Description(),
                            Details = status.GetConcatString()
                        }
                    );
                }
            }
            catch(Exception e)
            {
                Logger.Error(e, "Update Threshold filed with" + e.Message);
                throw e;

            }
           

            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Deletes the threshold.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="thresholdId">The threshold identifier.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{thresholdId:guid}")]
        [SwaggerResponse(HttpStatusCode.OK, "Threshold was deleted.")]
        [SwaggerResponse(HttpStatusCode.NotFound, "Threshold with such identifier does not exist.")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Incorrent attributes provided in the request.")]
        [InvalidateOutputCache(typeof(ThresholdsController), nameof(ThresholdsController.GetThresholds), "patientId")]
        [InvalidateOutputCache(typeof(ThresholdsController), nameof(ThresholdsController.GetThreshold), "thresholdId")]
        [InvalidateOutputCache(typeof(VitalsController), nameof(VitalsController.GetVital), "patientId")]
        [InvalidateOutputCache(typeof(VitalsController), nameof(VitalsController.GetVitals), "patientId")]
        public async Task<IHttpActionResult> DeleteThreshold(
            int customerId,
            Guid patientId,
            Guid thresholdId
        )
        {
            try { 
                var status = await thresholdsControllerHelper.DeleteThreshold(customerId, patientId, thresholdId);

                if (status == GetDeleteThresholdStatus.NotFound)
                {
                    return Content(
                        HttpStatusCode.NotFound,
                        new ErrorResponseDto()
                        {
                            Error = ErrorCode.InvalidRequest,
                            Message = ErrorCode.InvalidRequest.Description(),
                            Details = status.Description()
                        }
                    );
                }
            }
            catch (Exception e) {
                Logger.Error(e, "Delete Threshold filed with" + e.Message);
                throw e;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Gets the threshold.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="thresholdId">The threshold identifier.</param>
        /// <returns></returns>
        [HttpGet]
        [CertificateAuthorize]
        [Route("{thresholdId:guid}")]
        [ResponseType(typeof(PatientThresholdDto))]
        [SwaggerResponse(HttpStatusCode.OK, "Existing threshold.", typeof(PatientThresholdDto))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Threshold does not exist.")]
        public async Task<IHttpActionResult> GetThreshold(
            int customerId,
            Guid patientId,
            Guid thresholdId
        )
        {
            try { 
                var result = await thresholdsControllerHelper.GetThreshold(customerId, patientId, thresholdId);

                if (result.Status == GetDeleteThresholdStatus.NotFound)
                {
                    return Content(
                        HttpStatusCode.NotFound,
                        new ErrorResponseDto()
                        {
                            Error = ErrorCode.InvalidRequest,
                            Message = ErrorCode.InvalidRequest.Description(),
                            Details = result.Status.Description()
                        }
                    );
                }

                return Ok(result.Content);
            }
            catch (Exception e)
            {
                Logger.Error(e, "Get Threshold filed with" + e.Message);
                throw e;

            }
        }

        /// <summary>
        /// Gets the thresholds.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpGet]
        [CertificateAuthorize]
        [Route("")]
        [ResponseType(typeof(PagedResultDto<BaseThresholdDto>))]
        [SwaggerResponse(HttpStatusCode.OK, "Response with thresholds.", typeof(PagedResultDto<BaseThresholdDto>))]
        public async Task<IHttpActionResult> GetThresholds(
            int customerId,
            Guid patientId,
            [FromUri]ThresholdsSearchDto request
        )
        {
            try { 
                var result = await thresholdsControllerHelper.GetThresholds(customerId, patientId, request);
                //This will add missing thresholds (not set) with all three severities and min/max set to null
                //This will only happen if the request mode is aggregate
                if (request.Mode == ThresholdSearchType.Aggregate)
                {
                    var customerAlertSeverities = await thresholdsControllerHelper.GetAlertSeverities(customerId);
                    //MS 3532 : dont process missing thresholds if no severities 
                    if (customerAlertSeverities != null && customerAlertSeverities.Count() > 0)
                            result = AddMissingThresholds(result, customerId, patientId, customerAlertSeverities);
                }

                HttpRequest currentRequest = HttpContext.Current.Request;
                if (currentRequest.Headers["User-Agent"] != null && currentRequest.Headers["User-Agent"] != string.Empty)
                {
                    string userAgent = currentRequest.Headers["User-Agent"].ToString().ToLower();
                    var skippedVersionList = ConfigurationManager.AppSettings["SkippedVersionList"];
                    if (!string.IsNullOrEmpty(skippedVersionList) && IsHandleNull(userAgent, skippedVersionList.ToLower()))
                    {
                        HandleNullValues(result);
                    }
                }

                return Ok(result);
            }
            catch (Exception e)
            {
                Logger.Error(e, "Get Thresholds filed with" + e.Message);
                throw e;

            }
        }




        /// <summary>
        /// Gets the patient threshold type
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <returns></returns>
        [HttpGet]
        [CertificateAuthorize]
        [Route("patientthresholdtypes")]
        [ResponseType(typeof(PagedResultDto<BaseThresholdDto>))]
        [SwaggerResponse(HttpStatusCode.OK, "Response with thresholds.", typeof(PagedResultDto<PatientThresholdTypeResponseDto>))]
        [InvalidateOutputCache(typeof(ThresholdsController), nameof(ThresholdsController.GetPatientThresholdTypes), "patientId")]
        public async Task<IHttpActionResult> GetPatientThresholdTypes(
            int customerId,
            Guid patientId
        )
        {
            try
            { 
                var result = await thresholdsControllerHelper.GetPatientThresholdTypes(customerId, patientId);

                return Ok(result);
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetPatientThresholdTypes Thresholds filed with" + e.Message);
                throw e;

            }
        }



        #region Handle nul threshold values for specific user app version
        /// <summary>
        /// This method is used to set some dummy min.max values for nulls for the configured used app version
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        private PagedResultDto<BaseThresholdDto> HandleNullValues(PagedResultDto<BaseThresholdDto> result)
        {
            foreach (BaseThresholdDto dto in result.Results)
            {
                dto.MaxValue = (dto.MaxValue == null) ? 99999 : dto.MaxValue;
                dto.MinValue = (dto.MinValue == null) ? -99999 : dto.MinValue;
            }
            return result;
        }

        private bool IsHandleNull(string userAgent, string skippedVersionList)
        {
            string[] arrVersions = skippedVersionList.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string version in arrVersions)
            {
                if (userAgent.Contains(version))
                    return true;
            }

            return false;
        }

        #endregion

        #region Add Missing thresholds
        /// <summary>
        /// Used to add thresholds which are not set. We are adding and returning all 13 measurements
        /// with 3 level severities Green,Yellow,Red
        /// </summary>
        /// <param name="result"></param>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <param name="customerAlertSeverities"></param>
        /// <returns></returns>
        private PagedResultDto<BaseThresholdDto> AddMissingThresholds(PagedResultDto<BaseThresholdDto> result,
            int customerId, Guid patientId, IList<AlertSeverity> customerAlertSeverities)
        {
            var resultsTemp = result.Results;
            foreach (VitalType type in Enum.GetValues(typeof(VitalType)))
            {
                if (type != VitalType.BodyMassIndex)
                {
                    //Check for the green threshold 
                    var thresholdGreen = (from threshold in result.Results.AsEnumerable()
                                          where threshold.Name == type && threshold.AlertSeverity.Severity == 1
                                          select threshold);

                    if (thresholdGreen != null)
                    {
                        if (thresholdGreen.Count() == 0)
                        {
                            BaseThresholdDto threshold = GetBaseThreshold(type, customerId, patientId, 1);
                            threshold.AlertSeverity.Id = customerAlertSeverities.First(p => p.Severity == 1).Id;
                            //Add green threshold
                            result.Results.Add(threshold);
                        }
                    }

                    //If we only have say Red set, then return nulls for Yellow
                    //Or if we only have Yellow set, then return nulls for red
                    //Check for the green threshold 
                    var thresholdYelow = (from threshold in result.Results.AsEnumerable()
                                          where threshold.Name == type && threshold.AlertSeverity.Severity == 2
                                          select threshold);
                    //Check for the green threshold 
                    var thresholdRed = (from threshold in result.Results.AsEnumerable()
                                        where threshold.Name == type && threshold.AlertSeverity.Severity == 3
                                        select threshold);

                    if (thresholdRed.Count() > 0 && thresholdYelow.Count() == 0)
                    {
                        BaseThresholdDto threshold = GetBaseThreshold(type, customerId, patientId, 2);
                        //Add yellow threshold
                        threshold.AlertSeverity.Id = customerAlertSeverities.First(p => p.Severity == 2).Id;
                        result.Results.Add(threshold);
                    }
                    if (thresholdRed.Count() == 0 && thresholdYelow.Count() > 0)
                    {
                        BaseThresholdDto threshold = GetBaseThreshold(type, customerId, patientId, 3);
                        //Add red threshold
                        threshold.AlertSeverity.Id = customerAlertSeverities.First(p => p.Severity == 3).Id;
                        result.Results.Add(threshold);
                    }
                }
            }

            //Check red or yellow threshold is set for the given vital
            //If red or yellow threshold is set then we should not set green min/max
            //to null. In other words, green will have its calulatted range 
            //based on red/yellow thresholds and not the default measurement range
            foreach (BaseThresholdDto dto in result.Results)
            {
                var thresholds = (from threshold in result.Results.AsEnumerable()
                                  where threshold.Name == dto.Name
                                        && (threshold.AlertSeverity.Severity == 2 || threshold.AlertSeverity.Severity == 3)
                                  select threshold);

                if ((thresholds == null || thresholds.Count() == 0) &&
                         dto.AlertSeverity.Severity == 1)
                {
                    dto.MinValue = null;
                    dto.MaxValue = null;
                }
            }
            return result;
        }

        private BaseThresholdDto GetBaseThreshold(VitalType type, int customerId, Guid patientID, int severity)
        {
            PatientThresholdDto dto = new PatientThresholdDto();
            dto.Id = Guid.NewGuid();
            dto.CustomerId = customerId;
            dto.Type = ThresholdType.Basic;
            dto.Name = type;
            dto.PatientId = patientID;
            dto.Unit = GetUnit(type.Description());//(UnitType)Enum.Parse(typeof(UnitType), type.Description(), true);
            PatientThresholdTypeResponseDto PatientThresholdType = GetPatientThresholdType(type, "Customer");
            PatientThresholdType.CustomerId = customerId;
            PatientThresholdType.PatientId = patientID;
            dto.PatientThresholdType = PatientThresholdType;
            AlertSeverityResponseDto alertSeverity = new AlertSeverityResponseDto();
            alertSeverity.Severity = severity;
            alertSeverity.Name = ((AlertSeverityTypes)severity).ToString();
            alertSeverity.ColorCode = ((AlertSeverityTypes)severity).Description();
            alertSeverity.CustomerId = customerId;
            dto.AlertSeverity = alertSeverity;

            return dto;
        }

        private UnitType GetUnit(string unit)
        {
            if (unit.Contains(","))
            {
                string[] arrUnits = unit.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                unit = arrUnits[0];
            }

            return (UnitType)Enum.Parse(typeof(UnitType), unit, true);
        }

        private int[] GetExistingAlertSeverity(IList<BaseThresholdDto> listThresholds)
        {
            int[] severities = new int[listThresholds.Count];
            switch (listThresholds.Count)
            {
                case 1:
                    severities[0] = listThresholds[0].AlertSeverity.Severity;
                    break;
                case 2:
                    for (int i = 0; i < listThresholds.Count - 1; i++)
                    {
                        severities.SetValue(listThresholds[i].AlertSeverity.Severity, i);
                    }
                    break;
            }

            return severities;
        }

        private PatientThresholdTypeResponseDto GetPatientThresholdType(VitalType type, string thresholdType)
        {
            return new PatientThresholdTypeResponseDto
            {
                VitalType = type,
                ThresholdType = thresholdType,
                ConditionId = null
            };
        }

        #endregion

    }
}
﻿using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using VitalsService.Web.Api.Caching;
using VitalsService.Web.Api.ExceptionHandling;
using WebApi2.RedisOutputCache;

namespace VitalsService.Web.Api
{
    /// <summary>
    /// WebApiConfig.
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// Registers the specified configuration.
        /// </summary>
        /// <param name="config">The configuration.</param>
        public static void Register(HttpConfiguration config)
        {
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Services.Add(typeof(IExceptionLogger), new GlobalExceptionLogger());
            config.Services.Replace(typeof(IExceptionHandler), new GlobalExceptionHandler());

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{customerId}/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional },
                constraints: new { customerId = @"\d+" }
            );

            config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());


            // 
            // redis output caching
            //

            var outputCacheConfig = config.GetOutputCacheConfiguration();

            // For catchall redis output caching, register catchall cache key generator as the default.
            outputCacheConfig.RegisterDefaultCacheKeyGeneratorProvider(() => new CatchallCacheKeyGenerator());

            // If a pub/sub channel prefix is provided in configuration, set it in the output caching configuration to enable local caching
            //   and pub/sub notifications for local cache invalidation.
            outputCacheConfig.EnableLocalCaching(RedisCache.Multiplexer, Settings.RedisInvalidateLocalCacheChannelPrefix);
        }
    }
}
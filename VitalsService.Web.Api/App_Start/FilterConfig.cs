﻿using System.Web.Http.Filters;
using VitalsService.Web.Api.Filters;

namespace VitalsService.Web.Api
{
    /// <summary>
    /// FilterConfig.
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        /// Registers the global filters.
        /// </summary>
        /// <param name="filters">The filters.</param>
        public static void RegisterGlobalFilters(HttpFilterCollection filters)
        {
            // Global filters
            filters.Add(new AuthorizeResultAttribute());

            // Model validation
            filters.Add(new ValidateModelAttribute());

            // Caching
            filters.Add(new CatchallOutputCacheAttribute
            {
                ServerTimeSpan = 3600,
                VaryByUserAgent = true
            });
        }
    }
}
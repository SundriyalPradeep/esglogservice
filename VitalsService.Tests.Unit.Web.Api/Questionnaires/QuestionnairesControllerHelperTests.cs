﻿using System;
using Shouldly;
using VitalsService.Domain.Enums;
using VitalsService.Domain.Enums.Questionnaires;
using VitalsService.Web.Api.Models.AlertSeverities;
using VitalsService.Web.Api.Models.Questionnaires;
using VitalsService.Web.Api.Models.Thresholds;
using Xunit;
using Xunit.Abstractions;

namespace VitalsService.Tests.Unit.Web.Api.Questionnaires
{
    public class QuestionnairesControllerHelperTests : IClassFixture<QuestionnairesControllerHelperTestFixture>
    {
        private const int TEST_CUSTOMER_ID = 8062013;
        private static readonly Guid _testPatientId = Guid.Parse("c1ab5bb6-5481-4e31-9a53-4d7812ff951e");

        private static readonly BaseThresholdDto[] _nullMinValueThresholds = new[]
        {
            CreateSpO2Threshold(null, 1),
            CreateFEV1Threshold(null, 1),
            CreateSYSThreshold(null, 1),
            CreateWeightThreshold(null, 1)
        };

        private static readonly BaseThresholdDto[] _validThresholds = new[]
        {
            CreateSpO2Threshold(20m, 1),
            CreateFEV1Threshold(40m, 1),
            CreateSYSThreshold(120m, 1),
            CreateWeightThreshold(200m, 1)
        };


        private readonly ITestOutputHelper _output;
        private readonly QuestionnairesControllerHelperTestFixture _fixture;

        public QuestionnairesControllerHelperTests(ITestOutputHelper output, QuestionnairesControllerHelperTestFixture fixture)
        {
            _output = output;
            _fixture = fixture;
        }


        #region CalculateTotalPreStratificationScore tests

        [Fact]
        public void CalculateTotalPreStratificationScore_NullRequest_ThrowsNullReferenceException()
        {
            Should.Throw<NullReferenceException>(() =>
            {
                _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, null, null);
            });
        }

        [Fact]
        public void CalculateTotalPreStratificationScore_NullThresholdsArray_ThrowsNullReferenceException()
        {
            var questionnaire = CreateQuestionnaireResponse();

            Should.Throw<ArgumentNullException>(() =>
            {
                _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, null);
            });
        }

        [Fact]
        public void CalculateTotalPreStratificationScore_EmptyThresholds_ThrowsInvalidOperationException()
        {
            var questionnaire = CreateQuestionnaireResponse();

            Should.Throw<InvalidOperationException>(() =>
            {
                _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, Array.Empty<BaseThresholdDto>());
            });
        }

        [Fact]
        public void CalculateTotalPreStratificationScore_ThresholdsWithNullMinValues_ThrowsInvalidOperationException()
        {
            var questionnaire = CreateQuestionnaireResponse();

            Should.Throw<InvalidOperationException>(() =>
            {
                _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _nullMinValueThresholds);
            });
        }

        [Fact]
        public void CalculateTotalPreStratificationScore_NoPreStratQuestions_Returns0()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreateGeneralSingleSelectQuestionAnswer("What is the meaning of life?", "42"),
                CreateGeneralSingleSelectQuestionAnswer("ORLY?", "ROFLCOPTER")
            });


            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(0);
        }

        [Fact]
        public void CalculateTotalPreStratificationScore_OnlyAlertConfirmation_Returns0()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreateAlertConfirmationQuestionAnswer("Are you well?", "YES")
            });


            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(0);
        }

        [Fact]
        public void CalculateTotalPreStratificationScore_COPD_Returns15()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreateAlertConfirmationQuestionAnswer("Are you well?", "NO"),
                CreateGeneralSingleSelectQuestionAnswer("Inhalers?", "YES"),
                CreateGeneralSingleSelectQuestionAnswer("Aumento?", "YES"),
                CreateGeneralSingleSelectQuestionAnswer("Expectoration?", "YES"),
                CreateGeneralSingleSelectQuestionAnswer("Esputo?", "YES"),
                CreateGeneralSingleSelectQuestionAnswer("Secreciones?", "YES"),
                CreatePreStratificationSingleSelectQuestionAnswer("Sputum color", "Yellow", answerChoiceScore: 1),
                CreatePreStratificationSingleSelectQuestionAnswer("Dyspnoe (MRC scale)", "Level 2", answerChoiceScore: 1),
                CreatePreStratificationMeasurementQuestionAnswer("Temperature", QuestionnaireMeasurementType.Temperature, 37.5m), // 2 points
                CreatePreStratificationMeasurementQuestionAnswer("HR", QuestionnaireMeasurementType.HR, 115m), // 2 points
                CreatePreStratificationMeasurementQuestionAnswer("SpO2", QuestionnaireMeasurementType.SpO2, 15m), // 1 point
                CreatePreStratificationMeasurementQuestionAnswer("FEV1", QuestionnaireMeasurementType.FEV1, 29m), // 2 points
                CreatePreStratificationMeasurementQuestionAnswer("SYS", QuestionnaireMeasurementType.SYS, 150m), // 2 points
                CreatePreStratificationMeasurementQuestionAnswer("Weight", QuestionnaireMeasurementType.Weight, 202m), // 3 points
                CreatePreStratificationSingleSelectQuestionAnswer("Co-morbidity", "Pluralpathological", answerChoiceScore: 1),
                CreatePreStratificationSingleSelectQuestionAnswer("Edemas", "No edema or stable", answerChoiceScore: 0),
            });
            
            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(15);
        }

        [Fact]
        public void CalculateTotalPreStratificationScore_CHF_Returns19()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreateAlertConfirmationQuestionAnswer("Are you well?", "NO"),
                CreateGeneralSingleSelectQuestionAnswer("Breathing difficulty?", "YES"),
                CreateGeneralSingleSelectQuestionAnswer("Almohadas?", "YES"),
                CreateGeneralSingleSelectQuestionAnswer("Abdominal permiter increased?", "YES"),
                CreateGeneralSingleSelectQuestionAnswer("Mas oscuro?", "YES"),
                CreateGeneralSingleSelectQuestionAnswer("Por la noche?", "YES"),
                CreateGeneralSingleSelectQuestionAnswer("Mas intensidad?", "YES"),
                CreateGeneralSingleSelectQuestionAnswer("Palpitations?", "YES"),
                CreatePreStratificationSingleSelectQuestionAnswer("Dyspnoe (NYHA scale)", "Level 3", answerChoiceScore: 2),
                CreatePreStratificationSingleSelectQuestionAnswer("Asthenia (analogue scale)", "8", answerChoiceScore: 2),
                CreatePreStratificationMeasurementQuestionAnswer("SpO2", QuestionnaireMeasurementType.SpO2, 15m), // 1 point
                CreatePreStratificationSingleSelectQuestionAnswer("Chest pain", "Heart Pain", answerChoiceScore: 3),
                CreatePreStratificationMeasurementQuestionAnswer("SYS", QuestionnaireMeasurementType.SYS, 150m), // 2 points
                CreatePreStratificationMeasurementQuestionAnswer("HR", QuestionnaireMeasurementType.HR, 115m), // 2 points
                CreatePreStratificationMeasurementQuestionAnswer("FEV1", QuestionnaireMeasurementType.FEV1, 29m), // 2 points
                CreatePreStratificationMeasurementQuestionAnswer("Weight", QuestionnaireMeasurementType.Weight, 202m), // 3 points
                CreatePreStratificationSingleSelectQuestionAnswer("Edemas", "Knee apparition", answerChoiceScore: 2),
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(19);
        }

        [Fact]
        public void CalculateTotalPreStratificationScore_COPDCHF_Returns23()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreateAlertConfirmationQuestionAnswer("Are you well?", "NO"),
                CreateGeneralSingleSelectQuestionAnswer("Inhalers?", "YES"),
                CreateGeneralSingleSelectQuestionAnswer("Aumento?", "YES"),
                CreateGeneralSingleSelectQuestionAnswer("Expectoration?", "YES"),
                CreateGeneralSingleSelectQuestionAnswer("Esputo?", "YES"),
                CreateGeneralSingleSelectQuestionAnswer("Secreciones?", "YES"),
                CreateGeneralSingleSelectQuestionAnswer("Breathing difficulty?", "YES"),
                CreateGeneralSingleSelectQuestionAnswer("Almohadas?", "YES"),
                CreateGeneralSingleSelectQuestionAnswer("Abdominal permiter increased?", "YES"),
                CreateGeneralSingleSelectQuestionAnswer("Mas oscuro?", "YES"),
                CreateGeneralSingleSelectQuestionAnswer("Por la noche?", "YES"),
                CreateGeneralSingleSelectQuestionAnswer("Mas intensidad?", "YES"),
                CreateGeneralSingleSelectQuestionAnswer("Palpitations?", "YES"),
                CreatePreStratificationSingleSelectQuestionAnswer("Sputum color", "Yellow", answerChoiceScore: 1),
                CreatePreStratificationSingleSelectQuestionAnswer("Dyspnoe (NYHA scale)", "Level 3", answerChoiceScore: 2),
                CreatePreStratificationMeasurementQuestionAnswer("Temperature", QuestionnaireMeasurementType.Temperature, 37.5m), // 2 points
                CreatePreStratificationMeasurementQuestionAnswer("SpO2", QuestionnaireMeasurementType.SpO2, 15m), // 1 point
                CreatePreStratificationMeasurementQuestionAnswer("FEV1", QuestionnaireMeasurementType.FEV1, 29m), // 2 points
                CreatePreStratificationMeasurementQuestionAnswer("SYS", QuestionnaireMeasurementType.SYS, 150m), // 2 points
                CreatePreStratificationMeasurementQuestionAnswer("HR", QuestionnaireMeasurementType.HR, 115m), // 2 points
                CreatePreStratificationSingleSelectQuestionAnswer("Asthenia (analogue scale)", "8", answerChoiceScore: 2),
                CreatePreStratificationSingleSelectQuestionAnswer("Co-morbidity", "Pluralpathological", answerChoiceScore: 1),
                CreatePreStratificationSingleSelectQuestionAnswer("Chest pain", "Heart Pain", answerChoiceScore: 3),
                CreatePreStratificationMeasurementQuestionAnswer("Weight", QuestionnaireMeasurementType.Weight, 202m), // 3 points
                CreatePreStratificationSingleSelectQuestionAnswer("Edemas", "Knee apparition", answerChoiceScore: 2),
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(23);
        }

        [Fact]
        public void PreStratSingleSelectQuestionAnswer_ScoreEquals10()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationSingleSelectQuestionAnswer("Foo", "Bar", answerChoiceScore: 1),
                CreatePreStratificationSingleSelectQuestionAnswer("Foo", "Bar", answerChoiceScore: 2),
                CreatePreStratificationSingleSelectQuestionAnswer("Foo", "Bar", answerChoiceScore: 3),
                CreatePreStratificationSingleSelectQuestionAnswer("Foo", "Bar", answerChoiceScore: 0),
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(6);
        }

        [Fact]
        public void PreStratMeasurement_Temperature_ScoreEquals0()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationMeasurementQuestionAnswer("Temperature", QuestionnaireMeasurementType.Temperature, 37.0m)
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(0);
        }

        [Fact]
        public void PreStratMeasurement_Temperature_ScoreEquals1()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationMeasurementQuestionAnswer("Temperature", QuestionnaireMeasurementType.Temperature, 37.49m)
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(1);
        }

        [Fact]
        public void PreStratMeasurement_Temperature_ScoreEquals2()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationMeasurementQuestionAnswer("Temperature", QuestionnaireMeasurementType.Temperature, 37.99m)
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(2);
        }

        [Fact]
        public void PreStratMeasurement_Temperature_ScoreEquals3()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationMeasurementQuestionAnswer("Temperature", QuestionnaireMeasurementType.Temperature, 38.0m)
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(3);
        }

        [Fact]
        public void PreStratMeasurement_HR_ScoreEquals0()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationMeasurementQuestionAnswer("HR", QuestionnaireMeasurementType.HR, 100m)
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(0);
        }

        [Fact]
        public void PreStratMeasurement_HR_ScoreEquals1()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationMeasurementQuestionAnswer("HR", QuestionnaireMeasurementType.HR, 110m)
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(1);
        }

        [Fact]
        public void PreStratMeasurement_HR_ScoreEquals2()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationMeasurementQuestionAnswer("HR", QuestionnaireMeasurementType.HR, 115m)
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(2);
        }

        [Fact]
        public void PreStratMeasurement_HR_ScoreEquals3_Low()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationMeasurementQuestionAnswer("HR", QuestionnaireMeasurementType.HR, 45m)
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(3);
        }

        [Fact]
        public void PreStratMeasurement_HR_ScoreEquals3_High()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationMeasurementQuestionAnswer("HR", QuestionnaireMeasurementType.HR, 116m)
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(3);
        }

        [Fact]
        public void PreStratMeasurement_SpO2_ScoreEquals0()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationMeasurementQuestionAnswer("SpO2", QuestionnaireMeasurementType.SpO2, 19m)
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(0);
        }

        [Fact]
        public void PreStratMeasurement_SpO2_ScoreEquals1()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationMeasurementQuestionAnswer("SpO2", QuestionnaireMeasurementType.SpO2, 15m)
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(1);
        }

        [Fact]
        public void PreStratMeasurement_SpO2_ScoreEquals2()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationMeasurementQuestionAnswer("SpO2", QuestionnaireMeasurementType.SpO2, 14m)
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(2);
        }

        [Fact]
        public void PreStratMeasurement_SpO2_ScoreEquals3()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationMeasurementQuestionAnswer("SpO2", QuestionnaireMeasurementType.SpO2, 12m)
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(3);
        }

        [Fact]
        public void PreStratMeasurement_FEV1_ScoreEquals0()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationMeasurementQuestionAnswer("FEV1", QuestionnaireMeasurementType.FEV1, 35m)
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(0);
        }

        [Fact]
        public void PreStratMeasurement_FEV1_ScoreStillEquals0()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationMeasurementQuestionAnswer("FEV1", QuestionnaireMeasurementType.FEV1, 31m)
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(0);
        }

        [Fact]
        public void PreStratMeasurement_FEV1_ScoreEquals2()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationMeasurementQuestionAnswer("FEV1", QuestionnaireMeasurementType.FEV1, 26m)
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(2);
        }

        [Fact]
        public void PreStratMeasurement_FEV1_ScoreEquals3()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationMeasurementQuestionAnswer("FEV1", QuestionnaireMeasurementType.FEV1, 25m)
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(3);
        }

        [Fact]
        public void PreStratMeasurement_SYS_ScoreEquals0()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationMeasurementQuestionAnswer("SYS", QuestionnaireMeasurementType.SYS, 135m)
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(0);
        }

        [Fact]
        public void PreStratMeasurement_SYS_ScoreEquals1()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationMeasurementQuestionAnswer("SYS", QuestionnaireMeasurementType.SYS, 136m)
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(1);
        }

        [Fact]
        public void PreStratMeasurement_SYS_ScoreEquals2()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationMeasurementQuestionAnswer("SYS", QuestionnaireMeasurementType.SYS, 150m)
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(2);
        }

        [Fact]
        public void PreStratMeasurement_SYS_ScoreEquals3()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationMeasurementQuestionAnswer("SYS", QuestionnaireMeasurementType.SYS, 151m)
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(3);
        }

        [Fact]
        public void PreStratMeasurement_Weight_ScoreEquals0()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationMeasurementQuestionAnswer("Weight", QuestionnaireMeasurementType.Weight, 200.1m)
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(0);
        }

        [Fact]
        public void PreStratMeasurement_Weight_ScoreEquals1()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationMeasurementQuestionAnswer("Weight", QuestionnaireMeasurementType.Weight, 200.5m)
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(1);
        }

        [Fact]
        public void PreStratMeasurement_Weight_ScoreEquals2()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationMeasurementQuestionAnswer("Weight", QuestionnaireMeasurementType.Weight, 200.75m)
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(2);
        }

        [Fact]
        public void PreStratMeasurement_Weight_ScoreEquals3()
        {
            var questionnaire = CreateQuestionnaireResponse();
            questionnaire.QuestionAnswers.AddRange(new[]
            {
                CreatePreStratificationMeasurementQuestionAnswer("Weight", QuestionnaireMeasurementType.Weight, 201.1m)
            });

            var score = _fixture.ControllerHelper.CalculateTotalPreStratificationScore(TEST_CUSTOMER_ID, _testPatientId, questionnaire, _validThresholds);

            score.ShouldBe(3);
        }

        #endregion


        #region Helpers

        //
        // Note: all of these ids, scores, and text values are irrelevant because the service doesn't check them (except validation).
        //

        private QuestionnaireResponseRequestDto CreateQuestionnaireResponse()
        {
            return new QuestionnaireResponseRequestDto
            {
                QuestionnaireId = Guid.Parse("2dd68779-679e-4f38-a5ea-3f6051b6987c"),
                ClinicianId = Guid.Parse("b2756852-ff29-4d01-930e-71a45a9e7277")
            };
        }

        private QuestionnaireQuestionAnswerDto CreateAlertConfirmationQuestionAnswer(string questionText, string answerChoiceText)
        {
            // Answer choice id doesn't matter.
            return CreateQuestionAnswer(
                questionText,
                QuestionnaireQuestionType.AlertConfirmation,
                QuestionnaireAnswerType.SingleSelect,
                answerChoiceId: Guid.NewGuid(),
                answerChoiceText: answerChoiceText
                );
        }

        private QuestionnaireQuestionAnswerDto CreateGeneralSingleSelectQuestionAnswer(string questionText, string answerChoiceText)
        {
            // Answer choice id doesn't matter.
            return CreateQuestionAnswer(
                questionText,
                QuestionnaireQuestionType.General,
                QuestionnaireAnswerType.SingleSelect,
                answerChoiceId: Guid.NewGuid(),
                answerChoiceText: answerChoiceText
                );
        }

        private QuestionnaireQuestionAnswerDto CreatePreStratificationSingleSelectQuestionAnswer(string questionText, string answerChoiceText, int answerChoiceScore)
        {
            // Answer choice id doesn't matter.
            return CreateQuestionAnswer(
                questionText,
                QuestionnaireQuestionType.PreStratification,
                QuestionnaireAnswerType.SingleSelect,
                answerChoiceId: Guid.NewGuid(),
                answerChoiceText: answerChoiceText,
                answerChoiceScore: answerChoiceScore
                );
        }

        private QuestionnaireQuestionAnswerDto CreatePreStratificationMeasurementQuestionAnswer(string questionText, QuestionnaireMeasurementType measurementType, decimal measurementValue)
        {
            return CreateQuestionAnswer(
                questionText,
                QuestionnaireQuestionType.PreStratification,
                QuestionnaireAnswerType.Measurement,
                measurementType: measurementType,
                measurementValue: measurementValue
                );
        }

        private QuestionnaireQuestionAnswerDto CreateQuestionAnswer(string questionText, QuestionnaireQuestionType questionType, QuestionnaireAnswerType answerType, Guid? answerChoiceId = null, string answerChoiceText = null, int? answerChoiceScore = null, QuestionnaireMeasurementType? measurementType = null, decimal? measurementValue = null)
        {
            return new QuestionnaireQuestionAnswerDto
            {
                QuestionId = Guid.NewGuid(),
                QuestionText = questionText,
                QuestionType = questionType,
                AnswerType = answerType,
                AnswerChoiceId = answerChoiceId,
                AnswerChoiceText = answerChoiceText,
                AnswerChoiceScore = answerChoiceScore,
                MeasurementType = measurementType,
                MeasurementValue = measurementValue
            };
        }


        // We don't need thresholds for Temperate or Heart Rate because they're compared to absolute ranges
        //   of values, not patient reference values.

        private static BaseThresholdDto CreateSpO2Threshold(decimal? minValue, int alertSeverity)
        {
            return CreateThreshold(VitalType.OxygenSaturation, minValue, UnitType.Percent, alertSeverity);
        }

        private static BaseThresholdDto CreateFEV1Threshold(decimal? minValue, int alertSeverity)
        {
            return CreateThreshold(VitalType.ForcedExpiratoryVolume, minValue, UnitType.L, alertSeverity);
        }

        private static BaseThresholdDto CreateSYSThreshold(decimal? minValue, int alertSeverity)
        {
            return CreateThreshold(VitalType.SystolicBloodPressure, minValue, UnitType.mmHg, alertSeverity);
        }

        private static BaseThresholdDto CreateWeightThreshold(decimal? minValue, int alertSeverity)
        {
            return CreateThreshold(VitalType.Weight, minValue, UnitType.kg, alertSeverity);
        }

        private static BaseThresholdDto CreateThreshold(VitalType name, decimal? minValue, UnitType unit, int alertSeverity)
        {
            return new BaseThresholdDto
            {
                Id = Guid.NewGuid(),
                CustomerId = TEST_CUSTOMER_ID,
                Type = ThresholdType.Basic,
                Name = name,
                MinValue = minValue,
                MaxValue = 8675309m,
                Unit = unit,
                AlertSeverity = new AlertSeverityResponseDto
                {
                    Id = Guid.NewGuid(),
                    CustomerId = TEST_CUSTOMER_ID,
                    Name = "Don't care",
                    ColorCode = "Also don't care",
                    Severity = alertSeverity
                }
            };
        }

        #endregion
    }
}

﻿using System;
using VitalsService.Web.Api.Helpers.Implementation;

namespace VitalsService.Tests.Unit.Web.Api.Questionnaires
{
    public class QuestionnairesControllerHelperTestFixture : IDisposable
    {
        public QuestionnairesControllerHelper ControllerHelper { get; private set; }

        public QuestionnairesControllerHelperTestFixture()
        {
            // We're not testing interactions with services or the database, or using AutoMapper to map any objects, 
            //   so just pass null for each parameter.
            ControllerHelper = new QuestionnairesControllerHelper(null, null, null);
        }


        public void Dispose()
        {
            Dispose(disposing: true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // No-op
            }
        }
    }
}

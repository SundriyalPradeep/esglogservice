﻿using VitalsService.Tests.Integration.Infrastructure;
using Xunit;
using Xunit.Abstractions;

namespace VitalsService.Tests.Integration.Questionnaires
{
    public class ChfPostRequestTests : BaseQuestionnaireResponseTest, IClassFixture<VitalsServiceTestFixture>
    {
        public ChfPostRequestTests(ITestOutputHelper output, VitalsServiceTestFixture fixture) 
            : base(output, fixture)
        { }
    }
}

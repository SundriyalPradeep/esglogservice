﻿using System.Net;
using System.Threading.Tasks;
using Shouldly;
using VitalsService.Helpers;
using VitalsService.Tests.Integration.Infrastructure;
using VitalsService.Tests.Integration.Models.Questionnaires;
using VitalsService.Tests.Integration.Serialization;
using Xunit;
using Xunit.Abstractions;
using static VitalsService.Tests.Integration.Questionnaires.QuestionnaireResponseHelper;

namespace VitalsService.Tests.Integration.Questionnaires
{
    public class CopdPostRequestTests : BaseQuestionnaireResponseTest, IClassFixture<VitalsServiceTestFixture>
    {
        public CopdPostRequestTests(ITestOutputHelper output, VitalsServiceTestFixture fixture) 
            : base(output, fixture)
        {
            Fixture.EnsurePatientThresholdExists(TestCustomerId, TestPatientId);
        }


        [Fact]
        public async Task Post_Copd_QuestionnaireResponse_Returns201Created()
        {
            var copdPostRequest = CreateCopdPostRequest(SequentialGuidGenerator.Generate());
            copdPostRequest.QuestionAnswers.AddRange(new QuestionnairePostRequest.QuestionAnswer[]
            {
                CreateSingleSelectQuestionAnswer_AlertConf_AreYouWell(yes: false),

                CreateSingleSelectQuestionAnswer_Copd_IncreaseOfInhalers(yes: true),
                CreateSingleSelectQuestionAnswer_Copd_IncreaseInCough(yes: false),
                CreateSingleSelectQuestionAnswer_Copd_ExpectorationDarker(yes: true),
                CreateSingleSelectQuestionAnswer_Copd_IsSputumColored(yes: false),
                CreateSingleSelectQuestionAnswer_Copd_MoreNoisesWhenBreathing(yes: true),

                CreateSingleSelectQuestionAnswer_PreStrat_SputumColor(SputumColor.Brown),
                CreateSingleSelectQuestionAnswer_PreStrat_DispnoeMrcScale(DyspnoeMrcScale.Level3),
                CreateMeasurementQuestionAnswer_PreStrat_Temperature(35m),
                CreateMeasurementQuestionAnswer_PreStrat_SpO2(6m),
                CreateMeasurementQuestionAnswer_PreStrat_FEV1(20m),
                CreateMeasurementQuestionAnswer_PreStrat_SYS(120m),
                CreateMeasurementQuestionAnswer_PreStrat_HR(300m),
                CreateSingleSelectQuestionAnswer_PreStrat_Comorbidity(Comorbidity.Pluripathological),
                CreateSingleSelectQuestionAnswer_PreStrat_Copd_Edemas(CopdEdemas.ApparitionOrIncreaseOfPrevious)
            });

            var json = JsonNet.Serialize(copdPostRequest);
            var response = await PostAsync(TestCustomerId, TestPatientId, json);

            response.StatusCode.ShouldBe(HttpStatusCode.Created);
            await PrintResponseAsync(response);
        }
    }
}

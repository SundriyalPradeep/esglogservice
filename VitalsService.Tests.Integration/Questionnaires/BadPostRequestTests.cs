﻿using System.Net;
using System.Threading.Tasks;
using Shouldly;
using VitalsService.Helpers;
using VitalsService.Tests.Integration.Infrastructure;
using VitalsService.Tests.Integration.Models.Questionnaires;
using VitalsService.Tests.Integration.Serialization;
using Xunit;
using Xunit.Abstractions;

namespace VitalsService.Tests.Integration.Questionnaires
{
    public class BadPostRequestTests : BaseQuestionnaireResponseTest, IClassFixture<VitalsServiceTestFixture>
    {
        public BadPostRequestTests(ITestOutputHelper output, VitalsServiceTestFixture fixture) 
            : base(output, fixture)
        { }


        [Fact]
        public async Task EmptyBody_Returns400BadRequest()
        {
            var json = JsonNet.Serialize(new { });
            var response = await PostAsync(TestCustomerId, TestPatientId, json);

            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
            await PrintResponseAsync(response);
        }

        [Fact]
        public async Task NullQuestionnaireName_Returns400BadRequest()
        {
            var postRequest = QuestionnaireResponseHelper.CreatePostRequest(SequentialGuidGenerator.Generate(), null, SequentialGuidGenerator.Generate());
            postRequest.QuestionAnswers.Add(QuestionnaireResponseHelper.CreateSingleSelectQuestionAnswer(SequentialGuidGenerator.Generate(), "Ohai", QuestionnaireQuestionType.General, SequentialGuidGenerator.Generate(), "Sí"));

            var json = JsonNet.Serialize(postRequest);
            var response = await PostAsync(TestCustomerId, TestPatientId, json);

            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
            await PrintResponseAsync(response);
        }

        [Fact]
        public async Task EmptyQuestionnaireName_Returns400BadRequest()
        {
            var postRequest = QuestionnaireResponseHelper.CreatePostRequest(SequentialGuidGenerator.Generate(), "", SequentialGuidGenerator.Generate());
            postRequest.QuestionAnswers.Add(QuestionnaireResponseHelper.CreateSingleSelectQuestionAnswer(SequentialGuidGenerator.Generate(), "Ohai", QuestionnaireQuestionType.General, SequentialGuidGenerator.Generate(), "Sí"));

            var json = JsonNet.Serialize(postRequest);
            var response = await PostAsync(TestCustomerId, TestPatientId, json);

            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
            await PrintResponseAsync(response);
        }

        [Fact]
        public async Task NoQuestionAnswersCollection_Returns400BadRequest()
        {
            var json = JsonNet.Serialize(new { QuestionnaireId = SequentialGuidGenerator.Generate(), ClinicianId = SequentialGuidGenerator.Generate() });
            var response = await PostAsync(TestCustomerId, TestPatientId, json);

            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
            await PrintResponseAsync(response);
        }

        [Fact]
        public async Task EmptyAnswersCollection_Returns400BadRequest()
        {
            var json = JsonNet.Serialize(QuestionnaireResponseHelper.CreatePostRequest(SequentialGuidGenerator.Generate(), "Bad Request", SequentialGuidGenerator.Generate()));
            var response = await PostAsync(TestCustomerId, TestPatientId, json);

            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
            await PrintResponseAsync(response);
        }        

        [Fact]
        public async Task DefaultQuestionAnswer_Returns400BadRequest()
        {
            var postRequest = QuestionnaireResponseHelper.CreatePostRequest(SequentialGuidGenerator.Generate(), "Bad Request", SequentialGuidGenerator.Generate());
            postRequest.QuestionAnswers.Add(new QuestionnairePostRequest.QuestionAnswer());

            var json = JsonNet.Serialize(postRequest);
            var response = await PostAsync(TestCustomerId, TestPatientId, json);

            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
            await PrintResponseAsync(response);
        }

        [Fact]
        public async Task QuestionAnswer_NullQuestionText_Returns400BadRequest()
        {
            var postRequest = QuestionnaireResponseHelper.CreatePostRequest(SequentialGuidGenerator.Generate(), "Bad Request", SequentialGuidGenerator.Generate());
            postRequest.QuestionAnswers.Add(QuestionnaireResponseHelper.CreateSingleSelectQuestionAnswer(SequentialGuidGenerator.Generate(), null, QuestionnaireQuestionType.General, SequentialGuidGenerator.Generate(), "Sí"));

            var json = JsonNet.Serialize(postRequest);
            var response = await PostAsync(TestCustomerId, TestPatientId, json);

            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
            await PrintResponseAsync(response);
        }

        [Fact]
        public async Task QuestionAnswer_EmptyQuestionText_Returns400BadRequest()
        {
            var postRequest = QuestionnaireResponseHelper.CreatePostRequest(SequentialGuidGenerator.Generate(), "Bad Request", SequentialGuidGenerator.Generate());
            postRequest.QuestionAnswers.Add(QuestionnaireResponseHelper.CreateSingleSelectQuestionAnswer(SequentialGuidGenerator.Generate(), string.Empty, QuestionnaireQuestionType.General, SequentialGuidGenerator.Generate(), "Sí"));

            var json = JsonNet.Serialize(postRequest);
            var response = await PostAsync(TestCustomerId, TestPatientId, json);

            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
            await PrintResponseAsync(response);
        }

        [Theory]
        [InlineData(QuestionnaireQuestionType.Unknown)]
        [InlineData((QuestionnaireQuestionType)(-1))]
        [InlineData((QuestionnaireQuestionType)(0))]
        [InlineData((QuestionnaireQuestionType)(42))]
        [InlineData((QuestionnaireQuestionType)(int.MaxValue))]
        public async Task QuestionAnswer_InvalidQuestionTypes_Returns400BadRequest(QuestionnaireQuestionType questionType)
        {
            var postRequest = QuestionnaireResponseHelper.CreatePostRequest(SequentialGuidGenerator.Generate(), "Bad Request", SequentialGuidGenerator.Generate());
            postRequest.QuestionAnswers.Add(QuestionnaireResponseHelper.CreateSingleSelectQuestionAnswer(SequentialGuidGenerator.Generate(), "Some question text", questionType, SequentialGuidGenerator.Generate(), "Sí"));

            var json = JsonNet.Serialize(postRequest);
            var response = await PostAsync(TestCustomerId, TestPatientId, json);

            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
            await PrintResponseAsync(response);
        }

        [Fact]
        public async Task QuestionAnswer_SingleSelect_NullAnswerChoiceId_Returns400BadRequest()
        {
            var postRequest = QuestionnaireResponseHelper.CreatePostRequest(SequentialGuidGenerator.Generate(), "Bad Request", SequentialGuidGenerator.Generate());
            postRequest.QuestionAnswers.Add(QuestionnaireResponseHelper.CreateSingleSelectQuestionAnswer(SequentialGuidGenerator.Generate(), "Some question text", QuestionnaireQuestionType.General, answerChoiceId: null, answerChoiceText: "Sí"));

            var json = JsonNet.Serialize(postRequest);
            var response = await PostAsync(TestCustomerId, TestPatientId, json);

            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
            await PrintResponseAsync(response);
        }

        [Fact]
        public async Task QuestionAnswer_SingleSelect_NullAnswerChoiceText_Returns400BadRequest()
        {
            var postRequest = QuestionnaireResponseHelper.CreatePostRequest(SequentialGuidGenerator.Generate(), "Bad Request", SequentialGuidGenerator.Generate());
            postRequest.QuestionAnswers.Add(QuestionnaireResponseHelper.CreateSingleSelectQuestionAnswer(SequentialGuidGenerator.Generate(), "Some question text", QuestionnaireQuestionType.General, SequentialGuidGenerator.Generate(), answerChoiceText: null));

            var json = JsonNet.Serialize(postRequest);
            var response = await PostAsync(TestCustomerId, TestPatientId, json);

            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
            await PrintResponseAsync(response);
        }

        [Fact]
        public async Task QuestionAnswer_SingleSelect_EmptyAnswerChoiceText_Returns400BadRequest()
        {
            var postRequest = QuestionnaireResponseHelper.CreatePostRequest(SequentialGuidGenerator.Generate(), "Bad Request", SequentialGuidGenerator.Generate());
            postRequest.QuestionAnswers.Add(QuestionnaireResponseHelper.CreateSingleSelectQuestionAnswer(SequentialGuidGenerator.Generate(), "Some question text", QuestionnaireQuestionType.General, SequentialGuidGenerator.Generate(), answerChoiceText: string.Empty));

            var json = JsonNet.Serialize(postRequest);
            var response = await PostAsync(TestCustomerId, TestPatientId, json);

            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
            await PrintResponseAsync(response);
        }

        [Fact]
        public async Task QuestionAnswer_PreStrat_SingleSelect_EmptyAnswerChoiceScore_Returns400BadRequest()
        {
            var postRequest = QuestionnaireResponseHelper.CreatePostRequest(SequentialGuidGenerator.Generate(), "Bad Request", SequentialGuidGenerator.Generate());
            postRequest.QuestionAnswers.Add(QuestionnaireResponseHelper.CreateSingleSelectQuestionAnswer(SequentialGuidGenerator.Generate(), "Some question text", QuestionnaireQuestionType.PreStratification, SequentialGuidGenerator.Generate(), answerChoiceText: "This is an answer", answerChoiceScore: null));

            var json = JsonNet.Serialize(postRequest);
            var response = await PostAsync(TestCustomerId, TestPatientId, json);

            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
            await PrintResponseAsync(response);
        }

        [Fact]
        public async Task QuestionAnswer_Measurement_NullMeasurementType_Returns400BadRequest()
        {
            var postRequest = QuestionnaireResponseHelper.CreatePostRequest(SequentialGuidGenerator.Generate(), "Bad Request", SequentialGuidGenerator.Generate());
            postRequest.QuestionAnswers.Add(QuestionnaireResponseHelper.CreateMeasurementQuestionAnswer(SequentialGuidGenerator.Generate(), "Some question text", QuestionnaireQuestionType.General, measurementType: null, measurementValue: 42m));

            var json = JsonNet.Serialize(postRequest);
            var response = await PostAsync(TestCustomerId, TestPatientId, json);

            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
            await PrintResponseAsync(response);
        }

        [Theory]
        [InlineData(QuestionnaireMeasurementType.Unknown)]
        [InlineData((QuestionnaireMeasurementType)(-1))]
        [InlineData((QuestionnaireMeasurementType)(0))]
        [InlineData((QuestionnaireMeasurementType)(42))]
        [InlineData((QuestionnaireMeasurementType)(int.MaxValue))]
        public async Task QuestionAnswer_Measurement_InvalidMeasurementTypes_Returns400BadRequest(QuestionnaireMeasurementType measurementType)
        {
            var postRequest = QuestionnaireResponseHelper.CreatePostRequest(SequentialGuidGenerator.Generate(), "Bad Request", SequentialGuidGenerator.Generate());
            postRequest.QuestionAnswers.Add(QuestionnaireResponseHelper.CreateMeasurementQuestionAnswer(SequentialGuidGenerator.Generate(), "Some question text", QuestionnaireQuestionType.General, measurementType: measurementType, measurementValue: 42m));

            var json = JsonNet.Serialize(postRequest);
            var response = await PostAsync(TestCustomerId, TestPatientId, json);

            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
            await PrintResponseAsync(response);
        }

        [Fact]
        public async Task QuestionAnswer_Measurement_NullMeasurementValue_Returns400BadRequest()
        {
            var postRequest = QuestionnaireResponseHelper.CreatePostRequest(SequentialGuidGenerator.Generate(), "Bad Request", SequentialGuidGenerator.Generate());
            postRequest.QuestionAnswers.Add(QuestionnaireResponseHelper.CreateMeasurementQuestionAnswer(SequentialGuidGenerator.Generate(), "Some question text", QuestionnaireQuestionType.General, measurementType: QuestionnaireMeasurementType.HR, measurementValue: null));

            var json = JsonNet.Serialize(postRequest);
            var response = await PostAsync(TestCustomerId, TestPatientId, json);

            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
            await PrintResponseAsync(response);
        }
    }
}

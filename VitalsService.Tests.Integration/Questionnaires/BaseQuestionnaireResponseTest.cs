﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using VitalsService.Tests.Integration.Configuration;
using VitalsService.Tests.Integration.Helpers;
using VitalsService.Tests.Integration.Infrastructure;
using Xunit.Abstractions;

namespace VitalsService.Tests.Integration.Questionnaires
{
    public class BaseQuestionnaireResponseTest
    {
        protected const int TestCustomerId = 8062013;
        protected static readonly Guid TestPatientId = Guid.Parse("6C4B910D-BC12-4176-814C-40A0EC96D490");

        /// <summary>
        /// {0} = customerId, {1} = patientId
        /// </summary>
        protected const string PostUrlFormat = "api/{0}/questionnaires/{1}";

        /// <summary>
        /// {0} = customerId, {1} = patientId, {2} = questionnaireResponseId
        /// </summary>
        protected const string PutUrlFormat = "api/{0}/questionnaires/{1}/response/{2}";

        protected ITestOutputHelper Output { get; }

        protected VitalsServiceTestFixture Fixture { get; }


        public BaseQuestionnaireResponseTest(ITestOutputHelper output, VitalsServiceTestFixture fixture)
        {
            Output = output;
            Fixture = fixture;

            PingWebApi();
        }


        protected async Task<HttpResponseMessage> PostAsync(int customerId, Guid patientId, string json)
        {
            return await PostWithTokenAsync(json);
        }

        protected async Task<HttpResponseMessage> PutAsync(string json)
        {
            return await PutWithTokenAsync(PutUrlFormat, json);
        }

        protected async Task PrintResponseAsync(HttpResponseMessage response)
        {
            try
            {
                var body = await response.Content.ReadAsStringAsync();
                Output.WriteLine("Response body:");
                Output.WriteLine(body);
            }
            catch (Exception ex)
            {
                Output.WriteLine("Unable to print response due to unhandled exception\r\n{0}", ex);

                // Don't throw and cause the test to fail because we couldn't print the response.
            }
        }


        private async Task<HttpResponseMessage> PostWithTokenAsync(string json)
        {
            var postUrl = string.Format(PostUrlFormat, TestCustomerId, TestPatientId);
            var response = await Fixture.Client.PostAsync(postUrl, new StringContent(json, Encoding.UTF8, "application/json"));

            if (response.StatusCode != HttpStatusCode.Unauthorized)
            {
                return response;
            }

            // Try to refresh the token before giving up completely.
            Fixture.RefreshToken();

            response = await Fixture.Client.PostAsync(PostUrlFormat, new StringContent(json, Encoding.UTF8, "application/json"));
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new Exception($"Unable to get a suitable token from TokenService at {TestConfiguration.TokenServiceUrl} with Username = {TestConfiguration.TokenServiceUsername} and Password = {TestConfiguration.TokenServicePassword}");
            }

            return response;
        }

        private async Task<HttpResponseMessage> PutWithTokenAsync(string url, string json)
        {
            var response = await Fixture.Client.PutAsync(url, new StringContent(json, Encoding.UTF8, "application/json"));
            if (response.StatusCode != HttpStatusCode.Unauthorized)
            {
                return response;
            }

            // Try to refresh the token before giving up completely.
            Fixture.RefreshToken();

            response = await Fixture.Client.PutAsync(url, new StringContent(json, Encoding.UTF8, "application/json"));
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new Exception($"Unable to get a suitable token from TokenService at {TestConfiguration.TokenServiceUrl} with Username = {TestConfiguration.TokenServiceUsername} and Password = {TestConfiguration.TokenServicePassword}");
            }

            return response;
        }

        /// <summary>
        /// Make a GET request to /healthcheck/prtg to ensure the site is up and running.
        /// </summary>
        private void PingWebApi()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromSeconds(15);
                    client.BaseAddress = new Uri(TestConfiguration.VitalsServiceUrl);

                    var response = AsyncHelper.RunSync(() => client.GetAsync("healthcheck/prtg"));

                    response.EnsureSuccessStatusCode();
                }
            }
            catch (Exception ex)
            {
                throw new WebApiSiteUnavailableException($"Unable to ping the WebApi site {TestConfiguration.VitalsServiceUrl}. Please ensure it is started before running the integration tests.", ex);
            }
        }
    }
}

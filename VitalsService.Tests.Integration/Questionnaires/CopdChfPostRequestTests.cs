﻿using VitalsService.Tests.Integration.Infrastructure;
using Xunit;
using Xunit.Abstractions;

namespace VitalsService.Tests.Integration.Questionnaires
{
    public class CopdChfPostRequestTests : BaseQuestionnaireResponseTest, IClassFixture<VitalsServiceTestFixture>
    {
        public CopdChfPostRequestTests(ITestOutputHelper output, VitalsServiceTestFixture fixture) 
            : base(output, fixture)
        { }
    }
}

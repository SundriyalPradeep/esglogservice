﻿namespace VitalsService.Tests.Integration.Models.Alerts
{
    public class AlertSeverityRequestDto
    {
        public string Name { get; set; }

        public string ColorCode { get; set; }

        public int Severity { get; set; }
    }
}

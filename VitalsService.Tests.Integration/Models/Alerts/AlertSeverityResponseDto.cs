﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VitalsService.Tests.Integration.Models.Alerts
{
    public class AlertSeverityResponseDto : AlertSeverityRequestDto
    {
        public Guid Id { get; set; }

        public int CustomerId { get; set; }
    }
}

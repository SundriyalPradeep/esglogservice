﻿using System;
using VitalsService.Tests.Integration.Models.Alerts;

namespace VitalsService.Tests.Integration.Models.Thresholds
{
    /// <summary>
    /// BaseThresholdDto.
    /// </summary>
    public class BaseThresholdDto
    {
        public Guid Id { get; set; }

        public int CustomerId { get; set; }

        public Guid? ConditionId { get; set; } //MS-2883 Fix

        public ThresholdType Type { get; set; }

        public VitalType Name { get; set; }

        public decimal? MinValue { get; set; }

        public decimal? MaxValue { get; set; }

        /// <summary>
        /// Gets or sets the thresholds status.
        /// </summary>
        public bool IsDeleted { get; set; }

        public UnitType Unit { get; set; }

        public AlertSeverityResponseDto AlertSeverity { get; set; }

        public PatientThresholdTypeResponseDto PatientThresholdType { get; set; }
    }
}

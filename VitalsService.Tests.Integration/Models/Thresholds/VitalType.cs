﻿namespace VitalsService.Tests.Integration.Models.Thresholds
{
    /// <summary>
    /// VitalType.
    /// </summary>
    public enum VitalType
    {
        /// <summary>
        /// The systolic blood pressure.
        /// </summary>
        SystolicBloodPressure = 1,

        /// <summary>
        /// The diastolic blood pressure.
        /// </summary>
        DiastolicBloodPressure,

        /// <summary>
        /// The heart rate.
        /// </summary>
        HeartRate,

        /// <summary>
        /// The body temperature.
        /// </summary>
        Temperature,

        /// <summary>
        /// The weight.
        /// </summary>
        Weight,

        /// <summary>
        /// The body mass index.
        /// </summary>
        BodyMassIndex,

        /// <summary>
        /// The oxygen saturation.
        /// </summary>
        OxygenSaturation,

        /// <summary>
        /// The blood glucose.
        /// </summary>
        BloodGlucose,

        /// <summary>
        /// The peak expiratory flow.
        /// </summary>
        PeakExpiratoryFlow,

        /// <summary>
        /// The forced expiratory volume.
        /// </summary>
        ForcedExpiratoryVolume,

        /// <summary>
        /// The walking steps.
        /// </summary>
        WalkingSteps,

        /// <summary>
        /// The running steps.
        /// </summary>
        RunningSteps,

        /// <summary>
        /// Forced Vital Capacity.
        /// </summary>
        ForcedVitalCapacity,

        /// <summary>
        /// FEV1/FVC
        /// </summary>
        FEV1_FVC
    }
}

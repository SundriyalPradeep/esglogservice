﻿using System;

namespace VitalsService.Tests.Integration.Models.Thresholds
{
    public class ThresholdRequestDto
    {
        public ThresholdType Type { get; set; }

        public VitalType Name { get; set; }

        public decimal? MinValue { get; set; }

        public decimal? MaxValue { get; set; }

        public UnitType Unit { get; set; }

        public Guid? AlertSeverityId { get; set; }

        public bool IsDeleted { get; set; }

        public Guid? ConditionId { get; set; }

        public string ThresholdType { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VitalsService.Tests.Integration.Models.Thresholds
{
    public enum ThresholdDefaultType
    {
        /// <summary>
        /// The customer type.
        /// </summary>
        Customer = 1,

        /// <summary>
        /// The condition type.
        /// </summary>
        Condition
    }
}

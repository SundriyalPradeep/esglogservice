﻿namespace VitalsService.Tests.Integration.Models.Thresholds
{
    public class DefaultThresholdRequestDto : ThresholdRequestDto
    {
        public ThresholdDefaultType DefaultType { get; set; }

        public bool? IsConditional { get; set; }
    }
}

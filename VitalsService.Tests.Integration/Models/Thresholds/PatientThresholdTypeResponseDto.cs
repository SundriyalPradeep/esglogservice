﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VitalsService.Tests.Integration.Models.Thresholds
{
    public class PatientThresholdTypeResponseDto
    {
        /// <summary>
        /// Gets or Sets the customerid
        /// </summary>
        public int CustomerId { get; set; }
        /// <summary>
        /// Gets or Sets the patientId
        /// </summary>
        public Guid PatientId { get; set; }

        /// <summary>
        /// Gets or Sets the ThresholdType
        /// </summary>
        public string ThresholdType { get; set; }

        /// <summary>
        /// Gets or Sets the ConditionId
        /// </summary>
        public Guid? ConditionId { get; set; }

        /// <summary>
        /// Gets or sets the VitalType .
        /// </summary>
        /// <value>
        /// The VitalType.
        /// </value>
        public VitalType VitalType { get; set; }

        /// <summary>
        /// Gets or sets the IsDeleted .
        /// </summary>
        /// <value>
        /// The IsDeleted.
        /// </value>
        public bool IsDeleted { get; set; }
    }
}

﻿namespace VitalsService.Tests.Integration.Models.Questionnaires
{
    public enum QuestionnaireMeasurementType
    {
        Unknown = 0,

        Temperature = 1,

        SpO2 = 2,

        FEV1 = 3,

        SYS = 4,

        HR = 5,

        Weight = 6
    }
}

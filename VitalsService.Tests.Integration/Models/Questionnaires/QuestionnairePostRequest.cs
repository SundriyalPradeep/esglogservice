﻿using System;
using System.Collections.Generic;

namespace VitalsService.Tests.Integration.Models.Questionnaires
{
    public class QuestionnairePostRequest
    {
        public Guid QuestionnaireId { get; set; }
        public string QuestionnaireName { get; set; }
        public Guid ClinicianId { get; set; }

        public List<QuestionAnswer> QuestionAnswers { get; private set; } = new List<QuestionAnswer>();


        /// <summary>
        /// Information about the question/answer on a questionnaire.
        /// </summary>
        public class QuestionAnswer
        {
            public Guid QuestionId { get; set; }
            public string QuestionText { get; set; }
            public QuestionnaireQuestionType QuestionType { get; set; }

            public QuestionnaireAnswerType AnswerType { get; set; }
            public Guid? AnswerChoiceId { get; set; }
            public string AnswerChoiceText { get; set; }
            public int? AnswerChoiceScore { get; set; }

            public QuestionnaireMeasurementType? MeasurementType { get; set; }
            public decimal? MeasurementValue { get; set; }

            /// <summary>
            /// For debugging
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                return AnswerType == QuestionnaireAnswerType.SingleSelect
                    ? $"({QuestionType} - {AnswerType}) {QuestionText}: {AnswerChoiceText}"
                    : $"({QuestionType} - {AnswerType}) {QuestionText}: {MeasurementType} = {MeasurementValue}";
            }
        }
    }
}

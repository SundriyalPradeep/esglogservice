﻿using System;

namespace VitalsService.Tests.Integration.Models.Questionnaires
{
    public enum SputumColor
    {
        White,
        Yellow,
        Green,
        Brown
    }

    public static class SputumColorExtensions
    {
        public static Guid ToAnswerChoiceId(this SputumColor color)
        {
            switch(color)
            {
                case SputumColor.White:
                    return Guid.Parse("e45da160-1241-49c6-8d57-a6fa00f48a30");

                case SputumColor.Yellow:
                    return Guid.Parse("9fe8f1ac-b715-4f05-9178-a6fa00f48a30");

                case SputumColor.Green:
                    return Guid.Parse("815e1d37-4c4c-4202-9951-a6fa00f48a30");

                case SputumColor.Brown:
                    return Guid.Parse("57337bb8-edf6-4768-a571-a6fa00f48a30");

                default:
                    throw new Exception($"Invalid {nameof(SputumColor)} value: {color}");
            }
        }

        public static string ToAnswerChoiceText(this SputumColor color)
        {
            switch (color)
            {
                case SputumColor.White:
                    return "^^^White";

                case SputumColor.Yellow:
                    return "^^^Yellow";

                case SputumColor.Green:
                    return "^^^Green";

                case SputumColor.Brown:
                    return "^^^Brown";

                default:
                    throw new Exception($"Invalid {nameof(SputumColor)} value: {color}");
            }
        }

        public static int ToAnswerChoiceScore(this SputumColor color)
        {
            switch (color)
            {
                case SputumColor.White:
                    return 0;

                case SputumColor.Yellow:
                    return 1;

                case SputumColor.Green:
                    return 2;

                case SputumColor.Brown:
                    return 3;

                default:
                    throw new Exception($"Invalid {nameof(SputumColor)} value: {color}");
            }
        }
    }
}

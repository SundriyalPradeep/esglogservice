﻿using System;

namespace VitalsService.Tests.Integration.Models.Questionnaires
{
    public enum DyspnoeMrcScale
    {
        Level01,
        Level2,
        Level3,
        Level4
    }

    public static class DyspnoeMrcScaleExtensions
    {
        public static Guid ToAnswerChoiceId(this DyspnoeMrcScale dyspnoe)
        {
            switch (dyspnoe)
            {
                case DyspnoeMrcScale.Level01:
                    return Guid.Parse("02bdc420-1fa5-470d-a0dd-a6fa00f48a30");

                case DyspnoeMrcScale.Level2:
                    return Guid.Parse("e845b40b-6153-455c-8e16-a6fa00f48a30");

                case DyspnoeMrcScale.Level3:
                    return Guid.Parse("863066c4-d85b-49f3-aa46-a6fa00f48a30");

                case DyspnoeMrcScale.Level4:
                    return Guid.Parse("f9b477d0-b344-484f-bc86-a6fa00f48a30");

                default:
                    throw new Exception($"Invalid {nameof(DyspnoeMrcScale)} value: {dyspnoe}");
            }
        }

        public static string ToAnswerChoiceText(this DyspnoeMrcScale dyspnoe)
        {
            switch (dyspnoe)
            {
                case DyspnoeMrcScale.Level01:
                    return "^^^Level 0-1";

                case DyspnoeMrcScale.Level2:
                    return "^^^Level 2";

                case DyspnoeMrcScale.Level3:
                    return "^^^Level 3";

                case DyspnoeMrcScale.Level4:
                    return "^^^Level 4";

                default:
                    throw new Exception($"Invalid {nameof(DyspnoeMrcScale)} value: {dyspnoe}");
            }
        }

        public static int ToAnswerChoiceScore(this DyspnoeMrcScale dyspnoe)
        {
            switch (dyspnoe)
            {
                case DyspnoeMrcScale.Level01:
                    return 0;

                case DyspnoeMrcScale.Level2:
                    return 1;

                case DyspnoeMrcScale.Level3:
                    return 2;

                case DyspnoeMrcScale.Level4:
                    return 3;

                default:
                    throw new Exception($"Invalid {nameof(DyspnoeMrcScale)} value: {dyspnoe}");
            }
        }
    }
}

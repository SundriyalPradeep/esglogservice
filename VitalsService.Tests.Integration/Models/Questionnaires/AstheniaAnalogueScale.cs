﻿using System;

namespace VitalsService.Tests.Integration.Models.Questionnaires
{
    public enum AstheniaAnalogueScale
    {
        Zero,
        One,
        Two,
        Three,
        Four,
        Five,
        Six,
        Seven,
        Eight,
        Nine,
        Ten
    }

    public static class AstheniaAnalogueScaleExtensions
    {
        public static Guid ToAnswerChoiceId(this AstheniaAnalogueScale asthenia)
        {
            switch (asthenia)
            {
                case AstheniaAnalogueScale.Zero:
                    return Guid.Parse("bc0f334d-ff06-4a67-a585-a6fa00f48a30");

                case AstheniaAnalogueScale.One:
                    return Guid.Parse("964ad9e9-93e0-409c-87da-a6fa00f48a30");

                case AstheniaAnalogueScale.Two:
                    return Guid.Parse("b943857e-5c90-4970-b2be-a6fa00f48a30");

                case AstheniaAnalogueScale.Three:
                    return Guid.Parse("f672ac83-8daa-452e-89cc-a6fa00f48a30");

                case AstheniaAnalogueScale.Four:
                    return Guid.Parse("8f45d00f-be8e-471f-acc3-a6fa00f48a30");

                case AstheniaAnalogueScale.Five:
                    return Guid.Parse("510aee6b-467d-40b4-8719-a6fa00f48a30");

                case AstheniaAnalogueScale.Six:
                    return Guid.Parse("9ddbabdb-debc-4866-84e2-a6fa00f48a30");

                case AstheniaAnalogueScale.Seven:
                    return Guid.Parse("71edec25-e13a-4037-a53f-a6fa00f48a30");

                case AstheniaAnalogueScale.Eight:
                    return Guid.Parse("64e4f8b3-eab5-4ace-bb97-a6fa00f48a30");

                case AstheniaAnalogueScale.Nine:
                    return Guid.Parse("becd6b72-31b4-4c71-a6cd-a6fa00f48a30");

                case AstheniaAnalogueScale.Ten:
                    return Guid.Parse("26d21b31-3e3e-4cf1-b803-a6fa00f48a30");

                default:
                    throw new Exception($"Invalid {nameof(AstheniaAnalogueScale)} value: {asthenia}");
            }
        }

        public static string ToAnswerChoiceText(this AstheniaAnalogueScale asthenia)
        {
            switch (asthenia)
            {
                case AstheniaAnalogueScale.Zero:
                    return "0";

                case AstheniaAnalogueScale.One:
                    return "1";

                case AstheniaAnalogueScale.Two:
                    return "2";

                case AstheniaAnalogueScale.Three:
                    return "3";

                case AstheniaAnalogueScale.Four:
                    return "4";

                case AstheniaAnalogueScale.Five:
                    return "5";

                case AstheniaAnalogueScale.Six:
                    return "6";

                case AstheniaAnalogueScale.Seven:
                    return "7";

                case AstheniaAnalogueScale.Eight:
                    return "8";

                case AstheniaAnalogueScale.Nine:
                    return "9";

                case AstheniaAnalogueScale.Ten:
                    return "10";

                default:
                    throw new Exception($"Invalid {nameof(AstheniaAnalogueScale)} value: {asthenia}");
            }
        }

        public static int ToAnswerChoiceScore(this AstheniaAnalogueScale asthenia)
        {
            switch (asthenia)
            {
                case AstheniaAnalogueScale.Zero:
                    return 0;

                case AstheniaAnalogueScale.One:
                case AstheniaAnalogueScale.Two:
                case AstheniaAnalogueScale.Three:
                case AstheniaAnalogueScale.Four:
                    return 1;

                case AstheniaAnalogueScale.Five:
                case AstheniaAnalogueScale.Six:
                case AstheniaAnalogueScale.Seven:
                case AstheniaAnalogueScale.Eight:
                    return 2;

                case AstheniaAnalogueScale.Nine:
                case AstheniaAnalogueScale.Ten:
                    return 3;

                default:
                    throw new Exception($"Invalid {nameof(AstheniaAnalogueScale)} value: {asthenia}");
            }
        }
    }
}

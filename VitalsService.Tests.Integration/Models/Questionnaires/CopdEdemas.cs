﻿using System;

namespace VitalsService.Tests.Integration.Models.Questionnaires
{
    public enum CopdEdemas
    {
        NoEdemaOrStable,
        ApparitionOrIncreaseOfPrevious,
        NA
    }

    public static class CopdEdemasExtensions
    {
        public static Guid ToAnswerChoiceId(this CopdEdemas edemas)
        {
            switch (edemas)
            {
                case CopdEdemas.NoEdemaOrStable:
                    return Guid.Parse("02bdc420-1fa5-470d-a0dd-a6fa00f48a30");

                case CopdEdemas.ApparitionOrIncreaseOfPrevious:
                    return Guid.Parse("e845b40b-6153-455c-8e16-a6fa00f48a30");

                case CopdEdemas.NA:
                    return Guid.Parse("adbce212-b7e4-4a11-86ea-a6fa00f48a30");

                default:
                    throw new Exception($"Invalid {nameof(CopdEdemas)} value: {edemas}");
            }
        }

        public static string ToAnswerChoiceText(this CopdEdemas edemas)
        {
            switch (edemas)
            {
                case CopdEdemas.NoEdemaOrStable:
                    return "^^^No edema or stable";

                case CopdEdemas.ApparitionOrIncreaseOfPrevious:
                    return "^^^Apparition or increase of previous";

                case CopdEdemas.NA:
                    return "^^^N/A";

                default:
                    throw new Exception($"Invalid {nameof(CopdEdemas)} value: {edemas}");
            }
        }

        public static int ToAnswerChoiceScore(this CopdEdemas edemas)
        {
            switch (edemas)
            {
                case CopdEdemas.NoEdemaOrStable:
                case CopdEdemas.NA:
                    return 0;

                case CopdEdemas.ApparitionOrIncreaseOfPrevious:
                    return 1;

                default:
                    throw new Exception($"Invalid {nameof(CopdEdemas)} value: {edemas}");
            }
        }
    }
}

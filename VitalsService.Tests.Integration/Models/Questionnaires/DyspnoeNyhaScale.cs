﻿using System;

namespace VitalsService.Tests.Integration.Models.Questionnaires
{
    public enum DyspnoeNyhaScale
    {
        Level1,
        Level2,
        Level3,
        Level4
    }

    public static class DyspnoeNyhaScaleExtensions
    {
        public static Guid ToAnswerChoiceId(this DyspnoeNyhaScale dyspnoe)
        {
            switch (dyspnoe)
            {
                case DyspnoeNyhaScale.Level1:
                    return Guid.Parse("1eb4748f-6fd0-4864-9f4c-a6fa00f48a30");

                case DyspnoeNyhaScale.Level2:
                    return Guid.Parse("9512e20c-bf9b-480b-85e0-a6fa00f48a30");

                case DyspnoeNyhaScale.Level3:
                    return Guid.Parse("60663966-e3a2-4bc0-8cef-a6fa00f48a30");

                case DyspnoeNyhaScale.Level4:
                    return Guid.Parse("350cb634-6220-4807-b7a9-a6fa00f48a30");

                default:
                    throw new Exception($"Invalid {nameof(DyspnoeNyhaScale)} value: {dyspnoe}");
            }
        }

        public static string ToAnswerChoiceText(this DyspnoeNyhaScale dyspnoe)
        {
            switch (dyspnoe)
            {
                case DyspnoeNyhaScale.Level1:
                    return "^^^Level 1";

                case DyspnoeNyhaScale.Level2:
                    return "^^^Level 2";

                case DyspnoeNyhaScale.Level3:
                    return "^^^Level 3";

                case DyspnoeNyhaScale.Level4:
                    return "^^^Level 4";

                default:
                    throw new Exception($"Invalid {nameof(DyspnoeNyhaScale)} value: {dyspnoe}");
            }
        }

        public static int ToAnswerChoiceScore(this DyspnoeNyhaScale dyspnoe)
        {
            switch (dyspnoe)
            {
                case DyspnoeNyhaScale.Level1:
                    return 0;

                case DyspnoeNyhaScale.Level2:
                    return 1;

                case DyspnoeNyhaScale.Level3:
                    return 2;

                case DyspnoeNyhaScale.Level4:
                    return 3;

                default:
                    throw new Exception($"Invalid {nameof(DyspnoeNyhaScale)} value: {dyspnoe}");
            }
        }
    }
}

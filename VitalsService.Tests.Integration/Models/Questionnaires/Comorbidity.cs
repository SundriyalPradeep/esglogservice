﻿using System;

namespace VitalsService.Tests.Integration.Models.Questionnaires
{
    public enum Comorbidity
    {
        Pluripathological,
        NA
    }

    public static class ComorbidityExtensions
    {
        public static Guid ToAnswerChoiceId(this Comorbidity comorbidity)
        {
            switch (comorbidity)
            {
                case Comorbidity.Pluripathological:
                    return Guid.Parse("00d337fc-1159-4ff7-9472-a6fa00f48a30");

                case Comorbidity.NA:
                    return Guid.Parse("7b65ef1a-3a21-4e0f-9a60-a6fa00f48a30");

                default:
                    throw new Exception($"Invalid {nameof(Comorbidity)} value: {comorbidity}");
            }
        }

        public static string ToAnswerChoiceText(this Comorbidity comorbidity)
        {
            switch (comorbidity)
            {
                case Comorbidity.Pluripathological:
                    return "^^^Pluripathological";

                case Comorbidity.NA:
                    return "^^^N/A";

                default:
                    throw new Exception($"Invalid {nameof(Comorbidity)} value: {comorbidity}");
            }
        }

        public static int ToAnswerChoiceScore(this Comorbidity comorbidity)
        {
            switch (comorbidity)
            {
                case Comorbidity.Pluripathological:
                    return 1;

                case Comorbidity.NA:
                    return 0;

                default:
                    throw new Exception($"Invalid {nameof(Comorbidity)} value: {comorbidity}");
            }
        }
    }
}

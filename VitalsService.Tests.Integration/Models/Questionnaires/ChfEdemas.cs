﻿using System;

namespace VitalsService.Tests.Integration.Models.Questionnaires
{
    public enum ChfEdemas
    {
        NoEdemaOrStable,
        AppearanceInAnkles,
        KneeApparition,
        IncreasedAbdominalPerimeter
    }

    public static class ChfEdemasExtensions
    {
        public static Guid ToAnswerChoiceId(this ChfEdemas edemas)
        {
            switch (edemas)
            {
                case ChfEdemas.NoEdemaOrStable:
                    return Guid.Parse("ea5f5ef6-5302-4cfa-806e-a6fa00f48a30");

                case ChfEdemas.AppearanceInAnkles:
                    return Guid.Parse("0de40de6-2057-4b38-84ec-a6fa00f48a30");

                case ChfEdemas.KneeApparition:
                    return Guid.Parse("8afc5373-faab-43ea-a938-a6fa00f48a30");

                case ChfEdemas.IncreasedAbdominalPerimeter:
                    return Guid.Parse("f22b93a4-519b-4833-bc2e-a6fa00f48a30");

                default:
                    throw new Exception($"Invalid {nameof(ChfEdemas)} value: {edemas}");
            }
        }

        public static string ToAnswerChoiceText(this ChfEdemas edemas)
        {
            switch (edemas)
            {
                case ChfEdemas.NoEdemaOrStable:
                    return "^^^No edema or stable";

                case ChfEdemas.AppearanceInAnkles:
                    return "^^^Appearance in ankles";

                case ChfEdemas.KneeApparition:
                    return "^^^Knee apparition";

                case ChfEdemas.IncreasedAbdominalPerimeter:
                    return "^^^Increased abdominal perimeter";

                default:
                    throw new Exception($"Invalid {nameof(ChfEdemas)} value: {edemas}");
            }
        }

        public static int ToAnswerChoiceScore(this ChfEdemas edemas)
        {
            switch (edemas)
            {
                case ChfEdemas.NoEdemaOrStable:
                    return 0;

                case ChfEdemas.AppearanceInAnkles:
                    return 1;

                case ChfEdemas.KneeApparition:
                    return 2;

                case ChfEdemas.IncreasedAbdominalPerimeter:
                    return 3;

                default:
                    throw new Exception($"Invalid {nameof(ChfEdemas)} value: {edemas}");
            }
        }
    }
}

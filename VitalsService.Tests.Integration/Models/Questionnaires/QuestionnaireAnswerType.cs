﻿namespace VitalsService.Tests.Integration.Models.Questionnaires
{
    public enum QuestionnaireAnswerType
    {
        Unknown = 0,

        /// <summary>
        /// The user is presented a list of possible answers and must select one.
        /// </summary>
        SingleSelect = 1,

        /// <summary>
        /// The user asks the patient to take a measurement, and enters the answer here.
        /// </summary>
        Measurement = 2
    }
}

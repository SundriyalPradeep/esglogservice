﻿using System;

namespace VitalsService.Tests.Integration.Models.Questionnaires
{
    public enum ChestPain
    {
        Palpitations,
        HeartPain,
        NA
    }

    public static class ChestPainExtensions
    {
        public static Guid ToAnswerChoiceId(this ChestPain chestPain)
        {
            switch (chestPain)
            {
                case ChestPain.Palpitations:
                    return Guid.Parse("3a0f56d6-fe9d-4af6-8623-a6fa00f48a30");

                case ChestPain.HeartPain:
                    return Guid.Parse("5fe267d0-81d0-453d-88e5-a6fa00f48a30");

                case ChestPain.NA:
                    return Guid.Parse("cf465ec3-7a9e-4a14-854e-a6fa00f48a30");

                default:
                    throw new Exception($"Invalid {nameof(ChestPain)} value: {chestPain}");
            }
        }

        public static string ToAnswerChoiceText(this ChestPain chestPain)
        {
            switch (chestPain)
            {
                case ChestPain.Palpitations:
                    return "^^^Palpitations";

                case ChestPain.HeartPain:
                    return "^^^Heart pain";

                case ChestPain.NA:
                    return "^^^N/A";

                default:
                    throw new Exception($"Invalid {nameof(ChestPain)} value: {chestPain}");
            }
        }

        public static int ToAnswerChoiceScore(this ChestPain chestPain)
        {
            switch (chestPain)
            {
                case ChestPain.Palpitations:
                    return 1;

                case ChestPain.HeartPain:
                    return 3;

                case ChestPain.NA:
                    return 0;

                default:
                    throw new Exception($"Invalid {nameof(ChestPain)} value: {chestPain}");
            }
        }
    }
}

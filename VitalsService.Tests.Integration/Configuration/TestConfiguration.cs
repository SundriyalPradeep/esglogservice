﻿using System;
using System.Configuration;

namespace VitalsService.Tests.Integration.Configuration
{
    public static class TestConfiguration
    {
        public static string VitalsServiceUrl
        {
            get
            {
                var value = ConfigurationManager.AppSettings[nameof(VitalsServiceUrl)];
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new Exception($"AppSetting {nameof(VitalsServiceUrl)} missing from App.config");
                }

                return value;
            }
        }

        public static string TokenServiceUrl
        {
            get
            {
                var value = ConfigurationManager.AppSettings[nameof(TokenServiceUrl)];
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new Exception($"{nameof(TokenServiceUrl)} is missing from App.config");
                }

                return value;
            }
        }

        public static string TokenServiceUsername
        {
            get
            {
                var value = ConfigurationManager.AppSettings[nameof(TokenServiceUsername)];
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new Exception($"{nameof(TokenServiceUsername)} is missing from App.config");
                }

                return value;
            }
        }

        public static string TokenServicePassword
        {
            get
            {
                var value = ConfigurationManager.AppSettings[nameof(TokenServicePassword)];
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new Exception($"{nameof(TokenServicePassword)} is missing from App.config");
                }

                return value;
            }
        }
    }
}

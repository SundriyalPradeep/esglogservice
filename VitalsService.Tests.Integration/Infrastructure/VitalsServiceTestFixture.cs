﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using VitalsService.Tests.Integration.Configuration;
using VitalsService.Tests.Integration.Helpers;
using VitalsService.Tests.Integration.Models;
using VitalsService.Tests.Integration.Models.Alerts;
using VitalsService.Tests.Integration.Models.Thresholds;
using VitalsService.Tests.Integration.Serialization;

namespace VitalsService.Tests.Integration.Infrastructure
{
    public class VitalsServiceTestFixture : IDisposable
    {
        public HttpClient Client { get; private set; }

        public VitalsServiceTestFixture()
        {
            Client = new HttpClient();
            Client.Timeout = TimeSpan.FromSeconds(30.0);
            Client.BaseAddress = new Uri(TestConfiguration.VitalsServiceUrl);
            Client.DefaultRequestHeaders.Accept.Clear();
            Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            RefreshToken();
        }

        public void Dispose()
        {
            Dispose(disposing: true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Client.Dispose();
            }
        }


        public void RefreshToken()
        {
            var token = GetToken();
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
        }

        private string GetToken()
        {
            try
            {
                using (var tokenClient = new HttpClient())
                {
                    tokenClient.Timeout = TimeSpan.FromSeconds(30.0);
                    tokenClient.BaseAddress = new Uri(TestConfiguration.TokenServiceUrl);
                    tokenClient.DefaultRequestHeaders.Accept.Clear();
                    tokenClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var requestBody = new
                    {
                        Username = TestConfiguration.TokenServiceUsername,
                        Password = TestConfiguration.TokenServicePassword
                    };

                    var json = JsonNet.Serialize(requestBody);

                    var response = AsyncHelper.RunSync(() => tokenClient.PostAsync("api/Tokens", new StringContent(json, Encoding.UTF8, "application/json")));
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        throw new Exception($"Unable to get token from TokenService at {TestConfiguration.TokenServiceUrl} with Username {TestConfiguration.TokenServiceUsername} and Password {TestConfiguration.TokenServicePassword}. Failed with status code {response.StatusCode}.");
                    }

                    var tokenResp = JsonNet.Deserialize<TokenResponse>(AsyncHelper.RunSync(() => response.Content.ReadAsStringAsync()));
                    if (!tokenResp.Success)
                    {
                        throw new Exception($"Unable to get token from TokenService at {TestConfiguration.TokenServiceUrl} with Username {TestConfiguration.TokenServiceUsername} and Password {TestConfiguration.TokenServicePassword}. The POST method returned Success = false.");
                    }

                    return tokenResp.Id;
                }
            }
            catch (Exception ex)
            {
                throw new WebApiSiteUnavailableException($"Unable to communicate with the TokenService at {TestConfiguration.TokenServiceUrl}. Please ensure it is started before running the integration tests.", ex);
            }
        }

        public void EnsurePatientThresholdExists(int customerId, Guid patientId)
        {
            //
            // Check to see if RYG alert severities exist for this customer. If not, create them.
            //

            var getAlertSeveritiesUrl = $"api/{customerId}/severities";
            var alertSeveritiesResponse = AsyncHelper.RunSync(() => Client.GetAsync(getAlertSeveritiesUrl));
            if (alertSeveritiesResponse.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception($"Unable to retrieve Alert Severities for customer {customerId} at {getAlertSeveritiesUrl}. HTTP Status: {alertSeveritiesResponse.StatusCode}");
            }

            var alertSeverityIdsBySeverity = new Dictionary<int, Guid>();

            var alertSeverities = JsonNet.Deserialize<PagedResultDto<AlertSeverityResponseDto>>(AsyncHelper.RunSync(() => alertSeveritiesResponse.Content.ReadAsStringAsync()));
            if (alertSeverities.Results.Count == 0)
            {
                // RYG alert severities don't exist for this customer. Create them.
                alertSeverityIdsBySeverity.Add(1, CreateAlertSeverity(customerId, "Green", "#0BA713", 1));
                alertSeverityIdsBySeverity.Add(2, CreateAlertSeverity(customerId, "Yellow", "#FFA500", 2));
                alertSeverityIdsBySeverity.Add(3, CreateAlertSeverity(customerId, "Red", "#B00707", 3));
            }
            else
            {
                // We're going to assume that we created all three successfully on a prior run.
                alertSeverityIdsBySeverity.Add(1, alertSeverities.Results.Single(asev => asev.Severity == 1).Id);
                alertSeverityIdsBySeverity.Add(2, alertSeverities.Results.Single(asev => asev.Severity == 2).Id);
                alertSeverityIdsBySeverity.Add(3, alertSeverities.Results.Single(asev => asev.Severity == 3).Id);
            }


            //
            // Check to see if default thresholds exist for this customer. If not, create Green thresholds.
            //

            //var getDefaultThresholdsUrl = $"api/{customerId}/thresholds/defaults?DefaultType=Customer";


            //
            // Check to see if patient thresholds exist for the questionaire types. If not, create them.
            //

            var patientThresholdsUrl = $"api/{customerId}/thresholds/{patientId}";

            var patientThresholdsResponse = AsyncHelper.RunSync(() => Client.GetAsync(patientThresholdsUrl));
            if (patientThresholdsResponse.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception($"Unable to retrieve Patient Thresholds for patient {patientId} on customer {customerId} at {patientThresholdsUrl}. HTTP Status: {patientThresholdsResponse.StatusCode}");
            }

            var patientThresholds = JsonNet.Deserialize<PagedResultDto<BaseThresholdDto>>(AsyncHelper.RunSync(() => patientThresholdsResponse.Content.ReadAsStringAsync()));
            if (patientThresholds.Results.Count == 0)
            {
                // These Green Thresholds are used for scoring Linde questionnaires.
                CreateThreshold(customerId, patientId, VitalType.OxygenSaturation, 20m, 100m, UnitType.Percent, alertSeverityIdsBySeverity[1]);
                CreateThreshold(customerId, patientId, VitalType.ForcedExpiratoryVolume, 5m, 20m, UnitType.L, alertSeverityIdsBySeverity[1]);
                CreateThreshold(customerId, patientId, VitalType.SystolicBloodPressure, 120m, 160m, UnitType.mmHg, alertSeverityIdsBySeverity[1]);
                CreateThreshold(customerId, patientId, VitalType.Weight, 200m, 220m, UnitType.kg, alertSeverityIdsBySeverity[1]);
            }
        }

        private Guid CreateAlertSeverity(int customerId, string name, string colorCode, int severity)
        {
            var createAlertSeverityUrl = $"api/{customerId}/severities";

            var requestBody = new AlertSeverityRequestDto
            {
                Name = name,
                ColorCode = colorCode,
                Severity = severity
            };

            var requestJson = JsonNet.Serialize(requestBody);

            var response = AsyncHelper.RunSync(() => Client.PostAsync(createAlertSeverityUrl, new StringContent(requestJson, Encoding.UTF8, "application/json")));
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"Unable to create AlertSeverity for customer {customerId} with Name={name}, ColorCode={colorCode}, and Severity={severity}. HTTP Status Code: {response.StatusCode}");
            }

            var responseDto = JsonNet.Deserialize<PostResponseDto<Guid>>(AsyncHelper.RunSync(() => response.Content.ReadAsStringAsync()));
            return responseDto.Id;
        }

        private Guid CreateThreshold(int customerId, Guid patientId, VitalType name, decimal minValue, decimal maxValue, UnitType unit, Guid greenAlertSeverityId)
        {
            var patientThresholdsUrl = $"api/{customerId}/thresholds/{patientId}";

            var requestBody = new ThresholdRequestDto
            {
                Type = ThresholdType.Basic,
                Name = name,
                MinValue = minValue,
                MaxValue = maxValue,
                Unit = unit,
                AlertSeverityId = greenAlertSeverityId,
                ThresholdType = "Patient"
            };

            var requestJson = JsonNet.Serialize(requestBody);

            var response = AsyncHelper.RunSync(() => Client.PostAsync(patientThresholdsUrl, new StringContent(requestJson, Encoding.UTF8, "application/json")));
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"Unable to create Threshold for patient {patientId} on customer {customerId} with Name={name}, MinValue={minValue}, MaxValue={maxValue}, Unit={unit}, and AlertSeverityId={greenAlertSeverityId}. HTTP Status Code: {response.StatusCode}");
            }

            var responseDto = JsonNet.Deserialize<PostResponseDto<Guid>>(AsyncHelper.RunSync(() => response.Content.ReadAsStringAsync()));
            return responseDto.Id;
        }


        #region Helper Classes

        private class TokenResponse
        {
            public string Id { get; set; }
            public bool Success { get; set; }
            public int Ttl { get; set; }
            public List<int> CustomerIds { get; set; }
        }

        #endregion
    }
}

﻿using System;

namespace VitalsService.Tests.Integration.Infrastructure
{
    public class WebApiSiteUnavailableException : Exception
    {
        public WebApiSiteUnavailableException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}

﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace VitalsService.Tests.Integration.Serialization
{
    public static class JsonNet
    {
        private static readonly JsonSerializerSettings _serializerSettings = new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        };


        public static string Serialize(object value)
        {
            return JsonConvert.SerializeObject(value, _serializerSettings);
        }

        public static T Deserialize<T>(string value)
        {
            return JsonConvert.DeserializeObject<T>(value);
        }
    }
}
